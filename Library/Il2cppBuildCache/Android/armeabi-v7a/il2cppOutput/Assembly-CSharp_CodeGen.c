﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 CharsData[] CharsBundleData::get_CharsData()
extern void CharsBundleData_get_CharsData_m2A642A6B655267B4CC126843873D43AA04F9875C (void);
// 0x00000002 System.Void CharsBundleData::set_CharsData(CharsData[])
extern void CharsBundleData_set_CharsData_m81AE91FF2AA2625038AF4388C7FE7CCEFFC511BE (void);
// 0x00000003 System.Void CharsBundleData::.ctor()
extern void CharsBundleData__ctor_m106D367F39A7FAD0DB260E5744651FC98ED96B14 (void);
// 0x00000004 System.String CharsData::get_Identifier()
extern void CharsData_get_Identifier_mC20D9B2E42F23E7CFA66FA65141011F719AB2681 (void);
// 0x00000005 UnityEngine.Sprite CharsData::get_Sprite()
extern void CharsData_get_Sprite_m1BFE6586725A0A68865F947F36E4E0F3BF85DE46 (void);
// 0x00000006 System.Void CharsData::.ctor()
extern void CharsData__ctor_mFCCB902117FCD0BF8AA6CFFAA9BE96461BDA87D2 (void);
// 0x00000007 CharsData CharControl::get_Chardata()
extern void CharControl_get_Chardata_m82B81759BB755408B138708B8D0E0D48835EAF0E (void);
// 0x00000008 System.Void CharControl::set_Chardata(CharsData)
extern void CharControl_set_Chardata_m389A1A07EF79E9431F64F3CFAC17A1156BB844EB (void);
// 0x00000009 TweenEffect CharControl::get_TweenEffect()
extern void CharControl_get_TweenEffect_m0936842652110F63724B3CF1BE3176069B810A6F (void);
// 0x0000000A System.Void CharControl::set_TweenEffect(TweenEffect)
extern void CharControl_set_TweenEffect_m2DAAD124E5169D25B22033EE6F8232AC703CF164 (void);
// 0x0000000B UnityEngine.UI.Image CharControl::get_CharImage()
extern void CharControl_get_CharImage_m6717F383E31A7235179A68C6BBB341C6FC9E2AFB (void);
// 0x0000000C System.Void CharControl::set_CharImage(UnityEngine.UI.Image)
extern void CharControl_set_CharImage_mC6CC17A5B08C680FE11E3AAF11CC2AC2B38F9F10 (void);
// 0x0000000D System.Boolean CharControl::get_IsSelected()
extern void CharControl_get_IsSelected_m5DAAC6C82C108F3FD7842336558B1B62BD8206C8 (void);
// 0x0000000E System.Void CharControl::set_IsSelected(System.Boolean)
extern void CharControl_set_IsSelected_m84AD011947B55056DDC4F4202075B1A9EE9D78CF (void);
// 0x0000000F System.Void CharControl::OnEnable()
extern void CharControl_OnEnable_mBDBC0BF195EC0A8F76AC309607F41A1E87C6C0C5 (void);
// 0x00000010 System.Void CharControl::ButtonCallBack()
extern void CharControl_ButtonCallBack_mA2858D0CC04E3AE51F0833B8E502F753939A912D (void);
// 0x00000011 System.Collections.IEnumerator CharControl::CheckRelevate()
extern void CharControl_CheckRelevate_mC7196FD3F0797C15DC2A2672C5A944A470C0F062 (void);
// 0x00000012 System.Void CharControl::OnDisable()
extern void CharControl_OnDisable_m145F1B26B72DCBE03EA575C138A7CB8F834F6ECF (void);
// 0x00000013 System.Void CharControl::.ctor()
extern void CharControl__ctor_mBE2BFABAAF7937260B08A3A03291945AE9C11260 (void);
// 0x00000014 System.Void CharControl::<OnEnable>b__19_0()
extern void CharControl_U3COnEnableU3Eb__19_0_mE0D85D7B38041D91E133200CF23F814C0DF7B630 (void);
// 0x00000015 System.Void CharControl/<>c::.cctor()
extern void U3CU3Ec__cctor_m22867D0013C3895058C3589D3AA9837EE52FF0D2 (void);
// 0x00000016 System.Void CharControl/<>c::.ctor()
extern void U3CU3Ec__ctor_m0093EAA02FD05C07FBF35B4333C0C7D427357533 (void);
// 0x00000017 System.Boolean CharControl/<>c::<CheckRelevate>b__21_0(CharControl)
extern void U3CU3Ec_U3CCheckRelevateU3Eb__21_0_mD4D9A1B7BB939B76FE986B7B530B1C6A05596AA1 (void);
// 0x00000018 System.Void CharControl/<CheckRelevate>d__21::.ctor(System.Int32)
extern void U3CCheckRelevateU3Ed__21__ctor_mA361476A27B6D1FF49F6BA63693BEB85B52D0337 (void);
// 0x00000019 System.Void CharControl/<CheckRelevate>d__21::System.IDisposable.Dispose()
extern void U3CCheckRelevateU3Ed__21_System_IDisposable_Dispose_mE650F9D2D876A26FB6F1BD8C3CB92FB0F6B9406D (void);
// 0x0000001A System.Boolean CharControl/<CheckRelevate>d__21::MoveNext()
extern void U3CCheckRelevateU3Ed__21_MoveNext_m58096431A0A44BA83BB1679F134EAF5C0C4A6FBB (void);
// 0x0000001B System.Object CharControl/<CheckRelevate>d__21::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCheckRelevateU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCB10E92FB3B67BCA27564E938D4A284331728FC6 (void);
// 0x0000001C System.Object CharControl/<CheckRelevate>d__21::System.Collections.IEnumerator.get_Current()
extern void U3CCheckRelevateU3Ed__21_System_Collections_IEnumerator_get_Current_m464A48492D74BA93FD5AC6788AB971EB29B19485 (void);
// 0x0000001D CharsBundleData[] CharsContainer::get_CharBundles()
extern void CharsContainer_get_CharBundles_mA18F021F7787AC922E81E27192C083AAE634CF39 (void);
// 0x0000001E System.Void CharsContainer::LoadResources()
extern void CharsContainer_LoadResources_mC434AAC21E19265D813E0B7E3F2EB3B25FD1DABF (void);
// 0x0000001F System.Void CharsContainer::.ctor()
extern void CharsContainer__ctor_m605ACE9AA15C5819B90DFAF384BC7A972520D52F (void);
// 0x00000020 System.Collections.Generic.List`1<CharControl> CharsGenerator::get_CharsOnScene()
extern void CharsGenerator_get_CharsOnScene_m1D6EDE7E5FA590B1195B033ECE89B6BE1F29EBF3 (void);
// 0x00000021 System.Void CharsGenerator::set_CharsOnScene(System.Collections.Generic.List`1<CharControl>)
extern void CharsGenerator_set_CharsOnScene_mFAC4BC98889E613E9E7299301BE4E3B47AE00AC3 (void);
// 0x00000022 System.Collections.Generic.List`1<CharsData> CharsGenerator::get_CharsPrepareOnScene()
extern void CharsGenerator_get_CharsPrepareOnScene_m232D2807B2745CD94DC8AD5E1B1FA8DDE239AB32 (void);
// 0x00000023 System.Void CharsGenerator::set_CharsPrepareOnScene(System.Collections.Generic.List`1<CharsData>)
extern void CharsGenerator_set_CharsPrepareOnScene_m18D3C27B828BA65DD4724BCDC35A34FD88B5D085 (void);
// 0x00000024 System.Void CharsGenerator::Awake()
extern void CharsGenerator_Awake_mDD95323710FA15E6F4C60B347451C3CB006142B1 (void);
// 0x00000025 System.Void CharsGenerator::Start()
extern void CharsGenerator_Start_mB3AC3DF8DB11E07F1F25F1F8FF7D6776C5046E98 (void);
// 0x00000026 System.Void CharsGenerator::CharGenerate()
extern void CharsGenerator_CharGenerate_m8BC7A85E0E0DB9BAABEB7A473D8692635633FDB3 (void);
// 0x00000027 System.Void CharsGenerator::SetCharsOnScene()
extern void CharsGenerator_SetCharsOnScene_m9300C8B08B98F7512C9A5063969280675DBAD740 (void);
// 0x00000028 CharsData[] CharsGenerator::ShuffleArray(CharsData[])
extern void CharsGenerator_ShuffleArray_m7EAB71A9D642F1173D498254A1F7064F0362AE19 (void);
// 0x00000029 System.Void CharsGenerator::AddCharOnScene(CharsData)
extern void CharsGenerator_AddCharOnScene_m96078093CE4F424BD846646FBC495CE93C19AD03 (void);
// 0x0000002A System.Collections.IEnumerator CharsGenerator::InitializeEffect()
extern void CharsGenerator_InitializeEffect_m35BE446407A6FF90266A52ED23782D4FD05CA3E1 (void);
// 0x0000002B System.Void CharsGenerator::.ctor()
extern void CharsGenerator__ctor_mC5385FED60828331DC38005C2DF67ACA1E6C0B0A (void);
// 0x0000002C System.Boolean CharsGenerator::<CharGenerate>b__14_0(CharsData)
extern void CharsGenerator_U3CCharGenerateU3Eb__14_0_mA168EE1D18167DC640E364DAFF7164998D4C05D0 (void);
// 0x0000002D System.Void CharsGenerator/<>c__DisplayClass14_0::.ctor()
extern void U3CU3Ec__DisplayClass14_0__ctor_mDF2E4F72C494882B94DF063938E248EE6108DDAF (void);
// 0x0000002E System.Boolean CharsGenerator/<>c__DisplayClass14_0::<CharGenerate>b__1(CharsData)
extern void U3CU3Ec__DisplayClass14_0_U3CCharGenerateU3Eb__1_m42A572588759F99FADE2AD7CBE4DE57A6339F4AF (void);
// 0x0000002F System.Void CharsGenerator/<InitializeEffect>d__18::.ctor(System.Int32)
extern void U3CInitializeEffectU3Ed__18__ctor_m2963AC28072621513A43A21896B10ACA02389E7F (void);
// 0x00000030 System.Void CharsGenerator/<InitializeEffect>d__18::System.IDisposable.Dispose()
extern void U3CInitializeEffectU3Ed__18_System_IDisposable_Dispose_mE589EF6E332661A6A7811B65DDC1E246B2294114 (void);
// 0x00000031 System.Boolean CharsGenerator/<InitializeEffect>d__18::MoveNext()
extern void U3CInitializeEffectU3Ed__18_MoveNext_mBD7B90F646F6A65B92F420C255A52226951F161F (void);
// 0x00000032 System.Object CharsGenerator/<InitializeEffect>d__18::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CInitializeEffectU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m42ECACA7787922D32854CA8AD289144E6818C99C (void);
// 0x00000033 System.Object CharsGenerator/<InitializeEffect>d__18::System.Collections.IEnumerator.get_Current()
extern void U3CInitializeEffectU3Ed__18_System_Collections_IEnumerator_get_Current_m1F5AE063FBD3E0D97CB9EE0C007E93579AF15A90 (void);
// 0x00000034 T GlobalFunctions::GetComponentObject(System.String)
// 0x00000035 CharControl LevelProvider::get_CurrentChar()
extern void LevelProvider_get_CurrentChar_mB00F8D84D9CE0BEABC41F5B0CD8C62B0744EA00F (void);
// 0x00000036 System.Void LevelProvider::set_CurrentChar(CharControl)
extern void LevelProvider_set_CurrentChar_mC845B8A970536A1E9BA9B577A63175EB193E64E5 (void);
// 0x00000037 System.Int32 LevelProvider::get_CurrentCharsCount()
extern void LevelProvider_get_CurrentCharsCount_m3F727FCD9E869AE74F8BF985E69C22B17810A87E (void);
// 0x00000038 System.Void LevelProvider::set_CurrentCharsCount(System.Int32)
extern void LevelProvider_set_CurrentCharsCount_m27DF6B2ECC60D5C42FBB8A587B5DEE7235D7031F (void);
// 0x00000039 CharsGenerator LevelProvider::get_CharsGenerator()
extern void LevelProvider_get_CharsGenerator_mB8E799CB7B0821293DE8E6AB1A245AD4B11738B1 (void);
// 0x0000003A System.Void LevelProvider::set_CharsGenerator(CharsGenerator)
extern void LevelProvider_set_CharsGenerator_mDB904A9ED987FE4F6B86770D73A0909BF8948A99 (void);
// 0x0000003B System.Int32 LevelProvider::get_Score()
extern void LevelProvider_get_Score_m0E65DD5953788C0FBB8E65BEC1ACFF8A9F53A6F0 (void);
// 0x0000003C System.Void LevelProvider::set_Score(System.Int32)
extern void LevelProvider_set_Score_mBED651E1406AC9A3CDFA39281E646B3B01B63A8B (void);
// 0x0000003D System.Void LevelProvider::NextLevel()
extern void LevelProvider_NextLevel_mBFD5EAB47CF7715296B1FAF72BF1857983628EDF (void);
// 0x0000003E System.Void LevelProvider::RestartGame()
extern void LevelProvider_RestartGame_m1159957EE264AC0EF22A022413CB5D3D13F1F465 (void);
// 0x0000003F System.Void LevelProvider::WinPanelActive(System.Boolean)
extern void LevelProvider_WinPanelActive_m2C2BB89363A35E652A377A717451EFC37958B141 (void);
// 0x00000040 System.Void LevelProvider::.ctor()
extern void LevelProvider__ctor_mEB879CC69BDE15B41700F451692B744BCAE811F2 (void);
// 0x00000041 System.Void LoadingScreen::StartGame()
extern void LoadingScreen_StartGame_mECB829D2446F38D73C4530FE39AD42AEF6A9EF46 (void);
// 0x00000042 System.Void LoadingScreen::PanelActive(System.Boolean)
extern void LoadingScreen_PanelActive_m3D5780ABDCF9855B059512A617AC7C1E114C34C0 (void);
// 0x00000043 System.Collections.IEnumerator LoadingScreen::LoadScreen()
extern void LoadingScreen_LoadScreen_mDD89C18CE436D6D2063C31F49AE91F20F5127B18 (void);
// 0x00000044 System.Void LoadingScreen::.ctor()
extern void LoadingScreen__ctor_m294A22F1CBC235719588314E0E286059BC3C9586 (void);
// 0x00000045 System.Void LoadingScreen/<LoadScreen>d__3::.ctor(System.Int32)
extern void U3CLoadScreenU3Ed__3__ctor_m8C85D910055010DC08BB6EAFEE748471DA723D6B (void);
// 0x00000046 System.Void LoadingScreen/<LoadScreen>d__3::System.IDisposable.Dispose()
extern void U3CLoadScreenU3Ed__3_System_IDisposable_Dispose_m1D7C2F660F4B39CF5A467BD495803E90FBCADD2F (void);
// 0x00000047 System.Boolean LoadingScreen/<LoadScreen>d__3::MoveNext()
extern void U3CLoadScreenU3Ed__3_MoveNext_m689B22925AF38421EEB5CF7CC5A11972D26C987A (void);
// 0x00000048 System.Object LoadingScreen/<LoadScreen>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadScreenU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m798841D11E7201A7D6CCA34F760ABF3BA95C0456 (void);
// 0x00000049 System.Object LoadingScreen/<LoadScreen>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CLoadScreenU3Ed__3_System_Collections_IEnumerator_get_Current_mAAB03B606D079E0FB8EAE6538CEAD0D4BDB1C7FA (void);
// 0x0000004A System.Void TweenEffect::BounceEffect()
extern void TweenEffect_BounceEffect_mB01EFD4885A764247CB6F16E495DD19974BFC58B (void);
// 0x0000004B System.Void TweenEffect::StrongBounceEffect()
extern void TweenEffect_StrongBounceEffect_m3D7E7C12E3636C041F06C1121C7646DEC82A5C97 (void);
// 0x0000004C System.Void TweenEffect::.ctor()
extern void TweenEffect__ctor_m30A886E518C80FC5309744866EC314EE321E2499 (void);
// 0x0000004D DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModulePhysics::DOPath(UnityEngine.Rigidbody,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,DG.Tweening.PathMode)
extern void DOTweenModulePhysics_DOPath_m3C49FFEB71D494F474F7223248D7105E90CF5BE7 (void);
// 0x0000004E DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModulePhysics::DOLocalPath(UnityEngine.Rigidbody,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,DG.Tweening.PathMode)
extern void DOTweenModulePhysics_DOLocalPath_m6F939DAF66D56DA9B52DA62546C70F3862E14193 (void);
// 0x0000004F System.Void DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass9_0::.ctor()
extern void U3CU3Ec__DisplayClass9_0__ctor_mCA1E6F241784A13DEC15E12A0219479D552BB6BB (void);
// 0x00000050 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass9_0::<DOPath>b__0()
extern void U3CU3Ec__DisplayClass9_0_U3CDOPathU3Eb__0_mD050544C13BA54260FC94E0964EF204B4EDB2628 (void);
// 0x00000051 System.Void DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_m819824BC9854A0C90662B04D48B88CA7B052BF83 (void);
// 0x00000052 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass10_0::<DOLocalPath>b__0()
extern void U3CU3Ec__DisplayClass10_0_U3CDOLocalPathU3Eb__0_m3089CA33F25546258A5E846EA51421D71AC55D70 (void);
// 0x00000053 System.Void DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass10_0::<DOLocalPath>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass10_0_U3CDOLocalPathU3Eb__1_mD201EFBB4403FBDEC0D377D39A768A581E6CCDDD (void);
// 0x00000054 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOFade(UnityEngine.UI.Image,System.Single,System.Single)
extern void DOTweenModuleUI_DOFade_mE66C8B7D137E0DDE54AAA96F551811FB8BDB8754 (void);
// 0x00000055 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOFade(UnityEngine.UI.Text,System.Single,System.Single)
extern void DOTweenModuleUI_DOFade_m21F7CCBB732ABBD63BFD893D1A8D9837532A70D1 (void);
// 0x00000056 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m5CD451B862F3CC71D8D25BC3A69725FCDD6144C4 (void);
// 0x00000057 UnityEngine.Color DG.Tweening.DOTweenModuleUI/<>c__DisplayClass4_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__0_m507CD55516001BB4A76D7E58CB159B5394D6413C (void);
// 0x00000058 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass4_0::<DOFade>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__1_mE65F1E4D380EC305AF0AE93E0315B0B23CA36310 (void);
// 0x00000059 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass36_0::.ctor()
extern void U3CU3Ec__DisplayClass36_0__ctor_mE2912C6BF4318E28870DE7AA92E4B05C80D92EE0 (void);
// 0x0000005A UnityEngine.Color DG.Tweening.DOTweenModuleUI/<>c__DisplayClass36_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass36_0_U3CDOFadeU3Eb__0_mA3238766629BBF7E2750683F62F7629C5B097D87 (void);
// 0x0000005B System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass36_0::<DOFade>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass36_0_U3CDOFadeU3Eb__1_m119575F5E470CA6D0C37EF9E1A9C3A2CF8B2AA3A (void);
// 0x0000005C System.Void DG.Tweening.DOTweenModuleUtils::Init()
extern void DOTweenModuleUtils_Init_mFF2188F42FA7128FE1A9BBC2CB9E6351919C9D15 (void);
// 0x0000005D System.Void DG.Tweening.DOTweenModuleUtils::Preserver()
extern void DOTweenModuleUtils_Preserver_mBE8D1FE2AE913FBFFCCF43B5A615D941B3706F43 (void);
// 0x0000005E System.Void DG.Tweening.DOTweenModuleUtils/Physics::SetOrientationOnPath(DG.Tweening.Plugins.Options.PathOptions,DG.Tweening.Tween,UnityEngine.Quaternion,UnityEngine.Transform)
extern void Physics_SetOrientationOnPath_mCC376173A621DA244564EDF8A6347AB2A0F47816 (void);
// 0x0000005F System.Boolean DG.Tweening.DOTweenModuleUtils/Physics::HasRigidbody(UnityEngine.Component)
extern void Physics_HasRigidbody_m3082D72A658CCDDF6BB1F3D4A3EFBD6397D6E862 (void);
// 0x00000060 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModuleUtils/Physics::CreateDOTweenPathTween(UnityEngine.MonoBehaviour,System.Boolean,System.Boolean,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,DG.Tweening.PathMode)
extern void Physics_CreateDOTweenPathTween_m94D41B65E37500E5ACCF6B6F3B5D53FB031C4245 (void);
static Il2CppMethodPointer s_methodPointers[96] = 
{
	CharsBundleData_get_CharsData_m2A642A6B655267B4CC126843873D43AA04F9875C,
	CharsBundleData_set_CharsData_m81AE91FF2AA2625038AF4388C7FE7CCEFFC511BE,
	CharsBundleData__ctor_m106D367F39A7FAD0DB260E5744651FC98ED96B14,
	CharsData_get_Identifier_mC20D9B2E42F23E7CFA66FA65141011F719AB2681,
	CharsData_get_Sprite_m1BFE6586725A0A68865F947F36E4E0F3BF85DE46,
	CharsData__ctor_mFCCB902117FCD0BF8AA6CFFAA9BE96461BDA87D2,
	CharControl_get_Chardata_m82B81759BB755408B138708B8D0E0D48835EAF0E,
	CharControl_set_Chardata_m389A1A07EF79E9431F64F3CFAC17A1156BB844EB,
	CharControl_get_TweenEffect_m0936842652110F63724B3CF1BE3176069B810A6F,
	CharControl_set_TweenEffect_m2DAAD124E5169D25B22033EE6F8232AC703CF164,
	CharControl_get_CharImage_m6717F383E31A7235179A68C6BBB341C6FC9E2AFB,
	CharControl_set_CharImage_mC6CC17A5B08C680FE11E3AAF11CC2AC2B38F9F10,
	CharControl_get_IsSelected_m5DAAC6C82C108F3FD7842336558B1B62BD8206C8,
	CharControl_set_IsSelected_m84AD011947B55056DDC4F4202075B1A9EE9D78CF,
	CharControl_OnEnable_mBDBC0BF195EC0A8F76AC309607F41A1E87C6C0C5,
	CharControl_ButtonCallBack_mA2858D0CC04E3AE51F0833B8E502F753939A912D,
	CharControl_CheckRelevate_mC7196FD3F0797C15DC2A2672C5A944A470C0F062,
	CharControl_OnDisable_m145F1B26B72DCBE03EA575C138A7CB8F834F6ECF,
	CharControl__ctor_mBE2BFABAAF7937260B08A3A03291945AE9C11260,
	CharControl_U3COnEnableU3Eb__19_0_mE0D85D7B38041D91E133200CF23F814C0DF7B630,
	U3CU3Ec__cctor_m22867D0013C3895058C3589D3AA9837EE52FF0D2,
	U3CU3Ec__ctor_m0093EAA02FD05C07FBF35B4333C0C7D427357533,
	U3CU3Ec_U3CCheckRelevateU3Eb__21_0_mD4D9A1B7BB939B76FE986B7B530B1C6A05596AA1,
	U3CCheckRelevateU3Ed__21__ctor_mA361476A27B6D1FF49F6BA63693BEB85B52D0337,
	U3CCheckRelevateU3Ed__21_System_IDisposable_Dispose_mE650F9D2D876A26FB6F1BD8C3CB92FB0F6B9406D,
	U3CCheckRelevateU3Ed__21_MoveNext_m58096431A0A44BA83BB1679F134EAF5C0C4A6FBB,
	U3CCheckRelevateU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCB10E92FB3B67BCA27564E938D4A284331728FC6,
	U3CCheckRelevateU3Ed__21_System_Collections_IEnumerator_get_Current_m464A48492D74BA93FD5AC6788AB971EB29B19485,
	CharsContainer_get_CharBundles_mA18F021F7787AC922E81E27192C083AAE634CF39,
	CharsContainer_LoadResources_mC434AAC21E19265D813E0B7E3F2EB3B25FD1DABF,
	CharsContainer__ctor_m605ACE9AA15C5819B90DFAF384BC7A972520D52F,
	CharsGenerator_get_CharsOnScene_m1D6EDE7E5FA590B1195B033ECE89B6BE1F29EBF3,
	CharsGenerator_set_CharsOnScene_mFAC4BC98889E613E9E7299301BE4E3B47AE00AC3,
	CharsGenerator_get_CharsPrepareOnScene_m232D2807B2745CD94DC8AD5E1B1FA8DDE239AB32,
	CharsGenerator_set_CharsPrepareOnScene_m18D3C27B828BA65DD4724BCDC35A34FD88B5D085,
	CharsGenerator_Awake_mDD95323710FA15E6F4C60B347451C3CB006142B1,
	CharsGenerator_Start_mB3AC3DF8DB11E07F1F25F1F8FF7D6776C5046E98,
	CharsGenerator_CharGenerate_m8BC7A85E0E0DB9BAABEB7A473D8692635633FDB3,
	CharsGenerator_SetCharsOnScene_m9300C8B08B98F7512C9A5063969280675DBAD740,
	CharsGenerator_ShuffleArray_m7EAB71A9D642F1173D498254A1F7064F0362AE19,
	CharsGenerator_AddCharOnScene_m96078093CE4F424BD846646FBC495CE93C19AD03,
	CharsGenerator_InitializeEffect_m35BE446407A6FF90266A52ED23782D4FD05CA3E1,
	CharsGenerator__ctor_mC5385FED60828331DC38005C2DF67ACA1E6C0B0A,
	CharsGenerator_U3CCharGenerateU3Eb__14_0_mA168EE1D18167DC640E364DAFF7164998D4C05D0,
	U3CU3Ec__DisplayClass14_0__ctor_mDF2E4F72C494882B94DF063938E248EE6108DDAF,
	U3CU3Ec__DisplayClass14_0_U3CCharGenerateU3Eb__1_m42A572588759F99FADE2AD7CBE4DE57A6339F4AF,
	U3CInitializeEffectU3Ed__18__ctor_m2963AC28072621513A43A21896B10ACA02389E7F,
	U3CInitializeEffectU3Ed__18_System_IDisposable_Dispose_mE589EF6E332661A6A7811B65DDC1E246B2294114,
	U3CInitializeEffectU3Ed__18_MoveNext_mBD7B90F646F6A65B92F420C255A52226951F161F,
	U3CInitializeEffectU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m42ECACA7787922D32854CA8AD289144E6818C99C,
	U3CInitializeEffectU3Ed__18_System_Collections_IEnumerator_get_Current_m1F5AE063FBD3E0D97CB9EE0C007E93579AF15A90,
	NULL,
	LevelProvider_get_CurrentChar_mB00F8D84D9CE0BEABC41F5B0CD8C62B0744EA00F,
	LevelProvider_set_CurrentChar_mC845B8A970536A1E9BA9B577A63175EB193E64E5,
	LevelProvider_get_CurrentCharsCount_m3F727FCD9E869AE74F8BF985E69C22B17810A87E,
	LevelProvider_set_CurrentCharsCount_m27DF6B2ECC60D5C42FBB8A587B5DEE7235D7031F,
	LevelProvider_get_CharsGenerator_mB8E799CB7B0821293DE8E6AB1A245AD4B11738B1,
	LevelProvider_set_CharsGenerator_mDB904A9ED987FE4F6B86770D73A0909BF8948A99,
	LevelProvider_get_Score_m0E65DD5953788C0FBB8E65BEC1ACFF8A9F53A6F0,
	LevelProvider_set_Score_mBED651E1406AC9A3CDFA39281E646B3B01B63A8B,
	LevelProvider_NextLevel_mBFD5EAB47CF7715296B1FAF72BF1857983628EDF,
	LevelProvider_RestartGame_m1159957EE264AC0EF22A022413CB5D3D13F1F465,
	LevelProvider_WinPanelActive_m2C2BB89363A35E652A377A717451EFC37958B141,
	LevelProvider__ctor_mEB879CC69BDE15B41700F451692B744BCAE811F2,
	LoadingScreen_StartGame_mECB829D2446F38D73C4530FE39AD42AEF6A9EF46,
	LoadingScreen_PanelActive_m3D5780ABDCF9855B059512A617AC7C1E114C34C0,
	LoadingScreen_LoadScreen_mDD89C18CE436D6D2063C31F49AE91F20F5127B18,
	LoadingScreen__ctor_m294A22F1CBC235719588314E0E286059BC3C9586,
	U3CLoadScreenU3Ed__3__ctor_m8C85D910055010DC08BB6EAFEE748471DA723D6B,
	U3CLoadScreenU3Ed__3_System_IDisposable_Dispose_m1D7C2F660F4B39CF5A467BD495803E90FBCADD2F,
	U3CLoadScreenU3Ed__3_MoveNext_m689B22925AF38421EEB5CF7CC5A11972D26C987A,
	U3CLoadScreenU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m798841D11E7201A7D6CCA34F760ABF3BA95C0456,
	U3CLoadScreenU3Ed__3_System_Collections_IEnumerator_get_Current_mAAB03B606D079E0FB8EAE6538CEAD0D4BDB1C7FA,
	TweenEffect_BounceEffect_mB01EFD4885A764247CB6F16E495DD19974BFC58B,
	TweenEffect_StrongBounceEffect_m3D7E7C12E3636C041F06C1121C7646DEC82A5C97,
	TweenEffect__ctor_m30A886E518C80FC5309744866EC314EE321E2499,
	DOTweenModulePhysics_DOPath_m3C49FFEB71D494F474F7223248D7105E90CF5BE7,
	DOTweenModulePhysics_DOLocalPath_m6F939DAF66D56DA9B52DA62546C70F3862E14193,
	U3CU3Ec__DisplayClass9_0__ctor_mCA1E6F241784A13DEC15E12A0219479D552BB6BB,
	U3CU3Ec__DisplayClass9_0_U3CDOPathU3Eb__0_mD050544C13BA54260FC94E0964EF204B4EDB2628,
	U3CU3Ec__DisplayClass10_0__ctor_m819824BC9854A0C90662B04D48B88CA7B052BF83,
	U3CU3Ec__DisplayClass10_0_U3CDOLocalPathU3Eb__0_m3089CA33F25546258A5E846EA51421D71AC55D70,
	U3CU3Ec__DisplayClass10_0_U3CDOLocalPathU3Eb__1_mD201EFBB4403FBDEC0D377D39A768A581E6CCDDD,
	DOTweenModuleUI_DOFade_mE66C8B7D137E0DDE54AAA96F551811FB8BDB8754,
	DOTweenModuleUI_DOFade_m21F7CCBB732ABBD63BFD893D1A8D9837532A70D1,
	U3CU3Ec__DisplayClass4_0__ctor_m5CD451B862F3CC71D8D25BC3A69725FCDD6144C4,
	U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__0_m507CD55516001BB4A76D7E58CB159B5394D6413C,
	U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__1_mE65F1E4D380EC305AF0AE93E0315B0B23CA36310,
	U3CU3Ec__DisplayClass36_0__ctor_mE2912C6BF4318E28870DE7AA92E4B05C80D92EE0,
	U3CU3Ec__DisplayClass36_0_U3CDOFadeU3Eb__0_mA3238766629BBF7E2750683F62F7629C5B097D87,
	U3CU3Ec__DisplayClass36_0_U3CDOFadeU3Eb__1_m119575F5E470CA6D0C37EF9E1A9C3A2CF8B2AA3A,
	DOTweenModuleUtils_Init_mFF2188F42FA7128FE1A9BBC2CB9E6351919C9D15,
	DOTweenModuleUtils_Preserver_mBE8D1FE2AE913FBFFCCF43B5A615D941B3706F43,
	Physics_SetOrientationOnPath_mCC376173A621DA244564EDF8A6347AB2A0F47816,
	Physics_HasRigidbody_m3082D72A658CCDDF6BB1F3D4A3EFBD6397D6E862,
	Physics_CreateDOTweenPathTween_m94D41B65E37500E5ACCF6B6F3B5D53FB031C4245,
};
static const int32_t s_InvokerIndices[96] = 
{
	1310,
	1123,
	1352,
	1310,
	1310,
	1352,
	1310,
	1123,
	1310,
	1123,
	1310,
	1123,
	1330,
	1140,
	1352,
	1352,
	1310,
	1352,
	1352,
	1352,
	2240,
	1352,
	963,
	1114,
	1352,
	1330,
	1310,
	1310,
	1310,
	1352,
	1352,
	1310,
	1123,
	1310,
	1123,
	1352,
	1352,
	1352,
	1352,
	864,
	1123,
	1310,
	1352,
	963,
	1352,
	963,
	1114,
	1352,
	1330,
	1310,
	1310,
	-1,
	1310,
	1123,
	1299,
	1114,
	1310,
	1123,
	1299,
	1114,
	1352,
	1352,
	1140,
	1352,
	1352,
	1140,
	1310,
	1352,
	1114,
	1352,
	1330,
	1310,
	1310,
	1352,
	1352,
	1352,
	1587,
	1587,
	1352,
	1348,
	1352,
	1348,
	1158,
	1728,
	1728,
	1352,
	1278,
	1091,
	1352,
	1278,
	1091,
	2240,
	2240,
	1651,
	2164,
	1419,
};
static const Il2CppTokenRangePair s_rgctxIndices[1] = 
{
	{ 0x06000034, { 0, 1 } },
};
extern const uint32_t g_rgctx_GameObject_TryGetComponent_TisT_t208D407B3528FEDAC85B7D6361F0FB07F3A37C98_m617D5679AF606B2BCFF9B7261632C90EE4B804D5;
static const Il2CppRGCTXDefinition s_rgctxValues[1] = 
{
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_GameObject_TryGetComponent_TisT_t208D407B3528FEDAC85B7D6361F0FB07F3A37C98_m617D5679AF606B2BCFF9B7261632C90EE4B804D5 },
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	96,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	1,
	s_rgctxIndices,
	1,
	s_rgctxValues,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
