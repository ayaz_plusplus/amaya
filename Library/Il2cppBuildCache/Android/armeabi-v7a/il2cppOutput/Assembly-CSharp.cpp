﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename T1>
struct VirtualActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct VirtualFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1>
struct VirtualFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>
struct ABSTweenPlugin_3_t6A1019F3F26DC4DFAFFA4439E8D04997B8A86E75;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>
struct ABSTweenPlugin_3_tB684BB30E8E054CCB3EBBFB96EDE8032FBD0284E;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>
struct ABSTweenPlugin_3_t0048BB94992A96F02D427E2C26C6E92AC9AECD32;
// System.Action`4<DG.Tweening.Plugins.Options.PathOptions,System.Object,UnityEngine.Quaternion,System.Object>
struct Action_4_t0E97DD6848031923736201DE6A983F114E84EFFF;
// System.Action`4<DG.Tweening.Plugins.Options.PathOptions,DG.Tweening.Tween,UnityEngine.Quaternion,UnityEngine.Transform>
struct Action_4_t758FC0992B1B40F36598AA26D46FB5626C503AFE;
// DG.Tweening.Core.DOGetter`1<UnityEngine.Color>
struct DOGetter_1_tCC9DEA9BB7DA5C2570ED2CE9BD13EF5C154B7B10;
// DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>
struct DOGetter_1_tE8D2419A0219EB945463B02351F4905C77235E1A;
// DG.Tweening.Core.DOSetter`1<UnityEngine.Color>
struct DOSetter_1_tB62013E4617ECB51822384C8592AAB764E1662BF;
// DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>
struct DOSetter_1_tAEE38AA9321AE4A0A4A4E5D496EA753BFB746B85;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399;
// System.EventHandler`1<System.Runtime.ExceptionServices.FirstChanceExceptionEventArgs>
struct EventHandler_1_t7F26BD2270AD4531F2328FD1382278E975249DF1;
// System.Func`2<CharControl,System.Boolean>
struct Func_2_t20F4B130EF75554CF71A9826AA0AC83CCB3EEDAC;
// System.Func`2<CharsData,System.Boolean>
struct Func_2_tF8C630B1BAE8BC459BCA2C994588C319051E39EB;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8;
// System.Collections.Generic.IEnumerable`1<CharControl>
struct IEnumerable_1_t79CEE8E26B399DDFFD00FD5A19FAEBBFA664ACBA;
// System.Collections.Generic.IEnumerable`1<CharsData>
struct IEnumerable_1_tEB25636DF290C71583C57701DD50BC6EFCDD51D9;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t52B1AC8D9E5E1ED28DF6C46A37C9A1B00B394F9D;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t34AA4AF4E7352129CA58045901530E41445AC16D;
// System.Collections.Generic.List`1<CharControl>
struct List_1_t37187EA3F5A7945D0B45C8A071194C1191FC32FE;
// System.Collections.Generic.List`1<CharsData>
struct List_1_t49AE3466419291B57C0EF7E386E193A7AFE92C02;
// System.Collections.Generic.List`1<UnityEngine.UI.Image>
struct List_1_t815A476B0A21E183042059E705F9E505478CD8AE;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5;
// System.Collections.Generic.List`1<System.String>
struct List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3;
// DG.Tweening.TweenCallback`1<System.Int32>
struct TweenCallback_1_t145CD5D30F08B617B445D2B1B79A7BAADC305B9B;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>
struct TweenerCore_3_t551A69CD4FF610AE9DF07C97A86B6FE12EBEB597;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>
struct TweenerCore_3_t3456DC6DCE3C72CB0D8A037FE930A2C2D0B92569;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>
struct TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A;
// System.Reflection.Assembly[]
struct AssemblyU5BU5D_t42061B4CA43487DD8ECD8C3AACCEF783FA081FF0;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// CharControl[]
struct CharControlU5BU5D_t1BB82C75B29D38BD727D0DDD1B514DEDB1BFB8B3;
// CharsBundleData[]
struct CharsBundleDataU5BU5D_t49861CE889FB0A7632E6FD7126DE29B0F470F700;
// CharsData[]
struct CharsDataU5BU5D_t6E1A2B31C527A76C761C25915A588ED83C6B4CDE;
// DG.Tweening.Plugins.Core.PathCore.ControlPoint[]
struct ControlPointU5BU5D_t9F61665C9F79DD5715B9C6C357BC1A9F16326884;
// System.Delegate[]
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8;
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
// UnityEngine.UI.Selectable[]
struct SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535;
// System.Single[]
struct SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4;
// DG.Tweening.Plugins.Core.PathCore.ABSPathDecoder
struct ABSPathDecoder_tC2631579BAB41F64C63A0909E8386B88B45F93D1;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11;
// System.AppDomain
struct AppDomain_tBEB6322D51DCB12C09A56A49886C2D09BA1C1A8A;
// System.Reflection.Assembly
struct Assembly_t;
// System.AssemblyLoadEventHandler
struct AssemblyLoadEventHandler_tE06B38463937F6FBCCECF4DF6519F83C1683FE0C;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// UnityEngine.UI.Button
struct Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D;
// UnityEngine.Canvas
struct Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E;
// CharControl
struct CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD;
// CharsBundleData
struct CharsBundleData_tC2C62F2A7FAEEDB189C65454E3E32437ECD36285;
// CharsContainer
struct CharsContainer_tB2FB3D6B21722D0168C892A2C3837608C80C7CC5;
// CharsData
struct CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595;
// CharsGenerator
struct CharsGenerator_t7E18FC7617D416B9E55B664B885A3138E43EE2A2;
// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684;
// UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7;
// System.DelegateData
struct DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288;
// DG.Tweening.EaseFunction
struct EaseFunction_tC7ECEFDCAE4EC041E6FD7AC7C021E7B7680EFEB9;
// System.EventHandler
struct EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B;
// UnityEngine.UI.FontData
struct FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738;
// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319;
// UnityEngine.UI.Graphic
struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24;
// System.Collections.IEnumerator
struct IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105;
// UnityEngine.UI.Image
struct Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9;
// LevelProvider
struct LevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841;
// LoadingScreen
struct LoadingScreen_tD1CD7EA45A62C3A1A641D929E47F330644D353B2;
// UnityEngine.Material
struct Material_t8927C00353A72755313F046D0CE85178AE8218EE;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// UnityEngine.Mesh
struct Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A;
// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A;
// DG.Tweening.Plugins.Core.PathCore.Path
struct Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC;
// System.Random
struct Random_t6C9E9775A149D0ADCFEB4B252C408F03EE870118;
// UnityEngine.UI.RectMask2D
struct RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15;
// UnityEngine.RectTransform
struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072;
// System.ResolveEventHandler
struct ResolveEventHandler_tC6827B550D5B6553B57571630B1EE01AC12A1089;
// UnityEngine.Rigidbody
struct Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A;
// UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A;
// UnityEngine.UI.Selectable
struct Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD;
// DG.Tweening.Sequence
struct Sequence_tE01FFFCA34A537CE2FF32EDAF451CDEC55A1399E;
// UnityEngine.Sprite
struct Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9;
// System.String
struct String_t;
// UnityEngine.UI.Text
struct Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1;
// UnityEngine.TextGenerator
struct TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70;
// UnityEngine.Texture2D
struct Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF;
// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1;
// DG.Tweening.Tween
struct Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941;
// DG.Tweening.TweenCallback
struct TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB;
// TweenEffect
struct TweenEffect_tB80E075E7B5F736D5D3E0BDAB6A253BF4DA1BBF1;
// DG.Tweening.Tweener
struct Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8;
// System.Type
struct Type_t;
// System.UnhandledExceptionEventHandler
struct UnhandledExceptionEventHandler_t1DF125A860ED9B70F24ADFA6CB0781533A23DA64;
// UnityEngine.Events.UnityAction
struct UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099;
// UnityEngine.Events.UnityEvent
struct UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4;
// UnityEngine.Events.UnityEventBase
struct UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB;
// UnityEngine.UI.VertexHelper
struct VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013;
// System.Reflection.Assembly/ResolveEventHolder
struct ResolveEventHolder_tA37081FAEBE21D83D216225B4489BA8A37B4E13C;
// UnityEngine.UI.Button/ButtonClickedEvent
struct ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F;
// CharControl/<>c
struct U3CU3Ec_t354A1590D8043D0E9E5A2724672CBF8E9E8F1AA4;
// CharControl/<CheckRelevate>d__21
struct U3CCheckRelevateU3Ed__21_t1EC48447076186B1190E53CBB1A680B3ECA1E750;
// CharsGenerator/<>c__DisplayClass14_0
struct U3CU3Ec__DisplayClass14_0_t79B660C3CA0F908E689E0E2E7221DF954F7E5E42;
// CharsGenerator/<InitializeEffect>d__18
struct U3CInitializeEffectU3Ed__18_tB5CE3179A9E4153E9DF3477D7479D251A763486C;
// DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass10_0
struct U3CU3Ec__DisplayClass10_0_t6B6DFEAD8FF1DD6A724C294AA0B120F82A7A6937;
// DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass9_0
struct U3CU3Ec__DisplayClass9_0_tB9266040A99BBE64FC737A175355345A13E86D21;
// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass36_0
struct U3CU3Ec__DisplayClass36_0_tCE8E904A62CBD9F143B3D692CA532EFA7A6F777D;
// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass4_0
struct U3CU3Ec__DisplayClass4_0_t9CD30C09F13A8901FBD37CEEE628023A3AC88587;
// LoadingScreen/<LoadScreen>d__3
struct U3CLoadScreenU3Ed__3_tAB779AB619F104C740B1B46095B34AB1EA682770;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE;

IL2CPP_EXTERN_C RuntimeClass* Action_4_t758FC0992B1B40F36598AA26D46FB5626C503AFE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* CharsContainer_tB2FB3D6B21722D0168C892A2C3837608C80C7CC5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* DOGetter_1_tCC9DEA9BB7DA5C2570ED2CE9BD13EF5C154B7B10_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* DOGetter_1_tE8D2419A0219EB945463B02351F4905C77235E1A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* DOSetter_1_tAEE38AA9321AE4A0A4A4E5D496EA753BFB746B85_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* DOSetter_1_tB62013E4617ECB51822384C8592AAB764E1662BF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* DOTweenModuleUtils_t1924C98C4EB46411DA5B22524EB14EFDC0203EA0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t20F4B130EF75554CF71A9826AA0AC83CCB3EEDAC_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_tF8C630B1BAE8BC459BCA2C994588C319051E39EB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t37187EA3F5A7945D0B45C8A071194C1191FC32FE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t49AE3466419291B57C0EF7E386E193A7AFE92C02_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Random_t6C9E9775A149D0ADCFEB4B252C408F03EE870118_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Type_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CCheckRelevateU3Ed__21_t1EC48447076186B1190E53CBB1A680B3ECA1E750_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CInitializeEffectU3Ed__18_tB5CE3179A9E4153E9DF3477D7479D251A763486C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CLoadScreenU3Ed__3_tAB779AB619F104C740B1B46095B34AB1EA682770_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass10_0_t6B6DFEAD8FF1DD6A724C294AA0B120F82A7A6937_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass14_0_t79B660C3CA0F908E689E0E2E7221DF954F7E5E42_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass36_0_tCE8E904A62CBD9F143B3D692CA532EFA7A6F777D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass4_0_t9CD30C09F13A8901FBD37CEEE628023A3AC88587_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass9_0_tB9266040A99BBE64FC737A175355345A13E86D21_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_t354A1590D8043D0E9E5A2724672CBF8E9E8F1AA4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral0D1BA8C0E521925077224DB11A6C93FB8E930E14;
IL2CPP_EXTERN_C String_t* _stringLiteral5C8C82299B594D71927B7DD56150A1806D97B335;
IL2CPP_EXTERN_C String_t* _stringLiteral679C291DDDABA344C75D8BC842F0F95E46B6B2EA;
IL2CPP_EXTERN_C String_t* _stringLiteral9E2131BA989553E04EAB0985721463E5686AF29D;
IL2CPP_EXTERN_C String_t* _stringLiteralCE45B43BD3A18C0A8B92DDB8A21820E8B64F43CC;
IL2CPP_EXTERN_C const RuntimeMethod* Action_4__ctor_mB344A638C116BEC788ED3EA352844312528F1175_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* CharControl_U3COnEnableU3Eb__19_0_mE0D85D7B38041D91E133200CF23F814C0DF7B630_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* CharsGenerator_U3CCharGenerateU3Eb__14_0_mA168EE1D18167DC640E364DAFF7164998D4C05D0_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mED8C9575844B41F67CB4C2B13685FC0174CB61BC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisImage_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_m5D5D0C1BB7E1E67F46C955DA2861E7B83FC7301D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisRigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_mB4A92A619135F9258670FB93AE08F229A41D0980_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisTweenEffect_tB80E075E7B5F736D5D3E0BDAB6A253BF4DA1BBF1_mFE8E5BE945ECF3B5557CF812B04BA9D19010FBEA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* DOGetter_1__ctor_m7537F12632FAD0C4475AC13323E07B2D2C6FF081_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* DOGetter_1__ctor_mAC32C501C09F9F6EC5B0DD2C1A8473BAE88DC8CE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* DOSetter_1__ctor_m23DF8B62FE29CBDA90726662AEA2755DF765E733_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* DOSetter_1__ctor_m41D0232769B743141B4389F8A4599EC78F310352_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* DOTween_To_TisVector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_TisPath_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5_TisPathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A_m7C0FFBC94D84A7FAD1568988AC768527794B9F65_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_Count_TisCharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD_mB9C151F66AEAD221FF7001ABD445C652FCA07DC8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_Count_TisCharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595_mF5CBB5FDA01799955A84315875FF0232F7C53FA0_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_ToArray_TisCharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595_m0EC984F784602B3309C821383DC5C87FDB30619B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_Where_TisCharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD_m71644EECCC484BCD70DAF76B0AAA095B910D839B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_Where_TisCharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595_m23A550BB94D43A3560AA9F70C03BFAE61ACB6806_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_mBCB2D275F9431E47AFAE49744FEDF77EDAACA2D5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_mD3BE2F5A91D965C021520535B9413F1429D8A60B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_mF4D92D56E4EE8FDADA509B9D8BA7D3CF7CED1B58_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Func_2__ctor_m8AE4F7EFDDED3C0E1D6C23FDA851725EA4F91FF1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Func_2__ctor_m8E70087591BD5ED6F195C93CF07DA159BEAC2FB9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisCharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD_m8194715FED3744A59E689261461A2D3106DC7A30_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisImage_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_m08E9D50CF81C1176D98860FE7038B9544046EB7D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GlobalFunctions_GetComponentObject_TisLevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841_m2F25E5DB707F42022C693C71FCF04CC50FEE9D25_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GlobalFunctions_GetComponentObject_TisRectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_mD0BF1DF9A674788852D027927E99D65003AF708E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m6225D4407E2F7A63072835132E8B4A07A69FA874_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m62B652590809428BA6EDDA52CC5DD40A0ABC0BA6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Clear_m08CCA40C44AE6824282FFCD856929F8F04530A99_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Clear_m325068E9A3443E1D9B92507B12607D342B659527_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_GetEnumerator_m924E2A798B90A4A58216B9E7ACE17C3F078EDB02_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_ToArray_m947BDA62E5FAFE13045E535DACCC691670751CA1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m62662109EBBF48C763AF94C725FCC99A03E364FD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m6C9F4356B1609DBC166481253083E775FDA52FFC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_mACEFD07C5A0FE9C85BC165DF1E4A825675719EAC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Physics_SetOrientationOnPath_mCC376173A621DA244564EDF8A6347AB2A0F47816_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Resources_LoadAll_TisCharsBundleData_tC2C62F2A7FAEEDB189C65454E3E32437ECD36285_m2073CC955A118CCAA7E5A2145F245359C72C881A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Rigidbody_MovePosition_mB3CBBF21FD0ABB88BC6C004B993DED25673001C7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_mBF1153F332C5ED368722DFB51E2E088E1A8648E0_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t551A69CD4FF610AE9DF07C97A86B6FE12EBEB597_m8DA85B9EBDD18D4817AB73585673258C3904ED11_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec_U3CCheckRelevateU3Eb__21_0_mD4D9A1B7BB939B76FE986B7B530B1C6A05596AA1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass10_0_U3CDOLocalPathU3Eb__0_m3089CA33F25546258A5E846EA51421D71AC55D70_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass10_0_U3CDOLocalPathU3Eb__1_mD201EFBB4403FBDEC0D377D39A768A581E6CCDDD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass14_0_U3CCharGenerateU3Eb__1_m42A572588759F99FADE2AD7CBE4DE57A6339F4AF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass36_0_U3CDOFadeU3Eb__0_mA3238766629BBF7E2750683F62F7629C5B097D87_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass36_0_U3CDOFadeU3Eb__1_m119575F5E470CA6D0C37EF9E1A9C3A2CF8B2AA3A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__0_m507CD55516001BB4A76D7E58CB159B5394D6413C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__1_mE65F1E4D380EC305AF0AE93E0315B0B23CA36310_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass9_0_U3CDOPathU3Eb__0_mD050544C13BA54260FC94E0964EF204B4EDB2628_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeType* MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A_0_0_0_var;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;

struct AssemblyU5BU5D_t42061B4CA43487DD8ECD8C3AACCEF783FA081FF0;
struct CharsBundleDataU5BU5D_t49861CE889FB0A7632E6FD7126DE29B0F470F700;
struct CharsDataU5BU5D_t6E1A2B31C527A76C761C25915A588ED83C6B4CDE;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_tFDCAFCBB4B3431CFF2DC4D3E03FBFDF54EFF7E9A 
{
public:

public:
};


// System.Object


// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>
struct ABSTweenPlugin_3_t0048BB94992A96F02D427E2C26C6E92AC9AECD32  : public RuntimeObject
{
public:

public:
};


// System.Collections.Generic.List`1<CharControl>
struct List_1_t37187EA3F5A7945D0B45C8A071194C1191FC32FE  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	CharControlU5BU5D_t1BB82C75B29D38BD727D0DDD1B514DEDB1BFB8B3* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t37187EA3F5A7945D0B45C8A071194C1191FC32FE, ____items_1)); }
	inline CharControlU5BU5D_t1BB82C75B29D38BD727D0DDD1B514DEDB1BFB8B3* get__items_1() const { return ____items_1; }
	inline CharControlU5BU5D_t1BB82C75B29D38BD727D0DDD1B514DEDB1BFB8B3** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(CharControlU5BU5D_t1BB82C75B29D38BD727D0DDD1B514DEDB1BFB8B3* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t37187EA3F5A7945D0B45C8A071194C1191FC32FE, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t37187EA3F5A7945D0B45C8A071194C1191FC32FE, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t37187EA3F5A7945D0B45C8A071194C1191FC32FE, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t37187EA3F5A7945D0B45C8A071194C1191FC32FE_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	CharControlU5BU5D_t1BB82C75B29D38BD727D0DDD1B514DEDB1BFB8B3* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t37187EA3F5A7945D0B45C8A071194C1191FC32FE_StaticFields, ____emptyArray_5)); }
	inline CharControlU5BU5D_t1BB82C75B29D38BD727D0DDD1B514DEDB1BFB8B3* get__emptyArray_5() const { return ____emptyArray_5; }
	inline CharControlU5BU5D_t1BB82C75B29D38BD727D0DDD1B514DEDB1BFB8B3** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(CharControlU5BU5D_t1BB82C75B29D38BD727D0DDD1B514DEDB1BFB8B3* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<CharsData>
struct List_1_t49AE3466419291B57C0EF7E386E193A7AFE92C02  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	CharsDataU5BU5D_t6E1A2B31C527A76C761C25915A588ED83C6B4CDE* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t49AE3466419291B57C0EF7E386E193A7AFE92C02, ____items_1)); }
	inline CharsDataU5BU5D_t6E1A2B31C527A76C761C25915A588ED83C6B4CDE* get__items_1() const { return ____items_1; }
	inline CharsDataU5BU5D_t6E1A2B31C527A76C761C25915A588ED83C6B4CDE** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(CharsDataU5BU5D_t6E1A2B31C527A76C761C25915A588ED83C6B4CDE* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t49AE3466419291B57C0EF7E386E193A7AFE92C02, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t49AE3466419291B57C0EF7E386E193A7AFE92C02, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t49AE3466419291B57C0EF7E386E193A7AFE92C02, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t49AE3466419291B57C0EF7E386E193A7AFE92C02_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	CharsDataU5BU5D_t6E1A2B31C527A76C761C25915A588ED83C6B4CDE* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t49AE3466419291B57C0EF7E386E193A7AFE92C02_StaticFields, ____emptyArray_5)); }
	inline CharsDataU5BU5D_t6E1A2B31C527A76C761C25915A588ED83C6B4CDE* get__emptyArray_5() const { return ____emptyArray_5; }
	inline CharsDataU5BU5D_t6E1A2B31C527A76C761C25915A588ED83C6B4CDE** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(CharsDataU5BU5D_t6E1A2B31C527A76C761C25915A588ED83C6B4CDE* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};

struct Il2CppArrayBounds;

// System.Array


// CharsContainer
struct CharsContainer_tB2FB3D6B21722D0168C892A2C3837608C80C7CC5  : public RuntimeObject
{
public:
	// CharsBundleData[] CharsContainer::_charBundles
	CharsBundleDataU5BU5D_t49861CE889FB0A7632E6FD7126DE29B0F470F700* ____charBundles_0;

public:
	inline static int32_t get_offset_of__charBundles_0() { return static_cast<int32_t>(offsetof(CharsContainer_tB2FB3D6B21722D0168C892A2C3837608C80C7CC5, ____charBundles_0)); }
	inline CharsBundleDataU5BU5D_t49861CE889FB0A7632E6FD7126DE29B0F470F700* get__charBundles_0() const { return ____charBundles_0; }
	inline CharsBundleDataU5BU5D_t49861CE889FB0A7632E6FD7126DE29B0F470F700** get_address_of__charBundles_0() { return &____charBundles_0; }
	inline void set__charBundles_0(CharsBundleDataU5BU5D_t49861CE889FB0A7632E6FD7126DE29B0F470F700* value)
	{
		____charBundles_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____charBundles_0), (void*)value);
	}
};


// CharsData
struct CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595  : public RuntimeObject
{
public:
	// System.String CharsData::_identifier
	String_t* ____identifier_0;
	// UnityEngine.Sprite CharsData::_sprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ____sprite_1;

public:
	inline static int32_t get_offset_of__identifier_0() { return static_cast<int32_t>(offsetof(CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595, ____identifier_0)); }
	inline String_t* get__identifier_0() const { return ____identifier_0; }
	inline String_t** get_address_of__identifier_0() { return &____identifier_0; }
	inline void set__identifier_0(String_t* value)
	{
		____identifier_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____identifier_0), (void*)value);
	}

	inline static int32_t get_offset_of__sprite_1() { return static_cast<int32_t>(offsetof(CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595, ____sprite_1)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get__sprite_1() const { return ____sprite_1; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of__sprite_1() { return &____sprite_1; }
	inline void set__sprite_1(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		____sprite_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____sprite_1), (void*)value);
	}
};


// DG.Tweening.DOTweenModulePhysics
struct DOTweenModulePhysics_tFCC295D4FA2826D2BA7BB5B6BB1A9B28ECCF4036  : public RuntimeObject
{
public:

public:
};


// DG.Tweening.DOTweenModuleUI
struct DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B  : public RuntimeObject
{
public:

public:
};


// DG.Tweening.DOTweenModuleUtils
struct DOTweenModuleUtils_t1924C98C4EB46411DA5B22524EB14EFDC0203EA0  : public RuntimeObject
{
public:

public:
};

struct DOTweenModuleUtils_t1924C98C4EB46411DA5B22524EB14EFDC0203EA0_StaticFields
{
public:
	// System.Boolean DG.Tweening.DOTweenModuleUtils::_initialized
	bool ____initialized_0;

public:
	inline static int32_t get_offset_of__initialized_0() { return static_cast<int32_t>(offsetof(DOTweenModuleUtils_t1924C98C4EB46411DA5B22524EB14EFDC0203EA0_StaticFields, ____initialized_0)); }
	inline bool get__initialized_0() const { return ____initialized_0; }
	inline bool* get_address_of__initialized_0() { return &____initialized_0; }
	inline void set__initialized_0(bool value)
	{
		____initialized_0 = value;
	}
};


// GlobalFunctions
struct GlobalFunctions_tD94FED27C51C8F79CFFA2F5C40C753773DDB20C6  : public RuntimeObject
{
public:

public:
};


// System.MarshalByRefObject
struct MarshalByRefObject_tD4DF91B488B284F899417EC468D8E50E933306A8  : public RuntimeObject
{
public:
	// System.Object System.MarshalByRefObject::_identity
	RuntimeObject * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_tD4DF91B488B284F899417EC468D8E50E933306A8, ____identity_0)); }
	inline RuntimeObject * get__identity_0() const { return ____identity_0; }
	inline RuntimeObject ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(RuntimeObject * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____identity_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tD4DF91B488B284F899417EC468D8E50E933306A8_marshaled_pinvoke
{
	Il2CppIUnknown* ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tD4DF91B488B284F899417EC468D8E50E933306A8_marshaled_com
{
	Il2CppIUnknown* ____identity_0;
};

// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.Random
struct Random_t6C9E9775A149D0ADCFEB4B252C408F03EE870118  : public RuntimeObject
{
public:
	// System.Int32 System.Random::inext
	int32_t ___inext_0;
	// System.Int32 System.Random::inextp
	int32_t ___inextp_1;
	// System.Int32[] System.Random::SeedArray
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___SeedArray_2;

public:
	inline static int32_t get_offset_of_inext_0() { return static_cast<int32_t>(offsetof(Random_t6C9E9775A149D0ADCFEB4B252C408F03EE870118, ___inext_0)); }
	inline int32_t get_inext_0() const { return ___inext_0; }
	inline int32_t* get_address_of_inext_0() { return &___inext_0; }
	inline void set_inext_0(int32_t value)
	{
		___inext_0 = value;
	}

	inline static int32_t get_offset_of_inextp_1() { return static_cast<int32_t>(offsetof(Random_t6C9E9775A149D0ADCFEB4B252C408F03EE870118, ___inextp_1)); }
	inline int32_t get_inextp_1() const { return ___inextp_1; }
	inline int32_t* get_address_of_inextp_1() { return &___inextp_1; }
	inline void set_inextp_1(int32_t value)
	{
		___inextp_1 = value;
	}

	inline static int32_t get_offset_of_SeedArray_2() { return static_cast<int32_t>(offsetof(Random_t6C9E9775A149D0ADCFEB4B252C408F03EE870118, ___SeedArray_2)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_SeedArray_2() const { return ___SeedArray_2; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_SeedArray_2() { return &___SeedArray_2; }
	inline void set_SeedArray_2(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___SeedArray_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___SeedArray_2), (void*)value);
	}
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// UnityEngine.Events.UnityEventBase
struct UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * ___m_PersistentCalls_1;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_2;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_Calls_0)); }
	inline InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Calls_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PersistentCalls_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_2() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_CallsDirty_2)); }
	inline bool get_m_CallsDirty_2() const { return ___m_CallsDirty_2; }
	inline bool* get_address_of_m_CallsDirty_2() { return &___m_CallsDirty_2; }
	inline void set_m_CallsDirty_2(bool value)
	{
		___m_CallsDirty_2 = value;
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_com
{
};

// CharControl/<>c
struct U3CU3Ec_t354A1590D8043D0E9E5A2724672CBF8E9E8F1AA4  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t354A1590D8043D0E9E5A2724672CBF8E9E8F1AA4_StaticFields
{
public:
	// CharControl/<>c CharControl/<>c::<>9
	U3CU3Ec_t354A1590D8043D0E9E5A2724672CBF8E9E8F1AA4 * ___U3CU3E9_0;
	// System.Func`2<CharControl,System.Boolean> CharControl/<>c::<>9__21_0
	Func_2_t20F4B130EF75554CF71A9826AA0AC83CCB3EEDAC * ___U3CU3E9__21_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t354A1590D8043D0E9E5A2724672CBF8E9E8F1AA4_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t354A1590D8043D0E9E5A2724672CBF8E9E8F1AA4 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t354A1590D8043D0E9E5A2724672CBF8E9E8F1AA4 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t354A1590D8043D0E9E5A2724672CBF8E9E8F1AA4 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__21_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t354A1590D8043D0E9E5A2724672CBF8E9E8F1AA4_StaticFields, ___U3CU3E9__21_0_1)); }
	inline Func_2_t20F4B130EF75554CF71A9826AA0AC83CCB3EEDAC * get_U3CU3E9__21_0_1() const { return ___U3CU3E9__21_0_1; }
	inline Func_2_t20F4B130EF75554CF71A9826AA0AC83CCB3EEDAC ** get_address_of_U3CU3E9__21_0_1() { return &___U3CU3E9__21_0_1; }
	inline void set_U3CU3E9__21_0_1(Func_2_t20F4B130EF75554CF71A9826AA0AC83CCB3EEDAC * value)
	{
		___U3CU3E9__21_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__21_0_1), (void*)value);
	}
};


// CharControl/<CheckRelevate>d__21
struct U3CCheckRelevateU3Ed__21_t1EC48447076186B1190E53CBB1A680B3ECA1E750  : public RuntimeObject
{
public:
	// System.Int32 CharControl/<CheckRelevate>d__21::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object CharControl/<CheckRelevate>d__21::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// CharControl CharControl/<CheckRelevate>d__21::<>4__this
	CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CCheckRelevateU3Ed__21_t1EC48447076186B1190E53CBB1A680B3ECA1E750, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CCheckRelevateU3Ed__21_t1EC48447076186B1190E53CBB1A680B3ECA1E750, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CCheckRelevateU3Ed__21_t1EC48447076186B1190E53CBB1A680B3ECA1E750, ___U3CU3E4__this_2)); }
	inline CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// CharsGenerator/<>c__DisplayClass14_0
struct U3CU3Ec__DisplayClass14_0_t79B660C3CA0F908E689E0E2E7221DF954F7E5E42  : public RuntimeObject
{
public:
	// CharsData CharsGenerator/<>c__DisplayClass14_0::data
	CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595 * ___data_0;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass14_0_t79B660C3CA0F908E689E0E2E7221DF954F7E5E42, ___data_0)); }
	inline CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595 * get_data_0() const { return ___data_0; }
	inline CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595 ** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595 * value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_0), (void*)value);
	}
};


// CharsGenerator/<InitializeEffect>d__18
struct U3CInitializeEffectU3Ed__18_tB5CE3179A9E4153E9DF3477D7479D251A763486C  : public RuntimeObject
{
public:
	// System.Int32 CharsGenerator/<InitializeEffect>d__18::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object CharsGenerator/<InitializeEffect>d__18::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// CharsGenerator CharsGenerator/<InitializeEffect>d__18::<>4__this
	CharsGenerator_t7E18FC7617D416B9E55B664B885A3138E43EE2A2 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CInitializeEffectU3Ed__18_tB5CE3179A9E4153E9DF3477D7479D251A763486C, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CInitializeEffectU3Ed__18_tB5CE3179A9E4153E9DF3477D7479D251A763486C, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CInitializeEffectU3Ed__18_tB5CE3179A9E4153E9DF3477D7479D251A763486C, ___U3CU3E4__this_2)); }
	inline CharsGenerator_t7E18FC7617D416B9E55B664B885A3138E43EE2A2 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline CharsGenerator_t7E18FC7617D416B9E55B664B885A3138E43EE2A2 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(CharsGenerator_t7E18FC7617D416B9E55B664B885A3138E43EE2A2 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass10_0
struct U3CU3Ec__DisplayClass10_0_t6B6DFEAD8FF1DD6A724C294AA0B120F82A7A6937  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass10_0::trans
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___trans_0;
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass10_0::target
	Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * ___target_1;

public:
	inline static int32_t get_offset_of_trans_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_0_t6B6DFEAD8FF1DD6A724C294AA0B120F82A7A6937, ___trans_0)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_trans_0() const { return ___trans_0; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_trans_0() { return &___trans_0; }
	inline void set_trans_0(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___trans_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___trans_0), (void*)value);
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_0_t6B6DFEAD8FF1DD6A724C294AA0B120F82A7A6937, ___target_1)); }
	inline Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * get_target_1() const { return ___target_1; }
	inline Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_1), (void*)value);
	}
};


// DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass9_0
struct U3CU3Ec__DisplayClass9_0_tB9266040A99BBE64FC737A175355345A13E86D21  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass9_0::target
	Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass9_0_tB9266040A99BBE64FC737A175355345A13E86D21, ___target_0)); }
	inline Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * get_target_0() const { return ___target_0; }
	inline Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass36_0
struct U3CU3Ec__DisplayClass36_0_tCE8E904A62CBD9F143B3D692CA532EFA7A6F777D  : public RuntimeObject
{
public:
	// UnityEngine.UI.Text DG.Tweening.DOTweenModuleUI/<>c__DisplayClass36_0::target
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass36_0_tCE8E904A62CBD9F143B3D692CA532EFA7A6F777D, ___target_0)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_target_0() const { return ___target_0; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass4_0
struct U3CU3Ec__DisplayClass4_0_t9CD30C09F13A8901FBD37CEEE628023A3AC88587  : public RuntimeObject
{
public:
	// UnityEngine.UI.Image DG.Tweening.DOTweenModuleUI/<>c__DisplayClass4_0::target
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t9CD30C09F13A8901FBD37CEEE628023A3AC88587, ___target_0)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_target_0() const { return ___target_0; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUtils/Physics
struct Physics_t379352FCCF26C01744720033D3784D7F57DE0D8F  : public RuntimeObject
{
public:

public:
};


// LoadingScreen/<LoadScreen>d__3
struct U3CLoadScreenU3Ed__3_tAB779AB619F104C740B1B46095B34AB1EA682770  : public RuntimeObject
{
public:
	// System.Int32 LoadingScreen/<LoadScreen>d__3::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object LoadingScreen/<LoadScreen>d__3::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// LoadingScreen LoadingScreen/<LoadScreen>d__3::<>4__this
	LoadingScreen_tD1CD7EA45A62C3A1A641D929E47F330644D353B2 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadScreenU3Ed__3_tAB779AB619F104C740B1B46095B34AB1EA682770, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadScreenU3Ed__3_tAB779AB619F104C740B1B46095B34AB1EA682770, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CLoadScreenU3Ed__3_tAB779AB619F104C740B1B46095B34AB1EA682770, ___U3CU3E4__this_2)); }
	inline LoadingScreen_tD1CD7EA45A62C3A1A641D929E47F330644D353B2 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline LoadingScreen_tD1CD7EA45A62C3A1A641D929E47F330644D353B2 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(LoadingScreen_tD1CD7EA45A62C3A1A641D929E47F330644D353B2 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// System.Collections.Generic.List`1/Enumerator<CharControl>
struct Enumerator_t9930E1A60798842A72D23D981BA4A2330BF36578 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t37187EA3F5A7945D0B45C8A071194C1191FC32FE * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t9930E1A60798842A72D23D981BA4A2330BF36578, ___list_0)); }
	inline List_1_t37187EA3F5A7945D0B45C8A071194C1191FC32FE * get_list_0() const { return ___list_0; }
	inline List_1_t37187EA3F5A7945D0B45C8A071194C1191FC32FE ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t37187EA3F5A7945D0B45C8A071194C1191FC32FE * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t9930E1A60798842A72D23D981BA4A2330BF36578, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t9930E1A60798842A72D23D981BA4A2330BF36578, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t9930E1A60798842A72D23D981BA4A2330BF36578, ___current_3)); }
	inline CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * get_current_3() const { return ___current_3; }
	inline CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// System.Collections.Generic.List`1/Enumerator<System.Object>
struct Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___list_0)); }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * get_list_0() const { return ___list_0; }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// UnityEngine.Color
struct Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// DG.Tweening.Plugins.Options.ColorOptions
struct ColorOptions_t25AF005F398643658A000DBAD00EFF82C944355A 
{
public:
	// System.Boolean DG.Tweening.Plugins.Options.ColorOptions::alphaOnly
	bool ___alphaOnly_0;

public:
	inline static int32_t get_offset_of_alphaOnly_0() { return static_cast<int32_t>(offsetof(ColorOptions_t25AF005F398643658A000DBAD00EFF82C944355A, ___alphaOnly_0)); }
	inline bool get_alphaOnly_0() const { return ___alphaOnly_0; }
	inline bool* get_address_of_alphaOnly_0() { return &___alphaOnly_0; }
	inline void set_alphaOnly_0(bool value)
	{
		___alphaOnly_0 = value;
	}
};

// Native definition for P/Invoke marshalling of DG.Tweening.Plugins.Options.ColorOptions
struct ColorOptions_t25AF005F398643658A000DBAD00EFF82C944355A_marshaled_pinvoke
{
	int32_t ___alphaOnly_0;
};
// Native definition for COM marshalling of DG.Tweening.Plugins.Options.ColorOptions
struct ColorOptions_t25AF005F398643658A000DBAD00EFF82C944355A_marshaled_com
{
	int32_t ___alphaOnly_0;
};

// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Reflection.MethodBase
struct MethodBase_t  : public MemberInfo_t
{
public:

public:
};


// UnityEngine.Quaternion
struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___identityQuaternion_4 = value;
	}
};


// System.Single
struct Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.UI.SpriteState
struct SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_SelectedSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_SelectedSprite_2;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_DisabledSprite_3;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_HighlightedSprite_0)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HighlightedSprite_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_PressedSprite_1)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PressedSprite_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectedSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_SelectedSprite_2)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_SelectedSprite_2() const { return ___m_SelectedSprite_2; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_SelectedSprite_2() { return &___m_SelectedSprite_2; }
	inline void set_m_SelectedSprite_2(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_SelectedSprite_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectedSprite_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_3() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_DisabledSprite_3)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_DisabledSprite_3() const { return ___m_DisabledSprite_3; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_DisabledSprite_3() { return &___m_DisabledSprite_3; }
	inline void set_m_DisabledSprite_3(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_DisabledSprite_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DisabledSprite_3), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E_marshaled_pinvoke
{
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_HighlightedSprite_0;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_PressedSprite_1;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_SelectedSprite_2;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_DisabledSprite_3;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E_marshaled_com
{
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_HighlightedSprite_0;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_PressedSprite_1;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_SelectedSprite_2;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_DisabledSprite_3;
};

// System.UInt16
struct UInt16_t894EA9D4FB7C799B244E7BBF2DF0EEEDBC77A8BD 
{
public:
	// System.UInt16 System.UInt16::m_value
	uint16_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt16_t894EA9D4FB7C799B244E7BBF2DF0EEEDBC77A8BD, ___m_value_0)); }
	inline uint16_t get_m_value_0() const { return ___m_value_0; }
	inline uint16_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint16_t value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.Events.UnityEvent
struct UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4  : public UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent::m_InvokeArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.Vector3
struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___zeroVector_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___oneVector_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___upVector_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___downVector_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___leftVector_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___rightVector_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___forwardVector_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___backVector_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// UnityEngine.Vector4
struct Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___zeroVector_5)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___oneVector_6)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___negativeInfinityVector_8 = value;
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// UnityEngine.WaitForSeconds
struct WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013  : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF
{
public:
	// System.Single UnityEngine.WaitForSeconds::m_Seconds
	float ___m_Seconds_0;

public:
	inline static int32_t get_offset_of_m_Seconds_0() { return static_cast<int32_t>(offsetof(WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013, ___m_Seconds_0)); }
	inline float get_m_Seconds_0() const { return ___m_Seconds_0; }
	inline float* get_address_of_m_Seconds_0() { return &___m_Seconds_0; }
	inline void set_m_Seconds_0(float value)
	{
		___m_Seconds_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_marshaled_pinvoke : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_pinvoke
{
	float ___m_Seconds_0;
};
// Native definition for COM marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_marshaled_com : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_com
{
	float ___m_Seconds_0;
};

// System.Nullable`1<UnityEngine.Vector3>
struct Nullable_1_t1829213F3538788DF79B4659AFC9D6A9C90C3258 
{
public:
	// T System.Nullable`1::value
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t1829213F3538788DF79B4659AFC9D6A9C90C3258, ___value_0)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_value_0() const { return ___value_0; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t1829213F3538788DF79B4659AFC9D6A9C90C3258, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};


// System.AppDomain
struct AppDomain_tBEB6322D51DCB12C09A56A49886C2D09BA1C1A8A  : public MarshalByRefObject_tD4DF91B488B284F899417EC468D8E50E933306A8
{
public:
	// System.IntPtr System.AppDomain::_mono_app_domain
	intptr_t ____mono_app_domain_1;
	// System.Object System.AppDomain::_evidence
	RuntimeObject * ____evidence_6;
	// System.Object System.AppDomain::_granted
	RuntimeObject * ____granted_7;
	// System.Int32 System.AppDomain::_principalPolicy
	int32_t ____principalPolicy_8;
	// System.AssemblyLoadEventHandler System.AppDomain::AssemblyLoad
	AssemblyLoadEventHandler_tE06B38463937F6FBCCECF4DF6519F83C1683FE0C * ___AssemblyLoad_11;
	// System.ResolveEventHandler System.AppDomain::AssemblyResolve
	ResolveEventHandler_tC6827B550D5B6553B57571630B1EE01AC12A1089 * ___AssemblyResolve_12;
	// System.EventHandler System.AppDomain::DomainUnload
	EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B * ___DomainUnload_13;
	// System.EventHandler System.AppDomain::ProcessExit
	EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B * ___ProcessExit_14;
	// System.ResolveEventHandler System.AppDomain::ResourceResolve
	ResolveEventHandler_tC6827B550D5B6553B57571630B1EE01AC12A1089 * ___ResourceResolve_15;
	// System.ResolveEventHandler System.AppDomain::TypeResolve
	ResolveEventHandler_tC6827B550D5B6553B57571630B1EE01AC12A1089 * ___TypeResolve_16;
	// System.UnhandledExceptionEventHandler System.AppDomain::UnhandledException
	UnhandledExceptionEventHandler_t1DF125A860ED9B70F24ADFA6CB0781533A23DA64 * ___UnhandledException_17;
	// System.EventHandler`1<System.Runtime.ExceptionServices.FirstChanceExceptionEventArgs> System.AppDomain::FirstChanceException
	EventHandler_1_t7F26BD2270AD4531F2328FD1382278E975249DF1 * ___FirstChanceException_18;
	// System.Object System.AppDomain::_domain_manager
	RuntimeObject * ____domain_manager_19;
	// System.ResolveEventHandler System.AppDomain::ReflectionOnlyAssemblyResolve
	ResolveEventHandler_tC6827B550D5B6553B57571630B1EE01AC12A1089 * ___ReflectionOnlyAssemblyResolve_20;
	// System.Object System.AppDomain::_activation
	RuntimeObject * ____activation_21;
	// System.Object System.AppDomain::_applicationIdentity
	RuntimeObject * ____applicationIdentity_22;
	// System.Collections.Generic.List`1<System.String> System.AppDomain::compatibility_switch
	List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * ___compatibility_switch_23;

public:
	inline static int32_t get_offset_of__mono_app_domain_1() { return static_cast<int32_t>(offsetof(AppDomain_tBEB6322D51DCB12C09A56A49886C2D09BA1C1A8A, ____mono_app_domain_1)); }
	inline intptr_t get__mono_app_domain_1() const { return ____mono_app_domain_1; }
	inline intptr_t* get_address_of__mono_app_domain_1() { return &____mono_app_domain_1; }
	inline void set__mono_app_domain_1(intptr_t value)
	{
		____mono_app_domain_1 = value;
	}

	inline static int32_t get_offset_of__evidence_6() { return static_cast<int32_t>(offsetof(AppDomain_tBEB6322D51DCB12C09A56A49886C2D09BA1C1A8A, ____evidence_6)); }
	inline RuntimeObject * get__evidence_6() const { return ____evidence_6; }
	inline RuntimeObject ** get_address_of__evidence_6() { return &____evidence_6; }
	inline void set__evidence_6(RuntimeObject * value)
	{
		____evidence_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____evidence_6), (void*)value);
	}

	inline static int32_t get_offset_of__granted_7() { return static_cast<int32_t>(offsetof(AppDomain_tBEB6322D51DCB12C09A56A49886C2D09BA1C1A8A, ____granted_7)); }
	inline RuntimeObject * get__granted_7() const { return ____granted_7; }
	inline RuntimeObject ** get_address_of__granted_7() { return &____granted_7; }
	inline void set__granted_7(RuntimeObject * value)
	{
		____granted_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____granted_7), (void*)value);
	}

	inline static int32_t get_offset_of__principalPolicy_8() { return static_cast<int32_t>(offsetof(AppDomain_tBEB6322D51DCB12C09A56A49886C2D09BA1C1A8A, ____principalPolicy_8)); }
	inline int32_t get__principalPolicy_8() const { return ____principalPolicy_8; }
	inline int32_t* get_address_of__principalPolicy_8() { return &____principalPolicy_8; }
	inline void set__principalPolicy_8(int32_t value)
	{
		____principalPolicy_8 = value;
	}

	inline static int32_t get_offset_of_AssemblyLoad_11() { return static_cast<int32_t>(offsetof(AppDomain_tBEB6322D51DCB12C09A56A49886C2D09BA1C1A8A, ___AssemblyLoad_11)); }
	inline AssemblyLoadEventHandler_tE06B38463937F6FBCCECF4DF6519F83C1683FE0C * get_AssemblyLoad_11() const { return ___AssemblyLoad_11; }
	inline AssemblyLoadEventHandler_tE06B38463937F6FBCCECF4DF6519F83C1683FE0C ** get_address_of_AssemblyLoad_11() { return &___AssemblyLoad_11; }
	inline void set_AssemblyLoad_11(AssemblyLoadEventHandler_tE06B38463937F6FBCCECF4DF6519F83C1683FE0C * value)
	{
		___AssemblyLoad_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___AssemblyLoad_11), (void*)value);
	}

	inline static int32_t get_offset_of_AssemblyResolve_12() { return static_cast<int32_t>(offsetof(AppDomain_tBEB6322D51DCB12C09A56A49886C2D09BA1C1A8A, ___AssemblyResolve_12)); }
	inline ResolveEventHandler_tC6827B550D5B6553B57571630B1EE01AC12A1089 * get_AssemblyResolve_12() const { return ___AssemblyResolve_12; }
	inline ResolveEventHandler_tC6827B550D5B6553B57571630B1EE01AC12A1089 ** get_address_of_AssemblyResolve_12() { return &___AssemblyResolve_12; }
	inline void set_AssemblyResolve_12(ResolveEventHandler_tC6827B550D5B6553B57571630B1EE01AC12A1089 * value)
	{
		___AssemblyResolve_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___AssemblyResolve_12), (void*)value);
	}

	inline static int32_t get_offset_of_DomainUnload_13() { return static_cast<int32_t>(offsetof(AppDomain_tBEB6322D51DCB12C09A56A49886C2D09BA1C1A8A, ___DomainUnload_13)); }
	inline EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B * get_DomainUnload_13() const { return ___DomainUnload_13; }
	inline EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B ** get_address_of_DomainUnload_13() { return &___DomainUnload_13; }
	inline void set_DomainUnload_13(EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B * value)
	{
		___DomainUnload_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DomainUnload_13), (void*)value);
	}

	inline static int32_t get_offset_of_ProcessExit_14() { return static_cast<int32_t>(offsetof(AppDomain_tBEB6322D51DCB12C09A56A49886C2D09BA1C1A8A, ___ProcessExit_14)); }
	inline EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B * get_ProcessExit_14() const { return ___ProcessExit_14; }
	inline EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B ** get_address_of_ProcessExit_14() { return &___ProcessExit_14; }
	inline void set_ProcessExit_14(EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B * value)
	{
		___ProcessExit_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ProcessExit_14), (void*)value);
	}

	inline static int32_t get_offset_of_ResourceResolve_15() { return static_cast<int32_t>(offsetof(AppDomain_tBEB6322D51DCB12C09A56A49886C2D09BA1C1A8A, ___ResourceResolve_15)); }
	inline ResolveEventHandler_tC6827B550D5B6553B57571630B1EE01AC12A1089 * get_ResourceResolve_15() const { return ___ResourceResolve_15; }
	inline ResolveEventHandler_tC6827B550D5B6553B57571630B1EE01AC12A1089 ** get_address_of_ResourceResolve_15() { return &___ResourceResolve_15; }
	inline void set_ResourceResolve_15(ResolveEventHandler_tC6827B550D5B6553B57571630B1EE01AC12A1089 * value)
	{
		___ResourceResolve_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ResourceResolve_15), (void*)value);
	}

	inline static int32_t get_offset_of_TypeResolve_16() { return static_cast<int32_t>(offsetof(AppDomain_tBEB6322D51DCB12C09A56A49886C2D09BA1C1A8A, ___TypeResolve_16)); }
	inline ResolveEventHandler_tC6827B550D5B6553B57571630B1EE01AC12A1089 * get_TypeResolve_16() const { return ___TypeResolve_16; }
	inline ResolveEventHandler_tC6827B550D5B6553B57571630B1EE01AC12A1089 ** get_address_of_TypeResolve_16() { return &___TypeResolve_16; }
	inline void set_TypeResolve_16(ResolveEventHandler_tC6827B550D5B6553B57571630B1EE01AC12A1089 * value)
	{
		___TypeResolve_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TypeResolve_16), (void*)value);
	}

	inline static int32_t get_offset_of_UnhandledException_17() { return static_cast<int32_t>(offsetof(AppDomain_tBEB6322D51DCB12C09A56A49886C2D09BA1C1A8A, ___UnhandledException_17)); }
	inline UnhandledExceptionEventHandler_t1DF125A860ED9B70F24ADFA6CB0781533A23DA64 * get_UnhandledException_17() const { return ___UnhandledException_17; }
	inline UnhandledExceptionEventHandler_t1DF125A860ED9B70F24ADFA6CB0781533A23DA64 ** get_address_of_UnhandledException_17() { return &___UnhandledException_17; }
	inline void set_UnhandledException_17(UnhandledExceptionEventHandler_t1DF125A860ED9B70F24ADFA6CB0781533A23DA64 * value)
	{
		___UnhandledException_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UnhandledException_17), (void*)value);
	}

	inline static int32_t get_offset_of_FirstChanceException_18() { return static_cast<int32_t>(offsetof(AppDomain_tBEB6322D51DCB12C09A56A49886C2D09BA1C1A8A, ___FirstChanceException_18)); }
	inline EventHandler_1_t7F26BD2270AD4531F2328FD1382278E975249DF1 * get_FirstChanceException_18() const { return ___FirstChanceException_18; }
	inline EventHandler_1_t7F26BD2270AD4531F2328FD1382278E975249DF1 ** get_address_of_FirstChanceException_18() { return &___FirstChanceException_18; }
	inline void set_FirstChanceException_18(EventHandler_1_t7F26BD2270AD4531F2328FD1382278E975249DF1 * value)
	{
		___FirstChanceException_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FirstChanceException_18), (void*)value);
	}

	inline static int32_t get_offset_of__domain_manager_19() { return static_cast<int32_t>(offsetof(AppDomain_tBEB6322D51DCB12C09A56A49886C2D09BA1C1A8A, ____domain_manager_19)); }
	inline RuntimeObject * get__domain_manager_19() const { return ____domain_manager_19; }
	inline RuntimeObject ** get_address_of__domain_manager_19() { return &____domain_manager_19; }
	inline void set__domain_manager_19(RuntimeObject * value)
	{
		____domain_manager_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____domain_manager_19), (void*)value);
	}

	inline static int32_t get_offset_of_ReflectionOnlyAssemblyResolve_20() { return static_cast<int32_t>(offsetof(AppDomain_tBEB6322D51DCB12C09A56A49886C2D09BA1C1A8A, ___ReflectionOnlyAssemblyResolve_20)); }
	inline ResolveEventHandler_tC6827B550D5B6553B57571630B1EE01AC12A1089 * get_ReflectionOnlyAssemblyResolve_20() const { return ___ReflectionOnlyAssemblyResolve_20; }
	inline ResolveEventHandler_tC6827B550D5B6553B57571630B1EE01AC12A1089 ** get_address_of_ReflectionOnlyAssemblyResolve_20() { return &___ReflectionOnlyAssemblyResolve_20; }
	inline void set_ReflectionOnlyAssemblyResolve_20(ResolveEventHandler_tC6827B550D5B6553B57571630B1EE01AC12A1089 * value)
	{
		___ReflectionOnlyAssemblyResolve_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ReflectionOnlyAssemblyResolve_20), (void*)value);
	}

	inline static int32_t get_offset_of__activation_21() { return static_cast<int32_t>(offsetof(AppDomain_tBEB6322D51DCB12C09A56A49886C2D09BA1C1A8A, ____activation_21)); }
	inline RuntimeObject * get__activation_21() const { return ____activation_21; }
	inline RuntimeObject ** get_address_of__activation_21() { return &____activation_21; }
	inline void set__activation_21(RuntimeObject * value)
	{
		____activation_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____activation_21), (void*)value);
	}

	inline static int32_t get_offset_of__applicationIdentity_22() { return static_cast<int32_t>(offsetof(AppDomain_tBEB6322D51DCB12C09A56A49886C2D09BA1C1A8A, ____applicationIdentity_22)); }
	inline RuntimeObject * get__applicationIdentity_22() const { return ____applicationIdentity_22; }
	inline RuntimeObject ** get_address_of__applicationIdentity_22() { return &____applicationIdentity_22; }
	inline void set__applicationIdentity_22(RuntimeObject * value)
	{
		____applicationIdentity_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____applicationIdentity_22), (void*)value);
	}

	inline static int32_t get_offset_of_compatibility_switch_23() { return static_cast<int32_t>(offsetof(AppDomain_tBEB6322D51DCB12C09A56A49886C2D09BA1C1A8A, ___compatibility_switch_23)); }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * get_compatibility_switch_23() const { return ___compatibility_switch_23; }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 ** get_address_of_compatibility_switch_23() { return &___compatibility_switch_23; }
	inline void set_compatibility_switch_23(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * value)
	{
		___compatibility_switch_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___compatibility_switch_23), (void*)value);
	}
};

struct AppDomain_tBEB6322D51DCB12C09A56A49886C2D09BA1C1A8A_StaticFields
{
public:
	// System.String System.AppDomain::_process_guid
	String_t* ____process_guid_2;
	// System.AppDomain System.AppDomain::default_domain
	AppDomain_tBEB6322D51DCB12C09A56A49886C2D09BA1C1A8A * ___default_domain_10;

public:
	inline static int32_t get_offset_of__process_guid_2() { return static_cast<int32_t>(offsetof(AppDomain_tBEB6322D51DCB12C09A56A49886C2D09BA1C1A8A_StaticFields, ____process_guid_2)); }
	inline String_t* get__process_guid_2() const { return ____process_guid_2; }
	inline String_t** get_address_of__process_guid_2() { return &____process_guid_2; }
	inline void set__process_guid_2(String_t* value)
	{
		____process_guid_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____process_guid_2), (void*)value);
	}

	inline static int32_t get_offset_of_default_domain_10() { return static_cast<int32_t>(offsetof(AppDomain_tBEB6322D51DCB12C09A56A49886C2D09BA1C1A8A_StaticFields, ___default_domain_10)); }
	inline AppDomain_tBEB6322D51DCB12C09A56A49886C2D09BA1C1A8A * get_default_domain_10() const { return ___default_domain_10; }
	inline AppDomain_tBEB6322D51DCB12C09A56A49886C2D09BA1C1A8A ** get_address_of_default_domain_10() { return &___default_domain_10; }
	inline void set_default_domain_10(AppDomain_tBEB6322D51DCB12C09A56A49886C2D09BA1C1A8A * value)
	{
		___default_domain_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___default_domain_10), (void*)value);
	}
};

struct AppDomain_tBEB6322D51DCB12C09A56A49886C2D09BA1C1A8A_ThreadStaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> System.AppDomain::type_resolve_in_progress
	Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * ___type_resolve_in_progress_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> System.AppDomain::assembly_resolve_in_progress
	Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * ___assembly_resolve_in_progress_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> System.AppDomain::assembly_resolve_in_progress_refonly
	Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * ___assembly_resolve_in_progress_refonly_5;
	// System.Object System.AppDomain::_principal
	RuntimeObject * ____principal_9;

public:
	inline static int32_t get_offset_of_type_resolve_in_progress_3() { return static_cast<int32_t>(offsetof(AppDomain_tBEB6322D51DCB12C09A56A49886C2D09BA1C1A8A_ThreadStaticFields, ___type_resolve_in_progress_3)); }
	inline Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * get_type_resolve_in_progress_3() const { return ___type_resolve_in_progress_3; }
	inline Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 ** get_address_of_type_resolve_in_progress_3() { return &___type_resolve_in_progress_3; }
	inline void set_type_resolve_in_progress_3(Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * value)
	{
		___type_resolve_in_progress_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___type_resolve_in_progress_3), (void*)value);
	}

	inline static int32_t get_offset_of_assembly_resolve_in_progress_4() { return static_cast<int32_t>(offsetof(AppDomain_tBEB6322D51DCB12C09A56A49886C2D09BA1C1A8A_ThreadStaticFields, ___assembly_resolve_in_progress_4)); }
	inline Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * get_assembly_resolve_in_progress_4() const { return ___assembly_resolve_in_progress_4; }
	inline Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 ** get_address_of_assembly_resolve_in_progress_4() { return &___assembly_resolve_in_progress_4; }
	inline void set_assembly_resolve_in_progress_4(Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * value)
	{
		___assembly_resolve_in_progress_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___assembly_resolve_in_progress_4), (void*)value);
	}

	inline static int32_t get_offset_of_assembly_resolve_in_progress_refonly_5() { return static_cast<int32_t>(offsetof(AppDomain_tBEB6322D51DCB12C09A56A49886C2D09BA1C1A8A_ThreadStaticFields, ___assembly_resolve_in_progress_refonly_5)); }
	inline Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * get_assembly_resolve_in_progress_refonly_5() const { return ___assembly_resolve_in_progress_refonly_5; }
	inline Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 ** get_address_of_assembly_resolve_in_progress_refonly_5() { return &___assembly_resolve_in_progress_refonly_5; }
	inline void set_assembly_resolve_in_progress_refonly_5(Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * value)
	{
		___assembly_resolve_in_progress_refonly_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___assembly_resolve_in_progress_refonly_5), (void*)value);
	}

	inline static int32_t get_offset_of__principal_9() { return static_cast<int32_t>(offsetof(AppDomain_tBEB6322D51DCB12C09A56A49886C2D09BA1C1A8A_ThreadStaticFields, ____principal_9)); }
	inline RuntimeObject * get__principal_9() const { return ____principal_9; }
	inline RuntimeObject ** get_address_of__principal_9() { return &____principal_9; }
	inline void set__principal_9(RuntimeObject * value)
	{
		____principal_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____principal_9), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.AppDomain
struct AppDomain_tBEB6322D51DCB12C09A56A49886C2D09BA1C1A8A_marshaled_pinvoke : public MarshalByRefObject_tD4DF91B488B284F899417EC468D8E50E933306A8_marshaled_pinvoke
{
	intptr_t ____mono_app_domain_1;
	Il2CppIUnknown* ____evidence_6;
	Il2CppIUnknown* ____granted_7;
	int32_t ____principalPolicy_8;
	Il2CppMethodPointer ___AssemblyLoad_11;
	Il2CppMethodPointer ___AssemblyResolve_12;
	Il2CppMethodPointer ___DomainUnload_13;
	Il2CppMethodPointer ___ProcessExit_14;
	Il2CppMethodPointer ___ResourceResolve_15;
	Il2CppMethodPointer ___TypeResolve_16;
	Il2CppMethodPointer ___UnhandledException_17;
	Il2CppMethodPointer ___FirstChanceException_18;
	Il2CppIUnknown* ____domain_manager_19;
	Il2CppMethodPointer ___ReflectionOnlyAssemblyResolve_20;
	Il2CppIUnknown* ____activation_21;
	Il2CppIUnknown* ____applicationIdentity_22;
	List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * ___compatibility_switch_23;
};
// Native definition for COM marshalling of System.AppDomain
struct AppDomain_tBEB6322D51DCB12C09A56A49886C2D09BA1C1A8A_marshaled_com : public MarshalByRefObject_tD4DF91B488B284F899417EC468D8E50E933306A8_marshaled_com
{
	intptr_t ____mono_app_domain_1;
	Il2CppIUnknown* ____evidence_6;
	Il2CppIUnknown* ____granted_7;
	int32_t ____principalPolicy_8;
	Il2CppMethodPointer ___AssemblyLoad_11;
	Il2CppMethodPointer ___AssemblyResolve_12;
	Il2CppMethodPointer ___DomainUnload_13;
	Il2CppMethodPointer ___ProcessExit_14;
	Il2CppMethodPointer ___ResourceResolve_15;
	Il2CppMethodPointer ___TypeResolve_16;
	Il2CppMethodPointer ___UnhandledException_17;
	Il2CppMethodPointer ___FirstChanceException_18;
	Il2CppIUnknown* ____domain_manager_19;
	Il2CppMethodPointer ___ReflectionOnlyAssemblyResolve_20;
	Il2CppIUnknown* ____activation_21;
	Il2CppIUnknown* ____applicationIdentity_22;
	List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * ___compatibility_switch_23;
};

// System.Reflection.Assembly
struct Assembly_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Reflection.Assembly::_mono_assembly
	intptr_t ____mono_assembly_0;
	// System.Reflection.Assembly/ResolveEventHolder System.Reflection.Assembly::resolve_event_holder
	ResolveEventHolder_tA37081FAEBE21D83D216225B4489BA8A37B4E13C * ___resolve_event_holder_1;
	// System.Object System.Reflection.Assembly::_evidence
	RuntimeObject * ____evidence_2;
	// System.Object System.Reflection.Assembly::_minimum
	RuntimeObject * ____minimum_3;
	// System.Object System.Reflection.Assembly::_optional
	RuntimeObject * ____optional_4;
	// System.Object System.Reflection.Assembly::_refuse
	RuntimeObject * ____refuse_5;
	// System.Object System.Reflection.Assembly::_granted
	RuntimeObject * ____granted_6;
	// System.Object System.Reflection.Assembly::_denied
	RuntimeObject * ____denied_7;
	// System.Boolean System.Reflection.Assembly::fromByteArray
	bool ___fromByteArray_8;
	// System.String System.Reflection.Assembly::assemblyName
	String_t* ___assemblyName_9;

public:
	inline static int32_t get_offset_of__mono_assembly_0() { return static_cast<int32_t>(offsetof(Assembly_t, ____mono_assembly_0)); }
	inline intptr_t get__mono_assembly_0() const { return ____mono_assembly_0; }
	inline intptr_t* get_address_of__mono_assembly_0() { return &____mono_assembly_0; }
	inline void set__mono_assembly_0(intptr_t value)
	{
		____mono_assembly_0 = value;
	}

	inline static int32_t get_offset_of_resolve_event_holder_1() { return static_cast<int32_t>(offsetof(Assembly_t, ___resolve_event_holder_1)); }
	inline ResolveEventHolder_tA37081FAEBE21D83D216225B4489BA8A37B4E13C * get_resolve_event_holder_1() const { return ___resolve_event_holder_1; }
	inline ResolveEventHolder_tA37081FAEBE21D83D216225B4489BA8A37B4E13C ** get_address_of_resolve_event_holder_1() { return &___resolve_event_holder_1; }
	inline void set_resolve_event_holder_1(ResolveEventHolder_tA37081FAEBE21D83D216225B4489BA8A37B4E13C * value)
	{
		___resolve_event_holder_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___resolve_event_holder_1), (void*)value);
	}

	inline static int32_t get_offset_of__evidence_2() { return static_cast<int32_t>(offsetof(Assembly_t, ____evidence_2)); }
	inline RuntimeObject * get__evidence_2() const { return ____evidence_2; }
	inline RuntimeObject ** get_address_of__evidence_2() { return &____evidence_2; }
	inline void set__evidence_2(RuntimeObject * value)
	{
		____evidence_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____evidence_2), (void*)value);
	}

	inline static int32_t get_offset_of__minimum_3() { return static_cast<int32_t>(offsetof(Assembly_t, ____minimum_3)); }
	inline RuntimeObject * get__minimum_3() const { return ____minimum_3; }
	inline RuntimeObject ** get_address_of__minimum_3() { return &____minimum_3; }
	inline void set__minimum_3(RuntimeObject * value)
	{
		____minimum_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____minimum_3), (void*)value);
	}

	inline static int32_t get_offset_of__optional_4() { return static_cast<int32_t>(offsetof(Assembly_t, ____optional_4)); }
	inline RuntimeObject * get__optional_4() const { return ____optional_4; }
	inline RuntimeObject ** get_address_of__optional_4() { return &____optional_4; }
	inline void set__optional_4(RuntimeObject * value)
	{
		____optional_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____optional_4), (void*)value);
	}

	inline static int32_t get_offset_of__refuse_5() { return static_cast<int32_t>(offsetof(Assembly_t, ____refuse_5)); }
	inline RuntimeObject * get__refuse_5() const { return ____refuse_5; }
	inline RuntimeObject ** get_address_of__refuse_5() { return &____refuse_5; }
	inline void set__refuse_5(RuntimeObject * value)
	{
		____refuse_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____refuse_5), (void*)value);
	}

	inline static int32_t get_offset_of__granted_6() { return static_cast<int32_t>(offsetof(Assembly_t, ____granted_6)); }
	inline RuntimeObject * get__granted_6() const { return ____granted_6; }
	inline RuntimeObject ** get_address_of__granted_6() { return &____granted_6; }
	inline void set__granted_6(RuntimeObject * value)
	{
		____granted_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____granted_6), (void*)value);
	}

	inline static int32_t get_offset_of__denied_7() { return static_cast<int32_t>(offsetof(Assembly_t, ____denied_7)); }
	inline RuntimeObject * get__denied_7() const { return ____denied_7; }
	inline RuntimeObject ** get_address_of__denied_7() { return &____denied_7; }
	inline void set__denied_7(RuntimeObject * value)
	{
		____denied_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____denied_7), (void*)value);
	}

	inline static int32_t get_offset_of_fromByteArray_8() { return static_cast<int32_t>(offsetof(Assembly_t, ___fromByteArray_8)); }
	inline bool get_fromByteArray_8() const { return ___fromByteArray_8; }
	inline bool* get_address_of_fromByteArray_8() { return &___fromByteArray_8; }
	inline void set_fromByteArray_8(bool value)
	{
		___fromByteArray_8 = value;
	}

	inline static int32_t get_offset_of_assemblyName_9() { return static_cast<int32_t>(offsetof(Assembly_t, ___assemblyName_9)); }
	inline String_t* get_assemblyName_9() const { return ___assemblyName_9; }
	inline String_t** get_address_of_assemblyName_9() { return &___assemblyName_9; }
	inline void set_assemblyName_9(String_t* value)
	{
		___assemblyName_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___assemblyName_9), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Reflection.Assembly
struct Assembly_t_marshaled_pinvoke
{
	intptr_t ____mono_assembly_0;
	ResolveEventHolder_tA37081FAEBE21D83D216225B4489BA8A37B4E13C * ___resolve_event_holder_1;
	Il2CppIUnknown* ____evidence_2;
	Il2CppIUnknown* ____minimum_3;
	Il2CppIUnknown* ____optional_4;
	Il2CppIUnknown* ____refuse_5;
	Il2CppIUnknown* ____granted_6;
	Il2CppIUnknown* ____denied_7;
	int32_t ___fromByteArray_8;
	char* ___assemblyName_9;
};
// Native definition for COM marshalling of System.Reflection.Assembly
struct Assembly_t_marshaled_com
{
	intptr_t ____mono_assembly_0;
	ResolveEventHolder_tA37081FAEBE21D83D216225B4489BA8A37B4E13C * ___resolve_event_holder_1;
	Il2CppIUnknown* ____evidence_2;
	Il2CppIUnknown* ____minimum_3;
	Il2CppIUnknown* ____optional_4;
	Il2CppIUnknown* ____refuse_5;
	Il2CppIUnknown* ____granted_6;
	Il2CppIUnknown* ____denied_7;
	int32_t ___fromByteArray_8;
	Il2CppChar* ___assemblyName_9;
};

// DG.Tweening.AxisConstraint
struct AxisConstraint_tA0D384964013674923F26C7DF2618FB76CD3D7F4 
{
public:
	// System.Int32 DG.Tweening.AxisConstraint::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisConstraint_tA0D384964013674923F26C7DF2618FB76CD3D7F4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.ColorBlock
struct ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_SelectedColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_SelectedColor_3;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_DisabledColor_4;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_5;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_6;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_NormalColor_0)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_HighlightedColor_1)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_PressedColor_2)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_SelectedColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_SelectedColor_3)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_SelectedColor_3() const { return ___m_SelectedColor_3; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_SelectedColor_3() { return &___m_SelectedColor_3; }
	inline void set_m_SelectedColor_3(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_SelectedColor_3 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_4() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_DisabledColor_4)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_DisabledColor_4() const { return ___m_DisabledColor_4; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_DisabledColor_4() { return &___m_DisabledColor_4; }
	inline void set_m_DisabledColor_4(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_DisabledColor_4 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_5() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_ColorMultiplier_5)); }
	inline float get_m_ColorMultiplier_5() const { return ___m_ColorMultiplier_5; }
	inline float* get_address_of_m_ColorMultiplier_5() { return &___m_ColorMultiplier_5; }
	inline void set_m_ColorMultiplier_5(float value)
	{
		___m_ColorMultiplier_5 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_6() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_FadeDuration_6)); }
	inline float get_m_FadeDuration_6() const { return ___m_FadeDuration_6; }
	inline float* get_address_of_m_FadeDuration_6() { return &___m_FadeDuration_6; }
	inline void set_m_FadeDuration_6(float value)
	{
		___m_FadeDuration_6 = value;
	}
};

struct ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955_StaticFields
{
public:
	// UnityEngine.UI.ColorBlock UnityEngine.UI.ColorBlock::defaultColorBlock
	ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  ___defaultColorBlock_7;

public:
	inline static int32_t get_offset_of_defaultColorBlock_7() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955_StaticFields, ___defaultColorBlock_7)); }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  get_defaultColorBlock_7() const { return ___defaultColorBlock_7; }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955 * get_address_of_defaultColorBlock_7() { return &___defaultColorBlock_7; }
	inline void set_defaultColorBlock_7(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  value)
	{
		___defaultColorBlock_7 = value;
	}
};


// UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7  : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF
{
public:
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7_marshaled_pinvoke : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7_marshaled_com : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// System.Delegate
struct Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * get_data_9() const { return ___data_9; }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};

// DG.Tweening.Ease
struct Ease_tB04D625230DDC5B40D74E825C8A9DBBE37A146B4 
{
public:
	// System.Int32 DG.Tweening.Ease::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Ease_tB04D625230DDC5B40D74E825C8A9DBBE37A146B4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DG.Tweening.LoopType
struct LoopType_tF807A5805F6A83F5228BE7D4E633B2572B1B859A 
{
public:
	// System.Int32 DG.Tweening.LoopType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LoopType_tF807A5805F6A83F5228BE7D4E633B2572B1B859A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Reflection.MethodInfo
struct MethodInfo_t  : public MethodBase_t
{
public:

public:
};


// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// DG.Tweening.Plugins.Options.OrientType
struct OrientType_t98C6DBF0A80C2A938CA315BCE990CF0201D52886 
{
public:
	// System.Int32 DG.Tweening.Plugins.Options.OrientType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(OrientType_t98C6DBF0A80C2A938CA315BCE990CF0201D52886, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DG.Tweening.PathMode
struct PathMode_tC3536FD34C73F94ADFB5C7DC9415985E353CE572 
{
public:
	// System.Int32 DG.Tweening.PathMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PathMode_tC3536FD34C73F94ADFB5C7DC9415985E353CE572, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DG.Tweening.PathType
struct PathType_tBA0D4391850F4868EE61BFE9579098DD42D02899 
{
public:
	// System.Int32 DG.Tweening.PathType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PathType_tBA0D4391850F4868EE61BFE9579098DD42D02899, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// DG.Tweening.Core.Enums.SpecialStartupMode
struct SpecialStartupMode_t0086D2AE798C217210762DD17C8D3572414ACD92 
{
public:
	// System.Int32 DG.Tweening.Core.Enums.SpecialStartupMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SpecialStartupMode_t0086D2AE798C217210762DD17C8D3572414ACD92, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DG.Tweening.TweenType
struct TweenType_tAB2DEC1268409EA172594368494218E51696EF5D 
{
public:
	// System.Int32 DG.Tweening.TweenType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TweenType_tAB2DEC1268409EA172594368494218E51696EF5D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DG.Tweening.UpdateType
struct UpdateType_t9D838506DD148F29E6183FB298E41921E51CC5ED 
{
public:
	// System.Int32 DG.Tweening.UpdateType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UpdateType_t9D838506DD148F29E6183FB298E41921E51CC5ED, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Button/ButtonClickedEvent
struct ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F  : public UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4
{
public:

public:
};


// UnityEngine.UI.Image/FillMethod
struct FillMethod_tC37E5898D113A8FBF25A6AB6FBA451CC51E211E2 
{
public:
	// System.Int32 UnityEngine.UI.Image/FillMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FillMethod_tC37E5898D113A8FBF25A6AB6FBA451CC51E211E2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Image/Type
struct Type_tDCB08AB7425CAB70C1E46CC341F877423B5A5E12 
{
public:
	// System.Int32 UnityEngine.UI.Image/Type::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Type_tDCB08AB7425CAB70C1E46CC341F877423B5A5E12, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Navigation/Mode
struct Mode_t3113FDF05158BBA1DFC78D7F69E4C1D25135CB0F 
{
public:
	// System.Int32 UnityEngine.UI.Navigation/Mode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Mode_t3113FDF05158BBA1DFC78D7F69E4C1D25135CB0F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Selectable/Transition
struct Transition_t1FC449676815A798E758D32E8BE6DC0A2511DF14 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/Transition::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Transition_t1FC449676815A798E758D32E8BE6DC0A2511DF14, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DG.Tweening.Core.ABSSequentiable
struct ABSSequentiable_t74F7F9AFFD39EE435A16A3E202A7872B46DA6C76  : public RuntimeObject
{
public:
	// DG.Tweening.TweenType DG.Tweening.Core.ABSSequentiable::tweenType
	int32_t ___tweenType_0;
	// System.Single DG.Tweening.Core.ABSSequentiable::sequencedPosition
	float ___sequencedPosition_1;
	// System.Single DG.Tweening.Core.ABSSequentiable::sequencedEndPosition
	float ___sequencedEndPosition_2;
	// DG.Tweening.TweenCallback DG.Tweening.Core.ABSSequentiable::onStart
	TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * ___onStart_3;

public:
	inline static int32_t get_offset_of_tweenType_0() { return static_cast<int32_t>(offsetof(ABSSequentiable_t74F7F9AFFD39EE435A16A3E202A7872B46DA6C76, ___tweenType_0)); }
	inline int32_t get_tweenType_0() const { return ___tweenType_0; }
	inline int32_t* get_address_of_tweenType_0() { return &___tweenType_0; }
	inline void set_tweenType_0(int32_t value)
	{
		___tweenType_0 = value;
	}

	inline static int32_t get_offset_of_sequencedPosition_1() { return static_cast<int32_t>(offsetof(ABSSequentiable_t74F7F9AFFD39EE435A16A3E202A7872B46DA6C76, ___sequencedPosition_1)); }
	inline float get_sequencedPosition_1() const { return ___sequencedPosition_1; }
	inline float* get_address_of_sequencedPosition_1() { return &___sequencedPosition_1; }
	inline void set_sequencedPosition_1(float value)
	{
		___sequencedPosition_1 = value;
	}

	inline static int32_t get_offset_of_sequencedEndPosition_2() { return static_cast<int32_t>(offsetof(ABSSequentiable_t74F7F9AFFD39EE435A16A3E202A7872B46DA6C76, ___sequencedEndPosition_2)); }
	inline float get_sequencedEndPosition_2() const { return ___sequencedEndPosition_2; }
	inline float* get_address_of_sequencedEndPosition_2() { return &___sequencedEndPosition_2; }
	inline void set_sequencedEndPosition_2(float value)
	{
		___sequencedEndPosition_2 = value;
	}

	inline static int32_t get_offset_of_onStart_3() { return static_cast<int32_t>(offsetof(ABSSequentiable_t74F7F9AFFD39EE435A16A3E202A7872B46DA6C76, ___onStart_3)); }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * get_onStart_3() const { return ___onStart_3; }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB ** get_address_of_onStart_3() { return &___onStart_3; }
	inline void set_onStart_3(TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * value)
	{
		___onStart_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onStart_3), (void*)value);
	}
};


// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// UnityEngine.UI.Navigation
struct Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A 
{
public:
	// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// System.Boolean UnityEngine.UI.Navigation::m_WrapAround
	bool ___m_WrapAround_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnUp_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnDown_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnLeft_4;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnRight_5;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_WrapAround_1() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_WrapAround_1)); }
	inline bool get_m_WrapAround_1() const { return ___m_WrapAround_1; }
	inline bool* get_address_of_m_WrapAround_1() { return &___m_WrapAround_1; }
	inline void set_m_WrapAround_1(bool value)
	{
		___m_WrapAround_1 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_2() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnUp_2)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnUp_2() const { return ___m_SelectOnUp_2; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnUp_2() { return &___m_SelectOnUp_2; }
	inline void set_m_SelectOnUp_2(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnUp_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnUp_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_3() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnDown_3)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnDown_3() const { return ___m_SelectOnDown_3; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnDown_3() { return &___m_SelectOnDown_3; }
	inline void set_m_SelectOnDown_3(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnDown_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnDown_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_4() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnLeft_4)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnLeft_4() const { return ___m_SelectOnLeft_4; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnLeft_4() { return &___m_SelectOnLeft_4; }
	inline void set_m_SelectOnLeft_4(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnLeft_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnLeft_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_5() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnRight_5)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnRight_5() const { return ___m_SelectOnRight_5; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnRight_5() { return &___m_SelectOnRight_5; }
	inline void set_m_SelectOnRight_5(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnRight_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnRight_5), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	int32_t ___m_WrapAround_1;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnUp_2;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnDown_3;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnLeft_4;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnRight_5;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A_marshaled_com
{
	int32_t ___m_Mode_0;
	int32_t ___m_WrapAround_1;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnUp_2;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnDown_3;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnLeft_4;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnRight_5;
};

// DG.Tweening.Plugins.Core.PathCore.Path
struct Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5  : public RuntimeObject
{
public:
	// System.Single[] DG.Tweening.Plugins.Core.PathCore.Path::wpLengths
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___wpLengths_0;
	// UnityEngine.Vector3[] DG.Tweening.Plugins.Core.PathCore.Path::wps
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___wps_1;
	// DG.Tweening.PathType DG.Tweening.Plugins.Core.PathCore.Path::type
	int32_t ___type_2;
	// System.Int32 DG.Tweening.Plugins.Core.PathCore.Path::subdivisionsXSegment
	int32_t ___subdivisionsXSegment_3;
	// System.Int32 DG.Tweening.Plugins.Core.PathCore.Path::subdivisions
	int32_t ___subdivisions_4;
	// DG.Tweening.Plugins.Core.PathCore.ControlPoint[] DG.Tweening.Plugins.Core.PathCore.Path::controlPoints
	ControlPointU5BU5D_t9F61665C9F79DD5715B9C6C357BC1A9F16326884* ___controlPoints_5;
	// System.Single DG.Tweening.Plugins.Core.PathCore.Path::length
	float ___length_6;
	// System.Boolean DG.Tweening.Plugins.Core.PathCore.Path::isFinalized
	bool ___isFinalized_7;
	// System.Single[] DG.Tweening.Plugins.Core.PathCore.Path::timesTable
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___timesTable_8;
	// System.Single[] DG.Tweening.Plugins.Core.PathCore.Path::lengthsTable
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___lengthsTable_9;
	// System.Int32 DG.Tweening.Plugins.Core.PathCore.Path::linearWPIndex
	int32_t ___linearWPIndex_10;
	// System.Boolean DG.Tweening.Plugins.Core.PathCore.Path::addedExtraStartWp
	bool ___addedExtraStartWp_11;
	// System.Boolean DG.Tweening.Plugins.Core.PathCore.Path::addedExtraEndWp
	bool ___addedExtraEndWp_12;
	// DG.Tweening.Plugins.Core.PathCore.Path DG.Tweening.Plugins.Core.PathCore.Path::_incrementalClone
	Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * ____incrementalClone_13;
	// System.Int32 DG.Tweening.Plugins.Core.PathCore.Path::_incrementalIndex
	int32_t ____incrementalIndex_14;
	// DG.Tweening.Plugins.Core.PathCore.ABSPathDecoder DG.Tweening.Plugins.Core.PathCore.Path::_decoder
	ABSPathDecoder_tC2631579BAB41F64C63A0909E8386B88B45F93D1 * ____decoder_15;
	// System.Boolean DG.Tweening.Plugins.Core.PathCore.Path::_changed
	bool ____changed_16;
	// UnityEngine.Vector3[] DG.Tweening.Plugins.Core.PathCore.Path::nonLinearDrawWps
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___nonLinearDrawWps_17;
	// UnityEngine.Vector3 DG.Tweening.Plugins.Core.PathCore.Path::targetPosition
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___targetPosition_18;
	// System.Nullable`1<UnityEngine.Vector3> DG.Tweening.Plugins.Core.PathCore.Path::lookAtPosition
	Nullable_1_t1829213F3538788DF79B4659AFC9D6A9C90C3258  ___lookAtPosition_19;
	// UnityEngine.Color DG.Tweening.Plugins.Core.PathCore.Path::gizmoColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___gizmoColor_20;

public:
	inline static int32_t get_offset_of_wpLengths_0() { return static_cast<int32_t>(offsetof(Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5, ___wpLengths_0)); }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* get_wpLengths_0() const { return ___wpLengths_0; }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA** get_address_of_wpLengths_0() { return &___wpLengths_0; }
	inline void set_wpLengths_0(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* value)
	{
		___wpLengths_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___wpLengths_0), (void*)value);
	}

	inline static int32_t get_offset_of_wps_1() { return static_cast<int32_t>(offsetof(Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5, ___wps_1)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_wps_1() const { return ___wps_1; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_wps_1() { return &___wps_1; }
	inline void set_wps_1(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___wps_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___wps_1), (void*)value);
	}

	inline static int32_t get_offset_of_type_2() { return static_cast<int32_t>(offsetof(Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5, ___type_2)); }
	inline int32_t get_type_2() const { return ___type_2; }
	inline int32_t* get_address_of_type_2() { return &___type_2; }
	inline void set_type_2(int32_t value)
	{
		___type_2 = value;
	}

	inline static int32_t get_offset_of_subdivisionsXSegment_3() { return static_cast<int32_t>(offsetof(Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5, ___subdivisionsXSegment_3)); }
	inline int32_t get_subdivisionsXSegment_3() const { return ___subdivisionsXSegment_3; }
	inline int32_t* get_address_of_subdivisionsXSegment_3() { return &___subdivisionsXSegment_3; }
	inline void set_subdivisionsXSegment_3(int32_t value)
	{
		___subdivisionsXSegment_3 = value;
	}

	inline static int32_t get_offset_of_subdivisions_4() { return static_cast<int32_t>(offsetof(Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5, ___subdivisions_4)); }
	inline int32_t get_subdivisions_4() const { return ___subdivisions_4; }
	inline int32_t* get_address_of_subdivisions_4() { return &___subdivisions_4; }
	inline void set_subdivisions_4(int32_t value)
	{
		___subdivisions_4 = value;
	}

	inline static int32_t get_offset_of_controlPoints_5() { return static_cast<int32_t>(offsetof(Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5, ___controlPoints_5)); }
	inline ControlPointU5BU5D_t9F61665C9F79DD5715B9C6C357BC1A9F16326884* get_controlPoints_5() const { return ___controlPoints_5; }
	inline ControlPointU5BU5D_t9F61665C9F79DD5715B9C6C357BC1A9F16326884** get_address_of_controlPoints_5() { return &___controlPoints_5; }
	inline void set_controlPoints_5(ControlPointU5BU5D_t9F61665C9F79DD5715B9C6C357BC1A9F16326884* value)
	{
		___controlPoints_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___controlPoints_5), (void*)value);
	}

	inline static int32_t get_offset_of_length_6() { return static_cast<int32_t>(offsetof(Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5, ___length_6)); }
	inline float get_length_6() const { return ___length_6; }
	inline float* get_address_of_length_6() { return &___length_6; }
	inline void set_length_6(float value)
	{
		___length_6 = value;
	}

	inline static int32_t get_offset_of_isFinalized_7() { return static_cast<int32_t>(offsetof(Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5, ___isFinalized_7)); }
	inline bool get_isFinalized_7() const { return ___isFinalized_7; }
	inline bool* get_address_of_isFinalized_7() { return &___isFinalized_7; }
	inline void set_isFinalized_7(bool value)
	{
		___isFinalized_7 = value;
	}

	inline static int32_t get_offset_of_timesTable_8() { return static_cast<int32_t>(offsetof(Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5, ___timesTable_8)); }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* get_timesTable_8() const { return ___timesTable_8; }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA** get_address_of_timesTable_8() { return &___timesTable_8; }
	inline void set_timesTable_8(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* value)
	{
		___timesTable_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___timesTable_8), (void*)value);
	}

	inline static int32_t get_offset_of_lengthsTable_9() { return static_cast<int32_t>(offsetof(Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5, ___lengthsTable_9)); }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* get_lengthsTable_9() const { return ___lengthsTable_9; }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA** get_address_of_lengthsTable_9() { return &___lengthsTable_9; }
	inline void set_lengthsTable_9(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* value)
	{
		___lengthsTable_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lengthsTable_9), (void*)value);
	}

	inline static int32_t get_offset_of_linearWPIndex_10() { return static_cast<int32_t>(offsetof(Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5, ___linearWPIndex_10)); }
	inline int32_t get_linearWPIndex_10() const { return ___linearWPIndex_10; }
	inline int32_t* get_address_of_linearWPIndex_10() { return &___linearWPIndex_10; }
	inline void set_linearWPIndex_10(int32_t value)
	{
		___linearWPIndex_10 = value;
	}

	inline static int32_t get_offset_of_addedExtraStartWp_11() { return static_cast<int32_t>(offsetof(Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5, ___addedExtraStartWp_11)); }
	inline bool get_addedExtraStartWp_11() const { return ___addedExtraStartWp_11; }
	inline bool* get_address_of_addedExtraStartWp_11() { return &___addedExtraStartWp_11; }
	inline void set_addedExtraStartWp_11(bool value)
	{
		___addedExtraStartWp_11 = value;
	}

	inline static int32_t get_offset_of_addedExtraEndWp_12() { return static_cast<int32_t>(offsetof(Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5, ___addedExtraEndWp_12)); }
	inline bool get_addedExtraEndWp_12() const { return ___addedExtraEndWp_12; }
	inline bool* get_address_of_addedExtraEndWp_12() { return &___addedExtraEndWp_12; }
	inline void set_addedExtraEndWp_12(bool value)
	{
		___addedExtraEndWp_12 = value;
	}

	inline static int32_t get_offset_of__incrementalClone_13() { return static_cast<int32_t>(offsetof(Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5, ____incrementalClone_13)); }
	inline Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * get__incrementalClone_13() const { return ____incrementalClone_13; }
	inline Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 ** get_address_of__incrementalClone_13() { return &____incrementalClone_13; }
	inline void set__incrementalClone_13(Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * value)
	{
		____incrementalClone_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____incrementalClone_13), (void*)value);
	}

	inline static int32_t get_offset_of__incrementalIndex_14() { return static_cast<int32_t>(offsetof(Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5, ____incrementalIndex_14)); }
	inline int32_t get__incrementalIndex_14() const { return ____incrementalIndex_14; }
	inline int32_t* get_address_of__incrementalIndex_14() { return &____incrementalIndex_14; }
	inline void set__incrementalIndex_14(int32_t value)
	{
		____incrementalIndex_14 = value;
	}

	inline static int32_t get_offset_of__decoder_15() { return static_cast<int32_t>(offsetof(Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5, ____decoder_15)); }
	inline ABSPathDecoder_tC2631579BAB41F64C63A0909E8386B88B45F93D1 * get__decoder_15() const { return ____decoder_15; }
	inline ABSPathDecoder_tC2631579BAB41F64C63A0909E8386B88B45F93D1 ** get_address_of__decoder_15() { return &____decoder_15; }
	inline void set__decoder_15(ABSPathDecoder_tC2631579BAB41F64C63A0909E8386B88B45F93D1 * value)
	{
		____decoder_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____decoder_15), (void*)value);
	}

	inline static int32_t get_offset_of__changed_16() { return static_cast<int32_t>(offsetof(Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5, ____changed_16)); }
	inline bool get__changed_16() const { return ____changed_16; }
	inline bool* get_address_of__changed_16() { return &____changed_16; }
	inline void set__changed_16(bool value)
	{
		____changed_16 = value;
	}

	inline static int32_t get_offset_of_nonLinearDrawWps_17() { return static_cast<int32_t>(offsetof(Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5, ___nonLinearDrawWps_17)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_nonLinearDrawWps_17() const { return ___nonLinearDrawWps_17; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_nonLinearDrawWps_17() { return &___nonLinearDrawWps_17; }
	inline void set_nonLinearDrawWps_17(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___nonLinearDrawWps_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___nonLinearDrawWps_17), (void*)value);
	}

	inline static int32_t get_offset_of_targetPosition_18() { return static_cast<int32_t>(offsetof(Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5, ___targetPosition_18)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_targetPosition_18() const { return ___targetPosition_18; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_targetPosition_18() { return &___targetPosition_18; }
	inline void set_targetPosition_18(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___targetPosition_18 = value;
	}

	inline static int32_t get_offset_of_lookAtPosition_19() { return static_cast<int32_t>(offsetof(Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5, ___lookAtPosition_19)); }
	inline Nullable_1_t1829213F3538788DF79B4659AFC9D6A9C90C3258  get_lookAtPosition_19() const { return ___lookAtPosition_19; }
	inline Nullable_1_t1829213F3538788DF79B4659AFC9D6A9C90C3258 * get_address_of_lookAtPosition_19() { return &___lookAtPosition_19; }
	inline void set_lookAtPosition_19(Nullable_1_t1829213F3538788DF79B4659AFC9D6A9C90C3258  value)
	{
		___lookAtPosition_19 = value;
	}

	inline static int32_t get_offset_of_gizmoColor_20() { return static_cast<int32_t>(offsetof(Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5, ___gizmoColor_20)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_gizmoColor_20() const { return ___gizmoColor_20; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_gizmoColor_20() { return &___gizmoColor_20; }
	inline void set_gizmoColor_20(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___gizmoColor_20 = value;
	}
};


// DG.Tweening.Plugins.Options.PathOptions
struct PathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A 
{
public:
	// DG.Tweening.PathMode DG.Tweening.Plugins.Options.PathOptions::mode
	int32_t ___mode_0;
	// DG.Tweening.Plugins.Options.OrientType DG.Tweening.Plugins.Options.PathOptions::orientType
	int32_t ___orientType_1;
	// DG.Tweening.AxisConstraint DG.Tweening.Plugins.Options.PathOptions::lockPositionAxis
	int32_t ___lockPositionAxis_2;
	// DG.Tweening.AxisConstraint DG.Tweening.Plugins.Options.PathOptions::lockRotationAxis
	int32_t ___lockRotationAxis_3;
	// System.Boolean DG.Tweening.Plugins.Options.PathOptions::isClosedPath
	bool ___isClosedPath_4;
	// UnityEngine.Vector3 DG.Tweening.Plugins.Options.PathOptions::lookAtPosition
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___lookAtPosition_5;
	// UnityEngine.Transform DG.Tweening.Plugins.Options.PathOptions::lookAtTransform
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___lookAtTransform_6;
	// System.Single DG.Tweening.Plugins.Options.PathOptions::lookAhead
	float ___lookAhead_7;
	// System.Boolean DG.Tweening.Plugins.Options.PathOptions::hasCustomForwardDirection
	bool ___hasCustomForwardDirection_8;
	// UnityEngine.Quaternion DG.Tweening.Plugins.Options.PathOptions::forward
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___forward_9;
	// System.Boolean DG.Tweening.Plugins.Options.PathOptions::useLocalPosition
	bool ___useLocalPosition_10;
	// UnityEngine.Transform DG.Tweening.Plugins.Options.PathOptions::parent
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___parent_11;
	// System.Boolean DG.Tweening.Plugins.Options.PathOptions::isRigidbody
	bool ___isRigidbody_12;
	// System.Boolean DG.Tweening.Plugins.Options.PathOptions::stableZRotation
	bool ___stableZRotation_13;
	// UnityEngine.Quaternion DG.Tweening.Plugins.Options.PathOptions::startupRot
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___startupRot_14;
	// System.Single DG.Tweening.Plugins.Options.PathOptions::startupZRot
	float ___startupZRot_15;
	// System.Boolean DG.Tweening.Plugins.Options.PathOptions::addedExtraStartWp
	bool ___addedExtraStartWp_16;
	// System.Boolean DG.Tweening.Plugins.Options.PathOptions::addedExtraEndWp
	bool ___addedExtraEndWp_17;

public:
	inline static int32_t get_offset_of_mode_0() { return static_cast<int32_t>(offsetof(PathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A, ___mode_0)); }
	inline int32_t get_mode_0() const { return ___mode_0; }
	inline int32_t* get_address_of_mode_0() { return &___mode_0; }
	inline void set_mode_0(int32_t value)
	{
		___mode_0 = value;
	}

	inline static int32_t get_offset_of_orientType_1() { return static_cast<int32_t>(offsetof(PathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A, ___orientType_1)); }
	inline int32_t get_orientType_1() const { return ___orientType_1; }
	inline int32_t* get_address_of_orientType_1() { return &___orientType_1; }
	inline void set_orientType_1(int32_t value)
	{
		___orientType_1 = value;
	}

	inline static int32_t get_offset_of_lockPositionAxis_2() { return static_cast<int32_t>(offsetof(PathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A, ___lockPositionAxis_2)); }
	inline int32_t get_lockPositionAxis_2() const { return ___lockPositionAxis_2; }
	inline int32_t* get_address_of_lockPositionAxis_2() { return &___lockPositionAxis_2; }
	inline void set_lockPositionAxis_2(int32_t value)
	{
		___lockPositionAxis_2 = value;
	}

	inline static int32_t get_offset_of_lockRotationAxis_3() { return static_cast<int32_t>(offsetof(PathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A, ___lockRotationAxis_3)); }
	inline int32_t get_lockRotationAxis_3() const { return ___lockRotationAxis_3; }
	inline int32_t* get_address_of_lockRotationAxis_3() { return &___lockRotationAxis_3; }
	inline void set_lockRotationAxis_3(int32_t value)
	{
		___lockRotationAxis_3 = value;
	}

	inline static int32_t get_offset_of_isClosedPath_4() { return static_cast<int32_t>(offsetof(PathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A, ___isClosedPath_4)); }
	inline bool get_isClosedPath_4() const { return ___isClosedPath_4; }
	inline bool* get_address_of_isClosedPath_4() { return &___isClosedPath_4; }
	inline void set_isClosedPath_4(bool value)
	{
		___isClosedPath_4 = value;
	}

	inline static int32_t get_offset_of_lookAtPosition_5() { return static_cast<int32_t>(offsetof(PathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A, ___lookAtPosition_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_lookAtPosition_5() const { return ___lookAtPosition_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_lookAtPosition_5() { return &___lookAtPosition_5; }
	inline void set_lookAtPosition_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___lookAtPosition_5 = value;
	}

	inline static int32_t get_offset_of_lookAtTransform_6() { return static_cast<int32_t>(offsetof(PathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A, ___lookAtTransform_6)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_lookAtTransform_6() const { return ___lookAtTransform_6; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_lookAtTransform_6() { return &___lookAtTransform_6; }
	inline void set_lookAtTransform_6(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___lookAtTransform_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lookAtTransform_6), (void*)value);
	}

	inline static int32_t get_offset_of_lookAhead_7() { return static_cast<int32_t>(offsetof(PathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A, ___lookAhead_7)); }
	inline float get_lookAhead_7() const { return ___lookAhead_7; }
	inline float* get_address_of_lookAhead_7() { return &___lookAhead_7; }
	inline void set_lookAhead_7(float value)
	{
		___lookAhead_7 = value;
	}

	inline static int32_t get_offset_of_hasCustomForwardDirection_8() { return static_cast<int32_t>(offsetof(PathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A, ___hasCustomForwardDirection_8)); }
	inline bool get_hasCustomForwardDirection_8() const { return ___hasCustomForwardDirection_8; }
	inline bool* get_address_of_hasCustomForwardDirection_8() { return &___hasCustomForwardDirection_8; }
	inline void set_hasCustomForwardDirection_8(bool value)
	{
		___hasCustomForwardDirection_8 = value;
	}

	inline static int32_t get_offset_of_forward_9() { return static_cast<int32_t>(offsetof(PathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A, ___forward_9)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_forward_9() const { return ___forward_9; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_forward_9() { return &___forward_9; }
	inline void set_forward_9(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___forward_9 = value;
	}

	inline static int32_t get_offset_of_useLocalPosition_10() { return static_cast<int32_t>(offsetof(PathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A, ___useLocalPosition_10)); }
	inline bool get_useLocalPosition_10() const { return ___useLocalPosition_10; }
	inline bool* get_address_of_useLocalPosition_10() { return &___useLocalPosition_10; }
	inline void set_useLocalPosition_10(bool value)
	{
		___useLocalPosition_10 = value;
	}

	inline static int32_t get_offset_of_parent_11() { return static_cast<int32_t>(offsetof(PathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A, ___parent_11)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_parent_11() const { return ___parent_11; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_parent_11() { return &___parent_11; }
	inline void set_parent_11(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___parent_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parent_11), (void*)value);
	}

	inline static int32_t get_offset_of_isRigidbody_12() { return static_cast<int32_t>(offsetof(PathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A, ___isRigidbody_12)); }
	inline bool get_isRigidbody_12() const { return ___isRigidbody_12; }
	inline bool* get_address_of_isRigidbody_12() { return &___isRigidbody_12; }
	inline void set_isRigidbody_12(bool value)
	{
		___isRigidbody_12 = value;
	}

	inline static int32_t get_offset_of_stableZRotation_13() { return static_cast<int32_t>(offsetof(PathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A, ___stableZRotation_13)); }
	inline bool get_stableZRotation_13() const { return ___stableZRotation_13; }
	inline bool* get_address_of_stableZRotation_13() { return &___stableZRotation_13; }
	inline void set_stableZRotation_13(bool value)
	{
		___stableZRotation_13 = value;
	}

	inline static int32_t get_offset_of_startupRot_14() { return static_cast<int32_t>(offsetof(PathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A, ___startupRot_14)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_startupRot_14() const { return ___startupRot_14; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_startupRot_14() { return &___startupRot_14; }
	inline void set_startupRot_14(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___startupRot_14 = value;
	}

	inline static int32_t get_offset_of_startupZRot_15() { return static_cast<int32_t>(offsetof(PathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A, ___startupZRot_15)); }
	inline float get_startupZRot_15() const { return ___startupZRot_15; }
	inline float* get_address_of_startupZRot_15() { return &___startupZRot_15; }
	inline void set_startupZRot_15(float value)
	{
		___startupZRot_15 = value;
	}

	inline static int32_t get_offset_of_addedExtraStartWp_16() { return static_cast<int32_t>(offsetof(PathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A, ___addedExtraStartWp_16)); }
	inline bool get_addedExtraStartWp_16() const { return ___addedExtraStartWp_16; }
	inline bool* get_address_of_addedExtraStartWp_16() { return &___addedExtraStartWp_16; }
	inline void set_addedExtraStartWp_16(bool value)
	{
		___addedExtraStartWp_16 = value;
	}

	inline static int32_t get_offset_of_addedExtraEndWp_17() { return static_cast<int32_t>(offsetof(PathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A, ___addedExtraEndWp_17)); }
	inline bool get_addedExtraEndWp_17() const { return ___addedExtraEndWp_17; }
	inline bool* get_address_of_addedExtraEndWp_17() { return &___addedExtraEndWp_17; }
	inline void set_addedExtraEndWp_17(bool value)
	{
		___addedExtraEndWp_17 = value;
	}
};

// Native definition for P/Invoke marshalling of DG.Tweening.Plugins.Options.PathOptions
struct PathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A_marshaled_pinvoke
{
	int32_t ___mode_0;
	int32_t ___orientType_1;
	int32_t ___lockPositionAxis_2;
	int32_t ___lockRotationAxis_3;
	int32_t ___isClosedPath_4;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___lookAtPosition_5;
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___lookAtTransform_6;
	float ___lookAhead_7;
	int32_t ___hasCustomForwardDirection_8;
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___forward_9;
	int32_t ___useLocalPosition_10;
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___parent_11;
	int32_t ___isRigidbody_12;
	int32_t ___stableZRotation_13;
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___startupRot_14;
	float ___startupZRot_15;
	int32_t ___addedExtraStartWp_16;
	int32_t ___addedExtraEndWp_17;
};
// Native definition for COM marshalling of DG.Tweening.Plugins.Options.PathOptions
struct PathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A_marshaled_com
{
	int32_t ___mode_0;
	int32_t ___orientType_1;
	int32_t ___lockPositionAxis_2;
	int32_t ___lockRotationAxis_3;
	int32_t ___isClosedPath_4;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___lookAtPosition_5;
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___lookAtTransform_6;
	float ___lookAhead_7;
	int32_t ___hasCustomForwardDirection_8;
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___forward_9;
	int32_t ___useLocalPosition_10;
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___parent_11;
	int32_t ___isRigidbody_12;
	int32_t ___stableZRotation_13;
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___startupRot_14;
	float ___startupZRot_15;
	int32_t ___addedExtraStartWp_16;
	int32_t ___addedExtraEndWp_17;
};

// UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A_marshaled_pinvoke : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A_marshaled_com : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
};

// UnityEngine.Sprite
struct Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};


// System.Action`4<DG.Tweening.Plugins.Options.PathOptions,DG.Tweening.Tween,UnityEngine.Quaternion,UnityEngine.Transform>
struct Action_4_t758FC0992B1B40F36598AA26D46FB5626C503AFE  : public MulticastDelegate_t
{
public:

public:
};


// DG.Tweening.Core.DOGetter`1<UnityEngine.Color>
struct DOGetter_1_tCC9DEA9BB7DA5C2570ED2CE9BD13EF5C154B7B10  : public MulticastDelegate_t
{
public:

public:
};


// DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>
struct DOGetter_1_tE8D2419A0219EB945463B02351F4905C77235E1A  : public MulticastDelegate_t
{
public:

public:
};


// DG.Tweening.Core.DOSetter`1<UnityEngine.Color>
struct DOSetter_1_tB62013E4617ECB51822384C8592AAB764E1662BF  : public MulticastDelegate_t
{
public:

public:
};


// DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>
struct DOSetter_1_tAEE38AA9321AE4A0A4A4E5D496EA753BFB746B85  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<CharControl,System.Boolean>
struct Func_2_t20F4B130EF75554CF71A9826AA0AC83CCB3EEDAC  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<CharsData,System.Boolean>
struct Func_2_tF8C630B1BAE8BC459BCA2C994588C319051E39EB  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Behaviour
struct Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// CharsBundleData
struct CharsBundleData_tC2C62F2A7FAEEDB189C65454E3E32437ECD36285  : public ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A
{
public:
	// CharsData[] CharsBundleData::_charsData
	CharsDataU5BU5D_t6E1A2B31C527A76C761C25915A588ED83C6B4CDE* ____charsData_4;

public:
	inline static int32_t get_offset_of__charsData_4() { return static_cast<int32_t>(offsetof(CharsBundleData_tC2C62F2A7FAEEDB189C65454E3E32437ECD36285, ____charsData_4)); }
	inline CharsDataU5BU5D_t6E1A2B31C527A76C761C25915A588ED83C6B4CDE* get__charsData_4() const { return ____charsData_4; }
	inline CharsDataU5BU5D_t6E1A2B31C527A76C761C25915A588ED83C6B4CDE** get_address_of__charsData_4() { return &____charsData_4; }
	inline void set__charsData_4(CharsDataU5BU5D_t6E1A2B31C527A76C761C25915A588ED83C6B4CDE* value)
	{
		____charsData_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____charsData_4), (void*)value);
	}
};


// UnityEngine.Rigidbody
struct Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// DG.Tweening.Tween
struct Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941  : public ABSSequentiable_t74F7F9AFFD39EE435A16A3E202A7872B46DA6C76
{
public:
	// System.Single DG.Tweening.Tween::timeScale
	float ___timeScale_4;
	// System.Boolean DG.Tweening.Tween::isBackwards
	bool ___isBackwards_5;
	// System.Object DG.Tweening.Tween::id
	RuntimeObject * ___id_6;
	// System.String DG.Tweening.Tween::stringId
	String_t* ___stringId_7;
	// System.Int32 DG.Tweening.Tween::intId
	int32_t ___intId_8;
	// System.Object DG.Tweening.Tween::target
	RuntimeObject * ___target_9;
	// DG.Tweening.UpdateType DG.Tweening.Tween::updateType
	int32_t ___updateType_10;
	// System.Boolean DG.Tweening.Tween::isIndependentUpdate
	bool ___isIndependentUpdate_11;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onPlay
	TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * ___onPlay_12;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onPause
	TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * ___onPause_13;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onRewind
	TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * ___onRewind_14;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onUpdate
	TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * ___onUpdate_15;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onStepComplete
	TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * ___onStepComplete_16;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onComplete
	TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * ___onComplete_17;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onKill
	TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * ___onKill_18;
	// DG.Tweening.TweenCallback`1<System.Int32> DG.Tweening.Tween::onWaypointChange
	TweenCallback_1_t145CD5D30F08B617B445D2B1B79A7BAADC305B9B * ___onWaypointChange_19;
	// System.Boolean DG.Tweening.Tween::isFrom
	bool ___isFrom_20;
	// System.Boolean DG.Tweening.Tween::isBlendable
	bool ___isBlendable_21;
	// System.Boolean DG.Tweening.Tween::isRecyclable
	bool ___isRecyclable_22;
	// System.Boolean DG.Tweening.Tween::isSpeedBased
	bool ___isSpeedBased_23;
	// System.Boolean DG.Tweening.Tween::autoKill
	bool ___autoKill_24;
	// System.Single DG.Tweening.Tween::duration
	float ___duration_25;
	// System.Int32 DG.Tweening.Tween::loops
	int32_t ___loops_26;
	// DG.Tweening.LoopType DG.Tweening.Tween::loopType
	int32_t ___loopType_27;
	// System.Single DG.Tweening.Tween::delay
	float ___delay_28;
	// System.Boolean DG.Tweening.Tween::<isRelative>k__BackingField
	bool ___U3CisRelativeU3Ek__BackingField_29;
	// DG.Tweening.Ease DG.Tweening.Tween::easeType
	int32_t ___easeType_30;
	// DG.Tweening.EaseFunction DG.Tweening.Tween::customEase
	EaseFunction_tC7ECEFDCAE4EC041E6FD7AC7C021E7B7680EFEB9 * ___customEase_31;
	// System.Single DG.Tweening.Tween::easeOvershootOrAmplitude
	float ___easeOvershootOrAmplitude_32;
	// System.Single DG.Tweening.Tween::easePeriod
	float ___easePeriod_33;
	// System.String DG.Tweening.Tween::debugTargetId
	String_t* ___debugTargetId_34;
	// System.Type DG.Tweening.Tween::typeofT1
	Type_t * ___typeofT1_35;
	// System.Type DG.Tweening.Tween::typeofT2
	Type_t * ___typeofT2_36;
	// System.Type DG.Tweening.Tween::typeofTPlugOptions
	Type_t * ___typeofTPlugOptions_37;
	// System.Boolean DG.Tweening.Tween::<active>k__BackingField
	bool ___U3CactiveU3Ek__BackingField_38;
	// System.Boolean DG.Tweening.Tween::isSequenced
	bool ___isSequenced_39;
	// DG.Tweening.Sequence DG.Tweening.Tween::sequenceParent
	Sequence_tE01FFFCA34A537CE2FF32EDAF451CDEC55A1399E * ___sequenceParent_40;
	// System.Int32 DG.Tweening.Tween::activeId
	int32_t ___activeId_41;
	// DG.Tweening.Core.Enums.SpecialStartupMode DG.Tweening.Tween::specialStartupMode
	int32_t ___specialStartupMode_42;
	// System.Boolean DG.Tweening.Tween::creationLocked
	bool ___creationLocked_43;
	// System.Boolean DG.Tweening.Tween::startupDone
	bool ___startupDone_44;
	// System.Boolean DG.Tweening.Tween::<playedOnce>k__BackingField
	bool ___U3CplayedOnceU3Ek__BackingField_45;
	// System.Single DG.Tweening.Tween::<position>k__BackingField
	float ___U3CpositionU3Ek__BackingField_46;
	// System.Single DG.Tweening.Tween::fullDuration
	float ___fullDuration_47;
	// System.Int32 DG.Tweening.Tween::completedLoops
	int32_t ___completedLoops_48;
	// System.Boolean DG.Tweening.Tween::isPlaying
	bool ___isPlaying_49;
	// System.Boolean DG.Tweening.Tween::isComplete
	bool ___isComplete_50;
	// System.Single DG.Tweening.Tween::elapsedDelay
	float ___elapsedDelay_51;
	// System.Boolean DG.Tweening.Tween::delayComplete
	bool ___delayComplete_52;
	// System.Int32 DG.Tweening.Tween::miscInt
	int32_t ___miscInt_53;

public:
	inline static int32_t get_offset_of_timeScale_4() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___timeScale_4)); }
	inline float get_timeScale_4() const { return ___timeScale_4; }
	inline float* get_address_of_timeScale_4() { return &___timeScale_4; }
	inline void set_timeScale_4(float value)
	{
		___timeScale_4 = value;
	}

	inline static int32_t get_offset_of_isBackwards_5() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___isBackwards_5)); }
	inline bool get_isBackwards_5() const { return ___isBackwards_5; }
	inline bool* get_address_of_isBackwards_5() { return &___isBackwards_5; }
	inline void set_isBackwards_5(bool value)
	{
		___isBackwards_5 = value;
	}

	inline static int32_t get_offset_of_id_6() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___id_6)); }
	inline RuntimeObject * get_id_6() const { return ___id_6; }
	inline RuntimeObject ** get_address_of_id_6() { return &___id_6; }
	inline void set_id_6(RuntimeObject * value)
	{
		___id_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___id_6), (void*)value);
	}

	inline static int32_t get_offset_of_stringId_7() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___stringId_7)); }
	inline String_t* get_stringId_7() const { return ___stringId_7; }
	inline String_t** get_address_of_stringId_7() { return &___stringId_7; }
	inline void set_stringId_7(String_t* value)
	{
		___stringId_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stringId_7), (void*)value);
	}

	inline static int32_t get_offset_of_intId_8() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___intId_8)); }
	inline int32_t get_intId_8() const { return ___intId_8; }
	inline int32_t* get_address_of_intId_8() { return &___intId_8; }
	inline void set_intId_8(int32_t value)
	{
		___intId_8 = value;
	}

	inline static int32_t get_offset_of_target_9() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___target_9)); }
	inline RuntimeObject * get_target_9() const { return ___target_9; }
	inline RuntimeObject ** get_address_of_target_9() { return &___target_9; }
	inline void set_target_9(RuntimeObject * value)
	{
		___target_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_9), (void*)value);
	}

	inline static int32_t get_offset_of_updateType_10() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___updateType_10)); }
	inline int32_t get_updateType_10() const { return ___updateType_10; }
	inline int32_t* get_address_of_updateType_10() { return &___updateType_10; }
	inline void set_updateType_10(int32_t value)
	{
		___updateType_10 = value;
	}

	inline static int32_t get_offset_of_isIndependentUpdate_11() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___isIndependentUpdate_11)); }
	inline bool get_isIndependentUpdate_11() const { return ___isIndependentUpdate_11; }
	inline bool* get_address_of_isIndependentUpdate_11() { return &___isIndependentUpdate_11; }
	inline void set_isIndependentUpdate_11(bool value)
	{
		___isIndependentUpdate_11 = value;
	}

	inline static int32_t get_offset_of_onPlay_12() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___onPlay_12)); }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * get_onPlay_12() const { return ___onPlay_12; }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB ** get_address_of_onPlay_12() { return &___onPlay_12; }
	inline void set_onPlay_12(TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * value)
	{
		___onPlay_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPlay_12), (void*)value);
	}

	inline static int32_t get_offset_of_onPause_13() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___onPause_13)); }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * get_onPause_13() const { return ___onPause_13; }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB ** get_address_of_onPause_13() { return &___onPause_13; }
	inline void set_onPause_13(TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * value)
	{
		___onPause_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPause_13), (void*)value);
	}

	inline static int32_t get_offset_of_onRewind_14() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___onRewind_14)); }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * get_onRewind_14() const { return ___onRewind_14; }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB ** get_address_of_onRewind_14() { return &___onRewind_14; }
	inline void set_onRewind_14(TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * value)
	{
		___onRewind_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onRewind_14), (void*)value);
	}

	inline static int32_t get_offset_of_onUpdate_15() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___onUpdate_15)); }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * get_onUpdate_15() const { return ___onUpdate_15; }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB ** get_address_of_onUpdate_15() { return &___onUpdate_15; }
	inline void set_onUpdate_15(TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * value)
	{
		___onUpdate_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onUpdate_15), (void*)value);
	}

	inline static int32_t get_offset_of_onStepComplete_16() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___onStepComplete_16)); }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * get_onStepComplete_16() const { return ___onStepComplete_16; }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB ** get_address_of_onStepComplete_16() { return &___onStepComplete_16; }
	inline void set_onStepComplete_16(TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * value)
	{
		___onStepComplete_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onStepComplete_16), (void*)value);
	}

	inline static int32_t get_offset_of_onComplete_17() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___onComplete_17)); }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * get_onComplete_17() const { return ___onComplete_17; }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB ** get_address_of_onComplete_17() { return &___onComplete_17; }
	inline void set_onComplete_17(TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * value)
	{
		___onComplete_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onComplete_17), (void*)value);
	}

	inline static int32_t get_offset_of_onKill_18() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___onKill_18)); }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * get_onKill_18() const { return ___onKill_18; }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB ** get_address_of_onKill_18() { return &___onKill_18; }
	inline void set_onKill_18(TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * value)
	{
		___onKill_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onKill_18), (void*)value);
	}

	inline static int32_t get_offset_of_onWaypointChange_19() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___onWaypointChange_19)); }
	inline TweenCallback_1_t145CD5D30F08B617B445D2B1B79A7BAADC305B9B * get_onWaypointChange_19() const { return ___onWaypointChange_19; }
	inline TweenCallback_1_t145CD5D30F08B617B445D2B1B79A7BAADC305B9B ** get_address_of_onWaypointChange_19() { return &___onWaypointChange_19; }
	inline void set_onWaypointChange_19(TweenCallback_1_t145CD5D30F08B617B445D2B1B79A7BAADC305B9B * value)
	{
		___onWaypointChange_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onWaypointChange_19), (void*)value);
	}

	inline static int32_t get_offset_of_isFrom_20() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___isFrom_20)); }
	inline bool get_isFrom_20() const { return ___isFrom_20; }
	inline bool* get_address_of_isFrom_20() { return &___isFrom_20; }
	inline void set_isFrom_20(bool value)
	{
		___isFrom_20 = value;
	}

	inline static int32_t get_offset_of_isBlendable_21() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___isBlendable_21)); }
	inline bool get_isBlendable_21() const { return ___isBlendable_21; }
	inline bool* get_address_of_isBlendable_21() { return &___isBlendable_21; }
	inline void set_isBlendable_21(bool value)
	{
		___isBlendable_21 = value;
	}

	inline static int32_t get_offset_of_isRecyclable_22() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___isRecyclable_22)); }
	inline bool get_isRecyclable_22() const { return ___isRecyclable_22; }
	inline bool* get_address_of_isRecyclable_22() { return &___isRecyclable_22; }
	inline void set_isRecyclable_22(bool value)
	{
		___isRecyclable_22 = value;
	}

	inline static int32_t get_offset_of_isSpeedBased_23() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___isSpeedBased_23)); }
	inline bool get_isSpeedBased_23() const { return ___isSpeedBased_23; }
	inline bool* get_address_of_isSpeedBased_23() { return &___isSpeedBased_23; }
	inline void set_isSpeedBased_23(bool value)
	{
		___isSpeedBased_23 = value;
	}

	inline static int32_t get_offset_of_autoKill_24() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___autoKill_24)); }
	inline bool get_autoKill_24() const { return ___autoKill_24; }
	inline bool* get_address_of_autoKill_24() { return &___autoKill_24; }
	inline void set_autoKill_24(bool value)
	{
		___autoKill_24 = value;
	}

	inline static int32_t get_offset_of_duration_25() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___duration_25)); }
	inline float get_duration_25() const { return ___duration_25; }
	inline float* get_address_of_duration_25() { return &___duration_25; }
	inline void set_duration_25(float value)
	{
		___duration_25 = value;
	}

	inline static int32_t get_offset_of_loops_26() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___loops_26)); }
	inline int32_t get_loops_26() const { return ___loops_26; }
	inline int32_t* get_address_of_loops_26() { return &___loops_26; }
	inline void set_loops_26(int32_t value)
	{
		___loops_26 = value;
	}

	inline static int32_t get_offset_of_loopType_27() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___loopType_27)); }
	inline int32_t get_loopType_27() const { return ___loopType_27; }
	inline int32_t* get_address_of_loopType_27() { return &___loopType_27; }
	inline void set_loopType_27(int32_t value)
	{
		___loopType_27 = value;
	}

	inline static int32_t get_offset_of_delay_28() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___delay_28)); }
	inline float get_delay_28() const { return ___delay_28; }
	inline float* get_address_of_delay_28() { return &___delay_28; }
	inline void set_delay_28(float value)
	{
		___delay_28 = value;
	}

	inline static int32_t get_offset_of_U3CisRelativeU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___U3CisRelativeU3Ek__BackingField_29)); }
	inline bool get_U3CisRelativeU3Ek__BackingField_29() const { return ___U3CisRelativeU3Ek__BackingField_29; }
	inline bool* get_address_of_U3CisRelativeU3Ek__BackingField_29() { return &___U3CisRelativeU3Ek__BackingField_29; }
	inline void set_U3CisRelativeU3Ek__BackingField_29(bool value)
	{
		___U3CisRelativeU3Ek__BackingField_29 = value;
	}

	inline static int32_t get_offset_of_easeType_30() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___easeType_30)); }
	inline int32_t get_easeType_30() const { return ___easeType_30; }
	inline int32_t* get_address_of_easeType_30() { return &___easeType_30; }
	inline void set_easeType_30(int32_t value)
	{
		___easeType_30 = value;
	}

	inline static int32_t get_offset_of_customEase_31() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___customEase_31)); }
	inline EaseFunction_tC7ECEFDCAE4EC041E6FD7AC7C021E7B7680EFEB9 * get_customEase_31() const { return ___customEase_31; }
	inline EaseFunction_tC7ECEFDCAE4EC041E6FD7AC7C021E7B7680EFEB9 ** get_address_of_customEase_31() { return &___customEase_31; }
	inline void set_customEase_31(EaseFunction_tC7ECEFDCAE4EC041E6FD7AC7C021E7B7680EFEB9 * value)
	{
		___customEase_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___customEase_31), (void*)value);
	}

	inline static int32_t get_offset_of_easeOvershootOrAmplitude_32() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___easeOvershootOrAmplitude_32)); }
	inline float get_easeOvershootOrAmplitude_32() const { return ___easeOvershootOrAmplitude_32; }
	inline float* get_address_of_easeOvershootOrAmplitude_32() { return &___easeOvershootOrAmplitude_32; }
	inline void set_easeOvershootOrAmplitude_32(float value)
	{
		___easeOvershootOrAmplitude_32 = value;
	}

	inline static int32_t get_offset_of_easePeriod_33() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___easePeriod_33)); }
	inline float get_easePeriod_33() const { return ___easePeriod_33; }
	inline float* get_address_of_easePeriod_33() { return &___easePeriod_33; }
	inline void set_easePeriod_33(float value)
	{
		___easePeriod_33 = value;
	}

	inline static int32_t get_offset_of_debugTargetId_34() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___debugTargetId_34)); }
	inline String_t* get_debugTargetId_34() const { return ___debugTargetId_34; }
	inline String_t** get_address_of_debugTargetId_34() { return &___debugTargetId_34; }
	inline void set_debugTargetId_34(String_t* value)
	{
		___debugTargetId_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___debugTargetId_34), (void*)value);
	}

	inline static int32_t get_offset_of_typeofT1_35() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___typeofT1_35)); }
	inline Type_t * get_typeofT1_35() const { return ___typeofT1_35; }
	inline Type_t ** get_address_of_typeofT1_35() { return &___typeofT1_35; }
	inline void set_typeofT1_35(Type_t * value)
	{
		___typeofT1_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___typeofT1_35), (void*)value);
	}

	inline static int32_t get_offset_of_typeofT2_36() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___typeofT2_36)); }
	inline Type_t * get_typeofT2_36() const { return ___typeofT2_36; }
	inline Type_t ** get_address_of_typeofT2_36() { return &___typeofT2_36; }
	inline void set_typeofT2_36(Type_t * value)
	{
		___typeofT2_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___typeofT2_36), (void*)value);
	}

	inline static int32_t get_offset_of_typeofTPlugOptions_37() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___typeofTPlugOptions_37)); }
	inline Type_t * get_typeofTPlugOptions_37() const { return ___typeofTPlugOptions_37; }
	inline Type_t ** get_address_of_typeofTPlugOptions_37() { return &___typeofTPlugOptions_37; }
	inline void set_typeofTPlugOptions_37(Type_t * value)
	{
		___typeofTPlugOptions_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___typeofTPlugOptions_37), (void*)value);
	}

	inline static int32_t get_offset_of_U3CactiveU3Ek__BackingField_38() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___U3CactiveU3Ek__BackingField_38)); }
	inline bool get_U3CactiveU3Ek__BackingField_38() const { return ___U3CactiveU3Ek__BackingField_38; }
	inline bool* get_address_of_U3CactiveU3Ek__BackingField_38() { return &___U3CactiveU3Ek__BackingField_38; }
	inline void set_U3CactiveU3Ek__BackingField_38(bool value)
	{
		___U3CactiveU3Ek__BackingField_38 = value;
	}

	inline static int32_t get_offset_of_isSequenced_39() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___isSequenced_39)); }
	inline bool get_isSequenced_39() const { return ___isSequenced_39; }
	inline bool* get_address_of_isSequenced_39() { return &___isSequenced_39; }
	inline void set_isSequenced_39(bool value)
	{
		___isSequenced_39 = value;
	}

	inline static int32_t get_offset_of_sequenceParent_40() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___sequenceParent_40)); }
	inline Sequence_tE01FFFCA34A537CE2FF32EDAF451CDEC55A1399E * get_sequenceParent_40() const { return ___sequenceParent_40; }
	inline Sequence_tE01FFFCA34A537CE2FF32EDAF451CDEC55A1399E ** get_address_of_sequenceParent_40() { return &___sequenceParent_40; }
	inline void set_sequenceParent_40(Sequence_tE01FFFCA34A537CE2FF32EDAF451CDEC55A1399E * value)
	{
		___sequenceParent_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sequenceParent_40), (void*)value);
	}

	inline static int32_t get_offset_of_activeId_41() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___activeId_41)); }
	inline int32_t get_activeId_41() const { return ___activeId_41; }
	inline int32_t* get_address_of_activeId_41() { return &___activeId_41; }
	inline void set_activeId_41(int32_t value)
	{
		___activeId_41 = value;
	}

	inline static int32_t get_offset_of_specialStartupMode_42() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___specialStartupMode_42)); }
	inline int32_t get_specialStartupMode_42() const { return ___specialStartupMode_42; }
	inline int32_t* get_address_of_specialStartupMode_42() { return &___specialStartupMode_42; }
	inline void set_specialStartupMode_42(int32_t value)
	{
		___specialStartupMode_42 = value;
	}

	inline static int32_t get_offset_of_creationLocked_43() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___creationLocked_43)); }
	inline bool get_creationLocked_43() const { return ___creationLocked_43; }
	inline bool* get_address_of_creationLocked_43() { return &___creationLocked_43; }
	inline void set_creationLocked_43(bool value)
	{
		___creationLocked_43 = value;
	}

	inline static int32_t get_offset_of_startupDone_44() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___startupDone_44)); }
	inline bool get_startupDone_44() const { return ___startupDone_44; }
	inline bool* get_address_of_startupDone_44() { return &___startupDone_44; }
	inline void set_startupDone_44(bool value)
	{
		___startupDone_44 = value;
	}

	inline static int32_t get_offset_of_U3CplayedOnceU3Ek__BackingField_45() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___U3CplayedOnceU3Ek__BackingField_45)); }
	inline bool get_U3CplayedOnceU3Ek__BackingField_45() const { return ___U3CplayedOnceU3Ek__BackingField_45; }
	inline bool* get_address_of_U3CplayedOnceU3Ek__BackingField_45() { return &___U3CplayedOnceU3Ek__BackingField_45; }
	inline void set_U3CplayedOnceU3Ek__BackingField_45(bool value)
	{
		___U3CplayedOnceU3Ek__BackingField_45 = value;
	}

	inline static int32_t get_offset_of_U3CpositionU3Ek__BackingField_46() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___U3CpositionU3Ek__BackingField_46)); }
	inline float get_U3CpositionU3Ek__BackingField_46() const { return ___U3CpositionU3Ek__BackingField_46; }
	inline float* get_address_of_U3CpositionU3Ek__BackingField_46() { return &___U3CpositionU3Ek__BackingField_46; }
	inline void set_U3CpositionU3Ek__BackingField_46(float value)
	{
		___U3CpositionU3Ek__BackingField_46 = value;
	}

	inline static int32_t get_offset_of_fullDuration_47() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___fullDuration_47)); }
	inline float get_fullDuration_47() const { return ___fullDuration_47; }
	inline float* get_address_of_fullDuration_47() { return &___fullDuration_47; }
	inline void set_fullDuration_47(float value)
	{
		___fullDuration_47 = value;
	}

	inline static int32_t get_offset_of_completedLoops_48() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___completedLoops_48)); }
	inline int32_t get_completedLoops_48() const { return ___completedLoops_48; }
	inline int32_t* get_address_of_completedLoops_48() { return &___completedLoops_48; }
	inline void set_completedLoops_48(int32_t value)
	{
		___completedLoops_48 = value;
	}

	inline static int32_t get_offset_of_isPlaying_49() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___isPlaying_49)); }
	inline bool get_isPlaying_49() const { return ___isPlaying_49; }
	inline bool* get_address_of_isPlaying_49() { return &___isPlaying_49; }
	inline void set_isPlaying_49(bool value)
	{
		___isPlaying_49 = value;
	}

	inline static int32_t get_offset_of_isComplete_50() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___isComplete_50)); }
	inline bool get_isComplete_50() const { return ___isComplete_50; }
	inline bool* get_address_of_isComplete_50() { return &___isComplete_50; }
	inline void set_isComplete_50(bool value)
	{
		___isComplete_50 = value;
	}

	inline static int32_t get_offset_of_elapsedDelay_51() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___elapsedDelay_51)); }
	inline float get_elapsedDelay_51() const { return ___elapsedDelay_51; }
	inline float* get_address_of_elapsedDelay_51() { return &___elapsedDelay_51; }
	inline void set_elapsedDelay_51(float value)
	{
		___elapsedDelay_51 = value;
	}

	inline static int32_t get_offset_of_delayComplete_52() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___delayComplete_52)); }
	inline bool get_delayComplete_52() const { return ___delayComplete_52; }
	inline bool* get_address_of_delayComplete_52() { return &___delayComplete_52; }
	inline void set_delayComplete_52(bool value)
	{
		___delayComplete_52 = value;
	}

	inline static int32_t get_offset_of_miscInt_53() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___miscInt_53)); }
	inline int32_t get_miscInt_53() const { return ___miscInt_53; }
	inline int32_t* get_address_of_miscInt_53() { return &___miscInt_53; }
	inline void set_miscInt_53(int32_t value)
	{
		___miscInt_53 = value;
	}
};


// UnityEngine.Events.UnityAction
struct UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// UnityEngine.RectTransform
struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072  : public Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1
{
public:

public:
};

struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_StaticFields
{
public:
	// UnityEngine.RectTransform/ReapplyDrivenProperties UnityEngine.RectTransform::reapplyDrivenProperties
	ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE * ___reapplyDrivenProperties_4;

public:
	inline static int32_t get_offset_of_reapplyDrivenProperties_4() { return static_cast<int32_t>(offsetof(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_StaticFields, ___reapplyDrivenProperties_4)); }
	inline ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE * get_reapplyDrivenProperties_4() const { return ___reapplyDrivenProperties_4; }
	inline ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE ** get_address_of_reapplyDrivenProperties_4() { return &___reapplyDrivenProperties_4; }
	inline void set_reapplyDrivenProperties_4(ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE * value)
	{
		___reapplyDrivenProperties_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___reapplyDrivenProperties_4), (void*)value);
	}
};


// DG.Tweening.Tweener
struct Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8  : public Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941
{
public:
	// System.Boolean DG.Tweening.Tweener::hasManuallySetStartValue
	bool ___hasManuallySetStartValue_54;
	// System.Boolean DG.Tweening.Tweener::isFromAllowed
	bool ___isFromAllowed_55;

public:
	inline static int32_t get_offset_of_hasManuallySetStartValue_54() { return static_cast<int32_t>(offsetof(Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8, ___hasManuallySetStartValue_54)); }
	inline bool get_hasManuallySetStartValue_54() const { return ___hasManuallySetStartValue_54; }
	inline bool* get_address_of_hasManuallySetStartValue_54() { return &___hasManuallySetStartValue_54; }
	inline void set_hasManuallySetStartValue_54(bool value)
	{
		___hasManuallySetStartValue_54 = value;
	}

	inline static int32_t get_offset_of_isFromAllowed_55() { return static_cast<int32_t>(offsetof(Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8, ___isFromAllowed_55)); }
	inline bool get_isFromAllowed_55() const { return ___isFromAllowed_55; }
	inline bool* get_address_of_isFromAllowed_55() { return &___isFromAllowed_55; }
	inline void set_isFromAllowed_55(bool value)
	{
		___isFromAllowed_55 = value;
	}
};


// DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>
struct TweenerCore_3_t551A69CD4FF610AE9DF07C97A86B6FE12EBEB597  : public Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8
{
public:
	// T2 DG.Tweening.Core.TweenerCore`3::startValue
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___startValue_56;
	// T2 DG.Tweening.Core.TweenerCore`3::endValue
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___endValue_57;
	// T2 DG.Tweening.Core.TweenerCore`3::changeValue
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___changeValue_58;
	// TPlugOptions DG.Tweening.Core.TweenerCore`3::plugOptions
	ColorOptions_t25AF005F398643658A000DBAD00EFF82C944355A  ___plugOptions_59;
	// DG.Tweening.Core.DOGetter`1<T1> DG.Tweening.Core.TweenerCore`3::getter
	DOGetter_1_tCC9DEA9BB7DA5C2570ED2CE9BD13EF5C154B7B10 * ___getter_60;
	// DG.Tweening.Core.DOSetter`1<T1> DG.Tweening.Core.TweenerCore`3::setter
	DOSetter_1_tB62013E4617ECB51822384C8592AAB764E1662BF * ___setter_61;
	// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<T1,T2,TPlugOptions> DG.Tweening.Core.TweenerCore`3::tweenPlugin
	ABSTweenPlugin_3_t6A1019F3F26DC4DFAFFA4439E8D04997B8A86E75 * ___tweenPlugin_62;

public:
	inline static int32_t get_offset_of_startValue_56() { return static_cast<int32_t>(offsetof(TweenerCore_3_t551A69CD4FF610AE9DF07C97A86B6FE12EBEB597, ___startValue_56)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_startValue_56() const { return ___startValue_56; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_startValue_56() { return &___startValue_56; }
	inline void set_startValue_56(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___startValue_56 = value;
	}

	inline static int32_t get_offset_of_endValue_57() { return static_cast<int32_t>(offsetof(TweenerCore_3_t551A69CD4FF610AE9DF07C97A86B6FE12EBEB597, ___endValue_57)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_endValue_57() const { return ___endValue_57; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_endValue_57() { return &___endValue_57; }
	inline void set_endValue_57(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___endValue_57 = value;
	}

	inline static int32_t get_offset_of_changeValue_58() { return static_cast<int32_t>(offsetof(TweenerCore_3_t551A69CD4FF610AE9DF07C97A86B6FE12EBEB597, ___changeValue_58)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_changeValue_58() const { return ___changeValue_58; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_changeValue_58() { return &___changeValue_58; }
	inline void set_changeValue_58(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___changeValue_58 = value;
	}

	inline static int32_t get_offset_of_plugOptions_59() { return static_cast<int32_t>(offsetof(TweenerCore_3_t551A69CD4FF610AE9DF07C97A86B6FE12EBEB597, ___plugOptions_59)); }
	inline ColorOptions_t25AF005F398643658A000DBAD00EFF82C944355A  get_plugOptions_59() const { return ___plugOptions_59; }
	inline ColorOptions_t25AF005F398643658A000DBAD00EFF82C944355A * get_address_of_plugOptions_59() { return &___plugOptions_59; }
	inline void set_plugOptions_59(ColorOptions_t25AF005F398643658A000DBAD00EFF82C944355A  value)
	{
		___plugOptions_59 = value;
	}

	inline static int32_t get_offset_of_getter_60() { return static_cast<int32_t>(offsetof(TweenerCore_3_t551A69CD4FF610AE9DF07C97A86B6FE12EBEB597, ___getter_60)); }
	inline DOGetter_1_tCC9DEA9BB7DA5C2570ED2CE9BD13EF5C154B7B10 * get_getter_60() const { return ___getter_60; }
	inline DOGetter_1_tCC9DEA9BB7DA5C2570ED2CE9BD13EF5C154B7B10 ** get_address_of_getter_60() { return &___getter_60; }
	inline void set_getter_60(DOGetter_1_tCC9DEA9BB7DA5C2570ED2CE9BD13EF5C154B7B10 * value)
	{
		___getter_60 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___getter_60), (void*)value);
	}

	inline static int32_t get_offset_of_setter_61() { return static_cast<int32_t>(offsetof(TweenerCore_3_t551A69CD4FF610AE9DF07C97A86B6FE12EBEB597, ___setter_61)); }
	inline DOSetter_1_tB62013E4617ECB51822384C8592AAB764E1662BF * get_setter_61() const { return ___setter_61; }
	inline DOSetter_1_tB62013E4617ECB51822384C8592AAB764E1662BF ** get_address_of_setter_61() { return &___setter_61; }
	inline void set_setter_61(DOSetter_1_tB62013E4617ECB51822384C8592AAB764E1662BF * value)
	{
		___setter_61 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___setter_61), (void*)value);
	}

	inline static int32_t get_offset_of_tweenPlugin_62() { return static_cast<int32_t>(offsetof(TweenerCore_3_t551A69CD4FF610AE9DF07C97A86B6FE12EBEB597, ___tweenPlugin_62)); }
	inline ABSTweenPlugin_3_t6A1019F3F26DC4DFAFFA4439E8D04997B8A86E75 * get_tweenPlugin_62() const { return ___tweenPlugin_62; }
	inline ABSTweenPlugin_3_t6A1019F3F26DC4DFAFFA4439E8D04997B8A86E75 ** get_address_of_tweenPlugin_62() { return &___tweenPlugin_62; }
	inline void set_tweenPlugin_62(ABSTweenPlugin_3_t6A1019F3F26DC4DFAFFA4439E8D04997B8A86E75 * value)
	{
		___tweenPlugin_62 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tweenPlugin_62), (void*)value);
	}
};


// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>
struct TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A  : public Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8
{
public:
	// T2 DG.Tweening.Core.TweenerCore`3::startValue
	Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * ___startValue_56;
	// T2 DG.Tweening.Core.TweenerCore`3::endValue
	Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * ___endValue_57;
	// T2 DG.Tweening.Core.TweenerCore`3::changeValue
	Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * ___changeValue_58;
	// TPlugOptions DG.Tweening.Core.TweenerCore`3::plugOptions
	PathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A  ___plugOptions_59;
	// DG.Tweening.Core.DOGetter`1<T1> DG.Tweening.Core.TweenerCore`3::getter
	DOGetter_1_tE8D2419A0219EB945463B02351F4905C77235E1A * ___getter_60;
	// DG.Tweening.Core.DOSetter`1<T1> DG.Tweening.Core.TweenerCore`3::setter
	DOSetter_1_tAEE38AA9321AE4A0A4A4E5D496EA753BFB746B85 * ___setter_61;
	// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<T1,T2,TPlugOptions> DG.Tweening.Core.TweenerCore`3::tweenPlugin
	ABSTweenPlugin_3_t0048BB94992A96F02D427E2C26C6E92AC9AECD32 * ___tweenPlugin_62;

public:
	inline static int32_t get_offset_of_startValue_56() { return static_cast<int32_t>(offsetof(TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A, ___startValue_56)); }
	inline Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * get_startValue_56() const { return ___startValue_56; }
	inline Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 ** get_address_of_startValue_56() { return &___startValue_56; }
	inline void set_startValue_56(Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * value)
	{
		___startValue_56 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___startValue_56), (void*)value);
	}

	inline static int32_t get_offset_of_endValue_57() { return static_cast<int32_t>(offsetof(TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A, ___endValue_57)); }
	inline Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * get_endValue_57() const { return ___endValue_57; }
	inline Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 ** get_address_of_endValue_57() { return &___endValue_57; }
	inline void set_endValue_57(Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * value)
	{
		___endValue_57 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___endValue_57), (void*)value);
	}

	inline static int32_t get_offset_of_changeValue_58() { return static_cast<int32_t>(offsetof(TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A, ___changeValue_58)); }
	inline Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * get_changeValue_58() const { return ___changeValue_58; }
	inline Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 ** get_address_of_changeValue_58() { return &___changeValue_58; }
	inline void set_changeValue_58(Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * value)
	{
		___changeValue_58 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___changeValue_58), (void*)value);
	}

	inline static int32_t get_offset_of_plugOptions_59() { return static_cast<int32_t>(offsetof(TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A, ___plugOptions_59)); }
	inline PathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A  get_plugOptions_59() const { return ___plugOptions_59; }
	inline PathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A * get_address_of_plugOptions_59() { return &___plugOptions_59; }
	inline void set_plugOptions_59(PathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A  value)
	{
		___plugOptions_59 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___plugOptions_59))->___lookAtTransform_6), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___plugOptions_59))->___parent_11), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_getter_60() { return static_cast<int32_t>(offsetof(TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A, ___getter_60)); }
	inline DOGetter_1_tE8D2419A0219EB945463B02351F4905C77235E1A * get_getter_60() const { return ___getter_60; }
	inline DOGetter_1_tE8D2419A0219EB945463B02351F4905C77235E1A ** get_address_of_getter_60() { return &___getter_60; }
	inline void set_getter_60(DOGetter_1_tE8D2419A0219EB945463B02351F4905C77235E1A * value)
	{
		___getter_60 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___getter_60), (void*)value);
	}

	inline static int32_t get_offset_of_setter_61() { return static_cast<int32_t>(offsetof(TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A, ___setter_61)); }
	inline DOSetter_1_tAEE38AA9321AE4A0A4A4E5D496EA753BFB746B85 * get_setter_61() const { return ___setter_61; }
	inline DOSetter_1_tAEE38AA9321AE4A0A4A4E5D496EA753BFB746B85 ** get_address_of_setter_61() { return &___setter_61; }
	inline void set_setter_61(DOSetter_1_tAEE38AA9321AE4A0A4A4E5D496EA753BFB746B85 * value)
	{
		___setter_61 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___setter_61), (void*)value);
	}

	inline static int32_t get_offset_of_tweenPlugin_62() { return static_cast<int32_t>(offsetof(TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A, ___tweenPlugin_62)); }
	inline ABSTweenPlugin_3_t0048BB94992A96F02D427E2C26C6E92AC9AECD32 * get_tweenPlugin_62() const { return ___tweenPlugin_62; }
	inline ABSTweenPlugin_3_t0048BB94992A96F02D427E2C26C6E92AC9AECD32 ** get_address_of_tweenPlugin_62() { return &___tweenPlugin_62; }
	inline void set_tweenPlugin_62(ABSTweenPlugin_3_t0048BB94992A96F02D427E2C26C6E92AC9AECD32 * value)
	{
		___tweenPlugin_62 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tweenPlugin_62), (void*)value);
	}
};


// CharControl
struct CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.UI.Button CharControl::_sourceButton
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ____sourceButton_4;
	// LevelProvider CharControl::_levelProvider
	LevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841 * ____levelProvider_5;
	// CharsData CharControl::_chardata
	CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595 * ____chardata_6;
	// TweenEffect CharControl::_tweenEffect
	TweenEffect_tB80E075E7B5F736D5D3E0BDAB6A253BF4DA1BBF1 * ____tweenEffect_7;
	// UnityEngine.UI.Image CharControl::_charImage
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ____charImage_8;
	// UnityEngine.Sprite CharControl::_backgroundSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ____backgroundSprite_9;
	// System.Boolean CharControl::isSelected
	bool ___isSelected_10;

public:
	inline static int32_t get_offset_of__sourceButton_4() { return static_cast<int32_t>(offsetof(CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD, ____sourceButton_4)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get__sourceButton_4() const { return ____sourceButton_4; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of__sourceButton_4() { return &____sourceButton_4; }
	inline void set__sourceButton_4(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		____sourceButton_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____sourceButton_4), (void*)value);
	}

	inline static int32_t get_offset_of__levelProvider_5() { return static_cast<int32_t>(offsetof(CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD, ____levelProvider_5)); }
	inline LevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841 * get__levelProvider_5() const { return ____levelProvider_5; }
	inline LevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841 ** get_address_of__levelProvider_5() { return &____levelProvider_5; }
	inline void set__levelProvider_5(LevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841 * value)
	{
		____levelProvider_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____levelProvider_5), (void*)value);
	}

	inline static int32_t get_offset_of__chardata_6() { return static_cast<int32_t>(offsetof(CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD, ____chardata_6)); }
	inline CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595 * get__chardata_6() const { return ____chardata_6; }
	inline CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595 ** get_address_of__chardata_6() { return &____chardata_6; }
	inline void set__chardata_6(CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595 * value)
	{
		____chardata_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____chardata_6), (void*)value);
	}

	inline static int32_t get_offset_of__tweenEffect_7() { return static_cast<int32_t>(offsetof(CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD, ____tweenEffect_7)); }
	inline TweenEffect_tB80E075E7B5F736D5D3E0BDAB6A253BF4DA1BBF1 * get__tweenEffect_7() const { return ____tweenEffect_7; }
	inline TweenEffect_tB80E075E7B5F736D5D3E0BDAB6A253BF4DA1BBF1 ** get_address_of__tweenEffect_7() { return &____tweenEffect_7; }
	inline void set__tweenEffect_7(TweenEffect_tB80E075E7B5F736D5D3E0BDAB6A253BF4DA1BBF1 * value)
	{
		____tweenEffect_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____tweenEffect_7), (void*)value);
	}

	inline static int32_t get_offset_of__charImage_8() { return static_cast<int32_t>(offsetof(CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD, ____charImage_8)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get__charImage_8() const { return ____charImage_8; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of__charImage_8() { return &____charImage_8; }
	inline void set__charImage_8(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		____charImage_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____charImage_8), (void*)value);
	}

	inline static int32_t get_offset_of__backgroundSprite_9() { return static_cast<int32_t>(offsetof(CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD, ____backgroundSprite_9)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get__backgroundSprite_9() const { return ____backgroundSprite_9; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of__backgroundSprite_9() { return &____backgroundSprite_9; }
	inline void set__backgroundSprite_9(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		____backgroundSprite_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____backgroundSprite_9), (void*)value);
	}

	inline static int32_t get_offset_of_isSelected_10() { return static_cast<int32_t>(offsetof(CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD, ___isSelected_10)); }
	inline bool get_isSelected_10() const { return ___isSelected_10; }
	inline bool* get_address_of_isSelected_10() { return &___isSelected_10; }
	inline void set_isSelected_10(bool value)
	{
		___isSelected_10 = value;
	}
};


// CharsGenerator
struct CharsGenerator_t7E18FC7617D416B9E55B664B885A3138E43EE2A2  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject CharsGenerator::_cellPrefab
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ____cellPrefab_4;
	// LevelProvider CharsGenerator::_levelProvider
	LevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841 * ____levelProvider_5;
	// UnityEngine.RectTransform CharsGenerator::_charsScene
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ____charsScene_6;
	// CharsContainer CharsGenerator::_charsContainer
	CharsContainer_tB2FB3D6B21722D0168C892A2C3837608C80C7CC5 * ____charsContainer_7;
	// System.Collections.Generic.List`1<CharControl> CharsGenerator::charsOnScene
	List_1_t37187EA3F5A7945D0B45C8A071194C1191FC32FE * ___charsOnScene_8;
	// System.Collections.Generic.List`1<CharsData> CharsGenerator::charsPrepareOnScene
	List_1_t49AE3466419291B57C0EF7E386E193A7AFE92C02 * ___charsPrepareOnScene_9;

public:
	inline static int32_t get_offset_of__cellPrefab_4() { return static_cast<int32_t>(offsetof(CharsGenerator_t7E18FC7617D416B9E55B664B885A3138E43EE2A2, ____cellPrefab_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get__cellPrefab_4() const { return ____cellPrefab_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of__cellPrefab_4() { return &____cellPrefab_4; }
	inline void set__cellPrefab_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		____cellPrefab_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____cellPrefab_4), (void*)value);
	}

	inline static int32_t get_offset_of__levelProvider_5() { return static_cast<int32_t>(offsetof(CharsGenerator_t7E18FC7617D416B9E55B664B885A3138E43EE2A2, ____levelProvider_5)); }
	inline LevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841 * get__levelProvider_5() const { return ____levelProvider_5; }
	inline LevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841 ** get_address_of__levelProvider_5() { return &____levelProvider_5; }
	inline void set__levelProvider_5(LevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841 * value)
	{
		____levelProvider_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____levelProvider_5), (void*)value);
	}

	inline static int32_t get_offset_of__charsScene_6() { return static_cast<int32_t>(offsetof(CharsGenerator_t7E18FC7617D416B9E55B664B885A3138E43EE2A2, ____charsScene_6)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get__charsScene_6() const { return ____charsScene_6; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of__charsScene_6() { return &____charsScene_6; }
	inline void set__charsScene_6(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		____charsScene_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____charsScene_6), (void*)value);
	}

	inline static int32_t get_offset_of__charsContainer_7() { return static_cast<int32_t>(offsetof(CharsGenerator_t7E18FC7617D416B9E55B664B885A3138E43EE2A2, ____charsContainer_7)); }
	inline CharsContainer_tB2FB3D6B21722D0168C892A2C3837608C80C7CC5 * get__charsContainer_7() const { return ____charsContainer_7; }
	inline CharsContainer_tB2FB3D6B21722D0168C892A2C3837608C80C7CC5 ** get_address_of__charsContainer_7() { return &____charsContainer_7; }
	inline void set__charsContainer_7(CharsContainer_tB2FB3D6B21722D0168C892A2C3837608C80C7CC5 * value)
	{
		____charsContainer_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____charsContainer_7), (void*)value);
	}

	inline static int32_t get_offset_of_charsOnScene_8() { return static_cast<int32_t>(offsetof(CharsGenerator_t7E18FC7617D416B9E55B664B885A3138E43EE2A2, ___charsOnScene_8)); }
	inline List_1_t37187EA3F5A7945D0B45C8A071194C1191FC32FE * get_charsOnScene_8() const { return ___charsOnScene_8; }
	inline List_1_t37187EA3F5A7945D0B45C8A071194C1191FC32FE ** get_address_of_charsOnScene_8() { return &___charsOnScene_8; }
	inline void set_charsOnScene_8(List_1_t37187EA3F5A7945D0B45C8A071194C1191FC32FE * value)
	{
		___charsOnScene_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___charsOnScene_8), (void*)value);
	}

	inline static int32_t get_offset_of_charsPrepareOnScene_9() { return static_cast<int32_t>(offsetof(CharsGenerator_t7E18FC7617D416B9E55B664B885A3138E43EE2A2, ___charsPrepareOnScene_9)); }
	inline List_1_t49AE3466419291B57C0EF7E386E193A7AFE92C02 * get_charsPrepareOnScene_9() const { return ___charsPrepareOnScene_9; }
	inline List_1_t49AE3466419291B57C0EF7E386E193A7AFE92C02 ** get_address_of_charsPrepareOnScene_9() { return &___charsPrepareOnScene_9; }
	inline void set_charsPrepareOnScene_9(List_1_t49AE3466419291B57C0EF7E386E193A7AFE92C02 * value)
	{
		___charsPrepareOnScene_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___charsPrepareOnScene_9), (void*)value);
	}
};


// LevelProvider
struct LevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Int32 LevelProvider::score
	int32_t ___score_4;
	// UnityEngine.UI.Text LevelProvider::_taskLabel
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ____taskLabel_5;
	// CharsGenerator LevelProvider::_charsGenerator
	CharsGenerator_t7E18FC7617D416B9E55B664B885A3138E43EE2A2 * ____charsGenerator_6;
	// UnityEngine.GameObject LevelProvider::_winPanel
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ____winPanel_7;
	// CharControl LevelProvider::_currentChar
	CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * ____currentChar_8;
	// System.Int32 LevelProvider::_currentCharsCount
	int32_t ____currentCharsCount_9;
	// System.Single LevelProvider::_fadeInDuration
	float ____fadeInDuration_10;

public:
	inline static int32_t get_offset_of_score_4() { return static_cast<int32_t>(offsetof(LevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841, ___score_4)); }
	inline int32_t get_score_4() const { return ___score_4; }
	inline int32_t* get_address_of_score_4() { return &___score_4; }
	inline void set_score_4(int32_t value)
	{
		___score_4 = value;
	}

	inline static int32_t get_offset_of__taskLabel_5() { return static_cast<int32_t>(offsetof(LevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841, ____taskLabel_5)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get__taskLabel_5() const { return ____taskLabel_5; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of__taskLabel_5() { return &____taskLabel_5; }
	inline void set__taskLabel_5(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		____taskLabel_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____taskLabel_5), (void*)value);
	}

	inline static int32_t get_offset_of__charsGenerator_6() { return static_cast<int32_t>(offsetof(LevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841, ____charsGenerator_6)); }
	inline CharsGenerator_t7E18FC7617D416B9E55B664B885A3138E43EE2A2 * get__charsGenerator_6() const { return ____charsGenerator_6; }
	inline CharsGenerator_t7E18FC7617D416B9E55B664B885A3138E43EE2A2 ** get_address_of__charsGenerator_6() { return &____charsGenerator_6; }
	inline void set__charsGenerator_6(CharsGenerator_t7E18FC7617D416B9E55B664B885A3138E43EE2A2 * value)
	{
		____charsGenerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____charsGenerator_6), (void*)value);
	}

	inline static int32_t get_offset_of__winPanel_7() { return static_cast<int32_t>(offsetof(LevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841, ____winPanel_7)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get__winPanel_7() const { return ____winPanel_7; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of__winPanel_7() { return &____winPanel_7; }
	inline void set__winPanel_7(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		____winPanel_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____winPanel_7), (void*)value);
	}

	inline static int32_t get_offset_of__currentChar_8() { return static_cast<int32_t>(offsetof(LevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841, ____currentChar_8)); }
	inline CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * get__currentChar_8() const { return ____currentChar_8; }
	inline CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD ** get_address_of__currentChar_8() { return &____currentChar_8; }
	inline void set__currentChar_8(CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * value)
	{
		____currentChar_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____currentChar_8), (void*)value);
	}

	inline static int32_t get_offset_of__currentCharsCount_9() { return static_cast<int32_t>(offsetof(LevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841, ____currentCharsCount_9)); }
	inline int32_t get__currentCharsCount_9() const { return ____currentCharsCount_9; }
	inline int32_t* get_address_of__currentCharsCount_9() { return &____currentCharsCount_9; }
	inline void set__currentCharsCount_9(int32_t value)
	{
		____currentCharsCount_9 = value;
	}

	inline static int32_t get_offset_of__fadeInDuration_10() { return static_cast<int32_t>(offsetof(LevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841, ____fadeInDuration_10)); }
	inline float get__fadeInDuration_10() const { return ____fadeInDuration_10; }
	inline float* get_address_of__fadeInDuration_10() { return &____fadeInDuration_10; }
	inline void set__fadeInDuration_10(float value)
	{
		____fadeInDuration_10 = value;
	}
};


// LoadingScreen
struct LoadingScreen_tD1CD7EA45A62C3A1A641D929E47F330644D353B2  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single LoadingScreen::_fadeInDuration
	float ____fadeInDuration_4;

public:
	inline static int32_t get_offset_of__fadeInDuration_4() { return static_cast<int32_t>(offsetof(LoadingScreen_tD1CD7EA45A62C3A1A641D929E47F330644D353B2, ____fadeInDuration_4)); }
	inline float get__fadeInDuration_4() const { return ____fadeInDuration_4; }
	inline float* get_address_of__fadeInDuration_4() { return &____fadeInDuration_4; }
	inline void set__fadeInDuration_4(float value)
	{
		____fadeInDuration_4 = value;
	}
};


// TweenEffect
struct TweenEffect_tB80E075E7B5F736D5D3E0BDAB6A253BF4DA1BBF1  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single TweenEffect::_bounceDuration
	float ____bounceDuration_4;
	// UnityEngine.Vector3 TweenEffect::_bounceStrenght
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ____bounceStrenght_5;
	// UnityEngine.Vector3 TweenEffect::_bounceStrongStrenght
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ____bounceStrongStrenght_6;

public:
	inline static int32_t get_offset_of__bounceDuration_4() { return static_cast<int32_t>(offsetof(TweenEffect_tB80E075E7B5F736D5D3E0BDAB6A253BF4DA1BBF1, ____bounceDuration_4)); }
	inline float get__bounceDuration_4() const { return ____bounceDuration_4; }
	inline float* get_address_of__bounceDuration_4() { return &____bounceDuration_4; }
	inline void set__bounceDuration_4(float value)
	{
		____bounceDuration_4 = value;
	}

	inline static int32_t get_offset_of__bounceStrenght_5() { return static_cast<int32_t>(offsetof(TweenEffect_tB80E075E7B5F736D5D3E0BDAB6A253BF4DA1BBF1, ____bounceStrenght_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get__bounceStrenght_5() const { return ____bounceStrenght_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of__bounceStrenght_5() { return &____bounceStrenght_5; }
	inline void set__bounceStrenght_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		____bounceStrenght_5 = value;
	}

	inline static int32_t get_offset_of__bounceStrongStrenght_6() { return static_cast<int32_t>(offsetof(TweenEffect_tB80E075E7B5F736D5D3E0BDAB6A253BF4DA1BBF1, ____bounceStrongStrenght_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get__bounceStrongStrenght_6() const { return ____bounceStrongStrenght_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of__bounceStrongStrenght_6() { return &____bounceStrongStrenght_6; }
	inline void set__bounceStrongStrenght_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		____bounceStrongStrenght_6 = value;
	}
};


// UnityEngine.EventSystems.UIBehaviour
struct UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// UnityEngine.UI.Graphic
struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipLayoutUpdate
	bool ___m_SkipLayoutUpdate_8;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipMaterialUpdate
	bool ___m_SkipMaterialUpdate_9;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_10;
	// UnityEngine.Vector4 UnityEngine.UI.Graphic::m_RaycastPadding
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___m_RaycastPadding_11;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_RectTransform_12;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * ___m_CanvasRenderer_13;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * ___m_Canvas_14;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_15;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_16;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyLayoutCallback_17;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyVertsCallback_18;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyMaterialCallback_19;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::m_CachedMesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___m_CachedMesh_22;
	// UnityEngine.Vector2[] UnityEngine.UI.Graphic::m_CachedUvs
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___m_CachedUvs_23;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * ___m_ColorTweenRunner_24;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25;

public:
	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Material_6)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Material_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_Color_7() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Color_7)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_Color_7() const { return ___m_Color_7; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_Color_7() { return &___m_Color_7; }
	inline void set_m_Color_7(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_Color_7 = value;
	}

	inline static int32_t get_offset_of_m_SkipLayoutUpdate_8() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_SkipLayoutUpdate_8)); }
	inline bool get_m_SkipLayoutUpdate_8() const { return ___m_SkipLayoutUpdate_8; }
	inline bool* get_address_of_m_SkipLayoutUpdate_8() { return &___m_SkipLayoutUpdate_8; }
	inline void set_m_SkipLayoutUpdate_8(bool value)
	{
		___m_SkipLayoutUpdate_8 = value;
	}

	inline static int32_t get_offset_of_m_SkipMaterialUpdate_9() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_SkipMaterialUpdate_9)); }
	inline bool get_m_SkipMaterialUpdate_9() const { return ___m_SkipMaterialUpdate_9; }
	inline bool* get_address_of_m_SkipMaterialUpdate_9() { return &___m_SkipMaterialUpdate_9; }
	inline void set_m_SkipMaterialUpdate_9(bool value)
	{
		___m_SkipMaterialUpdate_9 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_10() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RaycastTarget_10)); }
	inline bool get_m_RaycastTarget_10() const { return ___m_RaycastTarget_10; }
	inline bool* get_address_of_m_RaycastTarget_10() { return &___m_RaycastTarget_10; }
	inline void set_m_RaycastTarget_10(bool value)
	{
		___m_RaycastTarget_10 = value;
	}

	inline static int32_t get_offset_of_m_RaycastPadding_11() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RaycastPadding_11)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_m_RaycastPadding_11() const { return ___m_RaycastPadding_11; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_m_RaycastPadding_11() { return &___m_RaycastPadding_11; }
	inline void set_m_RaycastPadding_11(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___m_RaycastPadding_11 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_12() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RectTransform_12)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_RectTransform_12() const { return ___m_RectTransform_12; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_RectTransform_12() { return &___m_RectTransform_12; }
	inline void set_m_RectTransform_12(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_RectTransform_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RectTransform_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_13() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CanvasRenderer_13)); }
	inline CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * get_m_CanvasRenderer_13() const { return ___m_CanvasRenderer_13; }
	inline CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E ** get_address_of_m_CanvasRenderer_13() { return &___m_CanvasRenderer_13; }
	inline void set_m_CanvasRenderer_13(CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * value)
	{
		___m_CanvasRenderer_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CanvasRenderer_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_Canvas_14() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Canvas_14)); }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * get_m_Canvas_14() const { return ___m_Canvas_14; }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA ** get_address_of_m_Canvas_14() { return &___m_Canvas_14; }
	inline void set_m_Canvas_14(Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * value)
	{
		___m_Canvas_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Canvas_14), (void*)value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_15() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_VertsDirty_15)); }
	inline bool get_m_VertsDirty_15() const { return ___m_VertsDirty_15; }
	inline bool* get_address_of_m_VertsDirty_15() { return &___m_VertsDirty_15; }
	inline void set_m_VertsDirty_15(bool value)
	{
		___m_VertsDirty_15 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_16() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_MaterialDirty_16)); }
	inline bool get_m_MaterialDirty_16() const { return ___m_MaterialDirty_16; }
	inline bool* get_address_of_m_MaterialDirty_16() { return &___m_MaterialDirty_16; }
	inline void set_m_MaterialDirty_16(bool value)
	{
		___m_MaterialDirty_16 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_17() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyLayoutCallback_17)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyLayoutCallback_17() const { return ___m_OnDirtyLayoutCallback_17; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyLayoutCallback_17() { return &___m_OnDirtyLayoutCallback_17; }
	inline void set_m_OnDirtyLayoutCallback_17(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyLayoutCallback_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyLayoutCallback_17), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_18() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyVertsCallback_18)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyVertsCallback_18() const { return ___m_OnDirtyVertsCallback_18; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyVertsCallback_18() { return &___m_OnDirtyVertsCallback_18; }
	inline void set_m_OnDirtyVertsCallback_18(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyVertsCallback_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyVertsCallback_18), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_19() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyMaterialCallback_19)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyMaterialCallback_19() const { return ___m_OnDirtyMaterialCallback_19; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyMaterialCallback_19() { return &___m_OnDirtyMaterialCallback_19; }
	inline void set_m_OnDirtyMaterialCallback_19(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyMaterialCallback_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyMaterialCallback_19), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedMesh_22() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CachedMesh_22)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_m_CachedMesh_22() const { return ___m_CachedMesh_22; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_m_CachedMesh_22() { return &___m_CachedMesh_22; }
	inline void set_m_CachedMesh_22(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___m_CachedMesh_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedMesh_22), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedUvs_23() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CachedUvs_23)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_m_CachedUvs_23() const { return ___m_CachedUvs_23; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_m_CachedUvs_23() { return &___m_CachedUvs_23; }
	inline void set_m_CachedUvs_23(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___m_CachedUvs_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedUvs_23), (void*)value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_24() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_ColorTweenRunner_24)); }
	inline TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * get_m_ColorTweenRunner_24() const { return ___m_ColorTweenRunner_24; }
	inline TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 ** get_address_of_m_ColorTweenRunner_24() { return &___m_ColorTweenRunner_24; }
	inline void set_m_ColorTweenRunner_24(TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * value)
	{
		___m_ColorTweenRunner_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ColorTweenRunner_24), (void*)value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_25; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_25(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_25 = value;
	}
};

struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___s_Mesh_20;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * ___s_VertexHelper_21;

public:
	inline static int32_t get_offset_of_s_DefaultUI_4() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_DefaultUI_4)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_DefaultUI_4() const { return ___s_DefaultUI_4; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_DefaultUI_4() { return &___s_DefaultUI_4; }
	inline void set_s_DefaultUI_4(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_DefaultUI_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultUI_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_5() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_WhiteTexture_5)); }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * get_s_WhiteTexture_5() const { return ___s_WhiteTexture_5; }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** get_address_of_s_WhiteTexture_5() { return &___s_WhiteTexture_5; }
	inline void set_s_WhiteTexture_5(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * value)
	{
		___s_WhiteTexture_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_WhiteTexture_5), (void*)value);
	}

	inline static int32_t get_offset_of_s_Mesh_20() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_Mesh_20)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_s_Mesh_20() const { return ___s_Mesh_20; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_s_Mesh_20() { return &___s_Mesh_20; }
	inline void set_s_Mesh_20(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___s_Mesh_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Mesh_20), (void*)value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_21() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_VertexHelper_21)); }
	inline VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * get_s_VertexHelper_21() const { return ___s_VertexHelper_21; }
	inline VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 ** get_address_of_s_VertexHelper_21() { return &___s_VertexHelper_21; }
	inline void set_s_VertexHelper_21(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * value)
	{
		___s_VertexHelper_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_VertexHelper_21), (void*)value);
	}
};


// UnityEngine.UI.Selectable
struct Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:
	// System.Boolean UnityEngine.UI.Selectable::m_EnableCalled
	bool ___m_EnableCalled_6;
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A  ___m_Navigation_7;
	// UnityEngine.UI.Selectable/Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_8;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  ___m_Colors_9;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E  ___m_SpriteState_10;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 * ___m_AnimationTriggers_11;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_12;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * ___m_TargetGraphic_13;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_14;
	// System.Int32 UnityEngine.UI.Selectable::m_CurrentIndex
	int32_t ___m_CurrentIndex_15;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_16;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_17;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_18;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t34AA4AF4E7352129CA58045901530E41445AC16D * ___m_CanvasGroupCache_19;

public:
	inline static int32_t get_offset_of_m_EnableCalled_6() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_EnableCalled_6)); }
	inline bool get_m_EnableCalled_6() const { return ___m_EnableCalled_6; }
	inline bool* get_address_of_m_EnableCalled_6() { return &___m_EnableCalled_6; }
	inline void set_m_EnableCalled_6(bool value)
	{
		___m_EnableCalled_6 = value;
	}

	inline static int32_t get_offset_of_m_Navigation_7() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Navigation_7)); }
	inline Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A  get_m_Navigation_7() const { return ___m_Navigation_7; }
	inline Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A * get_address_of_m_Navigation_7() { return &___m_Navigation_7; }
	inline void set_m_Navigation_7(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A  value)
	{
		___m_Navigation_7 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnUp_2), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnDown_3), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnLeft_4), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnRight_5), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_Transition_8() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Transition_8)); }
	inline int32_t get_m_Transition_8() const { return ___m_Transition_8; }
	inline int32_t* get_address_of_m_Transition_8() { return &___m_Transition_8; }
	inline void set_m_Transition_8(int32_t value)
	{
		___m_Transition_8 = value;
	}

	inline static int32_t get_offset_of_m_Colors_9() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Colors_9)); }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  get_m_Colors_9() const { return ___m_Colors_9; }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955 * get_address_of_m_Colors_9() { return &___m_Colors_9; }
	inline void set_m_Colors_9(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  value)
	{
		___m_Colors_9 = value;
	}

	inline static int32_t get_offset_of_m_SpriteState_10() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_SpriteState_10)); }
	inline SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E  get_m_SpriteState_10() const { return ___m_SpriteState_10; }
	inline SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E * get_address_of_m_SpriteState_10() { return &___m_SpriteState_10; }
	inline void set_m_SpriteState_10(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E  value)
	{
		___m_SpriteState_10 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_HighlightedSprite_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_PressedSprite_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_SelectedSprite_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_DisabledSprite_3), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_AnimationTriggers_11() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_AnimationTriggers_11)); }
	inline AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 * get_m_AnimationTriggers_11() const { return ___m_AnimationTriggers_11; }
	inline AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 ** get_address_of_m_AnimationTriggers_11() { return &___m_AnimationTriggers_11; }
	inline void set_m_AnimationTriggers_11(AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 * value)
	{
		___m_AnimationTriggers_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AnimationTriggers_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_Interactable_12() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Interactable_12)); }
	inline bool get_m_Interactable_12() const { return ___m_Interactable_12; }
	inline bool* get_address_of_m_Interactable_12() { return &___m_Interactable_12; }
	inline void set_m_Interactable_12(bool value)
	{
		___m_Interactable_12 = value;
	}

	inline static int32_t get_offset_of_m_TargetGraphic_13() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_TargetGraphic_13)); }
	inline Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * get_m_TargetGraphic_13() const { return ___m_TargetGraphic_13; }
	inline Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 ** get_address_of_m_TargetGraphic_13() { return &___m_TargetGraphic_13; }
	inline void set_m_TargetGraphic_13(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * value)
	{
		___m_TargetGraphic_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TargetGraphic_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_GroupsAllowInteraction_14() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_GroupsAllowInteraction_14)); }
	inline bool get_m_GroupsAllowInteraction_14() const { return ___m_GroupsAllowInteraction_14; }
	inline bool* get_address_of_m_GroupsAllowInteraction_14() { return &___m_GroupsAllowInteraction_14; }
	inline void set_m_GroupsAllowInteraction_14(bool value)
	{
		___m_GroupsAllowInteraction_14 = value;
	}

	inline static int32_t get_offset_of_m_CurrentIndex_15() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_CurrentIndex_15)); }
	inline int32_t get_m_CurrentIndex_15() const { return ___m_CurrentIndex_15; }
	inline int32_t* get_address_of_m_CurrentIndex_15() { return &___m_CurrentIndex_15; }
	inline void set_m_CurrentIndex_15(int32_t value)
	{
		___m_CurrentIndex_15 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerInsideU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___U3CisPointerInsideU3Ek__BackingField_16)); }
	inline bool get_U3CisPointerInsideU3Ek__BackingField_16() const { return ___U3CisPointerInsideU3Ek__BackingField_16; }
	inline bool* get_address_of_U3CisPointerInsideU3Ek__BackingField_16() { return &___U3CisPointerInsideU3Ek__BackingField_16; }
	inline void set_U3CisPointerInsideU3Ek__BackingField_16(bool value)
	{
		___U3CisPointerInsideU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerDownU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___U3CisPointerDownU3Ek__BackingField_17)); }
	inline bool get_U3CisPointerDownU3Ek__BackingField_17() const { return ___U3CisPointerDownU3Ek__BackingField_17; }
	inline bool* get_address_of_U3CisPointerDownU3Ek__BackingField_17() { return &___U3CisPointerDownU3Ek__BackingField_17; }
	inline void set_U3CisPointerDownU3Ek__BackingField_17(bool value)
	{
		___U3CisPointerDownU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3ChasSelectionU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___U3ChasSelectionU3Ek__BackingField_18)); }
	inline bool get_U3ChasSelectionU3Ek__BackingField_18() const { return ___U3ChasSelectionU3Ek__BackingField_18; }
	inline bool* get_address_of_U3ChasSelectionU3Ek__BackingField_18() { return &___U3ChasSelectionU3Ek__BackingField_18; }
	inline void set_U3ChasSelectionU3Ek__BackingField_18(bool value)
	{
		___U3ChasSelectionU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroupCache_19() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_CanvasGroupCache_19)); }
	inline List_1_t34AA4AF4E7352129CA58045901530E41445AC16D * get_m_CanvasGroupCache_19() const { return ___m_CanvasGroupCache_19; }
	inline List_1_t34AA4AF4E7352129CA58045901530E41445AC16D ** get_address_of_m_CanvasGroupCache_19() { return &___m_CanvasGroupCache_19; }
	inline void set_m_CanvasGroupCache_19(List_1_t34AA4AF4E7352129CA58045901530E41445AC16D * value)
	{
		___m_CanvasGroupCache_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CanvasGroupCache_19), (void*)value);
	}
};

struct Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD_StaticFields
{
public:
	// UnityEngine.UI.Selectable[] UnityEngine.UI.Selectable::s_Selectables
	SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535* ___s_Selectables_4;
	// System.Int32 UnityEngine.UI.Selectable::s_SelectableCount
	int32_t ___s_SelectableCount_5;

public:
	inline static int32_t get_offset_of_s_Selectables_4() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD_StaticFields, ___s_Selectables_4)); }
	inline SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535* get_s_Selectables_4() const { return ___s_Selectables_4; }
	inline SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535** get_address_of_s_Selectables_4() { return &___s_Selectables_4; }
	inline void set_s_Selectables_4(SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535* value)
	{
		___s_Selectables_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Selectables_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_SelectableCount_5() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD_StaticFields, ___s_SelectableCount_5)); }
	inline int32_t get_s_SelectableCount_5() const { return ___s_SelectableCount_5; }
	inline int32_t* get_address_of_s_SelectableCount_5() { return &___s_SelectableCount_5; }
	inline void set_s_SelectableCount_5(int32_t value)
	{
		___s_SelectableCount_5 = value;
	}
};


// UnityEngine.UI.Button
struct Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D  : public Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD
{
public:
	// UnityEngine.UI.Button/ButtonClickedEvent UnityEngine.UI.Button::m_OnClick
	ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F * ___m_OnClick_20;

public:
	inline static int32_t get_offset_of_m_OnClick_20() { return static_cast<int32_t>(offsetof(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D, ___m_OnClick_20)); }
	inline ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F * get_m_OnClick_20() const { return ___m_OnClick_20; }
	inline ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F ** get_address_of_m_OnClick_20() { return &___m_OnClick_20; }
	inline void set_m_OnClick_20(ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F * value)
	{
		___m_OnClick_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnClick_20), (void*)value);
	}
};


// UnityEngine.UI.MaskableGraphic
struct MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE  : public Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_26;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_MaskMaterial_27;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * ___m_ParentMask_28;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_29;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IsMaskingGraphic
	bool ___m_IsMaskingGraphic_30;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_31;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * ___m_OnCullStateChanged_32;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_33;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_34;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___m_Corners_35;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ShouldRecalculateStencil_26)); }
	inline bool get_m_ShouldRecalculateStencil_26() const { return ___m_ShouldRecalculateStencil_26; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_26() { return &___m_ShouldRecalculateStencil_26; }
	inline void set_m_ShouldRecalculateStencil_26(bool value)
	{
		___m_ShouldRecalculateStencil_26 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_MaskMaterial_27)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_MaskMaterial_27() const { return ___m_MaskMaterial_27; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_MaskMaterial_27() { return &___m_MaskMaterial_27; }
	inline void set_m_MaskMaterial_27(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_MaskMaterial_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_MaskMaterial_27), (void*)value);
	}

	inline static int32_t get_offset_of_m_ParentMask_28() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ParentMask_28)); }
	inline RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * get_m_ParentMask_28() const { return ___m_ParentMask_28; }
	inline RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 ** get_address_of_m_ParentMask_28() { return &___m_ParentMask_28; }
	inline void set_m_ParentMask_28(RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * value)
	{
		___m_ParentMask_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParentMask_28), (void*)value);
	}

	inline static int32_t get_offset_of_m_Maskable_29() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_Maskable_29)); }
	inline bool get_m_Maskable_29() const { return ___m_Maskable_29; }
	inline bool* get_address_of_m_Maskable_29() { return &___m_Maskable_29; }
	inline void set_m_Maskable_29(bool value)
	{
		___m_Maskable_29 = value;
	}

	inline static int32_t get_offset_of_m_IsMaskingGraphic_30() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_IsMaskingGraphic_30)); }
	inline bool get_m_IsMaskingGraphic_30() const { return ___m_IsMaskingGraphic_30; }
	inline bool* get_address_of_m_IsMaskingGraphic_30() { return &___m_IsMaskingGraphic_30; }
	inline void set_m_IsMaskingGraphic_30(bool value)
	{
		___m_IsMaskingGraphic_30 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_31() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_IncludeForMasking_31)); }
	inline bool get_m_IncludeForMasking_31() const { return ___m_IncludeForMasking_31; }
	inline bool* get_address_of_m_IncludeForMasking_31() { return &___m_IncludeForMasking_31; }
	inline void set_m_IncludeForMasking_31(bool value)
	{
		___m_IncludeForMasking_31 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_32() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_OnCullStateChanged_32)); }
	inline CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * get_m_OnCullStateChanged_32() const { return ___m_OnCullStateChanged_32; }
	inline CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 ** get_address_of_m_OnCullStateChanged_32() { return &___m_OnCullStateChanged_32; }
	inline void set_m_OnCullStateChanged_32(CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * value)
	{
		___m_OnCullStateChanged_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnCullStateChanged_32), (void*)value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_33() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ShouldRecalculate_33)); }
	inline bool get_m_ShouldRecalculate_33() const { return ___m_ShouldRecalculate_33; }
	inline bool* get_address_of_m_ShouldRecalculate_33() { return &___m_ShouldRecalculate_33; }
	inline void set_m_ShouldRecalculate_33(bool value)
	{
		___m_ShouldRecalculate_33 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_34() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_StencilValue_34)); }
	inline int32_t get_m_StencilValue_34() const { return ___m_StencilValue_34; }
	inline int32_t* get_address_of_m_StencilValue_34() { return &___m_StencilValue_34; }
	inline void set_m_StencilValue_34(int32_t value)
	{
		___m_StencilValue_34 = value;
	}

	inline static int32_t get_offset_of_m_Corners_35() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_Corners_35)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_m_Corners_35() const { return ___m_Corners_35; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_m_Corners_35() { return &___m_Corners_35; }
	inline void set_m_Corners_35(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___m_Corners_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Corners_35), (void*)value);
	}
};


// UnityEngine.UI.Image
struct Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C  : public MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE
{
public:
	// UnityEngine.Sprite UnityEngine.UI.Image::m_Sprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_Sprite_37;
	// UnityEngine.Sprite UnityEngine.UI.Image::m_OverrideSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_OverrideSprite_38;
	// UnityEngine.UI.Image/Type UnityEngine.UI.Image::m_Type
	int32_t ___m_Type_39;
	// System.Boolean UnityEngine.UI.Image::m_PreserveAspect
	bool ___m_PreserveAspect_40;
	// System.Boolean UnityEngine.UI.Image::m_FillCenter
	bool ___m_FillCenter_41;
	// UnityEngine.UI.Image/FillMethod UnityEngine.UI.Image::m_FillMethod
	int32_t ___m_FillMethod_42;
	// System.Single UnityEngine.UI.Image::m_FillAmount
	float ___m_FillAmount_43;
	// System.Boolean UnityEngine.UI.Image::m_FillClockwise
	bool ___m_FillClockwise_44;
	// System.Int32 UnityEngine.UI.Image::m_FillOrigin
	int32_t ___m_FillOrigin_45;
	// System.Single UnityEngine.UI.Image::m_AlphaHitTestMinimumThreshold
	float ___m_AlphaHitTestMinimumThreshold_46;
	// System.Boolean UnityEngine.UI.Image::m_Tracked
	bool ___m_Tracked_47;
	// System.Boolean UnityEngine.UI.Image::m_UseSpriteMesh
	bool ___m_UseSpriteMesh_48;
	// System.Single UnityEngine.UI.Image::m_PixelsPerUnitMultiplier
	float ___m_PixelsPerUnitMultiplier_49;
	// System.Single UnityEngine.UI.Image::m_CachedReferencePixelsPerUnit
	float ___m_CachedReferencePixelsPerUnit_50;

public:
	inline static int32_t get_offset_of_m_Sprite_37() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_Sprite_37)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_Sprite_37() const { return ___m_Sprite_37; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_Sprite_37() { return &___m_Sprite_37; }
	inline void set_m_Sprite_37(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_Sprite_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Sprite_37), (void*)value);
	}

	inline static int32_t get_offset_of_m_OverrideSprite_38() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_OverrideSprite_38)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_OverrideSprite_38() const { return ___m_OverrideSprite_38; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_OverrideSprite_38() { return &___m_OverrideSprite_38; }
	inline void set_m_OverrideSprite_38(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_OverrideSprite_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OverrideSprite_38), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type_39() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_Type_39)); }
	inline int32_t get_m_Type_39() const { return ___m_Type_39; }
	inline int32_t* get_address_of_m_Type_39() { return &___m_Type_39; }
	inline void set_m_Type_39(int32_t value)
	{
		___m_Type_39 = value;
	}

	inline static int32_t get_offset_of_m_PreserveAspect_40() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_PreserveAspect_40)); }
	inline bool get_m_PreserveAspect_40() const { return ___m_PreserveAspect_40; }
	inline bool* get_address_of_m_PreserveAspect_40() { return &___m_PreserveAspect_40; }
	inline void set_m_PreserveAspect_40(bool value)
	{
		___m_PreserveAspect_40 = value;
	}

	inline static int32_t get_offset_of_m_FillCenter_41() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillCenter_41)); }
	inline bool get_m_FillCenter_41() const { return ___m_FillCenter_41; }
	inline bool* get_address_of_m_FillCenter_41() { return &___m_FillCenter_41; }
	inline void set_m_FillCenter_41(bool value)
	{
		___m_FillCenter_41 = value;
	}

	inline static int32_t get_offset_of_m_FillMethod_42() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillMethod_42)); }
	inline int32_t get_m_FillMethod_42() const { return ___m_FillMethod_42; }
	inline int32_t* get_address_of_m_FillMethod_42() { return &___m_FillMethod_42; }
	inline void set_m_FillMethod_42(int32_t value)
	{
		___m_FillMethod_42 = value;
	}

	inline static int32_t get_offset_of_m_FillAmount_43() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillAmount_43)); }
	inline float get_m_FillAmount_43() const { return ___m_FillAmount_43; }
	inline float* get_address_of_m_FillAmount_43() { return &___m_FillAmount_43; }
	inline void set_m_FillAmount_43(float value)
	{
		___m_FillAmount_43 = value;
	}

	inline static int32_t get_offset_of_m_FillClockwise_44() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillClockwise_44)); }
	inline bool get_m_FillClockwise_44() const { return ___m_FillClockwise_44; }
	inline bool* get_address_of_m_FillClockwise_44() { return &___m_FillClockwise_44; }
	inline void set_m_FillClockwise_44(bool value)
	{
		___m_FillClockwise_44 = value;
	}

	inline static int32_t get_offset_of_m_FillOrigin_45() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillOrigin_45)); }
	inline int32_t get_m_FillOrigin_45() const { return ___m_FillOrigin_45; }
	inline int32_t* get_address_of_m_FillOrigin_45() { return &___m_FillOrigin_45; }
	inline void set_m_FillOrigin_45(int32_t value)
	{
		___m_FillOrigin_45 = value;
	}

	inline static int32_t get_offset_of_m_AlphaHitTestMinimumThreshold_46() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_AlphaHitTestMinimumThreshold_46)); }
	inline float get_m_AlphaHitTestMinimumThreshold_46() const { return ___m_AlphaHitTestMinimumThreshold_46; }
	inline float* get_address_of_m_AlphaHitTestMinimumThreshold_46() { return &___m_AlphaHitTestMinimumThreshold_46; }
	inline void set_m_AlphaHitTestMinimumThreshold_46(float value)
	{
		___m_AlphaHitTestMinimumThreshold_46 = value;
	}

	inline static int32_t get_offset_of_m_Tracked_47() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_Tracked_47)); }
	inline bool get_m_Tracked_47() const { return ___m_Tracked_47; }
	inline bool* get_address_of_m_Tracked_47() { return &___m_Tracked_47; }
	inline void set_m_Tracked_47(bool value)
	{
		___m_Tracked_47 = value;
	}

	inline static int32_t get_offset_of_m_UseSpriteMesh_48() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_UseSpriteMesh_48)); }
	inline bool get_m_UseSpriteMesh_48() const { return ___m_UseSpriteMesh_48; }
	inline bool* get_address_of_m_UseSpriteMesh_48() { return &___m_UseSpriteMesh_48; }
	inline void set_m_UseSpriteMesh_48(bool value)
	{
		___m_UseSpriteMesh_48 = value;
	}

	inline static int32_t get_offset_of_m_PixelsPerUnitMultiplier_49() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_PixelsPerUnitMultiplier_49)); }
	inline float get_m_PixelsPerUnitMultiplier_49() const { return ___m_PixelsPerUnitMultiplier_49; }
	inline float* get_address_of_m_PixelsPerUnitMultiplier_49() { return &___m_PixelsPerUnitMultiplier_49; }
	inline void set_m_PixelsPerUnitMultiplier_49(float value)
	{
		___m_PixelsPerUnitMultiplier_49 = value;
	}

	inline static int32_t get_offset_of_m_CachedReferencePixelsPerUnit_50() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_CachedReferencePixelsPerUnit_50)); }
	inline float get_m_CachedReferencePixelsPerUnit_50() const { return ___m_CachedReferencePixelsPerUnit_50; }
	inline float* get_address_of_m_CachedReferencePixelsPerUnit_50() { return &___m_CachedReferencePixelsPerUnit_50; }
	inline void set_m_CachedReferencePixelsPerUnit_50(float value)
	{
		___m_CachedReferencePixelsPerUnit_50 = value;
	}
};

struct Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Image::s_ETC1DefaultUI
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_ETC1DefaultUI_36;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_VertScratch
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___s_VertScratch_51;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_UVScratch
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___s_UVScratch_52;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Xy
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___s_Xy_53;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Uv
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___s_Uv_54;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> UnityEngine.UI.Image::m_TrackedTexturelessImages
	List_1_t815A476B0A21E183042059E705F9E505478CD8AE * ___m_TrackedTexturelessImages_55;
	// System.Boolean UnityEngine.UI.Image::s_Initialized
	bool ___s_Initialized_56;

public:
	inline static int32_t get_offset_of_s_ETC1DefaultUI_36() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_ETC1DefaultUI_36)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_ETC1DefaultUI_36() const { return ___s_ETC1DefaultUI_36; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_ETC1DefaultUI_36() { return &___s_ETC1DefaultUI_36; }
	inline void set_s_ETC1DefaultUI_36(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_ETC1DefaultUI_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ETC1DefaultUI_36), (void*)value);
	}

	inline static int32_t get_offset_of_s_VertScratch_51() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_VertScratch_51)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_s_VertScratch_51() const { return ___s_VertScratch_51; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_s_VertScratch_51() { return &___s_VertScratch_51; }
	inline void set_s_VertScratch_51(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___s_VertScratch_51 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_VertScratch_51), (void*)value);
	}

	inline static int32_t get_offset_of_s_UVScratch_52() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_UVScratch_52)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_s_UVScratch_52() const { return ___s_UVScratch_52; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_s_UVScratch_52() { return &___s_UVScratch_52; }
	inline void set_s_UVScratch_52(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___s_UVScratch_52 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_UVScratch_52), (void*)value);
	}

	inline static int32_t get_offset_of_s_Xy_53() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_Xy_53)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_s_Xy_53() const { return ___s_Xy_53; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_s_Xy_53() { return &___s_Xy_53; }
	inline void set_s_Xy_53(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___s_Xy_53 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Xy_53), (void*)value);
	}

	inline static int32_t get_offset_of_s_Uv_54() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_Uv_54)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_s_Uv_54() const { return ___s_Uv_54; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_s_Uv_54() { return &___s_Uv_54; }
	inline void set_s_Uv_54(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___s_Uv_54 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Uv_54), (void*)value);
	}

	inline static int32_t get_offset_of_m_TrackedTexturelessImages_55() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___m_TrackedTexturelessImages_55)); }
	inline List_1_t815A476B0A21E183042059E705F9E505478CD8AE * get_m_TrackedTexturelessImages_55() const { return ___m_TrackedTexturelessImages_55; }
	inline List_1_t815A476B0A21E183042059E705F9E505478CD8AE ** get_address_of_m_TrackedTexturelessImages_55() { return &___m_TrackedTexturelessImages_55; }
	inline void set_m_TrackedTexturelessImages_55(List_1_t815A476B0A21E183042059E705F9E505478CD8AE * value)
	{
		___m_TrackedTexturelessImages_55 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TrackedTexturelessImages_55), (void*)value);
	}

	inline static int32_t get_offset_of_s_Initialized_56() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_Initialized_56)); }
	inline bool get_s_Initialized_56() const { return ___s_Initialized_56; }
	inline bool* get_address_of_s_Initialized_56() { return &___s_Initialized_56; }
	inline void set_s_Initialized_56(bool value)
	{
		___s_Initialized_56 = value;
	}
};


// UnityEngine.UI.Text
struct Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1  : public MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE
{
public:
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 * ___m_FontData_36;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_37;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * ___m_TextCache_38;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * ___m_TextCacheForLayout_39;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_41;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* ___m_TempVerts_42;

public:
	inline static int32_t get_offset_of_m_FontData_36() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_FontData_36)); }
	inline FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 * get_m_FontData_36() const { return ___m_FontData_36; }
	inline FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 ** get_address_of_m_FontData_36() { return &___m_FontData_36; }
	inline void set_m_FontData_36(FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 * value)
	{
		___m_FontData_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_FontData_36), (void*)value);
	}

	inline static int32_t get_offset_of_m_Text_37() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_Text_37)); }
	inline String_t* get_m_Text_37() const { return ___m_Text_37; }
	inline String_t** get_address_of_m_Text_37() { return &___m_Text_37; }
	inline void set_m_Text_37(String_t* value)
	{
		___m_Text_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Text_37), (void*)value);
	}

	inline static int32_t get_offset_of_m_TextCache_38() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_TextCache_38)); }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * get_m_TextCache_38() const { return ___m_TextCache_38; }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 ** get_address_of_m_TextCache_38() { return &___m_TextCache_38; }
	inline void set_m_TextCache_38(TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * value)
	{
		___m_TextCache_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextCache_38), (void*)value);
	}

	inline static int32_t get_offset_of_m_TextCacheForLayout_39() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_TextCacheForLayout_39)); }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * get_m_TextCacheForLayout_39() const { return ___m_TextCacheForLayout_39; }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 ** get_address_of_m_TextCacheForLayout_39() { return &___m_TextCacheForLayout_39; }
	inline void set_m_TextCacheForLayout_39(TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * value)
	{
		___m_TextCacheForLayout_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextCacheForLayout_39), (void*)value);
	}

	inline static int32_t get_offset_of_m_DisableFontTextureRebuiltCallback_41() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_DisableFontTextureRebuiltCallback_41)); }
	inline bool get_m_DisableFontTextureRebuiltCallback_41() const { return ___m_DisableFontTextureRebuiltCallback_41; }
	inline bool* get_address_of_m_DisableFontTextureRebuiltCallback_41() { return &___m_DisableFontTextureRebuiltCallback_41; }
	inline void set_m_DisableFontTextureRebuiltCallback_41(bool value)
	{
		___m_DisableFontTextureRebuiltCallback_41 = value;
	}

	inline static int32_t get_offset_of_m_TempVerts_42() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_TempVerts_42)); }
	inline UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* get_m_TempVerts_42() const { return ___m_TempVerts_42; }
	inline UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A** get_address_of_m_TempVerts_42() { return &___m_TempVerts_42; }
	inline void set_m_TempVerts_42(UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* value)
	{
		___m_TempVerts_42 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TempVerts_42), (void*)value);
	}
};

struct Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_DefaultText_40;

public:
	inline static int32_t get_offset_of_s_DefaultText_40() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_StaticFields, ___s_DefaultText_40)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_DefaultText_40() const { return ___s_DefaultText_40; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_DefaultText_40() { return &___s_DefaultText_40; }
	inline void set_s_DefaultText_40(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_DefaultText_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultText_40), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// CharsData[]
struct CharsDataU5BU5D_t6E1A2B31C527A76C761C25915A588ED83C6B4CDE  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595 * m_Items[1];

public:
	inline CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// CharsBundleData[]
struct CharsBundleDataU5BU5D_t49861CE889FB0A7632E6FD7126DE29B0F470F700  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) CharsBundleData_tC2C62F2A7FAEEDB189C65454E3E32437ECD36285 * m_Items[1];

public:
	inline CharsBundleData_tC2C62F2A7FAEEDB189C65454E3E32437ECD36285 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline CharsBundleData_tC2C62F2A7FAEEDB189C65454E3E32437ECD36285 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, CharsBundleData_tC2C62F2A7FAEEDB189C65454E3E32437ECD36285 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline CharsBundleData_tC2C62F2A7FAEEDB189C65454E3E32437ECD36285 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline CharsBundleData_tC2C62F2A7FAEEDB189C65454E3E32437ECD36285 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, CharsBundleData_tC2C62F2A7FAEEDB189C65454E3E32437ECD36285 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Reflection.Assembly[]
struct AssemblyU5BU5D_t42061B4CA43487DD8ECD8C3AACCEF783FA081FF0  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Assembly_t * m_Items[1];

public:
	inline Assembly_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Assembly_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Assembly_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Assembly_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Assembly_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Assembly_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// !!0 UnityEngine.Component::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_mDC2250CC3F24F6FE45660AF6153056ABDA5ED60F_gshared (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// T GlobalFunctions::GetComponentObject<System.Object>(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GlobalFunctions_GetComponentObject_TisRuntimeObject_m1E81DA43408068B8C6627D97C1994CBC8A3A541F_gshared (String_t* ___tag0, const RuntimeMethod* method);
// !!0[] UnityEngine.Resources::LoadAll<System.Object>(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* Resources_LoadAll_TisRuntimeObject_m346B17472FD075C67F81BFA5A7D454944296C618_gshared (String_t* ___path0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Clear_m5FB5A9C59D8625FDFB06876C4D8848F0F07ABFD0_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// System.Void System.Func`2<System.Object,System.Boolean>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_mCA84157864A199574AD0B7F3083F99B54DC1F98C_gshared (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Enumerable_Where_TisRuntimeObject_mD8AE6780E78249FC87B2344E09D130624E70D7DA_gshared (RuntimeObject* ___source0, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate1, const RuntimeMethod* method);
// !!0[] System.Linq.Enumerable::ToArray<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* Enumerable_ToArray_TisRuntimeObject_mEB06425105813A21FC826C4144F8456EAE2304DE_gshared (RuntimeObject* ___source0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Add_mF15250BF947CA27BE9A23C08BAC6DB6F180B0EDD_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, RuntimeObject * ___item0, const RuntimeMethod* method);
// !0[] System.Collections.Generic.List`1<System.Object>::ToArray()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* List_1_ToArray_mC6E0B3CF74090974475F845BF79EC5E66D3A71AC_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0,UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Object_Instantiate_TisRuntimeObject_m78E51E899A91F621B74B2CF3D5081EF822049DA3_gshared (RuntimeObject * ___original0, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___parent1, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GameObject_GetComponent_TisRuntimeObject_m533C28B362284747FD5138B02D183642545146E8_gshared (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// System.Int32 System.Linq.Enumerable::Count<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Enumerable_Count_TisRuntimeObject_m1A161C58BCDDFCF3A206ED7DFBEB0F9231B18AE3_gshared (RuntimeObject* ___source0, const RuntimeMethod* method);
// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOGetter_1__ctor_m7537F12632FAD0C4475AC13323E07B2D2C6FF081_gshared (DOGetter_1_tE8D2419A0219EB945463B02351F4905C77235E1A * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOSetter_1__ctor_m23DF8B62FE29CBDA90726662AEA2755DF765E733_gshared (DOSetter_1_tAEE38AA9321AE4A0A4A4E5D496EA753BFB746B85 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// DG.Tweening.Core.TweenerCore`3<!!0,!!1,!!2> DG.Tweening.DOTween::To<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>(DG.Tweening.Plugins.Core.ABSTweenPlugin`3<!!0,!!1,!!2>,DG.Tweening.Core.DOGetter`1<!!0>,DG.Tweening.Core.DOSetter`1<!!0>,!!1,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TweenerCore_3_t3456DC6DCE3C72CB0D8A037FE930A2C2D0B92569 * DOTween_To_TisVector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_TisRuntimeObject_TisPathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A_m6B0079AA9B9A8A3B225325A3B13147B6684B2303_gshared (ABSTweenPlugin_3_tB684BB30E8E054CCB3EBBFB96EDE8032FBD0284E * ___plugin0, DOGetter_1_tE8D2419A0219EB945463B02351F4905C77235E1A * ___getter1, DOSetter_1_tAEE38AA9321AE4A0A4A4E5D496EA753BFB746B85 * ___setter2, RuntimeObject * ___endValue3, float ___duration4, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::SetTarget<System.Object>(!!0,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * TweenSettingsExtensions_SetTarget_TisRuntimeObject_m686297D300C2FE2F7C5430CF1AFA860E45801292_gshared (RuntimeObject * ___t0, RuntimeObject * ___target1, const RuntimeMethod* method);
// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Color>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOGetter_1__ctor_mAC32C501C09F9F6EC5B0DD2C1A8473BAE88DC8CE_gshared (DOGetter_1_tCC9DEA9BB7DA5C2570ED2CE9BD13EF5C154B7B10 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Color>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOSetter_1__ctor_m41D0232769B743141B4389F8A4599EC78F310352_gshared (DOSetter_1_tB62013E4617ECB51822384C8592AAB764E1662BF * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void System.Action`4<DG.Tweening.Plugins.Options.PathOptions,System.Object,UnityEngine.Quaternion,System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_4__ctor_m48578D4B8ED1C72857654C84320B364BD2E39E30_gshared (Action_4_t0E97DD6848031923736201DE6A983F114E84EFFF * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  List_1_GetEnumerator_m6339FC2D3D1CE4FA13CF21C7F9FC58CA4441BF0C_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_m4C91D0E84532DF10C654917487D82CB0AB27693B_gshared_inline (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0_gshared (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enumerator_Dispose_mCFB225D9E5E597A1CC8F958E53BEA1367D8AC7B8_gshared (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method);

// System.String CharsData::get_Identifier()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* CharsData_get_Identifier_mC20D9B2E42F23E7CFA66FA65141011F719AB2681_inline (CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Object::set_name(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_set_name_m87C4006618ADB325ABE5439DF159E10DD8DD0781 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * __this, String_t* ___value0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Button>()
inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * Component_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mED8C9575844B41F67CB4C2B13685FC0174CB61BC (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_mDC2250CC3F24F6FE45660AF6153056ABDA5ED60F_gshared)(__this, method);
}
// UnityEngine.UI.Button/ButtonClickedEvent UnityEngine.UI.Button::get_onClick()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F * Button_get_onClick_m28BD8C670676D4E2B292B5A7F59387D4BF61F8F4_inline (Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityAction__ctor_m48C04C4C0F46918CF216A2410A4E58D31B6362BA (UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent::AddListener(UnityEngine.Events.UnityAction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityEvent_AddListener_m0ACFF0706176ECCB20E0BC2542D07396616F436D (UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * __this, UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___call0, const RuntimeMethod* method);
// T GlobalFunctions::GetComponentObject<LevelProvider>(System.String)
inline LevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841 * GlobalFunctions_GetComponentObject_TisLevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841_m2F25E5DB707F42022C693C71FCF04CC50FEE9D25 (String_t* ___tag0, const RuntimeMethod* method)
{
	return ((  LevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841 * (*) (String_t*, const RuntimeMethod*))GlobalFunctions_GetComponentObject_TisRuntimeObject_m1E81DA43408068B8C6627D97C1994CBC8A3A541F_gshared)(___tag0, method);
}
// !!0 UnityEngine.Component::GetComponent<TweenEffect>()
inline TweenEffect_tB80E075E7B5F736D5D3E0BDAB6A253BF4DA1BBF1 * Component_GetComponent_TisTweenEffect_tB80E075E7B5F736D5D3E0BDAB6A253BF4DA1BBF1_mFE8E5BE945ECF3B5557CF812B04BA9D19010FBEA (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  TweenEffect_tB80E075E7B5F736D5D3E0BDAB6A253BF4DA1BBF1 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_mDC2250CC3F24F6FE45660AF6153056ABDA5ED60F_gshared)(__this, method);
}
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Image>()
inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * Component_GetComponent_TisImage_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_m5D5D0C1BB7E1E67F46C955DA2861E7B83FC7301D (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_mDC2250CC3F24F6FE45660AF6153056ABDA5ED60F_gshared)(__this, method);
}
// UnityEngine.Sprite UnityEngine.UI.Image::get_sprite()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * Image_get_sprite_mA6FB016B4E3FE5EFFAE4B3AEE2D2DF89C61E0AF3_inline (Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * __this, const RuntimeMethod* method);
// TweenEffect CharControl::get_TweenEffect()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR TweenEffect_tB80E075E7B5F736D5D3E0BDAB6A253BF4DA1BBF1 * CharControl_get_TweenEffect_m0936842652110F63724B3CF1BE3176069B810A6F_inline (CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * __this, const RuntimeMethod* method);
// System.Void TweenEffect::BounceEffect()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TweenEffect_BounceEffect_mB01EFD4885A764247CB6F16E495DD19974BFC58B (TweenEffect_tB80E075E7B5F736D5D3E0BDAB6A253BF4DA1BBF1 * __this, const RuntimeMethod* method);
// System.Boolean CharControl::get_IsSelected()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool CharControl_get_IsSelected_m5DAAC6C82C108F3FD7842336558B1B62BD8206C8_inline (CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * __this, const RuntimeMethod* method);
// System.Collections.IEnumerator CharControl::CheckRelevate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* CharControl_CheckRelevate_mC7196FD3F0797C15DC2A2672C5A944A470C0F062 (CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * __this, const RuntimeMethod* method);
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719 (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, RuntimeObject* ___routine0, const RuntimeMethod* method);
// System.Void CharControl/<CheckRelevate>d__21::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CCheckRelevateU3Ed__21__ctor_mA361476A27B6D1FF49F6BA63693BEB85B52D0337 (U3CCheckRelevateU3Ed__21_t1EC48447076186B1190E53CBB1A680B3ECA1E750 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEventBase::RemoveAllListeners()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityEventBase_RemoveAllListeners_m934834C85F5CFE62BEDA9CF4DE9C5B8B5470F1D9 (UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB * __this, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, const RuntimeMethod* method);
// System.Void CharControl::ButtonCallBack()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharControl_ButtonCallBack_mA2858D0CC04E3AE51F0833B8E502F753939A912D (CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * __this, const RuntimeMethod* method);
// System.Void UnityEngine.ScriptableObject::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ScriptableObject__ctor_m8DAE6CDCFA34E16F2543B02CC3669669FF203063 (ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A * __this, const RuntimeMethod* method);
// !!0[] UnityEngine.Resources::LoadAll<CharsBundleData>(System.String)
inline CharsBundleDataU5BU5D_t49861CE889FB0A7632E6FD7126DE29B0F470F700* Resources_LoadAll_TisCharsBundleData_tC2C62F2A7FAEEDB189C65454E3E32437ECD36285_m2073CC955A118CCAA7E5A2145F245359C72C881A (String_t* ___path0, const RuntimeMethod* method)
{
	return ((  CharsBundleDataU5BU5D_t49861CE889FB0A7632E6FD7126DE29B0F470F700* (*) (String_t*, const RuntimeMethod*))Resources_LoadAll_TisRuntimeObject_m346B17472FD075C67F81BFA5A7D454944296C618_gshared)(___path0, method);
}
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void CharsContainer::LoadResources()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharsContainer_LoadResources_mC434AAC21E19265D813E0B7E3F2EB3B25FD1DABF (CharsContainer_tB2FB3D6B21722D0168C892A2C3837608C80C7CC5 * __this, const RuntimeMethod* method);
// T GlobalFunctions::GetComponentObject<UnityEngine.RectTransform>(System.String)
inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * GlobalFunctions_GetComponentObject_TisRectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_mD0BF1DF9A674788852D027927E99D65003AF708E (String_t* ___tag0, const RuntimeMethod* method)
{
	return ((  RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * (*) (String_t*, const RuntimeMethod*))GlobalFunctions_GetComponentObject_TisRuntimeObject_m1E81DA43408068B8C6627D97C1994CBC8A3A541F_gshared)(___tag0, method);
}
// System.Void LevelProvider::NextLevel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LevelProvider_NextLevel_mBFD5EAB47CF7715296B1FAF72BF1857983628EDF (LevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841 * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1<CharControl> CharsGenerator::get_CharsOnScene()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR List_1_t37187EA3F5A7945D0B45C8A071194C1191FC32FE * CharsGenerator_get_CharsOnScene_m1D6EDE7E5FA590B1195B033ECE89B6BE1F29EBF3_inline (CharsGenerator_t7E18FC7617D416B9E55B664B885A3138E43EE2A2 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<CharControl>::Clear()
inline void List_1_Clear_m08CCA40C44AE6824282FFCD856929F8F04530A99 (List_1_t37187EA3F5A7945D0B45C8A071194C1191FC32FE * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t37187EA3F5A7945D0B45C8A071194C1191FC32FE *, const RuntimeMethod*))List_1_Clear_m5FB5A9C59D8625FDFB06876C4D8848F0F07ABFD0_gshared)(__this, method);
}
// System.Collections.Generic.List`1<CharsData> CharsGenerator::get_CharsPrepareOnScene()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR List_1_t49AE3466419291B57C0EF7E386E193A7AFE92C02 * CharsGenerator_get_CharsPrepareOnScene_m232D2807B2745CD94DC8AD5E1B1FA8DDE239AB32_inline (CharsGenerator_t7E18FC7617D416B9E55B664B885A3138E43EE2A2 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<CharsData>::Clear()
inline void List_1_Clear_m325068E9A3443E1D9B92507B12607D342B659527 (List_1_t49AE3466419291B57C0EF7E386E193A7AFE92C02 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t49AE3466419291B57C0EF7E386E193A7AFE92C02 *, const RuntimeMethod*))List_1_Clear_m5FB5A9C59D8625FDFB06876C4D8848F0F07ABFD0_gshared)(__this, method);
}
// System.Void LevelProvider::set_Score(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LevelProvider_set_Score_mBED651E1406AC9A3CDFA39281E646B3B01B63A8B (LevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Collections.IEnumerator UnityEngine.Transform::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Transform_GetEnumerator_mBA0E884A69F0AA05FCB69F4EE5F700177F75DD7E (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___obj0, const RuntimeMethod* method);
// CharsBundleData[] CharsContainer::get_CharBundles()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR CharsBundleDataU5BU5D_t49861CE889FB0A7632E6FD7126DE29B0F470F700* CharsContainer_get_CharBundles_mA18F021F7787AC922E81E27192C083AAE634CF39_inline (CharsContainer_tB2FB3D6B21722D0168C892A2C3837608C80C7CC5 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.Random::Range(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Random_Range_m4B3A0037ACA057F33C94508F908546B9317D996A (int32_t ___minInclusive0, int32_t ___maxExclusive1, const RuntimeMethod* method);
// CharsData[] CharsBundleData::get_CharsData()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR CharsDataU5BU5D_t6E1A2B31C527A76C761C25915A588ED83C6B4CDE* CharsBundleData_get_CharsData_m2A642A6B655267B4CC126843873D43AA04F9875C_inline (CharsBundleData_tC2C62F2A7FAEEDB189C65454E3E32437ECD36285 * __this, const RuntimeMethod* method);
// System.Void System.Func`2<CharsData,System.Boolean>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m8AE4F7EFDDED3C0E1D6C23FDA851725EA4F91FF1 (Func_2_tF8C630B1BAE8BC459BCA2C994588C319051E39EB * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_tF8C630B1BAE8BC459BCA2C994588C319051E39EB *, RuntimeObject *, intptr_t, const RuntimeMethod*))Func_2__ctor_mCA84157864A199574AD0B7F3083F99B54DC1F98C_gshared)(__this, ___object0, ___method1, method);
}
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<CharsData>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
inline RuntimeObject* Enumerable_Where_TisCharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595_m23A550BB94D43A3560AA9F70C03BFAE61ACB6806 (RuntimeObject* ___source0, Func_2_tF8C630B1BAE8BC459BCA2C994588C319051E39EB * ___predicate1, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (RuntimeObject*, Func_2_tF8C630B1BAE8BC459BCA2C994588C319051E39EB *, const RuntimeMethod*))Enumerable_Where_TisRuntimeObject_mD8AE6780E78249FC87B2344E09D130624E70D7DA_gshared)(___source0, ___predicate1, method);
}
// !!0[] System.Linq.Enumerable::ToArray<CharsData>(System.Collections.Generic.IEnumerable`1<!!0>)
inline CharsDataU5BU5D_t6E1A2B31C527A76C761C25915A588ED83C6B4CDE* Enumerable_ToArray_TisCharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595_m0EC984F784602B3309C821383DC5C87FDB30619B (RuntimeObject* ___source0, const RuntimeMethod* method)
{
	return ((  CharsDataU5BU5D_t6E1A2B31C527A76C761C25915A588ED83C6B4CDE* (*) (RuntimeObject*, const RuntimeMethod*))Enumerable_ToArray_TisRuntimeObject_mEB06425105813A21FC826C4144F8456EAE2304DE_gshared)(___source0, method);
}
// System.Void System.Collections.Generic.List`1<CharsData>::Add(!0)
inline void List_1_Add_m62B652590809428BA6EDDA52CC5DD40A0ABC0BA6 (List_1_t49AE3466419291B57C0EF7E386E193A7AFE92C02 * __this, CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595 * ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t49AE3466419291B57C0EF7E386E193A7AFE92C02 *, CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595 *, const RuntimeMethod*))List_1_Add_mF15250BF947CA27BE9A23C08BAC6DB6F180B0EDD_gshared)(__this, ___item0, method);
}
// System.Int32 LevelProvider::get_CurrentCharsCount()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t LevelProvider_get_CurrentCharsCount_m3F727FCD9E869AE74F8BF985E69C22B17810A87E_inline (LevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841 * __this, const RuntimeMethod* method);
// System.Void CharsGenerator::SetCharsOnScene()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharsGenerator_SetCharsOnScene_m9300C8B08B98F7512C9A5063969280675DBAD740 (CharsGenerator_t7E18FC7617D416B9E55B664B885A3138E43EE2A2 * __this, const RuntimeMethod* method);
// System.Collections.IEnumerator CharsGenerator::InitializeEffect()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* CharsGenerator_InitializeEffect_m35BE446407A6FF90266A52ED23782D4FD05CA3E1 (CharsGenerator_t7E18FC7617D416B9E55B664B885A3138E43EE2A2 * __this, const RuntimeMethod* method);
// !0[] System.Collections.Generic.List`1<CharsData>::ToArray()
inline CharsDataU5BU5D_t6E1A2B31C527A76C761C25915A588ED83C6B4CDE* List_1_ToArray_m947BDA62E5FAFE13045E535DACCC691670751CA1 (List_1_t49AE3466419291B57C0EF7E386E193A7AFE92C02 * __this, const RuntimeMethod* method)
{
	return ((  CharsDataU5BU5D_t6E1A2B31C527A76C761C25915A588ED83C6B4CDE* (*) (List_1_t49AE3466419291B57C0EF7E386E193A7AFE92C02 *, const RuntimeMethod*))List_1_ToArray_mC6E0B3CF74090974475F845BF79EC5E66D3A71AC_gshared)(__this, method);
}
// CharsData[] CharsGenerator::ShuffleArray(CharsData[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CharsDataU5BU5D_t6E1A2B31C527A76C761C25915A588ED83C6B4CDE* CharsGenerator_ShuffleArray_m7EAB71A9D642F1173D498254A1F7064F0362AE19 (CharsGenerator_t7E18FC7617D416B9E55B664B885A3138E43EE2A2 * __this, CharsDataU5BU5D_t6E1A2B31C527A76C761C25915A588ED83C6B4CDE* ___data0, const RuntimeMethod* method);
// System.Void CharsGenerator::AddCharOnScene(CharsData)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharsGenerator_AddCharOnScene_m96078093CE4F424BD846646FBC495CE93C19AD03 (CharsGenerator_t7E18FC7617D416B9E55B664B885A3138E43EE2A2 * __this, CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595 * ___randomCharData0, const RuntimeMethod* method);
// System.Void System.Random::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Random__ctor_mF40AD1812BABC06235B661CCE513E4F74EEE9F05 (Random_t6C9E9775A149D0ADCFEB4B252C408F03EE870118 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0,UnityEngine.Transform)
inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_mACEFD07C5A0FE9C85BC165DF1E4A825675719EAC (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___original0, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___parent1, const RuntimeMethod* method)
{
	return ((  GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 *, const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_m78E51E899A91F621B74B2CF3D5081EF822049DA3_gshared)(___original0, ___parent1, method);
}
// !!0 UnityEngine.GameObject::GetComponent<CharControl>()
inline CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * GameObject_GetComponent_TisCharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD_m8194715FED3744A59E689261461A2D3106DC7A30 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m533C28B362284747FD5138B02D183642545146E8_gshared)(__this, method);
}
// System.Void CharControl::set_Chardata(CharsData)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharControl_set_Chardata_m389A1A07EF79E9431F64F3CFAC17A1156BB844EB (CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * __this, CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595 * ___value0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<CharControl>::Add(!0)
inline void List_1_Add_m6225D4407E2F7A63072835132E8B4A07A69FA874 (List_1_t37187EA3F5A7945D0B45C8A071194C1191FC32FE * __this, CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t37187EA3F5A7945D0B45C8A071194C1191FC32FE *, CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD *, const RuntimeMethod*))List_1_Add_mF15250BF947CA27BE9A23C08BAC6DB6F180B0EDD_gshared)(__this, ___item0, method);
}
// System.Void CharsGenerator/<InitializeEffect>d__18::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CInitializeEffectU3Ed__18__ctor_m2963AC28072621513A43A21896B10ACA02389E7F (U3CInitializeEffectU3Ed__18_tB5CE3179A9E4153E9DF3477D7479D251A763486C * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Void CharsContainer::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharsContainer__ctor_m605ACE9AA15C5819B90DFAF384BC7A972520D52F (CharsContainer_tB2FB3D6B21722D0168C892A2C3837608C80C7CC5 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<CharControl>::.ctor()
inline void List_1__ctor_m6C9F4356B1609DBC166481253083E775FDA52FFC (List_1_t37187EA3F5A7945D0B45C8A071194C1191FC32FE * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t37187EA3F5A7945D0B45C8A071194C1191FC32FE *, const RuntimeMethod*))List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<CharsData>::.ctor()
inline void List_1__ctor_m62662109EBBF48C763AF94C725FCC99A03E364FD (List_1_t49AE3466419291B57C0EF7E386E193A7AFE92C02 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t49AE3466419291B57C0EF7E386E193A7AFE92C02 *, const RuntimeMethod*))List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared)(__this, method);
}
// System.Void CharsGenerator/<>c__DisplayClass14_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass14_0__ctor_mDF2E4F72C494882B94DF063938E248EE6108DDAF (U3CU3Ec__DisplayClass14_0_t79B660C3CA0F908E689E0E2E7221DF954F7E5E42 * __this, const RuntimeMethod* method);
// System.Int32 System.Linq.Enumerable::Count<CharsData>(System.Collections.Generic.IEnumerable`1<!!0>)
inline int32_t Enumerable_Count_TisCharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595_mF5CBB5FDA01799955A84315875FF0232F7C53FA0 (RuntimeObject* ___source0, const RuntimeMethod* method)
{
	return ((  int32_t (*) (RuntimeObject*, const RuntimeMethod*))Enumerable_Count_TisRuntimeObject_m1A161C58BCDDFCF3A206ED7DFBEB0F9231B18AE3_gshared)(___source0, method);
}
// System.Void DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass9_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass9_0__ctor_mCA1E6F241784A13DEC15E12A0219479D552BB6BB (U3CU3Ec__DisplayClass9_0_tB9266040A99BBE64FC737A175355345A13E86D21 * __this, const RuntimeMethod* method);
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.Plugins.PathPlugin::Get()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ABSTweenPlugin_3_t0048BB94992A96F02D427E2C26C6E92AC9AECD32 * PathPlugin_Get_m1D5B2D927828EF5D7543475191CAE0B620A0A5A3 (const RuntimeMethod* method);
// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>::.ctor(System.Object,System.IntPtr)
inline void DOGetter_1__ctor_m7537F12632FAD0C4475AC13323E07B2D2C6FF081 (DOGetter_1_tE8D2419A0219EB945463B02351F4905C77235E1A * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (DOGetter_1_tE8D2419A0219EB945463B02351F4905C77235E1A *, RuntimeObject *, intptr_t, const RuntimeMethod*))DOGetter_1__ctor_m7537F12632FAD0C4475AC13323E07B2D2C6FF081_gshared)(__this, ___object0, ___method1, method);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>::.ctor(System.Object,System.IntPtr)
inline void DOSetter_1__ctor_m23DF8B62FE29CBDA90726662AEA2755DF765E733 (DOSetter_1_tAEE38AA9321AE4A0A4A4E5D496EA753BFB746B85 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (DOSetter_1_tAEE38AA9321AE4A0A4A4E5D496EA753BFB746B85 *, RuntimeObject *, intptr_t, const RuntimeMethod*))DOSetter_1__ctor_m23DF8B62FE29CBDA90726662AEA2755DF765E733_gshared)(__this, ___object0, ___method1, method);
}
// DG.Tweening.Core.TweenerCore`3<!!0,!!1,!!2> DG.Tweening.DOTween::To<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>(DG.Tweening.Plugins.Core.ABSTweenPlugin`3<!!0,!!1,!!2>,DG.Tweening.Core.DOGetter`1<!!0>,DG.Tweening.Core.DOSetter`1<!!0>,!!1,System.Single)
inline TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * DOTween_To_TisVector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_TisPath_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5_TisPathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A_m7C0FFBC94D84A7FAD1568988AC768527794B9F65 (ABSTweenPlugin_3_t0048BB94992A96F02D427E2C26C6E92AC9AECD32 * ___plugin0, DOGetter_1_tE8D2419A0219EB945463B02351F4905C77235E1A * ___getter1, DOSetter_1_tAEE38AA9321AE4A0A4A4E5D496EA753BFB746B85 * ___setter2, Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * ___endValue3, float ___duration4, const RuntimeMethod* method)
{
	return ((  TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * (*) (ABSTweenPlugin_3_t0048BB94992A96F02D427E2C26C6E92AC9AECD32 *, DOGetter_1_tE8D2419A0219EB945463B02351F4905C77235E1A *, DOSetter_1_tAEE38AA9321AE4A0A4A4E5D496EA753BFB746B85 *, Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 *, float, const RuntimeMethod*))DOTween_To_TisVector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_TisRuntimeObject_TisPathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A_m6B0079AA9B9A8A3B225325A3B13147B6684B2303_gshared)(___plugin0, ___getter1, ___setter2, ___endValue3, ___duration4, method);
}
// !!0 DG.Tweening.TweenSettingsExtensions::SetTarget<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>>(!!0,System.Object)
inline TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_mBF1153F332C5ED368722DFB51E2E088E1A8648E0 (TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * ___t0, RuntimeObject * ___target1, const RuntimeMethod* method)
{
	return ((  TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * (*) (TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A *, RuntimeObject *, const RuntimeMethod*))TweenSettingsExtensions_SetTarget_TisRuntimeObject_m686297D300C2FE2F7C5430CF1AFA860E45801292_gshared)(___t0, ___target1, method);
}
// System.Void DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass10_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass10_0__ctor_m819824BC9854A0C90662B04D48B88CA7B052BF83 (U3CU3Ec__DisplayClass10_0_t6B6DFEAD8FF1DD6A724C294AA0B120F82A7A6937 * __this, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Component::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass4_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass4_0__ctor_m5CD451B862F3CC71D8D25BC3A69725FCDD6144C4 (U3CU3Ec__DisplayClass4_0_t9CD30C09F13A8901FBD37CEEE628023A3AC88587 * __this, const RuntimeMethod* method);
// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Color>::.ctor(System.Object,System.IntPtr)
inline void DOGetter_1__ctor_mAC32C501C09F9F6EC5B0DD2C1A8473BAE88DC8CE (DOGetter_1_tCC9DEA9BB7DA5C2570ED2CE9BD13EF5C154B7B10 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (DOGetter_1_tCC9DEA9BB7DA5C2570ED2CE9BD13EF5C154B7B10 *, RuntimeObject *, intptr_t, const RuntimeMethod*))DOGetter_1__ctor_mAC32C501C09F9F6EC5B0DD2C1A8473BAE88DC8CE_gshared)(__this, ___object0, ___method1, method);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Color>::.ctor(System.Object,System.IntPtr)
inline void DOSetter_1__ctor_m41D0232769B743141B4389F8A4599EC78F310352 (DOSetter_1_tB62013E4617ECB51822384C8592AAB764E1662BF * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (DOSetter_1_tB62013E4617ECB51822384C8592AAB764E1662BF *, RuntimeObject *, intptr_t, const RuntimeMethod*))DOSetter_1__ctor_m41D0232769B743141B4389F8A4599EC78F310352_gshared)(__this, ___object0, ___method1, method);
}
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTween::ToAlpha(DG.Tweening.Core.DOGetter`1<UnityEngine.Color>,DG.Tweening.Core.DOSetter`1<UnityEngine.Color>,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TweenerCore_3_t551A69CD4FF610AE9DF07C97A86B6FE12EBEB597 * DOTween_ToAlpha_mB80D122D92ABC7A45500CE5FDA58FAA8B7688C16 (DOGetter_1_tCC9DEA9BB7DA5C2570ED2CE9BD13EF5C154B7B10 * ___getter0, DOSetter_1_tB62013E4617ECB51822384C8592AAB764E1662BF * ___setter1, float ___endValue2, float ___duration3, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::SetTarget<DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>>(!!0,System.Object)
inline TweenerCore_3_t551A69CD4FF610AE9DF07C97A86B6FE12EBEB597 * TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t551A69CD4FF610AE9DF07C97A86B6FE12EBEB597_m8DA85B9EBDD18D4817AB73585673258C3904ED11 (TweenerCore_3_t551A69CD4FF610AE9DF07C97A86B6FE12EBEB597 * ___t0, RuntimeObject * ___target1, const RuntimeMethod* method)
{
	return ((  TweenerCore_3_t551A69CD4FF610AE9DF07C97A86B6FE12EBEB597 * (*) (TweenerCore_3_t551A69CD4FF610AE9DF07C97A86B6FE12EBEB597 *, RuntimeObject *, const RuntimeMethod*))TweenSettingsExtensions_SetTarget_TisRuntimeObject_m686297D300C2FE2F7C5430CF1AFA860E45801292_gshared)(___t0, ___target1, method);
}
// System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass36_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass36_0__ctor_mE2912C6BF4318E28870DE7AA92E4B05C80D92EE0 (U3CU3Ec__DisplayClass36_0_tCE8E904A62CBD9F143B3D692CA532EFA7A6F777D * __this, const RuntimeMethod* method);
// System.Void System.Action`4<DG.Tweening.Plugins.Options.PathOptions,DG.Tweening.Tween,UnityEngine.Quaternion,UnityEngine.Transform>::.ctor(System.Object,System.IntPtr)
inline void Action_4__ctor_mB344A638C116BEC788ED3EA352844312528F1175 (Action_4_t758FC0992B1B40F36598AA26D46FB5626C503AFE * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_4_t758FC0992B1B40F36598AA26D46FB5626C503AFE *, RuntimeObject *, intptr_t, const RuntimeMethod*))Action_4__ctor_m48578D4B8ED1C72857654C84320B364BD2E39E30_gshared)(__this, ___object0, ___method1, method);
}
// System.Void DG.Tweening.Core.DOTweenExternalCommand::add_SetOrientationOnPath(System.Action`4<DG.Tweening.Plugins.Options.PathOptions,DG.Tweening.Tween,UnityEngine.Quaternion,UnityEngine.Transform>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOTweenExternalCommand_add_SetOrientationOnPath_mF93DD658AA5364137A925E52006F94298FF1120B (Action_4_t758FC0992B1B40F36598AA26D46FB5626C503AFE * ___value0, const RuntimeMethod* method);
// System.AppDomain System.AppDomain::get_CurrentDomain()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR AppDomain_tBEB6322D51DCB12C09A56A49886C2D09BA1C1A8A * AppDomain_get_CurrentDomain_mC2FE307811914289CBBDEFEFF6175FCE2E96A55E (const RuntimeMethod* method);
// System.Reflection.Assembly[] System.AppDomain::GetAssemblies()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR AssemblyU5BU5D_t42061B4CA43487DD8ECD8C3AACCEF783FA081FF0* AppDomain_GetAssemblies_m7397BD0461B4D6BA76AE0974DE9FBEDAF70AEBFD (AppDomain_tBEB6322D51DCB12C09A56A49886C2D09BA1C1A8A * __this, const RuntimeMethod* method);
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t * Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E (RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ___handle0, const RuntimeMethod* method);
// System.Reflection.MethodInfo System.Type::GetMethod(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MethodInfo_t * Type_GetMethod_mDD47332AAF3036AAFC4C6626A999A452E7143DCF (Type_t * __this, String_t* ___name0, const RuntimeMethod* method);
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOFade(UnityEngine.UI.Text,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TweenerCore_3_t551A69CD4FF610AE9DF07C97A86B6FE12EBEB597 * DOTweenModuleUI_DOFade_m21F7CCBB732ABBD63BFD893D1A8D9837532A70D1 (Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___target0, float ___endValue1, float ___duration2, const RuntimeMethod* method);
// System.String System.String::Format(System.String,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Format_mB3D38E5238C3164DB4D7D29339D9E225A4496D17 (String_t* ___format0, RuntimeObject * ___arg01, const RuntimeMethod* method);
// System.Void CharsGenerator::CharGenerate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharsGenerator_CharGenerate_m8BC7A85E0E0DB9BAABEB7A473D8692635633FDB3 (CharsGenerator_t7E18FC7617D416B9E55B664B885A3138E43EE2A2 * __this, const RuntimeMethod* method);
// System.Void LevelProvider::WinPanelActive(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LevelProvider_WinPanelActive_m2C2BB89363A35E652A377A717451EFC37958B141 (LevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841 * __this, bool ___isActive0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Image>()
inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * GameObject_GetComponent_TisImage_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_m08E9D50CF81C1176D98860FE7038B9544046EB7D (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m533C28B362284747FD5138B02D183642545146E8_gshared)(__this, method);
}
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOFade(UnityEngine.UI.Image,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TweenerCore_3_t551A69CD4FF610AE9DF07C97A86B6FE12EBEB597 * DOTweenModuleUI_DOFade_mE66C8B7D137E0DDE54AAA96F551811FB8BDB8754 (Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___target0, float ___endValue1, float ___duration2, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, bool ___value0, const RuntimeMethod* method);
// System.Collections.IEnumerator LoadingScreen::LoadScreen()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* LoadingScreen_LoadScreen_mDD89C18CE436D6D2063C31F49AE91F20F5127B18 (LoadingScreen_tD1CD7EA45A62C3A1A641D929E47F330644D353B2 * __this, const RuntimeMethod* method);
// System.Void LoadingScreen/<LoadScreen>d__3::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CLoadScreenU3Ed__3__ctor_m8C85D910055010DC08BB6EAFEE748471DA723D6B (U3CLoadScreenU3Ed__3_tAB779AB619F104C740B1B46095B34AB1EA682770 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOShakePosition(UnityEngine.Transform,System.Single,UnityEngine.Vector3,System.Int32,System.Single,System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * ShortcutExtensions_DOShakePosition_mDEC0E682E9A8D470D6FD1F825161C3E6087DC3B8 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___target0, float ___duration1, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___strength2, int32_t ___vibrato3, float ___randomness4, bool ___snapping5, bool ___fadeOut6, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// System.Void CharControl/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m0093EAA02FD05C07FBF35B4333C0C7D427357533 (U3CU3Ec_t354A1590D8043D0E9E5A2724672CBF8E9E8F1AA4 * __this, const RuntimeMethod* method);
// UnityEngine.UI.Image CharControl::get_CharImage()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * CharControl_get_CharImage_m6717F383E31A7235179A68C6BBB341C6FC9E2AFB_inline (CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * __this, const RuntimeMethod* method);
// UnityEngine.Sprite CharsData::get_Sprite()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * CharsData_get_Sprite_m1BFE6586725A0A68865F947F36E4E0F3BF85DE46_inline (CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.UI.Image::set_sprite(UnityEngine.Sprite)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Image_set_sprite_m55C50F18ABA0A98E926FC777F7D07FA18A7D04E4 (Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * __this, Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___value0, const RuntimeMethod* method);
// System.Void CharControl::set_IsSelected(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CharControl_set_IsSelected_m84AD011947B55056DDC4F4202075B1A9EE9D78CF_inline (CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * __this, bool ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WaitForSeconds__ctor_mD298C4CB9532BBBDE172FC40F3397E30504038D4 (WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * __this, float ___seconds0, const RuntimeMethod* method);
// CharControl LevelProvider::get_CurrentChar()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * LevelProvider_get_CurrentChar_mB00F8D84D9CE0BEABC41F5B0CD8C62B0744EA00F_inline (LevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// CharsData CharControl::get_Chardata()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595 * CharControl_get_Chardata_m82B81759BB755408B138708B8D0E0D48835EAF0E_inline (CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * __this, const RuntimeMethod* method);
// System.Boolean System.String::op_Inequality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Inequality_mDDA2DDED3E7EF042987EB7180EE3E88105F0AAE2 (String_t* ___a0, String_t* ___b1, const RuntimeMethod* method);
// System.Int32 LevelProvider::get_Score()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t LevelProvider_get_Score_m0E65DD5953788C0FBB8E65BEC1ACFF8A9F53A6F0_inline (LevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841 * __this, const RuntimeMethod* method);
// System.Void LevelProvider::set_CurrentChar(CharControl)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LevelProvider_set_CurrentChar_mC845B8A970536A1E9BA9B577A63175EB193E64E5 (LevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841 * __this, CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * ___value0, const RuntimeMethod* method);
// CharsGenerator LevelProvider::get_CharsGenerator()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR CharsGenerator_t7E18FC7617D416B9E55B664B885A3138E43EE2A2 * LevelProvider_get_CharsGenerator_mB8E799CB7B0821293DE8E6AB1A245AD4B11738B1_inline (LevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841 * __this, const RuntimeMethod* method);
// System.Void System.Func`2<CharControl,System.Boolean>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m8E70087591BD5ED6F195C93CF07DA159BEAC2FB9 (Func_2_t20F4B130EF75554CF71A9826AA0AC83CCB3EEDAC * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t20F4B130EF75554CF71A9826AA0AC83CCB3EEDAC *, RuntimeObject *, intptr_t, const RuntimeMethod*))Func_2__ctor_mCA84157864A199574AD0B7F3083F99B54DC1F98C_gshared)(__this, ___object0, ___method1, method);
}
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<CharControl>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
inline RuntimeObject* Enumerable_Where_TisCharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD_m71644EECCC484BCD70DAF76B0AAA095B910D839B (RuntimeObject* ___source0, Func_2_t20F4B130EF75554CF71A9826AA0AC83CCB3EEDAC * ___predicate1, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (RuntimeObject*, Func_2_t20F4B130EF75554CF71A9826AA0AC83CCB3EEDAC *, const RuntimeMethod*))Enumerable_Where_TisRuntimeObject_mD8AE6780E78249FC87B2344E09D130624E70D7DA_gshared)(___source0, ___predicate1, method);
}
// System.Int32 System.Linq.Enumerable::Count<CharControl>(System.Collections.Generic.IEnumerable`1<!!0>)
inline int32_t Enumerable_Count_TisCharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD_mB9C151F66AEAD221FF7001ABD445C652FCA07DC8 (RuntimeObject* ___source0, const RuntimeMethod* method)
{
	return ((  int32_t (*) (RuntimeObject*, const RuntimeMethod*))Enumerable_Count_TisRuntimeObject_m1A161C58BCDDFCF3A206ED7DFBEB0F9231B18AE3_gshared)(___source0, method);
}
// System.Boolean System.String::op_Equality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB (String_t* ___a0, String_t* ___b1, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<CharControl>::GetEnumerator()
inline Enumerator_t9930E1A60798842A72D23D981BA4A2330BF36578  List_1_GetEnumerator_m924E2A798B90A4A58216B9E7ACE17C3F078EDB02 (List_1_t37187EA3F5A7945D0B45C8A071194C1191FC32FE * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_t9930E1A60798842A72D23D981BA4A2330BF36578  (*) (List_1_t37187EA3F5A7945D0B45C8A071194C1191FC32FE *, const RuntimeMethod*))List_1_GetEnumerator_m6339FC2D3D1CE4FA13CF21C7F9FC58CA4441BF0C_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<CharControl>::get_Current()
inline CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * Enumerator_get_Current_mF4D92D56E4EE8FDADA509B9D8BA7D3CF7CED1B58_inline (Enumerator_t9930E1A60798842A72D23D981BA4A2330BF36578 * __this, const RuntimeMethod* method)
{
	return ((  CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * (*) (Enumerator_t9930E1A60798842A72D23D981BA4A2330BF36578 *, const RuntimeMethod*))Enumerator_get_Current_m4C91D0E84532DF10C654917487D82CB0AB27693B_gshared_inline)(__this, method);
}
// System.Void TweenEffect::StrongBounceEffect()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TweenEffect_StrongBounceEffect_m3D7E7C12E3636C041F06C1121C7646DEC82A5C97 (TweenEffect_tB80E075E7B5F736D5D3E0BDAB6A253BF4DA1BBF1 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<CharControl>::MoveNext()
inline bool Enumerator_MoveNext_mD3BE2F5A91D965C021520535B9413F1429D8A60B (Enumerator_t9930E1A60798842A72D23D981BA4A2330BF36578 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t9930E1A60798842A72D23D981BA4A2330BF36578 *, const RuntimeMethod*))Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<CharControl>::Dispose()
inline void Enumerator_Dispose_mBCB2D275F9431E47AFAE49744FEDF77EDAACA2D5 (Enumerator_t9930E1A60798842A72D23D981BA4A2330BF36578 * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_t9930E1A60798842A72D23D981BA4A2330BF36578 *, const RuntimeMethod*))Enumerator_Dispose_mCFB225D9E5E597A1CC8F958E53BEA1367D8AC7B8_gshared)(__this, method);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_localPosition()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_localPosition_m527B8B5B625DA9A61E551E0FBCD3BE8CA4539FC2 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Transform::get_parent()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * Transform_get_parent_m7D06005D9CB55F90F39D42F6A2AF9C7BC80745C9 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::TransformPoint(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_TransformPoint_m68AF95765A9279192E601208A9C5170027A5F0D2 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position0, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody::MovePosition(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_MovePosition_mB3CBBF21FD0ABB88BC6C004B993DED25673001C7 (Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Rigidbody::get_position()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Rigidbody_get_position_m5F429382F610E324F39F33E8498A29D0828AD8E8 (Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody::set_rotation(UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_set_rotation_m3024C151FEC9BB75735DE9B4BA64F16AA779C5D6 (Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * __this, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_rotation_m1B5F3D4CE984AB31254615C9C71B0E54978583B4 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___value0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody>()
inline Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * Component_GetComponent_TisRigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_mB4A92A619135F9258670FB93AE08F229A41D0980 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_mDC2250CC3F24F6FE45660AF6153056ABDA5ED60F_gshared)(__this, method);
}
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModulePhysics::DOPath(UnityEngine.Rigidbody,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,DG.Tweening.PathMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * DOTweenModulePhysics_DOPath_m3C49FFEB71D494F474F7223248D7105E90CF5BE7 (Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * ___target0, Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * ___path1, float ___duration2, int32_t ___pathMode3, const RuntimeMethod* method);
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModulePhysics::DOLocalPath(UnityEngine.Rigidbody,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,DG.Tweening.PathMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * DOTweenModulePhysics_DOLocalPath_m6F939DAF66D56DA9B52DA62546C70F3862E14193 (Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * ___target0, Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * ___path1, float ___duration2, int32_t ___pathMode3, const RuntimeMethod* method);
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.ShortcutExtensions::DOPath(UnityEngine.Transform,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,DG.Tweening.PathMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * ShortcutExtensions_DOPath_m7AB84619455A541B79164E1D270A5AB511533B24 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___target0, Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * ___path1, float ___duration2, int32_t ___pathMode3, const RuntimeMethod* method);
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.ShortcutExtensions::DOLocalPath(UnityEngine.Transform,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,DG.Tweening.PathMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * ShortcutExtensions_DOLocalPath_m28185CD9787640FBB679A0B46F144E1BB0DB43F8 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___target0, Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * ___path1, float ___duration2, int32_t ___pathMode3, const RuntimeMethod* method);
// System.Void LoadingScreen::PanelActive(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LoadingScreen_PanelActive_m3D5780ABDCF9855B059512A617AC7C1E114C34C0 (LoadingScreen_tD1CD7EA45A62C3A1A641D929E47F330644D353B2 * __this, bool ___isActive0, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// CharsData CharControl::get_Chardata()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595 * CharControl_get_Chardata_m82B81759BB755408B138708B8D0E0D48835EAF0E (CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * __this, const RuntimeMethod* method)
{
	{
		// get => _chardata;
		CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595 * L_0 = __this->get__chardata_6();
		return L_0;
	}
}
// System.Void CharControl::set_Chardata(CharsData)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharControl_set_Chardata_m389A1A07EF79E9431F64F3CFAC17A1156BB844EB (CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * __this, CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595 * ___value0, const RuntimeMethod* method)
{
	{
		// this.name = value.Identifier;
		CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595 * L_0 = ___value0;
		NullCheck(L_0);
		String_t* L_1;
		L_1 = CharsData_get_Identifier_mC20D9B2E42F23E7CFA66FA65141011F719AB2681_inline(L_0, /*hidden argument*/NULL);
		Object_set_name_m87C4006618ADB325ABE5439DF159E10DD8DD0781(__this, L_1, /*hidden argument*/NULL);
		// _chardata = value;
		CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595 * L_2 = ___value0;
		__this->set__chardata_6(L_2);
		// }
		return;
	}
}
// TweenEffect CharControl::get_TweenEffect()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TweenEffect_tB80E075E7B5F736D5D3E0BDAB6A253BF4DA1BBF1 * CharControl_get_TweenEffect_m0936842652110F63724B3CF1BE3176069B810A6F (CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * __this, const RuntimeMethod* method)
{
	{
		// public TweenEffect TweenEffect { get => _tweenEffect; set => _tweenEffect = value; }
		TweenEffect_tB80E075E7B5F736D5D3E0BDAB6A253BF4DA1BBF1 * L_0 = __this->get__tweenEffect_7();
		return L_0;
	}
}
// System.Void CharControl::set_TweenEffect(TweenEffect)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharControl_set_TweenEffect_m2DAAD124E5169D25B22033EE6F8232AC703CF164 (CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * __this, TweenEffect_tB80E075E7B5F736D5D3E0BDAB6A253BF4DA1BBF1 * ___value0, const RuntimeMethod* method)
{
	{
		// public TweenEffect TweenEffect { get => _tweenEffect; set => _tweenEffect = value; }
		TweenEffect_tB80E075E7B5F736D5D3E0BDAB6A253BF4DA1BBF1 * L_0 = ___value0;
		__this->set__tweenEffect_7(L_0);
		return;
	}
}
// UnityEngine.UI.Image CharControl::get_CharImage()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * CharControl_get_CharImage_m6717F383E31A7235179A68C6BBB341C6FC9E2AFB (CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * __this, const RuntimeMethod* method)
{
	{
		// public Image CharImage { get => _charImage; set => _charImage = value; }
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_0 = __this->get__charImage_8();
		return L_0;
	}
}
// System.Void CharControl::set_CharImage(UnityEngine.UI.Image)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharControl_set_CharImage_mC6CC17A5B08C680FE11E3AAF11CC2AC2B38F9F10 (CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * __this, Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___value0, const RuntimeMethod* method)
{
	{
		// public Image CharImage { get => _charImage; set => _charImage = value; }
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_0 = ___value0;
		__this->set__charImage_8(L_0);
		return;
	}
}
// System.Boolean CharControl::get_IsSelected()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CharControl_get_IsSelected_m5DAAC6C82C108F3FD7842336558B1B62BD8206C8 (CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * __this, const RuntimeMethod* method)
{
	{
		// public bool IsSelected { get => isSelected; set => isSelected = value; }
		bool L_0 = __this->get_isSelected_10();
		return L_0;
	}
}
// System.Void CharControl::set_IsSelected(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharControl_set_IsSelected_m84AD011947B55056DDC4F4202075B1A9EE9D78CF (CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// public bool IsSelected { get => isSelected; set => isSelected = value; }
		bool L_0 = ___value0;
		__this->set_isSelected_10(L_0);
		return;
	}
}
// System.Void CharControl::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharControl_OnEnable_mBDBC0BF195EC0A8F76AC309607F41A1E87C6C0C5 (CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CharControl_U3COnEnableU3Eb__19_0_mE0D85D7B38041D91E133200CF23F814C0DF7B630_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mED8C9575844B41F67CB4C2B13685FC0174CB61BC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisImage_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_m5D5D0C1BB7E1E67F46C955DA2861E7B83FC7301D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisTweenEffect_tB80E075E7B5F736D5D3E0BDAB6A253BF4DA1BBF1_mFE8E5BE945ECF3B5557CF812B04BA9D19010FBEA_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GlobalFunctions_GetComponentObject_TisLevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841_m2F25E5DB707F42022C693C71FCF04CC50FEE9D25_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCE45B43BD3A18C0A8B92DDB8A21820E8B64F43CC);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _sourceButton = this.GetComponent<Button>();
		Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * L_0;
		L_0 = Component_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mED8C9575844B41F67CB4C2B13685FC0174CB61BC(__this, /*hidden argument*/Component_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mED8C9575844B41F67CB4C2B13685FC0174CB61BC_RuntimeMethod_var);
		__this->set__sourceButton_4(L_0);
		// _sourceButton.onClick.AddListener(() => ButtonCallBack());
		Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * L_1 = __this->get__sourceButton_4();
		NullCheck(L_1);
		ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F * L_2;
		L_2 = Button_get_onClick_m28BD8C670676D4E2B292B5A7F59387D4BF61F8F4_inline(L_1, /*hidden argument*/NULL);
		UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * L_3 = (UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 *)il2cpp_codegen_object_new(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099_il2cpp_TypeInfo_var);
		UnityAction__ctor_m48C04C4C0F46918CF216A2410A4E58D31B6362BA(L_3, __this, (intptr_t)((intptr_t)CharControl_U3COnEnableU3Eb__19_0_mE0D85D7B38041D91E133200CF23F814C0DF7B630_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_2);
		UnityEvent_AddListener_m0ACFF0706176ECCB20E0BC2542D07396616F436D(L_2, L_3, /*hidden argument*/NULL);
		// _levelProvider = GlobalFunctions.GetComponentObject<LevelProvider>(SceneConfig.LEVEL_MANAGEMENT_TAG);
		LevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841 * L_4;
		L_4 = GlobalFunctions_GetComponentObject_TisLevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841_m2F25E5DB707F42022C693C71FCF04CC50FEE9D25(_stringLiteralCE45B43BD3A18C0A8B92DDB8A21820E8B64F43CC, /*hidden argument*/GlobalFunctions_GetComponentObject_TisLevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841_m2F25E5DB707F42022C693C71FCF04CC50FEE9D25_RuntimeMethod_var);
		__this->set__levelProvider_5(L_4);
		// _tweenEffect = GetComponent<TweenEffect>();
		TweenEffect_tB80E075E7B5F736D5D3E0BDAB6A253BF4DA1BBF1 * L_5;
		L_5 = Component_GetComponent_TisTweenEffect_tB80E075E7B5F736D5D3E0BDAB6A253BF4DA1BBF1_mFE8E5BE945ECF3B5557CF812B04BA9D19010FBEA(__this, /*hidden argument*/Component_GetComponent_TisTweenEffect_tB80E075E7B5F736D5D3E0BDAB6A253BF4DA1BBF1_mFE8E5BE945ECF3B5557CF812B04BA9D19010FBEA_RuntimeMethod_var);
		__this->set__tweenEffect_7(L_5);
		// _charImage = GetComponent<Image>();
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_6;
		L_6 = Component_GetComponent_TisImage_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_m5D5D0C1BB7E1E67F46C955DA2861E7B83FC7301D(__this, /*hidden argument*/Component_GetComponent_TisImage_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_m5D5D0C1BB7E1E67F46C955DA2861E7B83FC7301D_RuntimeMethod_var);
		__this->set__charImage_8(L_6);
		// _backgroundSprite = _charImage.sprite;
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_7 = __this->get__charImage_8();
		NullCheck(L_7);
		Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * L_8;
		L_8 = Image_get_sprite_mA6FB016B4E3FE5EFFAE4B3AEE2D2DF89C61E0AF3_inline(L_7, /*hidden argument*/NULL);
		__this->set__backgroundSprite_9(L_8);
		// }
		return;
	}
}
// System.Void CharControl::ButtonCallBack()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharControl_ButtonCallBack_mA2858D0CC04E3AE51F0833B8E502F753939A912D (CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * __this, const RuntimeMethod* method)
{
	{
		// TweenEffect.BounceEffect();
		TweenEffect_tB80E075E7B5F736D5D3E0BDAB6A253BF4DA1BBF1 * L_0;
		L_0 = CharControl_get_TweenEffect_m0936842652110F63724B3CF1BE3176069B810A6F_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		TweenEffect_BounceEffect_mB01EFD4885A764247CB6F16E495DD19974BFC58B(L_0, /*hidden argument*/NULL);
		// if (!IsSelected)
		bool L_1;
		L_1 = CharControl_get_IsSelected_m5DAAC6C82C108F3FD7842336558B1B62BD8206C8_inline(__this, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0020;
		}
	}
	{
		// StartCoroutine(CheckRelevate());
		RuntimeObject* L_2;
		L_2 = CharControl_CheckRelevate_mC7196FD3F0797C15DC2A2672C5A944A470C0F062(__this, /*hidden argument*/NULL);
		Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * L_3;
		L_3 = MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719(__this, L_2, /*hidden argument*/NULL);
	}

IL_0020:
	{
		// }
		return;
	}
}
// System.Collections.IEnumerator CharControl::CheckRelevate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* CharControl_CheckRelevate_mC7196FD3F0797C15DC2A2672C5A944A470C0F062 (CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCheckRelevateU3Ed__21_t1EC48447076186B1190E53CBB1A680B3ECA1E750_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CCheckRelevateU3Ed__21_t1EC48447076186B1190E53CBB1A680B3ECA1E750 * L_0 = (U3CCheckRelevateU3Ed__21_t1EC48447076186B1190E53CBB1A680B3ECA1E750 *)il2cpp_codegen_object_new(U3CCheckRelevateU3Ed__21_t1EC48447076186B1190E53CBB1A680B3ECA1E750_il2cpp_TypeInfo_var);
		U3CCheckRelevateU3Ed__21__ctor_mA361476A27B6D1FF49F6BA63693BEB85B52D0337(L_0, 0, /*hidden argument*/NULL);
		U3CCheckRelevateU3Ed__21_t1EC48447076186B1190E53CBB1A680B3ECA1E750 * L_1 = L_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_2(__this);
		return L_1;
	}
}
// System.Void CharControl::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharControl_OnDisable_m145F1B26B72DCBE03EA575C138A7CB8F834F6ECF (CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * __this, const RuntimeMethod* method)
{
	{
		// _sourceButton.onClick.RemoveAllListeners();
		Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * L_0 = __this->get__sourceButton_4();
		NullCheck(L_0);
		ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F * L_1;
		L_1 = Button_get_onClick_m28BD8C670676D4E2B292B5A7F59387D4BF61F8F4_inline(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		UnityEventBase_RemoveAllListeners_m934834C85F5CFE62BEDA9CF4DE9C5B8B5470F1D9(L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CharControl::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharControl__ctor_mBE2BFABAAF7937260B08A3A03291945AE9C11260 (CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CharControl::<OnEnable>b__19_0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharControl_U3COnEnableU3Eb__19_0_mE0D85D7B38041D91E133200CF23F814C0DF7B630 (CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * __this, const RuntimeMethod* method)
{
	{
		// _sourceButton.onClick.AddListener(() => ButtonCallBack());
		CharControl_ButtonCallBack_mA2858D0CC04E3AE51F0833B8E502F753939A912D(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// CharsData[] CharsBundleData::get_CharsData()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CharsDataU5BU5D_t6E1A2B31C527A76C761C25915A588ED83C6B4CDE* CharsBundleData_get_CharsData_m2A642A6B655267B4CC126843873D43AA04F9875C (CharsBundleData_tC2C62F2A7FAEEDB189C65454E3E32437ECD36285 * __this, const RuntimeMethod* method)
{
	{
		// public CharsData[] CharsData { get => _charsData; set => _charsData = value; }
		CharsDataU5BU5D_t6E1A2B31C527A76C761C25915A588ED83C6B4CDE* L_0 = __this->get__charsData_4();
		return L_0;
	}
}
// System.Void CharsBundleData::set_CharsData(CharsData[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharsBundleData_set_CharsData_m81AE91FF2AA2625038AF4388C7FE7CCEFFC511BE (CharsBundleData_tC2C62F2A7FAEEDB189C65454E3E32437ECD36285 * __this, CharsDataU5BU5D_t6E1A2B31C527A76C761C25915A588ED83C6B4CDE* ___value0, const RuntimeMethod* method)
{
	{
		// public CharsData[] CharsData { get => _charsData; set => _charsData = value; }
		CharsDataU5BU5D_t6E1A2B31C527A76C761C25915A588ED83C6B4CDE* L_0 = ___value0;
		__this->set__charsData_4(L_0);
		return;
	}
}
// System.Void CharsBundleData::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharsBundleData__ctor_m106D367F39A7FAD0DB260E5744651FC98ED96B14 (CharsBundleData_tC2C62F2A7FAEEDB189C65454E3E32437ECD36285 * __this, const RuntimeMethod* method)
{
	{
		ScriptableObject__ctor_m8DAE6CDCFA34E16F2543B02CC3669669FF203063(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// CharsBundleData[] CharsContainer::get_CharBundles()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CharsBundleDataU5BU5D_t49861CE889FB0A7632E6FD7126DE29B0F470F700* CharsContainer_get_CharBundles_mA18F021F7787AC922E81E27192C083AAE634CF39 (CharsContainer_tB2FB3D6B21722D0168C892A2C3837608C80C7CC5 * __this, const RuntimeMethod* method)
{
	{
		// public CharsBundleData[] CharBundles { get => _charBundles; set => _charBundles = value; }
		CharsBundleDataU5BU5D_t49861CE889FB0A7632E6FD7126DE29B0F470F700* L_0 = __this->get__charBundles_0();
		return L_0;
	}
}
// System.Void CharsContainer::LoadResources()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharsContainer_LoadResources_mC434AAC21E19265D813E0B7E3F2EB3B25FD1DABF (CharsContainer_tB2FB3D6B21722D0168C892A2C3837608C80C7CC5 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Resources_LoadAll_TisCharsBundleData_tC2C62F2A7FAEEDB189C65454E3E32437ECD36285_m2073CC955A118CCAA7E5A2145F245359C72C881A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9E2131BA989553E04EAB0985721463E5686AF29D);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _charBundles = Resources.LoadAll<CharsBundleData>($"{SceneConfig.CHARS_RESOURCES_FOLDER}");
		CharsBundleDataU5BU5D_t49861CE889FB0A7632E6FD7126DE29B0F470F700* L_0;
		L_0 = Resources_LoadAll_TisCharsBundleData_tC2C62F2A7FAEEDB189C65454E3E32437ECD36285_m2073CC955A118CCAA7E5A2145F245359C72C881A(_stringLiteral9E2131BA989553E04EAB0985721463E5686AF29D, /*hidden argument*/Resources_LoadAll_TisCharsBundleData_tC2C62F2A7FAEEDB189C65454E3E32437ECD36285_m2073CC955A118CCAA7E5A2145F245359C72C881A_RuntimeMethod_var);
		__this->set__charBundles_0(L_0);
		// }
		return;
	}
}
// System.Void CharsContainer::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharsContainer__ctor_m605ACE9AA15C5819B90DFAF384BC7A972520D52F (CharsContainer_tB2FB3D6B21722D0168C892A2C3837608C80C7CC5 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String CharsData::get_Identifier()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* CharsData_get_Identifier_mC20D9B2E42F23E7CFA66FA65141011F719AB2681 (CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595 * __this, const RuntimeMethod* method)
{
	{
		// public string Identifier { get => _identifier; set => _identifier = value; }
		String_t* L_0 = __this->get__identifier_0();
		return L_0;
	}
}
// UnityEngine.Sprite CharsData::get_Sprite()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * CharsData_get_Sprite_m1BFE6586725A0A68865F947F36E4E0F3BF85DE46 (CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595 * __this, const RuntimeMethod* method)
{
	{
		// public Sprite Sprite { get => _sprite; set => _sprite = value; }
		Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * L_0 = __this->get__sprite_1();
		return L_0;
	}
}
// System.Void CharsData::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharsData__ctor_mFCCB902117FCD0BF8AA6CFFAA9BE96461BDA87D2 (CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Collections.Generic.List`1<CharControl> CharsGenerator::get_CharsOnScene()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_t37187EA3F5A7945D0B45C8A071194C1191FC32FE * CharsGenerator_get_CharsOnScene_m1D6EDE7E5FA590B1195B033ECE89B6BE1F29EBF3 (CharsGenerator_t7E18FC7617D416B9E55B664B885A3138E43EE2A2 * __this, const RuntimeMethod* method)
{
	{
		// public List<CharControl> CharsOnScene { get => charsOnScene; set => charsOnScene = value; }
		List_1_t37187EA3F5A7945D0B45C8A071194C1191FC32FE * L_0 = __this->get_charsOnScene_8();
		return L_0;
	}
}
// System.Void CharsGenerator::set_CharsOnScene(System.Collections.Generic.List`1<CharControl>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharsGenerator_set_CharsOnScene_mFAC4BC98889E613E9E7299301BE4E3B47AE00AC3 (CharsGenerator_t7E18FC7617D416B9E55B664B885A3138E43EE2A2 * __this, List_1_t37187EA3F5A7945D0B45C8A071194C1191FC32FE * ___value0, const RuntimeMethod* method)
{
	{
		// public List<CharControl> CharsOnScene { get => charsOnScene; set => charsOnScene = value; }
		List_1_t37187EA3F5A7945D0B45C8A071194C1191FC32FE * L_0 = ___value0;
		__this->set_charsOnScene_8(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<CharsData> CharsGenerator::get_CharsPrepareOnScene()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_t49AE3466419291B57C0EF7E386E193A7AFE92C02 * CharsGenerator_get_CharsPrepareOnScene_m232D2807B2745CD94DC8AD5E1B1FA8DDE239AB32 (CharsGenerator_t7E18FC7617D416B9E55B664B885A3138E43EE2A2 * __this, const RuntimeMethod* method)
{
	{
		// public List<CharsData> CharsPrepareOnScene { get => charsPrepareOnScene; set => charsPrepareOnScene = value; }
		List_1_t49AE3466419291B57C0EF7E386E193A7AFE92C02 * L_0 = __this->get_charsPrepareOnScene_9();
		return L_0;
	}
}
// System.Void CharsGenerator::set_CharsPrepareOnScene(System.Collections.Generic.List`1<CharsData>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharsGenerator_set_CharsPrepareOnScene_m18D3C27B828BA65DD4724BCDC35A34FD88B5D085 (CharsGenerator_t7E18FC7617D416B9E55B664B885A3138E43EE2A2 * __this, List_1_t49AE3466419291B57C0EF7E386E193A7AFE92C02 * ___value0, const RuntimeMethod* method)
{
	{
		// public List<CharsData> CharsPrepareOnScene { get => charsPrepareOnScene; set => charsPrepareOnScene = value; }
		List_1_t49AE3466419291B57C0EF7E386E193A7AFE92C02 * L_0 = ___value0;
		__this->set_charsPrepareOnScene_9(L_0);
		return;
	}
}
// System.Void CharsGenerator::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharsGenerator_Awake_mDD95323710FA15E6F4C60B347451C3CB006142B1 (CharsGenerator_t7E18FC7617D416B9E55B664B885A3138E43EE2A2 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GlobalFunctions_GetComponentObject_TisLevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841_m2F25E5DB707F42022C693C71FCF04CC50FEE9D25_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GlobalFunctions_GetComponentObject_TisRectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_mD0BF1DF9A674788852D027927E99D65003AF708E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5C8C82299B594D71927B7DD56150A1806D97B335);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCE45B43BD3A18C0A8B92DDB8A21820E8B64F43CC);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _charsContainer.LoadResources();
		CharsContainer_tB2FB3D6B21722D0168C892A2C3837608C80C7CC5 * L_0 = __this->get__charsContainer_7();
		NullCheck(L_0);
		CharsContainer_LoadResources_mC434AAC21E19265D813E0B7E3F2EB3B25FD1DABF(L_0, /*hidden argument*/NULL);
		// _charsScene = GlobalFunctions.GetComponentObject<RectTransform>(SceneConfig.CHARS_SCENE_TAG);
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_1;
		L_1 = GlobalFunctions_GetComponentObject_TisRectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_mD0BF1DF9A674788852D027927E99D65003AF708E(_stringLiteral5C8C82299B594D71927B7DD56150A1806D97B335, /*hidden argument*/GlobalFunctions_GetComponentObject_TisRectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_mD0BF1DF9A674788852D027927E99D65003AF708E_RuntimeMethod_var);
		__this->set__charsScene_6(L_1);
		// _levelProvider = GlobalFunctions.GetComponentObject<LevelProvider>(SceneConfig.LEVEL_MANAGEMENT_TAG);
		LevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841 * L_2;
		L_2 = GlobalFunctions_GetComponentObject_TisLevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841_m2F25E5DB707F42022C693C71FCF04CC50FEE9D25(_stringLiteralCE45B43BD3A18C0A8B92DDB8A21820E8B64F43CC, /*hidden argument*/GlobalFunctions_GetComponentObject_TisLevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841_m2F25E5DB707F42022C693C71FCF04CC50FEE9D25_RuntimeMethod_var);
		__this->set__levelProvider_5(L_2);
		// }
		return;
	}
}
// System.Void CharsGenerator::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharsGenerator_Start_mB3AC3DF8DB11E07F1F25F1F8FF7D6776C5046E98 (CharsGenerator_t7E18FC7617D416B9E55B664B885A3138E43EE2A2 * __this, const RuntimeMethod* method)
{
	{
		// _levelProvider.NextLevel();
		LevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841 * L_0 = __this->get__levelProvider_5();
		NullCheck(L_0);
		LevelProvider_NextLevel_mBFD5EAB47CF7715296B1FAF72BF1857983628EDF(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CharsGenerator::CharGenerate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharsGenerator_CharGenerate_m8BC7A85E0E0DB9BAABEB7A473D8692635633FDB3 (CharsGenerator_t7E18FC7617D416B9E55B664B885A3138E43EE2A2 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CharsGenerator_U3CCharGenerateU3Eb__14_0_mA168EE1D18167DC640E364DAFF7164998D4C05D0_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_ToArray_TisCharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595_m0EC984F784602B3309C821383DC5C87FDB30619B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_Where_TisCharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595_m23A550BB94D43A3560AA9F70C03BFAE61ACB6806_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2__ctor_m8AE4F7EFDDED3C0E1D6C23FDA851725EA4F91FF1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_tF8C630B1BAE8BC459BCA2C994588C319051E39EB_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m62B652590809428BA6EDDA52CC5DD40A0ABC0BA6_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Clear_m08CCA40C44AE6824282FFCD856929F8F04530A99_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Clear_m325068E9A3443E1D9B92507B12607D342B659527_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	CharsBundleData_tC2C62F2A7FAEEDB189C65454E3E32437ECD36285 * V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	RuntimeObject* V_2 = NULL;
	uint16_t V_3 = 0;
	CharsDataU5BU5D_t6E1A2B31C527A76C761C25915A588ED83C6B4CDE* V_4 = NULL;
	CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595 * V_5 = NULL;
	CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595 * V_6 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 1> __leave_targets;
	{
		// CharsOnScene.Clear();
		List_1_t37187EA3F5A7945D0B45C8A071194C1191FC32FE * L_0;
		L_0 = CharsGenerator_get_CharsOnScene_m1D6EDE7E5FA590B1195B033ECE89B6BE1F29EBF3_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		List_1_Clear_m08CCA40C44AE6824282FFCD856929F8F04530A99(L_0, /*hidden argument*/List_1_Clear_m08CCA40C44AE6824282FFCD856929F8F04530A99_RuntimeMethod_var);
		// CharsPrepareOnScene.Clear();
		List_1_t49AE3466419291B57C0EF7E386E193A7AFE92C02 * L_1;
		L_1 = CharsGenerator_get_CharsPrepareOnScene_m232D2807B2745CD94DC8AD5E1B1FA8DDE239AB32_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		List_1_Clear_m325068E9A3443E1D9B92507B12607D342B659527(L_1, /*hidden argument*/List_1_Clear_m325068E9A3443E1D9B92507B12607D342B659527_RuntimeMethod_var);
		// _levelProvider.Score = 0;
		LevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841 * L_2 = __this->get__levelProvider_5();
		NullCheck(L_2);
		LevelProvider_set_Score_mBED651E1406AC9A3CDFA39281E646B3B01B63A8B(L_2, 0, /*hidden argument*/NULL);
		// foreach (Transform oldChar in _charsScene)
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_3 = __this->get__charsScene_6();
		NullCheck(L_3);
		RuntimeObject* L_4;
		L_4 = Transform_GetEnumerator_mBA0E884A69F0AA05FCB69F4EE5F700177F75DD7E(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
	}

IL_002e:
	try
	{// begin try (depth: 1)
		{
			goto IL_0045;
		}

IL_0030:
		{
			// foreach (Transform oldChar in _charsScene)
			RuntimeObject* L_5 = V_1;
			NullCheck(L_5);
			RuntimeObject * L_6;
			L_6 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(1 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_5);
			// Destroy(oldChar.gameObject);
			NullCheck(((Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 *)CastclassClass((RuntimeObject*)L_6, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_il2cpp_TypeInfo_var)));
			GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_7;
			L_7 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(((Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 *)CastclassClass((RuntimeObject*)L_6, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
			Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_7, /*hidden argument*/NULL);
		}

IL_0045:
		{
			// foreach (Transform oldChar in _charsScene)
			RuntimeObject* L_8 = V_1;
			NullCheck(L_8);
			bool L_9;
			L_9 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_8);
			if (L_9)
			{
				goto IL_0030;
			}
		}

IL_004d:
		{
			IL2CPP_LEAVE(0x60, FINALLY_004f);
		}
	}// end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_004f;
	}

FINALLY_004f:
	{// begin finally (depth: 1)
		{
			RuntimeObject* L_10 = V_1;
			V_2 = ((RuntimeObject*)IsInst((RuntimeObject*)L_10, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var));
			RuntimeObject* L_11 = V_2;
			if (!L_11)
			{
				goto IL_005f;
			}
		}

IL_0059:
		{
			RuntimeObject* L_12 = V_2;
			NullCheck(L_12);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, L_12);
		}

IL_005f:
		{
			IL2CPP_END_FINALLY(79)
		}
	}// end finally (depth: 1)
	IL2CPP_CLEANUP(79)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x60, IL_0060)
	}

IL_0060:
	{
		// var currentCharBundle = _charsContainer.CharBundles[Random.Range(0, _charsContainer.CharBundles.Length)];
		CharsContainer_tB2FB3D6B21722D0168C892A2C3837608C80C7CC5 * L_13 = __this->get__charsContainer_7();
		NullCheck(L_13);
		CharsBundleDataU5BU5D_t49861CE889FB0A7632E6FD7126DE29B0F470F700* L_14;
		L_14 = CharsContainer_get_CharBundles_mA18F021F7787AC922E81E27192C083AAE634CF39_inline(L_13, /*hidden argument*/NULL);
		CharsContainer_tB2FB3D6B21722D0168C892A2C3837608C80C7CC5 * L_15 = __this->get__charsContainer_7();
		NullCheck(L_15);
		CharsBundleDataU5BU5D_t49861CE889FB0A7632E6FD7126DE29B0F470F700* L_16;
		L_16 = CharsContainer_get_CharBundles_mA18F021F7787AC922E81E27192C083AAE634CF39_inline(L_15, /*hidden argument*/NULL);
		NullCheck(L_16);
		int32_t L_17;
		L_17 = Random_Range_m4B3A0037ACA057F33C94508F908546B9317D996A(0, ((int32_t)((int32_t)(((RuntimeArray*)L_16)->max_length))), /*hidden argument*/NULL);
		NullCheck(L_14);
		int32_t L_18 = L_17;
		CharsBundleData_tC2C62F2A7FAEEDB189C65454E3E32437ECD36285 * L_19 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		V_0 = L_19;
		// for (ushort index = 1; index <= _levelProvider.CurrentCharsCount / 2; index++)
		V_3 = (uint16_t)1;
		goto IL_010b;
	}

IL_0087:
	{
		// var relevantBundle = currentCharBundle.CharsData
		//     .Where(data => charsPrepareOnScene.Where(charPrepare => charPrepare.Identifier == data.Identifier).Count() == 0).ToArray();
		CharsBundleData_tC2C62F2A7FAEEDB189C65454E3E32437ECD36285 * L_20 = V_0;
		NullCheck(L_20);
		CharsDataU5BU5D_t6E1A2B31C527A76C761C25915A588ED83C6B4CDE* L_21;
		L_21 = CharsBundleData_get_CharsData_m2A642A6B655267B4CC126843873D43AA04F9875C_inline(L_20, /*hidden argument*/NULL);
		Func_2_tF8C630B1BAE8BC459BCA2C994588C319051E39EB * L_22 = (Func_2_tF8C630B1BAE8BC459BCA2C994588C319051E39EB *)il2cpp_codegen_object_new(Func_2_tF8C630B1BAE8BC459BCA2C994588C319051E39EB_il2cpp_TypeInfo_var);
		Func_2__ctor_m8AE4F7EFDDED3C0E1D6C23FDA851725EA4F91FF1(L_22, __this, (intptr_t)((intptr_t)CharsGenerator_U3CCharGenerateU3Eb__14_0_mA168EE1D18167DC640E364DAFF7164998D4C05D0_RuntimeMethod_var), /*hidden argument*/Func_2__ctor_m8AE4F7EFDDED3C0E1D6C23FDA851725EA4F91FF1_RuntimeMethod_var);
		RuntimeObject* L_23;
		L_23 = Enumerable_Where_TisCharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595_m23A550BB94D43A3560AA9F70C03BFAE61ACB6806((RuntimeObject*)(RuntimeObject*)L_21, L_22, /*hidden argument*/Enumerable_Where_TisCharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595_m23A550BB94D43A3560AA9F70C03BFAE61ACB6806_RuntimeMethod_var);
		CharsDataU5BU5D_t6E1A2B31C527A76C761C25915A588ED83C6B4CDE* L_24;
		L_24 = Enumerable_ToArray_TisCharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595_m0EC984F784602B3309C821383DC5C87FDB30619B(L_23, /*hidden argument*/Enumerable_ToArray_TisCharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595_m0EC984F784602B3309C821383DC5C87FDB30619B_RuntimeMethod_var);
		V_4 = L_24;
		// if (relevantBundle.Length > 0)
		CharsDataU5BU5D_t6E1A2B31C527A76C761C25915A588ED83C6B4CDE* L_25 = V_4;
		NullCheck(L_25);
		if (!(((RuntimeArray*)L_25)->max_length))
		{
			goto IL_00d5;
		}
	}
	{
		// var bundle = relevantBundle[Random.Range(0, relevantBundle.Length)];
		CharsDataU5BU5D_t6E1A2B31C527A76C761C25915A588ED83C6B4CDE* L_26 = V_4;
		CharsDataU5BU5D_t6E1A2B31C527A76C761C25915A588ED83C6B4CDE* L_27 = V_4;
		NullCheck(L_27);
		int32_t L_28;
		L_28 = Random_Range_m4B3A0037ACA057F33C94508F908546B9317D996A(0, ((int32_t)((int32_t)(((RuntimeArray*)L_27)->max_length))), /*hidden argument*/NULL);
		NullCheck(L_26);
		int32_t L_29 = L_28;
		CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595 * L_30 = (L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_29));
		V_5 = L_30;
		// charsPrepareOnScene.Add(bundle);
		List_1_t49AE3466419291B57C0EF7E386E193A7AFE92C02 * L_31 = __this->get_charsPrepareOnScene_9();
		CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595 * L_32 = V_5;
		NullCheck(L_31);
		List_1_Add_m62B652590809428BA6EDDA52CC5DD40A0ABC0BA6(L_31, L_32, /*hidden argument*/List_1_Add_m62B652590809428BA6EDDA52CC5DD40A0ABC0BA6_RuntimeMethod_var);
		// charsPrepareOnScene.Add(bundle);
		List_1_t49AE3466419291B57C0EF7E386E193A7AFE92C02 * L_33 = __this->get_charsPrepareOnScene_9();
		CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595 * L_34 = V_5;
		NullCheck(L_33);
		List_1_Add_m62B652590809428BA6EDDA52CC5DD40A0ABC0BA6(L_33, L_34, /*hidden argument*/List_1_Add_m62B652590809428BA6EDDA52CC5DD40A0ABC0BA6_RuntimeMethod_var);
		// }
		goto IL_0106;
	}

IL_00d5:
	{
		// var randomChar = currentCharBundle.CharsData[Random.Range(0, currentCharBundle.CharsData.Length)];
		CharsBundleData_tC2C62F2A7FAEEDB189C65454E3E32437ECD36285 * L_35 = V_0;
		NullCheck(L_35);
		CharsDataU5BU5D_t6E1A2B31C527A76C761C25915A588ED83C6B4CDE* L_36;
		L_36 = CharsBundleData_get_CharsData_m2A642A6B655267B4CC126843873D43AA04F9875C_inline(L_35, /*hidden argument*/NULL);
		CharsBundleData_tC2C62F2A7FAEEDB189C65454E3E32437ECD36285 * L_37 = V_0;
		NullCheck(L_37);
		CharsDataU5BU5D_t6E1A2B31C527A76C761C25915A588ED83C6B4CDE* L_38;
		L_38 = CharsBundleData_get_CharsData_m2A642A6B655267B4CC126843873D43AA04F9875C_inline(L_37, /*hidden argument*/NULL);
		NullCheck(L_38);
		int32_t L_39;
		L_39 = Random_Range_m4B3A0037ACA057F33C94508F908546B9317D996A(0, ((int32_t)((int32_t)(((RuntimeArray*)L_38)->max_length))), /*hidden argument*/NULL);
		NullCheck(L_36);
		int32_t L_40 = L_39;
		CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595 * L_41 = (L_36)->GetAt(static_cast<il2cpp_array_size_t>(L_40));
		V_6 = L_41;
		// charsPrepareOnScene.Add(randomChar);
		List_1_t49AE3466419291B57C0EF7E386E193A7AFE92C02 * L_42 = __this->get_charsPrepareOnScene_9();
		CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595 * L_43 = V_6;
		NullCheck(L_42);
		List_1_Add_m62B652590809428BA6EDDA52CC5DD40A0ABC0BA6(L_42, L_43, /*hidden argument*/List_1_Add_m62B652590809428BA6EDDA52CC5DD40A0ABC0BA6_RuntimeMethod_var);
		// charsPrepareOnScene.Add(randomChar);
		List_1_t49AE3466419291B57C0EF7E386E193A7AFE92C02 * L_44 = __this->get_charsPrepareOnScene_9();
		CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595 * L_45 = V_6;
		NullCheck(L_44);
		List_1_Add_m62B652590809428BA6EDDA52CC5DD40A0ABC0BA6(L_44, L_45, /*hidden argument*/List_1_Add_m62B652590809428BA6EDDA52CC5DD40A0ABC0BA6_RuntimeMethod_var);
	}

IL_0106:
	{
		// for (ushort index = 1; index <= _levelProvider.CurrentCharsCount / 2; index++)
		uint16_t L_46 = V_3;
		V_3 = (uint16_t)((int32_t)((uint16_t)((int32_t)il2cpp_codegen_add((int32_t)L_46, (int32_t)1))));
	}

IL_010b:
	{
		// for (ushort index = 1; index <= _levelProvider.CurrentCharsCount / 2; index++)
		uint16_t L_47 = V_3;
		LevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841 * L_48 = __this->get__levelProvider_5();
		NullCheck(L_48);
		int32_t L_49;
		L_49 = LevelProvider_get_CurrentCharsCount_m3F727FCD9E869AE74F8BF985E69C22B17810A87E_inline(L_48, /*hidden argument*/NULL);
		if ((((int32_t)L_47) <= ((int32_t)((int32_t)((int32_t)L_49/(int32_t)2)))))
		{
			goto IL_0087;
		}
	}
	{
		// SetCharsOnScene();
		CharsGenerator_SetCharsOnScene_m9300C8B08B98F7512C9A5063969280675DBAD740(__this, /*hidden argument*/NULL);
		// StartCoroutine(InitializeEffect());
		RuntimeObject* L_50;
		L_50 = CharsGenerator_InitializeEffect_m35BE446407A6FF90266A52ED23782D4FD05CA3E1(__this, /*hidden argument*/NULL);
		Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * L_51;
		L_51 = MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719(__this, L_50, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CharsGenerator::SetCharsOnScene()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharsGenerator_SetCharsOnScene_m9300C8B08B98F7512C9A5063969280675DBAD740 (CharsGenerator_t7E18FC7617D416B9E55B664B885A3138E43EE2A2 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_ToArray_m947BDA62E5FAFE13045E535DACCC691670751CA1_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	CharsDataU5BU5D_t6E1A2B31C527A76C761C25915A588ED83C6B4CDE* V_0 = NULL;
	int32_t V_1 = 0;
	CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595 * V_2 = NULL;
	{
		// var shuffleArray = ShuffleArray(charsPrepareOnScene.ToArray());
		List_1_t49AE3466419291B57C0EF7E386E193A7AFE92C02 * L_0 = __this->get_charsPrepareOnScene_9();
		NullCheck(L_0);
		CharsDataU5BU5D_t6E1A2B31C527A76C761C25915A588ED83C6B4CDE* L_1;
		L_1 = List_1_ToArray_m947BDA62E5FAFE13045E535DACCC691670751CA1(L_0, /*hidden argument*/List_1_ToArray_m947BDA62E5FAFE13045E535DACCC691670751CA1_RuntimeMethod_var);
		CharsDataU5BU5D_t6E1A2B31C527A76C761C25915A588ED83C6B4CDE* L_2;
		L_2 = CharsGenerator_ShuffleArray_m7EAB71A9D642F1173D498254A1F7064F0362AE19(__this, L_1, /*hidden argument*/NULL);
		// foreach (var charData in shuffleArray)
		V_0 = L_2;
		V_1 = 0;
		goto IL_0025;
	}

IL_0016:
	{
		// foreach (var charData in shuffleArray)
		CharsDataU5BU5D_t6E1A2B31C527A76C761C25915A588ED83C6B4CDE* L_3 = V_0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595 * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		V_2 = L_6;
		// AddCharOnScene(charData);
		CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595 * L_7 = V_2;
		CharsGenerator_AddCharOnScene_m96078093CE4F424BD846646FBC495CE93C19AD03(__this, L_7, /*hidden argument*/NULL);
		int32_t L_8 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1));
	}

IL_0025:
	{
		// foreach (var charData in shuffleArray)
		int32_t L_9 = V_1;
		CharsDataU5BU5D_t6E1A2B31C527A76C761C25915A588ED83C6B4CDE* L_10 = V_0;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_10)->max_length))))))
		{
			goto IL_0016;
		}
	}
	{
		// }
		return;
	}
}
// CharsData[] CharsGenerator::ShuffleArray(CharsData[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CharsDataU5BU5D_t6E1A2B31C527A76C761C25915A588ED83C6B4CDE* CharsGenerator_ShuffleArray_m7EAB71A9D642F1173D498254A1F7064F0362AE19 (CharsGenerator_t7E18FC7617D416B9E55B664B885A3138E43EE2A2 * __this, CharsDataU5BU5D_t6E1A2B31C527A76C761C25915A588ED83C6B4CDE* ___data0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Random_t6C9E9775A149D0ADCFEB4B252C408F03EE870118_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Random_t6C9E9775A149D0ADCFEB4B252C408F03EE870118 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595 * V_3 = NULL;
	{
		// System.Random rnd = new System.Random();
		Random_t6C9E9775A149D0ADCFEB4B252C408F03EE870118 * L_0 = (Random_t6C9E9775A149D0ADCFEB4B252C408F03EE870118 *)il2cpp_codegen_object_new(Random_t6C9E9775A149D0ADCFEB4B252C408F03EE870118_il2cpp_TypeInfo_var);
		Random__ctor_mF40AD1812BABC06235B661CCE513E4F74EEE9F05(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		// for (int i = data.Length - 1; i >= 1; i--)
		CharsDataU5BU5D_t6E1A2B31C527A76C761C25915A588ED83C6B4CDE* L_1 = ___data0;
		NullCheck(L_1);
		V_1 = ((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_1)->max_length))), (int32_t)1));
		goto IL_002a;
	}

IL_000e:
	{
		// int j = rnd.Next(i+1);
		Random_t6C9E9775A149D0ADCFEB4B252C408F03EE870118 * L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		int32_t L_4;
		L_4 = VirtualFuncInvoker1< int32_t, int32_t >::Invoke(5 /* System.Int32 System.Random::Next(System.Int32) */, L_2, ((int32_t)il2cpp_codegen_add((int32_t)L_3, (int32_t)1)));
		V_2 = L_4;
		// var temp = data[j];
		CharsDataU5BU5D_t6E1A2B31C527A76C761C25915A588ED83C6B4CDE* L_5 = ___data0;
		int32_t L_6 = V_2;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595 * L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		V_3 = L_8;
		// data[j] = data[i];
		CharsDataU5BU5D_t6E1A2B31C527A76C761C25915A588ED83C6B4CDE* L_9 = ___data0;
		int32_t L_10 = V_2;
		CharsDataU5BU5D_t6E1A2B31C527A76C761C25915A588ED83C6B4CDE* L_11 = ___data0;
		int32_t L_12 = V_1;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595 * L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, L_14);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(L_10), (CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595 *)L_14);
		// data[i] = temp;
		CharsDataU5BU5D_t6E1A2B31C527A76C761C25915A588ED83C6B4CDE* L_15 = ___data0;
		int32_t L_16 = V_1;
		CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595 * L_17 = V_3;
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, L_17);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(L_16), (CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595 *)L_17);
		// for (int i = data.Length - 1; i >= 1; i--)
		int32_t L_18 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_18, (int32_t)1));
	}

IL_002a:
	{
		// for (int i = data.Length - 1; i >= 1; i--)
		int32_t L_19 = V_1;
		if ((((int32_t)L_19) >= ((int32_t)1)))
		{
			goto IL_000e;
		}
	}
	{
		// return data;
		CharsDataU5BU5D_t6E1A2B31C527A76C761C25915A588ED83C6B4CDE* L_20 = ___data0;
		return L_20;
	}
}
// System.Void CharsGenerator::AddCharOnScene(CharsData)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharsGenerator_AddCharOnScene_m96078093CE4F424BD846646FBC495CE93C19AD03 (CharsGenerator_t7E18FC7617D416B9E55B664B885A3138E43EE2A2 * __this, CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595 * ___randomCharData0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisCharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD_m8194715FED3744A59E689261461A2D3106DC7A30_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m6225D4407E2F7A63072835132E8B4A07A69FA874_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_mACEFD07C5A0FE9C85BC165DF1E4A825675719EAC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * V_0 = NULL;
	{
		// var newChar = Instantiate(_cellPrefab, _charsScene);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get__cellPrefab_4();
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_1 = __this->get__charsScene_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2;
		L_2 = Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_mACEFD07C5A0FE9C85BC165DF1E4A825675719EAC(L_0, L_1, /*hidden argument*/Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_mACEFD07C5A0FE9C85BC165DF1E4A825675719EAC_RuntimeMethod_var);
		// var control = newChar.GetComponent<CharControl>();
		NullCheck(L_2);
		CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * L_3;
		L_3 = GameObject_GetComponent_TisCharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD_m8194715FED3744A59E689261461A2D3106DC7A30(L_2, /*hidden argument*/GameObject_GetComponent_TisCharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD_m8194715FED3744A59E689261461A2D3106DC7A30_RuntimeMethod_var);
		V_0 = L_3;
		// control.Chardata = randomCharData;
		CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * L_4 = V_0;
		CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595 * L_5 = ___randomCharData0;
		NullCheck(L_4);
		CharControl_set_Chardata_m389A1A07EF79E9431F64F3CFAC17A1156BB844EB(L_4, L_5, /*hidden argument*/NULL);
		// CharsOnScene.Add(control);
		List_1_t37187EA3F5A7945D0B45C8A071194C1191FC32FE * L_6;
		L_6 = CharsGenerator_get_CharsOnScene_m1D6EDE7E5FA590B1195B033ECE89B6BE1F29EBF3_inline(__this, /*hidden argument*/NULL);
		CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * L_7 = V_0;
		NullCheck(L_6);
		List_1_Add_m6225D4407E2F7A63072835132E8B4A07A69FA874(L_6, L_7, /*hidden argument*/List_1_Add_m6225D4407E2F7A63072835132E8B4A07A69FA874_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Collections.IEnumerator CharsGenerator::InitializeEffect()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* CharsGenerator_InitializeEffect_m35BE446407A6FF90266A52ED23782D4FD05CA3E1 (CharsGenerator_t7E18FC7617D416B9E55B664B885A3138E43EE2A2 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CInitializeEffectU3Ed__18_tB5CE3179A9E4153E9DF3477D7479D251A763486C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CInitializeEffectU3Ed__18_tB5CE3179A9E4153E9DF3477D7479D251A763486C * L_0 = (U3CInitializeEffectU3Ed__18_tB5CE3179A9E4153E9DF3477D7479D251A763486C *)il2cpp_codegen_object_new(U3CInitializeEffectU3Ed__18_tB5CE3179A9E4153E9DF3477D7479D251A763486C_il2cpp_TypeInfo_var);
		U3CInitializeEffectU3Ed__18__ctor_m2963AC28072621513A43A21896B10ACA02389E7F(L_0, 0, /*hidden argument*/NULL);
		U3CInitializeEffectU3Ed__18_tB5CE3179A9E4153E9DF3477D7479D251A763486C * L_1 = L_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_2(__this);
		return L_1;
	}
}
// System.Void CharsGenerator::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharsGenerator__ctor_mC5385FED60828331DC38005C2DF67ACA1E6C0B0A (CharsGenerator_t7E18FC7617D416B9E55B664B885A3138E43EE2A2 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CharsContainer_tB2FB3D6B21722D0168C892A2C3837608C80C7CC5_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m62662109EBBF48C763AF94C725FCC99A03E364FD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m6C9F4356B1609DBC166481253083E775FDA52FFC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t37187EA3F5A7945D0B45C8A071194C1191FC32FE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t49AE3466419291B57C0EF7E386E193A7AFE92C02_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private CharsContainer _charsContainer = new CharsContainer();
		CharsContainer_tB2FB3D6B21722D0168C892A2C3837608C80C7CC5 * L_0 = (CharsContainer_tB2FB3D6B21722D0168C892A2C3837608C80C7CC5 *)il2cpp_codegen_object_new(CharsContainer_tB2FB3D6B21722D0168C892A2C3837608C80C7CC5_il2cpp_TypeInfo_var);
		CharsContainer__ctor_m605ACE9AA15C5819B90DFAF384BC7A972520D52F(L_0, /*hidden argument*/NULL);
		__this->set__charsContainer_7(L_0);
		// private List<CharControl> charsOnScene = new List<CharControl>();
		List_1_t37187EA3F5A7945D0B45C8A071194C1191FC32FE * L_1 = (List_1_t37187EA3F5A7945D0B45C8A071194C1191FC32FE *)il2cpp_codegen_object_new(List_1_t37187EA3F5A7945D0B45C8A071194C1191FC32FE_il2cpp_TypeInfo_var);
		List_1__ctor_m6C9F4356B1609DBC166481253083E775FDA52FFC(L_1, /*hidden argument*/List_1__ctor_m6C9F4356B1609DBC166481253083E775FDA52FFC_RuntimeMethod_var);
		__this->set_charsOnScene_8(L_1);
		// private List<CharsData> charsPrepareOnScene = new List<CharsData>();
		List_1_t49AE3466419291B57C0EF7E386E193A7AFE92C02 * L_2 = (List_1_t49AE3466419291B57C0EF7E386E193A7AFE92C02 *)il2cpp_codegen_object_new(List_1_t49AE3466419291B57C0EF7E386E193A7AFE92C02_il2cpp_TypeInfo_var);
		List_1__ctor_m62662109EBBF48C763AF94C725FCC99A03E364FD(L_2, /*hidden argument*/List_1__ctor_m62662109EBBF48C763AF94C725FCC99A03E364FD_RuntimeMethod_var);
		__this->set_charsPrepareOnScene_9(L_2);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean CharsGenerator::<CharGenerate>b__14_0(CharsData)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CharsGenerator_U3CCharGenerateU3Eb__14_0_mA168EE1D18167DC640E364DAFF7164998D4C05D0 (CharsGenerator_t7E18FC7617D416B9E55B664B885A3138E43EE2A2 * __this, CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595 * ___data0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_Count_TisCharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595_mF5CBB5FDA01799955A84315875FF0232F7C53FA0_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_Where_TisCharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595_m23A550BB94D43A3560AA9F70C03BFAE61ACB6806_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2__ctor_m8AE4F7EFDDED3C0E1D6C23FDA851725EA4F91FF1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_tF8C630B1BAE8BC459BCA2C994588C319051E39EB_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass14_0_U3CCharGenerateU3Eb__1_m42A572588759F99FADE2AD7CBE4DE57A6339F4AF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass14_0_t79B660C3CA0F908E689E0E2E7221DF954F7E5E42_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass14_0_t79B660C3CA0F908E689E0E2E7221DF954F7E5E42 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass14_0_t79B660C3CA0F908E689E0E2E7221DF954F7E5E42 * L_0 = (U3CU3Ec__DisplayClass14_0_t79B660C3CA0F908E689E0E2E7221DF954F7E5E42 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass14_0_t79B660C3CA0F908E689E0E2E7221DF954F7E5E42_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass14_0__ctor_mDF2E4F72C494882B94DF063938E248EE6108DDAF(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass14_0_t79B660C3CA0F908E689E0E2E7221DF954F7E5E42 * L_1 = V_0;
		CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595 * L_2 = ___data0;
		NullCheck(L_1);
		L_1->set_data_0(L_2);
		// .Where(data => charsPrepareOnScene.Where(charPrepare => charPrepare.Identifier == data.Identifier).Count() == 0).ToArray();
		List_1_t49AE3466419291B57C0EF7E386E193A7AFE92C02 * L_3 = __this->get_charsPrepareOnScene_9();
		U3CU3Ec__DisplayClass14_0_t79B660C3CA0F908E689E0E2E7221DF954F7E5E42 * L_4 = V_0;
		Func_2_tF8C630B1BAE8BC459BCA2C994588C319051E39EB * L_5 = (Func_2_tF8C630B1BAE8BC459BCA2C994588C319051E39EB *)il2cpp_codegen_object_new(Func_2_tF8C630B1BAE8BC459BCA2C994588C319051E39EB_il2cpp_TypeInfo_var);
		Func_2__ctor_m8AE4F7EFDDED3C0E1D6C23FDA851725EA4F91FF1(L_5, L_4, (intptr_t)((intptr_t)U3CU3Ec__DisplayClass14_0_U3CCharGenerateU3Eb__1_m42A572588759F99FADE2AD7CBE4DE57A6339F4AF_RuntimeMethod_var), /*hidden argument*/Func_2__ctor_m8AE4F7EFDDED3C0E1D6C23FDA851725EA4F91FF1_RuntimeMethod_var);
		RuntimeObject* L_6;
		L_6 = Enumerable_Where_TisCharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595_m23A550BB94D43A3560AA9F70C03BFAE61ACB6806(L_3, L_5, /*hidden argument*/Enumerable_Where_TisCharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595_m23A550BB94D43A3560AA9F70C03BFAE61ACB6806_RuntimeMethod_var);
		int32_t L_7;
		L_7 = Enumerable_Count_TisCharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595_mF5CBB5FDA01799955A84315875FF0232F7C53FA0(L_6, /*hidden argument*/Enumerable_Count_TisCharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595_mF5CBB5FDA01799955A84315875FF0232F7C53FA0_RuntimeMethod_var);
		return (bool)((((int32_t)L_7) == ((int32_t)0))? 1 : 0);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModulePhysics::DOPath(UnityEngine.Rigidbody,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,DG.Tweening.PathMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * DOTweenModulePhysics_DOPath_m3C49FFEB71D494F474F7223248D7105E90CF5BE7 (Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * ___target0, Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * ___path1, float ___duration2, int32_t ___pathMode3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOGetter_1__ctor_m7537F12632FAD0C4475AC13323E07B2D2C6FF081_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOGetter_1_tE8D2419A0219EB945463B02351F4905C77235E1A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOSetter_1__ctor_m23DF8B62FE29CBDA90726662AEA2755DF765E733_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOSetter_1_tAEE38AA9321AE4A0A4A4E5D496EA753BFB746B85_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOTween_To_TisVector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_TisPath_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5_TisPathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A_m7C0FFBC94D84A7FAD1568988AC768527794B9F65_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Rigidbody_MovePosition_mB3CBBF21FD0ABB88BC6C004B993DED25673001C7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_mBF1153F332C5ED368722DFB51E2E088E1A8648E0_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass9_0_U3CDOPathU3Eb__0_mD050544C13BA54260FC94E0964EF204B4EDB2628_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass9_0_tB9266040A99BBE64FC737A175355345A13E86D21_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass9_0_tB9266040A99BBE64FC737A175355345A13E86D21 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass9_0_tB9266040A99BBE64FC737A175355345A13E86D21 * L_0 = (U3CU3Ec__DisplayClass9_0_tB9266040A99BBE64FC737A175355345A13E86D21 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass9_0_tB9266040A99BBE64FC737A175355345A13E86D21_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass9_0__ctor_mCA1E6F241784A13DEC15E12A0219479D552BB6BB(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass9_0_tB9266040A99BBE64FC737A175355345A13E86D21 * L_1 = V_0;
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		// TweenerCore<Vector3, Path, PathOptions> t = DOTween.To(PathPlugin.Get(), () => target.position, target.MovePosition, path, duration)
		//     .SetTarget(target);
		ABSTweenPlugin_3_t0048BB94992A96F02D427E2C26C6E92AC9AECD32 * L_3;
		L_3 = PathPlugin_Get_m1D5B2D927828EF5D7543475191CAE0B620A0A5A3(/*hidden argument*/NULL);
		U3CU3Ec__DisplayClass9_0_tB9266040A99BBE64FC737A175355345A13E86D21 * L_4 = V_0;
		DOGetter_1_tE8D2419A0219EB945463B02351F4905C77235E1A * L_5 = (DOGetter_1_tE8D2419A0219EB945463B02351F4905C77235E1A *)il2cpp_codegen_object_new(DOGetter_1_tE8D2419A0219EB945463B02351F4905C77235E1A_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m7537F12632FAD0C4475AC13323E07B2D2C6FF081(L_5, L_4, (intptr_t)((intptr_t)U3CU3Ec__DisplayClass9_0_U3CDOPathU3Eb__0_mD050544C13BA54260FC94E0964EF204B4EDB2628_RuntimeMethod_var), /*hidden argument*/DOGetter_1__ctor_m7537F12632FAD0C4475AC13323E07B2D2C6FF081_RuntimeMethod_var);
		U3CU3Ec__DisplayClass9_0_tB9266040A99BBE64FC737A175355345A13E86D21 * L_6 = V_0;
		NullCheck(L_6);
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_7 = L_6->get_target_0();
		DOSetter_1_tAEE38AA9321AE4A0A4A4E5D496EA753BFB746B85 * L_8 = (DOSetter_1_tAEE38AA9321AE4A0A4A4E5D496EA753BFB746B85 *)il2cpp_codegen_object_new(DOSetter_1_tAEE38AA9321AE4A0A4A4E5D496EA753BFB746B85_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m23DF8B62FE29CBDA90726662AEA2755DF765E733(L_8, L_7, (intptr_t)((intptr_t)Rigidbody_MovePosition_mB3CBBF21FD0ABB88BC6C004B993DED25673001C7_RuntimeMethod_var), /*hidden argument*/DOSetter_1__ctor_m23DF8B62FE29CBDA90726662AEA2755DF765E733_RuntimeMethod_var);
		Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * L_9 = ___path1;
		float L_10 = ___duration2;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_il2cpp_TypeInfo_var);
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_11;
		L_11 = DOTween_To_TisVector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_TisPath_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5_TisPathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A_m7C0FFBC94D84A7FAD1568988AC768527794B9F65(L_3, L_5, L_8, L_9, L_10, /*hidden argument*/DOTween_To_TisVector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_TisPath_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5_TisPathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A_m7C0FFBC94D84A7FAD1568988AC768527794B9F65_RuntimeMethod_var);
		U3CU3Ec__DisplayClass9_0_tB9266040A99BBE64FC737A175355345A13E86D21 * L_12 = V_0;
		NullCheck(L_12);
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_13 = L_12->get_target_0();
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_14;
		L_14 = TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_mBF1153F332C5ED368722DFB51E2E088E1A8648E0(L_11, L_13, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_mBF1153F332C5ED368722DFB51E2E088E1A8648E0_RuntimeMethod_var);
		// t.plugOptions.isRigidbody = true;
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_15 = L_14;
		NullCheck(L_15);
		PathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A * L_16 = L_15->get_address_of_plugOptions_59();
		L_16->set_isRigidbody_12((bool)1);
		// t.plugOptions.mode = pathMode;
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_17 = L_15;
		NullCheck(L_17);
		PathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A * L_18 = L_17->get_address_of_plugOptions_59();
		int32_t L_19 = ___pathMode3;
		L_18->set_mode_0(L_19);
		// return t;
		return L_17;
	}
}
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModulePhysics::DOLocalPath(UnityEngine.Rigidbody,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,DG.Tweening.PathMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * DOTweenModulePhysics_DOLocalPath_m6F939DAF66D56DA9B52DA62546C70F3862E14193 (Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * ___target0, Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * ___path1, float ___duration2, int32_t ___pathMode3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOGetter_1__ctor_m7537F12632FAD0C4475AC13323E07B2D2C6FF081_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOGetter_1_tE8D2419A0219EB945463B02351F4905C77235E1A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOSetter_1__ctor_m23DF8B62FE29CBDA90726662AEA2755DF765E733_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOSetter_1_tAEE38AA9321AE4A0A4A4E5D496EA753BFB746B85_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOTween_To_TisVector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_TisPath_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5_TisPathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A_m7C0FFBC94D84A7FAD1568988AC768527794B9F65_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_mBF1153F332C5ED368722DFB51E2E088E1A8648E0_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass10_0_U3CDOLocalPathU3Eb__0_m3089CA33F25546258A5E846EA51421D71AC55D70_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass10_0_U3CDOLocalPathU3Eb__1_mD201EFBB4403FBDEC0D377D39A768A581E6CCDDD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass10_0_t6B6DFEAD8FF1DD6A724C294AA0B120F82A7A6937_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass10_0_t6B6DFEAD8FF1DD6A724C294AA0B120F82A7A6937 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass10_0_t6B6DFEAD8FF1DD6A724C294AA0B120F82A7A6937 * L_0 = (U3CU3Ec__DisplayClass10_0_t6B6DFEAD8FF1DD6A724C294AA0B120F82A7A6937 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass10_0_t6B6DFEAD8FF1DD6A724C294AA0B120F82A7A6937_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass10_0__ctor_m819824BC9854A0C90662B04D48B88CA7B052BF83(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass10_0_t6B6DFEAD8FF1DD6A724C294AA0B120F82A7A6937 * L_1 = V_0;
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_1(L_2);
		// Transform trans = target.transform;
		U3CU3Ec__DisplayClass10_0_t6B6DFEAD8FF1DD6A724C294AA0B120F82A7A6937 * L_3 = V_0;
		U3CU3Ec__DisplayClass10_0_t6B6DFEAD8FF1DD6A724C294AA0B120F82A7A6937 * L_4 = V_0;
		NullCheck(L_4);
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_5 = L_4->get_target_1();
		NullCheck(L_5);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_6;
		L_6 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_5, /*hidden argument*/NULL);
		NullCheck(L_3);
		L_3->set_trans_0(L_6);
		// TweenerCore<Vector3, Path, PathOptions> t = DOTween.To(PathPlugin.Get(), () => trans.localPosition, x => target.MovePosition(trans.parent == null ? x : trans.parent.TransformPoint(x)), path, duration)
		//     .SetTarget(target);
		ABSTweenPlugin_3_t0048BB94992A96F02D427E2C26C6E92AC9AECD32 * L_7;
		L_7 = PathPlugin_Get_m1D5B2D927828EF5D7543475191CAE0B620A0A5A3(/*hidden argument*/NULL);
		U3CU3Ec__DisplayClass10_0_t6B6DFEAD8FF1DD6A724C294AA0B120F82A7A6937 * L_8 = V_0;
		DOGetter_1_tE8D2419A0219EB945463B02351F4905C77235E1A * L_9 = (DOGetter_1_tE8D2419A0219EB945463B02351F4905C77235E1A *)il2cpp_codegen_object_new(DOGetter_1_tE8D2419A0219EB945463B02351F4905C77235E1A_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m7537F12632FAD0C4475AC13323E07B2D2C6FF081(L_9, L_8, (intptr_t)((intptr_t)U3CU3Ec__DisplayClass10_0_U3CDOLocalPathU3Eb__0_m3089CA33F25546258A5E846EA51421D71AC55D70_RuntimeMethod_var), /*hidden argument*/DOGetter_1__ctor_m7537F12632FAD0C4475AC13323E07B2D2C6FF081_RuntimeMethod_var);
		U3CU3Ec__DisplayClass10_0_t6B6DFEAD8FF1DD6A724C294AA0B120F82A7A6937 * L_10 = V_0;
		DOSetter_1_tAEE38AA9321AE4A0A4A4E5D496EA753BFB746B85 * L_11 = (DOSetter_1_tAEE38AA9321AE4A0A4A4E5D496EA753BFB746B85 *)il2cpp_codegen_object_new(DOSetter_1_tAEE38AA9321AE4A0A4A4E5D496EA753BFB746B85_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m23DF8B62FE29CBDA90726662AEA2755DF765E733(L_11, L_10, (intptr_t)((intptr_t)U3CU3Ec__DisplayClass10_0_U3CDOLocalPathU3Eb__1_mD201EFBB4403FBDEC0D377D39A768A581E6CCDDD_RuntimeMethod_var), /*hidden argument*/DOSetter_1__ctor_m23DF8B62FE29CBDA90726662AEA2755DF765E733_RuntimeMethod_var);
		Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * L_12 = ___path1;
		float L_13 = ___duration2;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_il2cpp_TypeInfo_var);
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_14;
		L_14 = DOTween_To_TisVector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_TisPath_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5_TisPathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A_m7C0FFBC94D84A7FAD1568988AC768527794B9F65(L_7, L_9, L_11, L_12, L_13, /*hidden argument*/DOTween_To_TisVector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_TisPath_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5_TisPathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A_m7C0FFBC94D84A7FAD1568988AC768527794B9F65_RuntimeMethod_var);
		U3CU3Ec__DisplayClass10_0_t6B6DFEAD8FF1DD6A724C294AA0B120F82A7A6937 * L_15 = V_0;
		NullCheck(L_15);
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_16 = L_15->get_target_1();
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_17;
		L_17 = TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_mBF1153F332C5ED368722DFB51E2E088E1A8648E0(L_14, L_16, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_mBF1153F332C5ED368722DFB51E2E088E1A8648E0_RuntimeMethod_var);
		// t.plugOptions.isRigidbody = true;
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_18 = L_17;
		NullCheck(L_18);
		PathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A * L_19 = L_18->get_address_of_plugOptions_59();
		L_19->set_isRigidbody_12((bool)1);
		// t.plugOptions.mode = pathMode;
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_20 = L_18;
		NullCheck(L_20);
		PathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A * L_21 = L_20->get_address_of_plugOptions_59();
		int32_t L_22 = ___pathMode3;
		L_21->set_mode_0(L_22);
		// t.plugOptions.useLocalPosition = true;
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_23 = L_20;
		NullCheck(L_23);
		PathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A * L_24 = L_23->get_address_of_plugOptions_59();
		L_24->set_useLocalPosition_10((bool)1);
		// return t;
		return L_23;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOFade(UnityEngine.UI.Image,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TweenerCore_3_t551A69CD4FF610AE9DF07C97A86B6FE12EBEB597 * DOTweenModuleUI_DOFade_mE66C8B7D137E0DDE54AAA96F551811FB8BDB8754 (Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___target0, float ___endValue1, float ___duration2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOGetter_1__ctor_mAC32C501C09F9F6EC5B0DD2C1A8473BAE88DC8CE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOGetter_1_tCC9DEA9BB7DA5C2570ED2CE9BD13EF5C154B7B10_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOSetter_1__ctor_m41D0232769B743141B4389F8A4599EC78F310352_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOSetter_1_tB62013E4617ECB51822384C8592AAB764E1662BF_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t551A69CD4FF610AE9DF07C97A86B6FE12EBEB597_m8DA85B9EBDD18D4817AB73585673258C3904ED11_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__0_m507CD55516001BB4A76D7E58CB159B5394D6413C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__1_mE65F1E4D380EC305AF0AE93E0315B0B23CA36310_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass4_0_t9CD30C09F13A8901FBD37CEEE628023A3AC88587_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass4_0_t9CD30C09F13A8901FBD37CEEE628023A3AC88587 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass4_0_t9CD30C09F13A8901FBD37CEEE628023A3AC88587 * L_0 = (U3CU3Ec__DisplayClass4_0_t9CD30C09F13A8901FBD37CEEE628023A3AC88587 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass4_0_t9CD30C09F13A8901FBD37CEEE628023A3AC88587_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass4_0__ctor_m5CD451B862F3CC71D8D25BC3A69725FCDD6144C4(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass4_0_t9CD30C09F13A8901FBD37CEEE628023A3AC88587 * L_1 = V_0;
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		// TweenerCore<Color, Color, ColorOptions> t = DOTween.ToAlpha(() => target.color, x => target.color = x, endValue, duration);
		U3CU3Ec__DisplayClass4_0_t9CD30C09F13A8901FBD37CEEE628023A3AC88587 * L_3 = V_0;
		DOGetter_1_tCC9DEA9BB7DA5C2570ED2CE9BD13EF5C154B7B10 * L_4 = (DOGetter_1_tCC9DEA9BB7DA5C2570ED2CE9BD13EF5C154B7B10 *)il2cpp_codegen_object_new(DOGetter_1_tCC9DEA9BB7DA5C2570ED2CE9BD13EF5C154B7B10_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_mAC32C501C09F9F6EC5B0DD2C1A8473BAE88DC8CE(L_4, L_3, (intptr_t)((intptr_t)U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__0_m507CD55516001BB4A76D7E58CB159B5394D6413C_RuntimeMethod_var), /*hidden argument*/DOGetter_1__ctor_mAC32C501C09F9F6EC5B0DD2C1A8473BAE88DC8CE_RuntimeMethod_var);
		U3CU3Ec__DisplayClass4_0_t9CD30C09F13A8901FBD37CEEE628023A3AC88587 * L_5 = V_0;
		DOSetter_1_tB62013E4617ECB51822384C8592AAB764E1662BF * L_6 = (DOSetter_1_tB62013E4617ECB51822384C8592AAB764E1662BF *)il2cpp_codegen_object_new(DOSetter_1_tB62013E4617ECB51822384C8592AAB764E1662BF_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m41D0232769B743141B4389F8A4599EC78F310352(L_6, L_5, (intptr_t)((intptr_t)U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__1_mE65F1E4D380EC305AF0AE93E0315B0B23CA36310_RuntimeMethod_var), /*hidden argument*/DOSetter_1__ctor_m41D0232769B743141B4389F8A4599EC78F310352_RuntimeMethod_var);
		float L_7 = ___endValue1;
		float L_8 = ___duration2;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_il2cpp_TypeInfo_var);
		TweenerCore_3_t551A69CD4FF610AE9DF07C97A86B6FE12EBEB597 * L_9;
		L_9 = DOTween_ToAlpha_mB80D122D92ABC7A45500CE5FDA58FAA8B7688C16(L_4, L_6, L_7, L_8, /*hidden argument*/NULL);
		// t.SetTarget(target);
		TweenerCore_3_t551A69CD4FF610AE9DF07C97A86B6FE12EBEB597 * L_10 = L_9;
		U3CU3Ec__DisplayClass4_0_t9CD30C09F13A8901FBD37CEEE628023A3AC88587 * L_11 = V_0;
		NullCheck(L_11);
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_12 = L_11->get_target_0();
		TweenerCore_3_t551A69CD4FF610AE9DF07C97A86B6FE12EBEB597 * L_13;
		L_13 = TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t551A69CD4FF610AE9DF07C97A86B6FE12EBEB597_m8DA85B9EBDD18D4817AB73585673258C3904ED11(L_10, L_12, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t551A69CD4FF610AE9DF07C97A86B6FE12EBEB597_m8DA85B9EBDD18D4817AB73585673258C3904ED11_RuntimeMethod_var);
		// return t;
		return L_10;
	}
}
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOFade(UnityEngine.UI.Text,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TweenerCore_3_t551A69CD4FF610AE9DF07C97A86B6FE12EBEB597 * DOTweenModuleUI_DOFade_m21F7CCBB732ABBD63BFD893D1A8D9837532A70D1 (Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___target0, float ___endValue1, float ___duration2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOGetter_1__ctor_mAC32C501C09F9F6EC5B0DD2C1A8473BAE88DC8CE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOGetter_1_tCC9DEA9BB7DA5C2570ED2CE9BD13EF5C154B7B10_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOSetter_1__ctor_m41D0232769B743141B4389F8A4599EC78F310352_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOSetter_1_tB62013E4617ECB51822384C8592AAB764E1662BF_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t551A69CD4FF610AE9DF07C97A86B6FE12EBEB597_m8DA85B9EBDD18D4817AB73585673258C3904ED11_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass36_0_U3CDOFadeU3Eb__0_mA3238766629BBF7E2750683F62F7629C5B097D87_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass36_0_U3CDOFadeU3Eb__1_m119575F5E470CA6D0C37EF9E1A9C3A2CF8B2AA3A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass36_0_tCE8E904A62CBD9F143B3D692CA532EFA7A6F777D_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass36_0_tCE8E904A62CBD9F143B3D692CA532EFA7A6F777D * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass36_0_tCE8E904A62CBD9F143B3D692CA532EFA7A6F777D * L_0 = (U3CU3Ec__DisplayClass36_0_tCE8E904A62CBD9F143B3D692CA532EFA7A6F777D *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass36_0_tCE8E904A62CBD9F143B3D692CA532EFA7A6F777D_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass36_0__ctor_mE2912C6BF4318E28870DE7AA92E4B05C80D92EE0(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass36_0_tCE8E904A62CBD9F143B3D692CA532EFA7A6F777D * L_1 = V_0;
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		// TweenerCore<Color, Color, ColorOptions> t = DOTween.ToAlpha(() => target.color, x => target.color = x, endValue, duration);
		U3CU3Ec__DisplayClass36_0_tCE8E904A62CBD9F143B3D692CA532EFA7A6F777D * L_3 = V_0;
		DOGetter_1_tCC9DEA9BB7DA5C2570ED2CE9BD13EF5C154B7B10 * L_4 = (DOGetter_1_tCC9DEA9BB7DA5C2570ED2CE9BD13EF5C154B7B10 *)il2cpp_codegen_object_new(DOGetter_1_tCC9DEA9BB7DA5C2570ED2CE9BD13EF5C154B7B10_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_mAC32C501C09F9F6EC5B0DD2C1A8473BAE88DC8CE(L_4, L_3, (intptr_t)((intptr_t)U3CU3Ec__DisplayClass36_0_U3CDOFadeU3Eb__0_mA3238766629BBF7E2750683F62F7629C5B097D87_RuntimeMethod_var), /*hidden argument*/DOGetter_1__ctor_mAC32C501C09F9F6EC5B0DD2C1A8473BAE88DC8CE_RuntimeMethod_var);
		U3CU3Ec__DisplayClass36_0_tCE8E904A62CBD9F143B3D692CA532EFA7A6F777D * L_5 = V_0;
		DOSetter_1_tB62013E4617ECB51822384C8592AAB764E1662BF * L_6 = (DOSetter_1_tB62013E4617ECB51822384C8592AAB764E1662BF *)il2cpp_codegen_object_new(DOSetter_1_tB62013E4617ECB51822384C8592AAB764E1662BF_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m41D0232769B743141B4389F8A4599EC78F310352(L_6, L_5, (intptr_t)((intptr_t)U3CU3Ec__DisplayClass36_0_U3CDOFadeU3Eb__1_m119575F5E470CA6D0C37EF9E1A9C3A2CF8B2AA3A_RuntimeMethod_var), /*hidden argument*/DOSetter_1__ctor_m41D0232769B743141B4389F8A4599EC78F310352_RuntimeMethod_var);
		float L_7 = ___endValue1;
		float L_8 = ___duration2;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_il2cpp_TypeInfo_var);
		TweenerCore_3_t551A69CD4FF610AE9DF07C97A86B6FE12EBEB597 * L_9;
		L_9 = DOTween_ToAlpha_mB80D122D92ABC7A45500CE5FDA58FAA8B7688C16(L_4, L_6, L_7, L_8, /*hidden argument*/NULL);
		// t.SetTarget(target);
		TweenerCore_3_t551A69CD4FF610AE9DF07C97A86B6FE12EBEB597 * L_10 = L_9;
		U3CU3Ec__DisplayClass36_0_tCE8E904A62CBD9F143B3D692CA532EFA7A6F777D * L_11 = V_0;
		NullCheck(L_11);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_12 = L_11->get_target_0();
		TweenerCore_3_t551A69CD4FF610AE9DF07C97A86B6FE12EBEB597 * L_13;
		L_13 = TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t551A69CD4FF610AE9DF07C97A86B6FE12EBEB597_m8DA85B9EBDD18D4817AB73585673258C3904ED11(L_10, L_12, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t551A69CD4FF610AE9DF07C97A86B6FE12EBEB597_m8DA85B9EBDD18D4817AB73585673258C3904ED11_RuntimeMethod_var);
		// return t;
		return L_10;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DG.Tweening.DOTweenModuleUtils::Init()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOTweenModuleUtils_Init_mFF2188F42FA7128FE1A9BBC2CB9E6351919C9D15 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_4__ctor_mB344A638C116BEC788ED3EA352844312528F1175_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_4_t758FC0992B1B40F36598AA26D46FB5626C503AFE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOTweenModuleUtils_t1924C98C4EB46411DA5B22524EB14EFDC0203EA0_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Physics_SetOrientationOnPath_mCC376173A621DA244564EDF8A6347AB2A0F47816_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (_initialized) return;
		bool L_0 = ((DOTweenModuleUtils_t1924C98C4EB46411DA5B22524EB14EFDC0203EA0_StaticFields*)il2cpp_codegen_static_fields_for(DOTweenModuleUtils_t1924C98C4EB46411DA5B22524EB14EFDC0203EA0_il2cpp_TypeInfo_var))->get__initialized_0();
		if (!L_0)
		{
			goto IL_0008;
		}
	}
	{
		// if (_initialized) return;
		return;
	}

IL_0008:
	{
		// _initialized = true;
		((DOTweenModuleUtils_t1924C98C4EB46411DA5B22524EB14EFDC0203EA0_StaticFields*)il2cpp_codegen_static_fields_for(DOTweenModuleUtils_t1924C98C4EB46411DA5B22524EB14EFDC0203EA0_il2cpp_TypeInfo_var))->set__initialized_0((bool)1);
		// DOTweenExternalCommand.SetOrientationOnPath += Physics.SetOrientationOnPath;
		Action_4_t758FC0992B1B40F36598AA26D46FB5626C503AFE * L_1 = (Action_4_t758FC0992B1B40F36598AA26D46FB5626C503AFE *)il2cpp_codegen_object_new(Action_4_t758FC0992B1B40F36598AA26D46FB5626C503AFE_il2cpp_TypeInfo_var);
		Action_4__ctor_mB344A638C116BEC788ED3EA352844312528F1175(L_1, NULL, (intptr_t)((intptr_t)Physics_SetOrientationOnPath_mCC376173A621DA244564EDF8A6347AB2A0F47816_RuntimeMethod_var), /*hidden argument*/Action_4__ctor_mB344A638C116BEC788ED3EA352844312528F1175_RuntimeMethod_var);
		DOTweenExternalCommand_add_SetOrientationOnPath_mF93DD658AA5364137A925E52006F94298FF1120B(L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void DG.Tweening.DOTweenModuleUtils::Preserver()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOTweenModuleUtils_Preserver_mBE8D1FE2AE913FBFFCCF43B5A615D941B3706F43 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Type_t_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral0D1BA8C0E521925077224DB11A6C93FB8E930E14);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Assembly[] loadedAssemblies = AppDomain.CurrentDomain.GetAssemblies();
		AppDomain_tBEB6322D51DCB12C09A56A49886C2D09BA1C1A8A * L_0;
		L_0 = AppDomain_get_CurrentDomain_mC2FE307811914289CBBDEFEFF6175FCE2E96A55E(/*hidden argument*/NULL);
		NullCheck(L_0);
		AssemblyU5BU5D_t42061B4CA43487DD8ECD8C3AACCEF783FA081FF0* L_1;
		L_1 = AppDomain_GetAssemblies_m7397BD0461B4D6BA76AE0974DE9FBEDAF70AEBFD(L_0, /*hidden argument*/NULL);
		// MethodInfo mi = typeof(MonoBehaviour).GetMethod("Stub");
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_2 = { reinterpret_cast<intptr_t> (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3;
		L_3 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		MethodInfo_t * L_4;
		L_4 = Type_GetMethod_mDD47332AAF3036AAFC4C6626A999A452E7143DCF(L_3, _stringLiteral0D1BA8C0E521925077224DB11A6C93FB8E930E14, /*hidden argument*/NULL);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// CharControl LevelProvider::get_CurrentChar()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * LevelProvider_get_CurrentChar_mB00F8D84D9CE0BEABC41F5B0CD8C62B0744EA00F (LevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841 * __this, const RuntimeMethod* method)
{
	{
		// get => _currentChar;
		CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * L_0 = __this->get__currentChar_8();
		return L_0;
	}
}
// System.Void LevelProvider::set_CurrentChar(CharControl)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LevelProvider_set_CurrentChar_mC845B8A970536A1E9BA9B577A63175EB193E64E5 (LevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841 * __this, CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * ___value0, const RuntimeMethod* method)
{
	{
		// _taskLabel.DOFade(1, _fadeInDuration);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_0 = __this->get__taskLabel_5();
		float L_1 = __this->get__fadeInDuration_10();
		TweenerCore_3_t551A69CD4FF610AE9DF07C97A86B6FE12EBEB597 * L_2;
		L_2 = DOTweenModuleUI_DOFade_m21F7CCBB732ABBD63BFD893D1A8D9837532A70D1(L_0, (1.0f), L_1, /*hidden argument*/NULL);
		// _currentChar = value;
		CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * L_3 = ___value0;
		__this->set__currentChar_8(L_3);
		// }
		return;
	}
}
// System.Int32 LevelProvider::get_CurrentCharsCount()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t LevelProvider_get_CurrentCharsCount_m3F727FCD9E869AE74F8BF985E69C22B17810A87E (LevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841 * __this, const RuntimeMethod* method)
{
	{
		// get => _currentCharsCount;
		int32_t L_0 = __this->get__currentCharsCount_9();
		return L_0;
	}
}
// System.Void LevelProvider::set_CurrentCharsCount(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LevelProvider_set_CurrentCharsCount_m27DF6B2ECC60D5C42FBB8A587B5DEE7235D7031F (LevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// _currentCharsCount = value;
		int32_t L_0 = ___value0;
		__this->set__currentCharsCount_9(L_0);
		// }
		return;
	}
}
// CharsGenerator LevelProvider::get_CharsGenerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CharsGenerator_t7E18FC7617D416B9E55B664B885A3138E43EE2A2 * LevelProvider_get_CharsGenerator_mB8E799CB7B0821293DE8E6AB1A245AD4B11738B1 (LevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841 * __this, const RuntimeMethod* method)
{
	{
		// public CharsGenerator CharsGenerator { get => _charsGenerator; set => _charsGenerator = value; }
		CharsGenerator_t7E18FC7617D416B9E55B664B885A3138E43EE2A2 * L_0 = __this->get__charsGenerator_6();
		return L_0;
	}
}
// System.Void LevelProvider::set_CharsGenerator(CharsGenerator)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LevelProvider_set_CharsGenerator_mDB904A9ED987FE4F6B86770D73A0909BF8948A99 (LevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841 * __this, CharsGenerator_t7E18FC7617D416B9E55B664B885A3138E43EE2A2 * ___value0, const RuntimeMethod* method)
{
	{
		// public CharsGenerator CharsGenerator { get => _charsGenerator; set => _charsGenerator = value; }
		CharsGenerator_t7E18FC7617D416B9E55B664B885A3138E43EE2A2 * L_0 = ___value0;
		__this->set__charsGenerator_6(L_0);
		return;
	}
}
// System.Int32 LevelProvider::get_Score()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t LevelProvider_get_Score_m0E65DD5953788C0FBB8E65BEC1ACFF8A9F53A6F0 (LevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841 * __this, const RuntimeMethod* method)
{
	{
		// get => score;
		int32_t L_0 = __this->get_score_4();
		return L_0;
	}
}
// System.Void LevelProvider::set_Score(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LevelProvider_set_Score_mBED651E1406AC9A3CDFA39281E646B3B01B63A8B (LevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral679C291DDDABA344C75D8BC842F0F95E46B6B2EA);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _taskLabel.text = $"Score: {value}";
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_0 = __this->get__taskLabel_5();
		int32_t L_1 = ___value0;
		int32_t L_2 = L_1;
		RuntimeObject * L_3 = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &L_2);
		String_t* L_4;
		L_4 = String_Format_mB3D38E5238C3164DB4D7D29339D9E225A4496D17(_stringLiteral679C291DDDABA344C75D8BC842F0F95E46B6B2EA, L_3, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtualActionInvoker1< String_t* >::Invoke(71 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_4);
		// score = value;
		int32_t L_5 = ___value0;
		__this->set_score_4(L_5);
		// }
		return;
	}
}
// System.Void LevelProvider::NextLevel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LevelProvider_NextLevel_mBFD5EAB47CF7715296B1FAF72BF1857983628EDF (LevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841 * __this, const RuntimeMethod* method)
{
	{
		// _taskLabel.DOFade(0, 0);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_0 = __this->get__taskLabel_5();
		TweenerCore_3_t551A69CD4FF610AE9DF07C97A86B6FE12EBEB597 * L_1;
		L_1 = DOTweenModuleUI_DOFade_m21F7CCBB732ABBD63BFD893D1A8D9837532A70D1(L_0, (0.0f), (0.0f), /*hidden argument*/NULL);
		// _charsGenerator.CharGenerate();
		CharsGenerator_t7E18FC7617D416B9E55B664B885A3138E43EE2A2 * L_2 = __this->get__charsGenerator_6();
		NullCheck(L_2);
		CharsGenerator_CharGenerate_m8BC7A85E0E0DB9BAABEB7A473D8692635633FDB3(L_2, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void LevelProvider::RestartGame()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LevelProvider_RestartGame_m1159957EE264AC0EF22A022413CB5D3D13F1F465 (LevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841 * __this, const RuntimeMethod* method)
{
	{
		// NextLevel();
		LevelProvider_NextLevel_mBFD5EAB47CF7715296B1FAF72BF1857983628EDF(__this, /*hidden argument*/NULL);
		// WinPanelActive(false);
		LevelProvider_WinPanelActive_m2C2BB89363A35E652A377A717451EFC37958B141(__this, (bool)0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void LevelProvider::WinPanelActive(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LevelProvider_WinPanelActive_m2C2BB89363A35E652A377A717451EFC37958B141 (LevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841 * __this, bool ___isActive0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisImage_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_m08E9D50CF81C1176D98860FE7038B9544046EB7D_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * V_0 = NULL;
	{
		// var winPanelImage = _winPanel.GetComponent<Image>();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get__winPanel_7();
		NullCheck(L_0);
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_1;
		L_1 = GameObject_GetComponent_TisImage_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_m08E9D50CF81C1176D98860FE7038B9544046EB7D(L_0, /*hidden argument*/GameObject_GetComponent_TisImage_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_m08E9D50CF81C1176D98860FE7038B9544046EB7D_RuntimeMethod_var);
		V_0 = L_1;
		// winPanelImage.DOFade(0, 0);
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_2 = V_0;
		TweenerCore_3_t551A69CD4FF610AE9DF07C97A86B6FE12EBEB597 * L_3;
		L_3 = DOTweenModuleUI_DOFade_mE66C8B7D137E0DDE54AAA96F551811FB8BDB8754(L_2, (0.0f), (0.0f), /*hidden argument*/NULL);
		// _winPanel.SetActive(isActive);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_4 = __this->get__winPanel_7();
		bool L_5 = ___isActive0;
		NullCheck(L_4);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_4, L_5, /*hidden argument*/NULL);
		// if (isActive)
		bool L_6 = ___isActive0;
		if (!L_6)
		{
			goto IL_003f;
		}
	}
	{
		// winPanelImage.DOFade(1, _fadeInDuration);
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_7 = V_0;
		float L_8 = __this->get__fadeInDuration_10();
		TweenerCore_3_t551A69CD4FF610AE9DF07C97A86B6FE12EBEB597 * L_9;
		L_9 = DOTweenModuleUI_DOFade_mE66C8B7D137E0DDE54AAA96F551811FB8BDB8754(L_7, (1.0f), L_8, /*hidden argument*/NULL);
		return;
	}

IL_003f:
	{
		// winPanelImage.DOFade(0, _fadeInDuration);
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_10 = V_0;
		float L_11 = __this->get__fadeInDuration_10();
		TweenerCore_3_t551A69CD4FF610AE9DF07C97A86B6FE12EBEB597 * L_12;
		L_12 = DOTweenModuleUI_DOFade_mE66C8B7D137E0DDE54AAA96F551811FB8BDB8754(L_10, (0.0f), L_11, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void LevelProvider::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LevelProvider__ctor_mEB879CC69BDE15B41700F451692B744BCAE811F2 (LevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841 * __this, const RuntimeMethod* method)
{
	{
		// private float _fadeInDuration = 5f;
		__this->set__fadeInDuration_10((5.0f));
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void LoadingScreen::StartGame()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LoadingScreen_StartGame_mECB829D2446F38D73C4530FE39AD42AEF6A9EF46 (LoadingScreen_tD1CD7EA45A62C3A1A641D929E47F330644D353B2 * __this, const RuntimeMethod* method)
{
	{
		// StartCoroutine(LoadScreen());
		RuntimeObject* L_0;
		L_0 = LoadingScreen_LoadScreen_mDD89C18CE436D6D2063C31F49AE91F20F5127B18(__this, /*hidden argument*/NULL);
		Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * L_1;
		L_1 = MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719(__this, L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void LoadingScreen::PanelActive(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LoadingScreen_PanelActive_m3D5780ABDCF9855B059512A617AC7C1E114C34C0 (LoadingScreen_tD1CD7EA45A62C3A1A641D929E47F330644D353B2 * __this, bool ___isActive0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisImage_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_m5D5D0C1BB7E1E67F46C955DA2861E7B83FC7301D_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * V_0 = NULL;
	{
		// var winPanelImage = GetComponent<Image>();
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_0;
		L_0 = Component_GetComponent_TisImage_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_m5D5D0C1BB7E1E67F46C955DA2861E7B83FC7301D(__this, /*hidden argument*/Component_GetComponent_TisImage_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_m5D5D0C1BB7E1E67F46C955DA2861E7B83FC7301D_RuntimeMethod_var);
		V_0 = L_0;
		// if (isActive)
		bool L_1 = ___isActive0;
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		// winPanelImage.DOFade(1, _fadeInDuration);
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_2 = V_0;
		float L_3 = __this->get__fadeInDuration_4();
		TweenerCore_3_t551A69CD4FF610AE9DF07C97A86B6FE12EBEB597 * L_4;
		L_4 = DOTweenModuleUI_DOFade_mE66C8B7D137E0DDE54AAA96F551811FB8BDB8754(L_2, (1.0f), L_3, /*hidden argument*/NULL);
		return;
	}

IL_001d:
	{
		// winPanelImage.DOFade(0, _fadeInDuration);
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_5 = V_0;
		float L_6 = __this->get__fadeInDuration_4();
		TweenerCore_3_t551A69CD4FF610AE9DF07C97A86B6FE12EBEB597 * L_7;
		L_7 = DOTweenModuleUI_DOFade_mE66C8B7D137E0DDE54AAA96F551811FB8BDB8754(L_5, (0.0f), L_6, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Collections.IEnumerator LoadingScreen::LoadScreen()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* LoadingScreen_LoadScreen_mDD89C18CE436D6D2063C31F49AE91F20F5127B18 (LoadingScreen_tD1CD7EA45A62C3A1A641D929E47F330644D353B2 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLoadScreenU3Ed__3_tAB779AB619F104C740B1B46095B34AB1EA682770_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CLoadScreenU3Ed__3_tAB779AB619F104C740B1B46095B34AB1EA682770 * L_0 = (U3CLoadScreenU3Ed__3_tAB779AB619F104C740B1B46095B34AB1EA682770 *)il2cpp_codegen_object_new(U3CLoadScreenU3Ed__3_tAB779AB619F104C740B1B46095B34AB1EA682770_il2cpp_TypeInfo_var);
		U3CLoadScreenU3Ed__3__ctor_m8C85D910055010DC08BB6EAFEE748471DA723D6B(L_0, 0, /*hidden argument*/NULL);
		U3CLoadScreenU3Ed__3_tAB779AB619F104C740B1B46095B34AB1EA682770 * L_1 = L_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_2(__this);
		return L_1;
	}
}
// System.Void LoadingScreen::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LoadingScreen__ctor_m294A22F1CBC235719588314E0E286059BC3C9586 (LoadingScreen_tD1CD7EA45A62C3A1A641D929E47F330644D353B2 * __this, const RuntimeMethod* method)
{
	{
		// private float _fadeInDuration = 4f;
		__this->set__fadeInDuration_4((4.0f));
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TweenEffect::BounceEffect()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TweenEffect_BounceEffect_mB01EFD4885A764247CB6F16E495DD19974BFC58B (TweenEffect_tB80E075E7B5F736D5D3E0BDAB6A253BF4DA1BBF1 * __this, const RuntimeMethod* method)
{
	{
		// transform.DOShakePosition(_bounceDuration, strength: _bounceStrenght, vibrato: 5, randomness: 1, snapping: false, fadeOut: true);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0;
		L_0 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		float L_1 = __this->get__bounceDuration_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = __this->get__bounceStrenght_5();
		Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * L_3;
		L_3 = ShortcutExtensions_DOShakePosition_mDEC0E682E9A8D470D6FD1F825161C3E6087DC3B8(L_0, L_1, L_2, 5, (1.0f), (bool)0, (bool)1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void TweenEffect::StrongBounceEffect()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TweenEffect_StrongBounceEffect_m3D7E7C12E3636C041F06C1121C7646DEC82A5C97 (TweenEffect_tB80E075E7B5F736D5D3E0BDAB6A253BF4DA1BBF1 * __this, const RuntimeMethod* method)
{
	{
		// transform.DOShakePosition(_bounceDuration, strength: _bounceStrongStrenght, vibrato: 10, randomness: 1, snapping: false, fadeOut: true);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0;
		L_0 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		float L_1 = __this->get__bounceDuration_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = __this->get__bounceStrongStrenght_6();
		Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * L_3;
		L_3 = ShortcutExtensions_DOShakePosition_mDEC0E682E9A8D470D6FD1F825161C3E6087DC3B8(L_0, L_1, L_2, ((int32_t)10), (1.0f), (bool)0, (bool)1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void TweenEffect::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TweenEffect__ctor_m30A886E518C80FC5309744866EC314EE321E2499 (TweenEffect_tB80E075E7B5F736D5D3E0BDAB6A253BF4DA1BBF1 * __this, const RuntimeMethod* method)
{
	{
		// private float _bounceDuration = 2f;
		__this->set__bounceDuration_4((2.0f));
		// private Vector3 _bounceStrenght = new Vector3(0, 3, 0);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0;
		memset((&L_0), 0, sizeof(L_0));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_0), (0.0f), (3.0f), (0.0f), /*hidden argument*/NULL);
		__this->set__bounceStrenght_5(L_0);
		// private Vector3 _bounceStrongStrenght = new Vector3(0, 5, 0);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1;
		memset((&L_1), 0, sizeof(L_1));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_1), (0.0f), (5.0f), (0.0f), /*hidden argument*/NULL);
		__this->set__bounceStrongStrenght_6(L_1);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CharControl/<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_m22867D0013C3895058C3589D3AA9837EE52FF0D2 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_t354A1590D8043D0E9E5A2724672CBF8E9E8F1AA4_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_t354A1590D8043D0E9E5A2724672CBF8E9E8F1AA4 * L_0 = (U3CU3Ec_t354A1590D8043D0E9E5A2724672CBF8E9E8F1AA4 *)il2cpp_codegen_object_new(U3CU3Ec_t354A1590D8043D0E9E5A2724672CBF8E9E8F1AA4_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_m0093EAA02FD05C07FBF35B4333C0C7D427357533(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_t354A1590D8043D0E9E5A2724672CBF8E9E8F1AA4_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t354A1590D8043D0E9E5A2724672CBF8E9E8F1AA4_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void CharControl/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m0093EAA02FD05C07FBF35B4333C0C7D427357533 (U3CU3Ec_t354A1590D8043D0E9E5A2724672CBF8E9E8F1AA4 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean CharControl/<>c::<CheckRelevate>b__21_0(CharControl)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec_U3CCheckRelevateU3Eb__21_0_mD4D9A1B7BB939B76FE986B7B530B1C6A05596AA1 (U3CU3Ec_t354A1590D8043D0E9E5A2724672CBF8E9E8F1AA4 * __this, CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * ___ch0, const RuntimeMethod* method)
{
	{
		// if (_levelProvider.CharsGenerator.CharsOnScene.Where(ch => ch.IsSelected == false).Count() == 0)
		CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * L_0 = ___ch0;
		NullCheck(L_0);
		bool L_1;
		L_1 = CharControl_get_IsSelected_m5DAAC6C82C108F3FD7842336558B1B62BD8206C8_inline(L_0, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CharControl/<CheckRelevate>d__21::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CCheckRelevateU3Ed__21__ctor_mA361476A27B6D1FF49F6BA63693BEB85B52D0337 (U3CCheckRelevateU3Ed__21_t1EC48447076186B1190E53CBB1A680B3ECA1E750 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void CharControl/<CheckRelevate>d__21::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CCheckRelevateU3Ed__21_System_IDisposable_Dispose_mE650F9D2D876A26FB6F1BD8C3CB92FB0F6B9406D (U3CCheckRelevateU3Ed__21_t1EC48447076186B1190E53CBB1A680B3ECA1E750 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean CharControl/<CheckRelevate>d__21::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CCheckRelevateU3Ed__21_MoveNext_m58096431A0A44BA83BB1679F134EAF5C0C4A6FBB (U3CCheckRelevateU3Ed__21_t1EC48447076186B1190E53CBB1A680B3ECA1E750 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_Count_TisCharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD_mB9C151F66AEAD221FF7001ABD445C652FCA07DC8_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_Where_TisCharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD_m71644EECCC484BCD70DAF76B0AAA095B910D839B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2__ctor_m8E70087591BD5ED6F195C93CF07DA159BEAC2FB9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t20F4B130EF75554CF71A9826AA0AC83CCB3EEDAC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_U3CCheckRelevateU3Eb__21_0_mD4D9A1B7BB939B76FE986B7B530B1C6A05596AA1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_t354A1590D8043D0E9E5A2724672CBF8E9E8F1AA4_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * V_1 = NULL;
	int32_t V_2 = 0;
	Func_2_t20F4B130EF75554CF71A9826AA0AC83CCB3EEDAC * G_B10_0 = NULL;
	List_1_t37187EA3F5A7945D0B45C8A071194C1191FC32FE * G_B10_1 = NULL;
	Func_2_t20F4B130EF75554CF71A9826AA0AC83CCB3EEDAC * G_B9_0 = NULL;
	List_1_t37187EA3F5A7945D0B45C8A071194C1191FC32FE * G_B9_1 = NULL;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * L_1 = __this->get_U3CU3E4__this_2();
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0054;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		__this->set_U3CU3E1__state_0((-1));
		// CharImage.sprite = _chardata.Sprite;
		CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * L_4 = V_1;
		NullCheck(L_4);
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_5;
		L_5 = CharControl_get_CharImage_m6717F383E31A7235179A68C6BBB341C6FC9E2AFB_inline(L_4, /*hidden argument*/NULL);
		CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * L_6 = V_1;
		NullCheck(L_6);
		CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595 * L_7 = L_6->get__chardata_6();
		NullCheck(L_7);
		Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * L_8;
		L_8 = CharsData_get_Sprite_m1BFE6586725A0A68865F947F36E4E0F3BF85DE46_inline(L_7, /*hidden argument*/NULL);
		NullCheck(L_5);
		Image_set_sprite_m55C50F18ABA0A98E926FC777F7D07FA18A7D04E4(L_5, L_8, /*hidden argument*/NULL);
		// IsSelected = true;
		CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * L_9 = V_1;
		NullCheck(L_9);
		CharControl_set_IsSelected_m84AD011947B55056DDC4F4202075B1A9EE9D78CF_inline(L_9, (bool)1, /*hidden argument*/NULL);
		// yield return new WaitForSeconds(1f);
		WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * L_10 = (WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 *)il2cpp_codegen_object_new(WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_mD298C4CB9532BBBDE172FC40F3397E30504038D4(L_10, (1.0f), /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_10);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_0054:
	{
		__this->set_U3CU3E1__state_0((-1));
		// if (_levelProvider.CurrentChar != null)
		CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * L_11 = V_1;
		NullCheck(L_11);
		LevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841 * L_12 = L_11->get__levelProvider_5();
		NullCheck(L_12);
		CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * L_13;
		L_13 = LevelProvider_get_CurrentChar_mB00F8D84D9CE0BEABC41F5B0CD8C62B0744EA00F_inline(L_12, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_14;
		L_14 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_13, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0148;
		}
	}
	{
		// if (_levelProvider.CurrentChar.Chardata.Identifier != _chardata.Identifier)
		CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * L_15 = V_1;
		NullCheck(L_15);
		LevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841 * L_16 = L_15->get__levelProvider_5();
		NullCheck(L_16);
		CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * L_17;
		L_17 = LevelProvider_get_CurrentChar_mB00F8D84D9CE0BEABC41F5B0CD8C62B0744EA00F_inline(L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595 * L_18;
		L_18 = CharControl_get_Chardata_m82B81759BB755408B138708B8D0E0D48835EAF0E_inline(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		String_t* L_19;
		L_19 = CharsData_get_Identifier_mC20D9B2E42F23E7CFA66FA65141011F719AB2681_inline(L_18, /*hidden argument*/NULL);
		CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * L_20 = V_1;
		NullCheck(L_20);
		CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595 * L_21 = L_20->get__chardata_6();
		NullCheck(L_21);
		String_t* L_22;
		L_22 = CharsData_get_Identifier_mC20D9B2E42F23E7CFA66FA65141011F719AB2681_inline(L_21, /*hidden argument*/NULL);
		bool L_23;
		L_23 = String_op_Inequality_mDDA2DDED3E7EF042987EB7180EE3E88105F0AAE2(L_19, L_22, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_00de;
		}
	}
	{
		// _levelProvider.CurrentChar.CharImage.sprite = _backgroundSprite;
		CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * L_24 = V_1;
		NullCheck(L_24);
		LevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841 * L_25 = L_24->get__levelProvider_5();
		NullCheck(L_25);
		CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * L_26;
		L_26 = LevelProvider_get_CurrentChar_mB00F8D84D9CE0BEABC41F5B0CD8C62B0744EA00F_inline(L_25, /*hidden argument*/NULL);
		NullCheck(L_26);
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_27;
		L_27 = CharControl_get_CharImage_m6717F383E31A7235179A68C6BBB341C6FC9E2AFB_inline(L_26, /*hidden argument*/NULL);
		CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * L_28 = V_1;
		NullCheck(L_28);
		Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * L_29 = L_28->get__backgroundSprite_9();
		NullCheck(L_27);
		Image_set_sprite_m55C50F18ABA0A98E926FC777F7D07FA18A7D04E4(L_27, L_29, /*hidden argument*/NULL);
		// CharImage.sprite = _backgroundSprite;
		CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * L_30 = V_1;
		NullCheck(L_30);
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_31;
		L_31 = CharControl_get_CharImage_m6717F383E31A7235179A68C6BBB341C6FC9E2AFB_inline(L_30, /*hidden argument*/NULL);
		CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * L_32 = V_1;
		NullCheck(L_32);
		Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * L_33 = L_32->get__backgroundSprite_9();
		NullCheck(L_31);
		Image_set_sprite_m55C50F18ABA0A98E926FC777F7D07FA18A7D04E4(L_31, L_33, /*hidden argument*/NULL);
		// _levelProvider.CurrentChar.IsSelected = false;
		CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * L_34 = V_1;
		NullCheck(L_34);
		LevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841 * L_35 = L_34->get__levelProvider_5();
		NullCheck(L_35);
		CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * L_36;
		L_36 = LevelProvider_get_CurrentChar_mB00F8D84D9CE0BEABC41F5B0CD8C62B0744EA00F_inline(L_35, /*hidden argument*/NULL);
		NullCheck(L_36);
		CharControl_set_IsSelected_m84AD011947B55056DDC4F4202075B1A9EE9D78CF_inline(L_36, (bool)0, /*hidden argument*/NULL);
		// IsSelected = false;
		CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * L_37 = V_1;
		NullCheck(L_37);
		CharControl_set_IsSelected_m84AD011947B55056DDC4F4202075B1A9EE9D78CF_inline(L_37, (bool)0, /*hidden argument*/NULL);
		// }
		goto IL_00f3;
	}

IL_00de:
	{
		// _levelProvider.Score++;
		CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * L_38 = V_1;
		NullCheck(L_38);
		LevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841 * L_39 = L_38->get__levelProvider_5();
		LevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841 * L_40 = L_39;
		NullCheck(L_40);
		int32_t L_41;
		L_41 = LevelProvider_get_Score_m0E65DD5953788C0FBB8E65BEC1ACFF8A9F53A6F0_inline(L_40, /*hidden argument*/NULL);
		V_2 = L_41;
		int32_t L_42 = V_2;
		NullCheck(L_40);
		LevelProvider_set_Score_mBED651E1406AC9A3CDFA39281E646B3B01B63A8B(L_40, ((int32_t)il2cpp_codegen_add((int32_t)L_42, (int32_t)1)), /*hidden argument*/NULL);
	}

IL_00f3:
	{
		// _levelProvider.CurrentChar = null;
		CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * L_43 = V_1;
		NullCheck(L_43);
		LevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841 * L_44 = L_43->get__levelProvider_5();
		NullCheck(L_44);
		LevelProvider_set_CurrentChar_mC845B8A970536A1E9BA9B577A63175EB193E64E5(L_44, (CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD *)NULL, /*hidden argument*/NULL);
		// if (_levelProvider.CharsGenerator.CharsOnScene.Where(ch => ch.IsSelected == false).Count() == 0)
		CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * L_45 = V_1;
		NullCheck(L_45);
		LevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841 * L_46 = L_45->get__levelProvider_5();
		NullCheck(L_46);
		CharsGenerator_t7E18FC7617D416B9E55B664B885A3138E43EE2A2 * L_47;
		L_47 = LevelProvider_get_CharsGenerator_mB8E799CB7B0821293DE8E6AB1A245AD4B11738B1_inline(L_46, /*hidden argument*/NULL);
		NullCheck(L_47);
		List_1_t37187EA3F5A7945D0B45C8A071194C1191FC32FE * L_48;
		L_48 = CharsGenerator_get_CharsOnScene_m1D6EDE7E5FA590B1195B033ECE89B6BE1F29EBF3_inline(L_47, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t354A1590D8043D0E9E5A2724672CBF8E9E8F1AA4_il2cpp_TypeInfo_var);
		Func_2_t20F4B130EF75554CF71A9826AA0AC83CCB3EEDAC * L_49 = ((U3CU3Ec_t354A1590D8043D0E9E5A2724672CBF8E9E8F1AA4_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t354A1590D8043D0E9E5A2724672CBF8E9E8F1AA4_il2cpp_TypeInfo_var))->get_U3CU3E9__21_0_1();
		Func_2_t20F4B130EF75554CF71A9826AA0AC83CCB3EEDAC * L_50 = L_49;
		G_B9_0 = L_50;
		G_B9_1 = L_48;
		if (L_50)
		{
			G_B10_0 = L_50;
			G_B10_1 = L_48;
			goto IL_012e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t354A1590D8043D0E9E5A2724672CBF8E9E8F1AA4_il2cpp_TypeInfo_var);
		U3CU3Ec_t354A1590D8043D0E9E5A2724672CBF8E9E8F1AA4 * L_51 = ((U3CU3Ec_t354A1590D8043D0E9E5A2724672CBF8E9E8F1AA4_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t354A1590D8043D0E9E5A2724672CBF8E9E8F1AA4_il2cpp_TypeInfo_var))->get_U3CU3E9_0();
		Func_2_t20F4B130EF75554CF71A9826AA0AC83CCB3EEDAC * L_52 = (Func_2_t20F4B130EF75554CF71A9826AA0AC83CCB3EEDAC *)il2cpp_codegen_object_new(Func_2_t20F4B130EF75554CF71A9826AA0AC83CCB3EEDAC_il2cpp_TypeInfo_var);
		Func_2__ctor_m8E70087591BD5ED6F195C93CF07DA159BEAC2FB9(L_52, L_51, (intptr_t)((intptr_t)U3CU3Ec_U3CCheckRelevateU3Eb__21_0_mD4D9A1B7BB939B76FE986B7B530B1C6A05596AA1_RuntimeMethod_var), /*hidden argument*/Func_2__ctor_m8E70087591BD5ED6F195C93CF07DA159BEAC2FB9_RuntimeMethod_var);
		Func_2_t20F4B130EF75554CF71A9826AA0AC83CCB3EEDAC * L_53 = L_52;
		((U3CU3Ec_t354A1590D8043D0E9E5A2724672CBF8E9E8F1AA4_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t354A1590D8043D0E9E5A2724672CBF8E9E8F1AA4_il2cpp_TypeInfo_var))->set_U3CU3E9__21_0_1(L_53);
		G_B10_0 = L_53;
		G_B10_1 = G_B9_1;
	}

IL_012e:
	{
		RuntimeObject* L_54;
		L_54 = Enumerable_Where_TisCharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD_m71644EECCC484BCD70DAF76B0AAA095B910D839B(G_B10_1, G_B10_0, /*hidden argument*/Enumerable_Where_TisCharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD_m71644EECCC484BCD70DAF76B0AAA095B910D839B_RuntimeMethod_var);
		int32_t L_55;
		L_55 = Enumerable_Count_TisCharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD_mB9C151F66AEAD221FF7001ABD445C652FCA07DC8(L_54, /*hidden argument*/Enumerable_Count_TisCharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD_mB9C151F66AEAD221FF7001ABD445C652FCA07DC8_RuntimeMethod_var);
		if (L_55)
		{
			goto IL_0154;
		}
	}
	{
		// _levelProvider.WinPanelActive(true);
		CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * L_56 = V_1;
		NullCheck(L_56);
		LevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841 * L_57 = L_56->get__levelProvider_5();
		NullCheck(L_57);
		LevelProvider_WinPanelActive_m2C2BB89363A35E652A377A717451EFC37958B141(L_57, (bool)1, /*hidden argument*/NULL);
		// }
		goto IL_0154;
	}

IL_0148:
	{
		// _levelProvider.CurrentChar = this;
		CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * L_58 = V_1;
		NullCheck(L_58);
		LevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841 * L_59 = L_58->get__levelProvider_5();
		CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * L_60 = V_1;
		NullCheck(L_59);
		LevelProvider_set_CurrentChar_mC845B8A970536A1E9BA9B577A63175EB193E64E5(L_59, L_60, /*hidden argument*/NULL);
	}

IL_0154:
	{
		// }
		return (bool)0;
	}
}
// System.Object CharControl/<CheckRelevate>d__21::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CCheckRelevateU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCB10E92FB3B67BCA27564E938D4A284331728FC6 (U3CCheckRelevateU3Ed__21_t1EC48447076186B1190E53CBB1A680B3ECA1E750 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Object CharControl/<CheckRelevate>d__21::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CCheckRelevateU3Ed__21_System_Collections_IEnumerator_get_Current_m464A48492D74BA93FD5AC6788AB971EB29B19485 (U3CCheckRelevateU3Ed__21_t1EC48447076186B1190E53CBB1A680B3ECA1E750 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CharsGenerator/<>c__DisplayClass14_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass14_0__ctor_mDF2E4F72C494882B94DF063938E248EE6108DDAF (U3CU3Ec__DisplayClass14_0_t79B660C3CA0F908E689E0E2E7221DF954F7E5E42 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean CharsGenerator/<>c__DisplayClass14_0::<CharGenerate>b__1(CharsData)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec__DisplayClass14_0_U3CCharGenerateU3Eb__1_m42A572588759F99FADE2AD7CBE4DE57A6339F4AF (U3CU3Ec__DisplayClass14_0_t79B660C3CA0F908E689E0E2E7221DF954F7E5E42 * __this, CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595 * ___charPrepare0, const RuntimeMethod* method)
{
	{
		// .Where(data => charsPrepareOnScene.Where(charPrepare => charPrepare.Identifier == data.Identifier).Count() == 0).ToArray();
		CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595 * L_0 = ___charPrepare0;
		NullCheck(L_0);
		String_t* L_1;
		L_1 = CharsData_get_Identifier_mC20D9B2E42F23E7CFA66FA65141011F719AB2681_inline(L_0, /*hidden argument*/NULL);
		CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595 * L_2 = __this->get_data_0();
		NullCheck(L_2);
		String_t* L_3;
		L_3 = CharsData_get_Identifier_mC20D9B2E42F23E7CFA66FA65141011F719AB2681_inline(L_2, /*hidden argument*/NULL);
		bool L_4;
		L_4 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_1, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CharsGenerator/<InitializeEffect>d__18::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CInitializeEffectU3Ed__18__ctor_m2963AC28072621513A43A21896B10ACA02389E7F (U3CInitializeEffectU3Ed__18_tB5CE3179A9E4153E9DF3477D7479D251A763486C * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void CharsGenerator/<InitializeEffect>d__18::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CInitializeEffectU3Ed__18_System_IDisposable_Dispose_mE589EF6E332661A6A7811B65DDC1E246B2294114 (U3CInitializeEffectU3Ed__18_tB5CE3179A9E4153E9DF3477D7479D251A763486C * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean CharsGenerator/<InitializeEffect>d__18::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CInitializeEffectU3Ed__18_MoveNext_mBD7B90F646F6A65B92F420C255A52226951F161F (U3CInitializeEffectU3Ed__18_tB5CE3179A9E4153E9DF3477D7479D251A763486C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_mBCB2D275F9431E47AFAE49744FEDF77EDAACA2D5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mD3BE2F5A91D965C021520535B9413F1429D8A60B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_mF4D92D56E4EE8FDADA509B9D8BA7D3CF7CED1B58_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m924E2A798B90A4A58216B9E7ACE17C3F078EDB02_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	CharsGenerator_t7E18FC7617D416B9E55B664B885A3138E43EE2A2 * V_1 = NULL;
	Enumerator_t9930E1A60798842A72D23D981BA4A2330BF36578  V_2;
	memset((&V_2), 0, sizeof(V_2));
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 1> __leave_targets;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		CharsGenerator_t7E18FC7617D416B9E55B664B885A3138E43EE2A2 * L_1 = __this->get_U3CU3E4__this_2();
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0037;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		__this->set_U3CU3E1__state_0((-1));
		// yield return new WaitForSeconds(0.5f);
		WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * L_4 = (WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 *)il2cpp_codegen_object_new(WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_mD298C4CB9532BBBDE172FC40F3397E30504038D4(L_4, (0.5f), /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_4);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_0037:
	{
		__this->set_U3CU3E1__state_0((-1));
		// foreach (var control in CharsOnScene)
		CharsGenerator_t7E18FC7617D416B9E55B664B885A3138E43EE2A2 * L_5 = V_1;
		NullCheck(L_5);
		List_1_t37187EA3F5A7945D0B45C8A071194C1191FC32FE * L_6;
		L_6 = CharsGenerator_get_CharsOnScene_m1D6EDE7E5FA590B1195B033ECE89B6BE1F29EBF3_inline(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Enumerator_t9930E1A60798842A72D23D981BA4A2330BF36578  L_7;
		L_7 = List_1_GetEnumerator_m924E2A798B90A4A58216B9E7ACE17C3F078EDB02(L_6, /*hidden argument*/List_1_GetEnumerator_m924E2A798B90A4A58216B9E7ACE17C3F078EDB02_RuntimeMethod_var);
		V_2 = L_7;
	}

IL_004a:
	try
	{// begin try (depth: 1)
		{
			goto IL_005d;
		}

IL_004c:
		{
			// foreach (var control in CharsOnScene)
			CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * L_8;
			L_8 = Enumerator_get_Current_mF4D92D56E4EE8FDADA509B9D8BA7D3CF7CED1B58_inline((Enumerator_t9930E1A60798842A72D23D981BA4A2330BF36578 *)(&V_2), /*hidden argument*/Enumerator_get_Current_mF4D92D56E4EE8FDADA509B9D8BA7D3CF7CED1B58_RuntimeMethod_var);
			// control.TweenEffect.StrongBounceEffect();
			NullCheck(L_8);
			TweenEffect_tB80E075E7B5F736D5D3E0BDAB6A253BF4DA1BBF1 * L_9;
			L_9 = CharControl_get_TweenEffect_m0936842652110F63724B3CF1BE3176069B810A6F_inline(L_8, /*hidden argument*/NULL);
			NullCheck(L_9);
			TweenEffect_StrongBounceEffect_m3D7E7C12E3636C041F06C1121C7646DEC82A5C97(L_9, /*hidden argument*/NULL);
		}

IL_005d:
		{
			// foreach (var control in CharsOnScene)
			bool L_10;
			L_10 = Enumerator_MoveNext_mD3BE2F5A91D965C021520535B9413F1429D8A60B((Enumerator_t9930E1A60798842A72D23D981BA4A2330BF36578 *)(&V_2), /*hidden argument*/Enumerator_MoveNext_mD3BE2F5A91D965C021520535B9413F1429D8A60B_RuntimeMethod_var);
			if (L_10)
			{
				goto IL_004c;
			}
		}

IL_0066:
		{
			IL2CPP_LEAVE(0x76, FINALLY_0068);
		}
	}// end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0068;
	}

FINALLY_0068:
	{// begin finally (depth: 1)
		Enumerator_Dispose_mBCB2D275F9431E47AFAE49744FEDF77EDAACA2D5((Enumerator_t9930E1A60798842A72D23D981BA4A2330BF36578 *)(&V_2), /*hidden argument*/Enumerator_Dispose_mBCB2D275F9431E47AFAE49744FEDF77EDAACA2D5_RuntimeMethod_var);
		IL2CPP_END_FINALLY(104)
	}// end finally (depth: 1)
	IL2CPP_CLEANUP(104)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x76, IL_0076)
	}

IL_0076:
	{
		// }
		return (bool)0;
	}
}
// System.Object CharsGenerator/<InitializeEffect>d__18::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CInitializeEffectU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m42ECACA7787922D32854CA8AD289144E6818C99C (U3CInitializeEffectU3Ed__18_tB5CE3179A9E4153E9DF3477D7479D251A763486C * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Object CharsGenerator/<InitializeEffect>d__18::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CInitializeEffectU3Ed__18_System_Collections_IEnumerator_get_Current_m1F5AE063FBD3E0D97CB9EE0C007E93579AF15A90 (U3CInitializeEffectU3Ed__18_tB5CE3179A9E4153E9DF3477D7479D251A763486C * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass10_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass10_0__ctor_m819824BC9854A0C90662B04D48B88CA7B052BF83 (U3CU3Ec__DisplayClass10_0_t6B6DFEAD8FF1DD6A724C294AA0B120F82A7A6937 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass10_0::<DOLocalPath>b__0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  U3CU3Ec__DisplayClass10_0_U3CDOLocalPathU3Eb__0_m3089CA33F25546258A5E846EA51421D71AC55D70 (U3CU3Ec__DisplayClass10_0_t6B6DFEAD8FF1DD6A724C294AA0B120F82A7A6937 * __this, const RuntimeMethod* method)
{
	{
		// TweenerCore<Vector3, Path, PathOptions> t = DOTween.To(PathPlugin.Get(), () => trans.localPosition, x => target.MovePosition(trans.parent == null ? x : trans.parent.TransformPoint(x)), path, duration)
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0 = __this->get_trans_0();
		NullCheck(L_0);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1;
		L_1 = Transform_get_localPosition_m527B8B5B625DA9A61E551E0FBCD3BE8CA4539FC2(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass10_0::<DOLocalPath>b__1(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass10_0_U3CDOLocalPathU3Eb__1_mD201EFBB4403FBDEC0D377D39A768A581E6CCDDD (U3CU3Ec__DisplayClass10_0_t6B6DFEAD8FF1DD6A724C294AA0B120F82A7A6937 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * G_B2_0 = NULL;
	Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * G_B1_0 = NULL;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  G_B3_0;
	memset((&G_B3_0), 0, sizeof(G_B3_0));
	Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * G_B3_1 = NULL;
	{
		// TweenerCore<Vector3, Path, PathOptions> t = DOTween.To(PathPlugin.Get(), () => trans.localPosition, x => target.MovePosition(trans.parent == null ? x : trans.parent.TransformPoint(x)), path, duration)
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_0 = __this->get_target_1();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1 = __this->get_trans_0();
		NullCheck(L_1);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_2;
		L_2 = Transform_get_parent_m7D06005D9CB55F90F39D42F6A2AF9C7BC80745C9(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_3;
		L_3 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_2, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		G_B1_0 = L_0;
		if (L_3)
		{
			G_B2_0 = L_0;
			goto IL_002c;
		}
	}
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_4 = __this->get_trans_0();
		NullCheck(L_4);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_5;
		L_5 = Transform_get_parent_m7D06005D9CB55F90F39D42F6A2AF9C7BC80745C9(L_4, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___x0;
		NullCheck(L_5);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7;
		L_7 = Transform_TransformPoint_m68AF95765A9279192E601208A9C5170027A5F0D2(L_5, L_6, /*hidden argument*/NULL);
		G_B3_0 = L_7;
		G_B3_1 = G_B1_0;
		goto IL_002d;
	}

IL_002c:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = ___x0;
		G_B3_0 = L_8;
		G_B3_1 = G_B2_0;
	}

IL_002d:
	{
		NullCheck(G_B3_1);
		Rigidbody_MovePosition_mB3CBBF21FD0ABB88BC6C004B993DED25673001C7(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass9_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass9_0__ctor_mCA1E6F241784A13DEC15E12A0219479D552BB6BB (U3CU3Ec__DisplayClass9_0_tB9266040A99BBE64FC737A175355345A13E86D21 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass9_0::<DOPath>b__0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  U3CU3Ec__DisplayClass9_0_U3CDOPathU3Eb__0_mD050544C13BA54260FC94E0964EF204B4EDB2628 (U3CU3Ec__DisplayClass9_0_tB9266040A99BBE64FC737A175355345A13E86D21 * __this, const RuntimeMethod* method)
{
	{
		// TweenerCore<Vector3, Path, PathOptions> t = DOTween.To(PathPlugin.Get(), () => target.position, target.MovePosition, path, duration)
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_0 = __this->get_target_0();
		NullCheck(L_0);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1;
		L_1 = Rigidbody_get_position_m5F429382F610E324F39F33E8498A29D0828AD8E8(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass36_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass36_0__ctor_mE2912C6BF4318E28870DE7AA92E4B05C80D92EE0 (U3CU3Ec__DisplayClass36_0_tCE8E904A62CBD9F143B3D692CA532EFA7A6F777D * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Color DG.Tweening.DOTweenModuleUI/<>c__DisplayClass36_0::<DOFade>b__0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  U3CU3Ec__DisplayClass36_0_U3CDOFadeU3Eb__0_mA3238766629BBF7E2750683F62F7629C5B097D87 (U3CU3Ec__DisplayClass36_0_tCE8E904A62CBD9F143B3D692CA532EFA7A6F777D * __this, const RuntimeMethod* method)
{
	{
		// TweenerCore<Color, Color, ColorOptions> t = DOTween.ToAlpha(() => target.color, x => target.color = x, endValue, duration);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_0 = __this->get_target_0();
		NullCheck(L_0);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_1;
		L_1 = VirtualFuncInvoker0< Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  >::Invoke(22 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_0);
		return L_1;
	}
}
// System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass36_0::<DOFade>b__1(UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass36_0_U3CDOFadeU3Eb__1_m119575F5E470CA6D0C37EF9E1A9C3A2CF8B2AA3A (U3CU3Ec__DisplayClass36_0_tCE8E904A62CBD9F143B3D692CA532EFA7A6F777D * __this, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___x0, const RuntimeMethod* method)
{
	{
		// TweenerCore<Color, Color, ColorOptions> t = DOTween.ToAlpha(() => target.color, x => target.color = x, endValue, duration);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_0 = __this->get_target_0();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_1 = ___x0;
		NullCheck(L_0);
		VirtualActionInvoker1< Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_0, L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass4_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass4_0__ctor_m5CD451B862F3CC71D8D25BC3A69725FCDD6144C4 (U3CU3Ec__DisplayClass4_0_t9CD30C09F13A8901FBD37CEEE628023A3AC88587 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Color DG.Tweening.DOTweenModuleUI/<>c__DisplayClass4_0::<DOFade>b__0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__0_m507CD55516001BB4A76D7E58CB159B5394D6413C (U3CU3Ec__DisplayClass4_0_t9CD30C09F13A8901FBD37CEEE628023A3AC88587 * __this, const RuntimeMethod* method)
{
	{
		// TweenerCore<Color, Color, ColorOptions> t = DOTween.ToAlpha(() => target.color, x => target.color = x, endValue, duration);
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_0 = __this->get_target_0();
		NullCheck(L_0);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_1;
		L_1 = VirtualFuncInvoker0< Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  >::Invoke(22 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_0);
		return L_1;
	}
}
// System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass4_0::<DOFade>b__1(UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__1_mE65F1E4D380EC305AF0AE93E0315B0B23CA36310 (U3CU3Ec__DisplayClass4_0_t9CD30C09F13A8901FBD37CEEE628023A3AC88587 * __this, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___x0, const RuntimeMethod* method)
{
	{
		// TweenerCore<Color, Color, ColorOptions> t = DOTween.ToAlpha(() => target.color, x => target.color = x, endValue, duration);
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_0 = __this->get_target_0();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_1 = ___x0;
		NullCheck(L_0);
		VirtualActionInvoker1< Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_0, L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DG.Tweening.DOTweenModuleUtils/Physics::SetOrientationOnPath(DG.Tweening.Plugins.Options.PathOptions,DG.Tweening.Tween,UnityEngine.Quaternion,UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Physics_SetOrientationOnPath_mCC376173A621DA244564EDF8A6347AB2A0F47816 (PathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A  ___options0, Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * ___t1, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___newRot2, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___trans3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (options.isRigidbody) ((Rigidbody)t.target).rotation = newRot;
		PathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A  L_0 = ___options0;
		bool L_1 = L_0.get_isRigidbody_12();
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		// if (options.isRigidbody) ((Rigidbody)t.target).rotation = newRot;
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_2 = ___t1;
		NullCheck(L_2);
		RuntimeObject * L_3 = L_2->get_target_9();
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_4 = ___newRot2;
		NullCheck(((Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A *)CastclassClass((RuntimeObject*)L_3, Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_il2cpp_TypeInfo_var)));
		Rigidbody_set_rotation_m3024C151FEC9BB75735DE9B4BA64F16AA779C5D6(((Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A *)CastclassClass((RuntimeObject*)L_3, Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_il2cpp_TypeInfo_var)), L_4, /*hidden argument*/NULL);
		return;
	}

IL_001a:
	{
		// else trans.rotation = newRot;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_5 = ___trans3;
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_6 = ___newRot2;
		NullCheck(L_5);
		Transform_set_rotation_m1B5F3D4CE984AB31254615C9C71B0E54978583B4(L_5, L_6, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Boolean DG.Tweening.DOTweenModuleUtils/Physics::HasRigidbody(UnityEngine.Component)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Physics_HasRigidbody_m3082D72A658CCDDF6BB1F3D4A3EFBD6397D6E862 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * ___target0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisRigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_mB4A92A619135F9258670FB93AE08F229A41D0980_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return target.GetComponent<Rigidbody>() != null;
		Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * L_0 = ___target0;
		NullCheck(L_0);
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_1;
		L_1 = Component_GetComponent_TisRigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_mB4A92A619135F9258670FB93AE08F229A41D0980(L_0, /*hidden argument*/Component_GetComponent_TisRigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_mB4A92A619135F9258670FB93AE08F229A41D0980_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_1, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		return L_2;
	}
}
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModuleUtils/Physics::CreateDOTweenPathTween(UnityEngine.MonoBehaviour,System.Boolean,System.Boolean,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,DG.Tweening.PathMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * Physics_CreateDOTweenPathTween_m94D41B65E37500E5ACCF6B6F3B5D53FB031C4245 (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * ___target0, bool ___tweenRigidbody1, bool ___isLocal2, Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * ___path3, float ___duration4, int32_t ___pathMode5, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisRigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_mB4A92A619135F9258670FB93AE08F229A41D0980_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * V_0 = NULL;
	Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * V_1 = NULL;
	Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * G_B3_0 = NULL;
	TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * G_B8_0 = NULL;
	TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * G_B12_0 = NULL;
	{
		// Rigidbody rBody = tweenRigidbody ? target.GetComponent<Rigidbody>() : null;
		bool L_0 = ___tweenRigidbody1;
		if (L_0)
		{
			goto IL_0006;
		}
	}
	{
		G_B3_0 = ((Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A *)(NULL));
		goto IL_000c;
	}

IL_0006:
	{
		MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * L_1 = ___target0;
		NullCheck(L_1);
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_2;
		L_2 = Component_GetComponent_TisRigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_mB4A92A619135F9258670FB93AE08F229A41D0980(L_1, /*hidden argument*/Component_GetComponent_TisRigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_mB4A92A619135F9258670FB93AE08F229A41D0980_RuntimeMethod_var);
		G_B3_0 = L_2;
	}

IL_000c:
	{
		V_1 = G_B3_0;
		// if (tweenRigidbody && rBody != null) {
		bool L_3 = ___tweenRigidbody1;
		if (!L_3)
		{
			goto IL_0037;
		}
	}
	{
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_4 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_5;
		L_5 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_4, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0037;
		}
	}
	{
		// t = isLocal
		//     ? rBody.DOLocalPath(path, duration, pathMode)
		//     : rBody.DOPath(path, duration, pathMode);
		bool L_6 = ___isLocal2;
		if (L_6)
		{
			goto IL_0029;
		}
	}
	{
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_7 = V_1;
		Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * L_8 = ___path3;
		float L_9 = ___duration4;
		int32_t L_10 = ___pathMode5;
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_11;
		L_11 = DOTweenModulePhysics_DOPath_m3C49FFEB71D494F474F7223248D7105E90CF5BE7(L_7, L_8, L_9, L_10, /*hidden argument*/NULL);
		G_B8_0 = L_11;
		goto IL_0034;
	}

IL_0029:
	{
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_12 = V_1;
		Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * L_13 = ___path3;
		float L_14 = ___duration4;
		int32_t L_15 = ___pathMode5;
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_16;
		L_16 = DOTweenModulePhysics_DOLocalPath_m6F939DAF66D56DA9B52DA62546C70F3862E14193(L_12, L_13, L_14, L_15, /*hidden argument*/NULL);
		G_B8_0 = L_16;
	}

IL_0034:
	{
		V_0 = G_B8_0;
		// } else {
		goto IL_005d;
	}

IL_0037:
	{
		// t = isLocal
		//     ? target.transform.DOLocalPath(path, duration, pathMode)
		//     : target.transform.DOPath(path, duration, pathMode);
		bool L_17 = ___isLocal2;
		if (L_17)
		{
			goto IL_004c;
		}
	}
	{
		MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * L_18 = ___target0;
		NullCheck(L_18);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_19;
		L_19 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_18, /*hidden argument*/NULL);
		Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * L_20 = ___path3;
		float L_21 = ___duration4;
		int32_t L_22 = ___pathMode5;
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_23;
		L_23 = ShortcutExtensions_DOPath_m7AB84619455A541B79164E1D270A5AB511533B24(L_19, L_20, L_21, L_22, /*hidden argument*/NULL);
		G_B12_0 = L_23;
		goto IL_005c;
	}

IL_004c:
	{
		MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * L_24 = ___target0;
		NullCheck(L_24);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_25;
		L_25 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_24, /*hidden argument*/NULL);
		Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * L_26 = ___path3;
		float L_27 = ___duration4;
		int32_t L_28 = ___pathMode5;
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_29;
		L_29 = ShortcutExtensions_DOLocalPath_m28185CD9787640FBB679A0B46F144E1BB0DB43F8(L_25, L_26, L_27, L_28, /*hidden argument*/NULL);
		G_B12_0 = L_29;
	}

IL_005c:
	{
		V_0 = G_B12_0;
	}

IL_005d:
	{
		// return t;
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_30 = V_0;
		return L_30;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void LoadingScreen/<LoadScreen>d__3::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CLoadScreenU3Ed__3__ctor_m8C85D910055010DC08BB6EAFEE748471DA723D6B (U3CLoadScreenU3Ed__3_tAB779AB619F104C740B1B46095B34AB1EA682770 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void LoadingScreen/<LoadScreen>d__3::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CLoadScreenU3Ed__3_System_IDisposable_Dispose_m1D7C2F660F4B39CF5A467BD495803E90FBCADD2F (U3CLoadScreenU3Ed__3_tAB779AB619F104C740B1B46095B34AB1EA682770 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean LoadingScreen/<LoadScreen>d__3::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CLoadScreenU3Ed__3_MoveNext_m689B22925AF38421EEB5CF7CC5A11972D26C987A (U3CLoadScreenU3Ed__3_tAB779AB619F104C740B1B46095B34AB1EA682770 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	LoadingScreen_tD1CD7EA45A62C3A1A641D929E47F330644D353B2 * V_1 = NULL;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		LoadingScreen_tD1CD7EA45A62C3A1A641D929E47F330644D353B2 * L_1 = __this->get_U3CU3E4__this_2();
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_003e;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		__this->set_U3CU3E1__state_0((-1));
		// PanelActive(false);
		LoadingScreen_tD1CD7EA45A62C3A1A641D929E47F330644D353B2 * L_4 = V_1;
		NullCheck(L_4);
		LoadingScreen_PanelActive_m3D5780ABDCF9855B059512A617AC7C1E114C34C0(L_4, (bool)0, /*hidden argument*/NULL);
		// yield return new WaitForSeconds(3f);
		WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * L_5 = (WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 *)il2cpp_codegen_object_new(WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_mD298C4CB9532BBBDE172FC40F3397E30504038D4(L_5, (3.0f), /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_5);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_003e:
	{
		__this->set_U3CU3E1__state_0((-1));
		// gameObject.SetActive(false);
		LoadingScreen_tD1CD7EA45A62C3A1A641D929E47F330644D353B2 * L_6 = V_1;
		NullCheck(L_6);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_7;
		L_7 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_7, (bool)0, /*hidden argument*/NULL);
		// }
		return (bool)0;
	}
}
// System.Object LoadingScreen/<LoadScreen>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CLoadScreenU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m798841D11E7201A7D6CCA34F760ABF3BA95C0456 (U3CLoadScreenU3Ed__3_tAB779AB619F104C740B1B46095B34AB1EA682770 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Object LoadingScreen/<LoadScreen>d__3::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CLoadScreenU3Ed__3_System_Collections_IEnumerator_get_Current_mAAB03B606D079E0FB8EAE6538CEAD0D4BDB1C7FA (U3CLoadScreenU3Ed__3_tAB779AB619F104C740B1B46095B34AB1EA682770 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* CharsData_get_Identifier_mC20D9B2E42F23E7CFA66FA65141011F719AB2681_inline (CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595 * __this, const RuntimeMethod* method)
{
	{
		// public string Identifier { get => _identifier; set => _identifier = value; }
		String_t* L_0 = __this->get__identifier_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F * Button_get_onClick_m28BD8C670676D4E2B292B5A7F59387D4BF61F8F4_inline (Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * __this, const RuntimeMethod* method)
{
	{
		// get { return m_OnClick; }
		ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F * L_0 = __this->get_m_OnClick_20();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * Image_get_sprite_mA6FB016B4E3FE5EFFAE4B3AEE2D2DF89C61E0AF3_inline (Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * __this, const RuntimeMethod* method)
{
	{
		// get { return m_Sprite; }
		Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * L_0 = __this->get_m_Sprite_37();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR TweenEffect_tB80E075E7B5F736D5D3E0BDAB6A253BF4DA1BBF1 * CharControl_get_TweenEffect_m0936842652110F63724B3CF1BE3176069B810A6F_inline (CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * __this, const RuntimeMethod* method)
{
	{
		// public TweenEffect TweenEffect { get => _tweenEffect; set => _tweenEffect = value; }
		TweenEffect_tB80E075E7B5F736D5D3E0BDAB6A253BF4DA1BBF1 * L_0 = __this->get__tweenEffect_7();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool CharControl_get_IsSelected_m5DAAC6C82C108F3FD7842336558B1B62BD8206C8_inline (CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * __this, const RuntimeMethod* method)
{
	{
		// public bool IsSelected { get => isSelected; set => isSelected = value; }
		bool L_0 = __this->get_isSelected_10();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR List_1_t37187EA3F5A7945D0B45C8A071194C1191FC32FE * CharsGenerator_get_CharsOnScene_m1D6EDE7E5FA590B1195B033ECE89B6BE1F29EBF3_inline (CharsGenerator_t7E18FC7617D416B9E55B664B885A3138E43EE2A2 * __this, const RuntimeMethod* method)
{
	{
		// public List<CharControl> CharsOnScene { get => charsOnScene; set => charsOnScene = value; }
		List_1_t37187EA3F5A7945D0B45C8A071194C1191FC32FE * L_0 = __this->get_charsOnScene_8();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR List_1_t49AE3466419291B57C0EF7E386E193A7AFE92C02 * CharsGenerator_get_CharsPrepareOnScene_m232D2807B2745CD94DC8AD5E1B1FA8DDE239AB32_inline (CharsGenerator_t7E18FC7617D416B9E55B664B885A3138E43EE2A2 * __this, const RuntimeMethod* method)
{
	{
		// public List<CharsData> CharsPrepareOnScene { get => charsPrepareOnScene; set => charsPrepareOnScene = value; }
		List_1_t49AE3466419291B57C0EF7E386E193A7AFE92C02 * L_0 = __this->get_charsPrepareOnScene_9();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR CharsBundleDataU5BU5D_t49861CE889FB0A7632E6FD7126DE29B0F470F700* CharsContainer_get_CharBundles_mA18F021F7787AC922E81E27192C083AAE634CF39_inline (CharsContainer_tB2FB3D6B21722D0168C892A2C3837608C80C7CC5 * __this, const RuntimeMethod* method)
{
	{
		// public CharsBundleData[] CharBundles { get => _charBundles; set => _charBundles = value; }
		CharsBundleDataU5BU5D_t49861CE889FB0A7632E6FD7126DE29B0F470F700* L_0 = __this->get__charBundles_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR CharsDataU5BU5D_t6E1A2B31C527A76C761C25915A588ED83C6B4CDE* CharsBundleData_get_CharsData_m2A642A6B655267B4CC126843873D43AA04F9875C_inline (CharsBundleData_tC2C62F2A7FAEEDB189C65454E3E32437ECD36285 * __this, const RuntimeMethod* method)
{
	{
		// public CharsData[] CharsData { get => _charsData; set => _charsData = value; }
		CharsDataU5BU5D_t6E1A2B31C527A76C761C25915A588ED83C6B4CDE* L_0 = __this->get__charsData_4();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t LevelProvider_get_CurrentCharsCount_m3F727FCD9E869AE74F8BF985E69C22B17810A87E_inline (LevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841 * __this, const RuntimeMethod* method)
{
	{
		// get => _currentCharsCount;
		int32_t L_0 = __this->get__currentCharsCount_9();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_2(L_0);
		float L_1 = ___y1;
		__this->set_y_3(L_1);
		float L_2 = ___z2;
		__this->set_z_4(L_2);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * CharControl_get_CharImage_m6717F383E31A7235179A68C6BBB341C6FC9E2AFB_inline (CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * __this, const RuntimeMethod* method)
{
	{
		// public Image CharImage { get => _charImage; set => _charImage = value; }
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_0 = __this->get__charImage_8();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * CharsData_get_Sprite_m1BFE6586725A0A68865F947F36E4E0F3BF85DE46_inline (CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595 * __this, const RuntimeMethod* method)
{
	{
		// public Sprite Sprite { get => _sprite; set => _sprite = value; }
		Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * L_0 = __this->get__sprite_1();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CharControl_set_IsSelected_m84AD011947B55056DDC4F4202075B1A9EE9D78CF_inline (CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// public bool IsSelected { get => isSelected; set => isSelected = value; }
		bool L_0 = ___value0;
		__this->set_isSelected_10(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * LevelProvider_get_CurrentChar_mB00F8D84D9CE0BEABC41F5B0CD8C62B0744EA00F_inline (LevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841 * __this, const RuntimeMethod* method)
{
	{
		// get => _currentChar;
		CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * L_0 = __this->get__currentChar_8();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595 * CharControl_get_Chardata_m82B81759BB755408B138708B8D0E0D48835EAF0E_inline (CharControl_t10EDE7476D71EC1BF0F738F59586B97FF8E916CD * __this, const RuntimeMethod* method)
{
	{
		// get => _chardata;
		CharsData_t4F40BF5462B11A5140339F9259C78CFF658E8595 * L_0 = __this->get__chardata_6();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t LevelProvider_get_Score_m0E65DD5953788C0FBB8E65BEC1ACFF8A9F53A6F0_inline (LevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841 * __this, const RuntimeMethod* method)
{
	{
		// get => score;
		int32_t L_0 = __this->get_score_4();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR CharsGenerator_t7E18FC7617D416B9E55B664B885A3138E43EE2A2 * LevelProvider_get_CharsGenerator_mB8E799CB7B0821293DE8E6AB1A245AD4B11738B1_inline (LevelProvider_tEC5FA7698461AE7083F04FFB7DC851ACDE50E841 * __this, const RuntimeMethod* method)
{
	{
		// public CharsGenerator CharsGenerator { get => _charsGenerator; set => _charsGenerator = value; }
		CharsGenerator_t7E18FC7617D416B9E55B664B885A3138E43EE2A2 * L_0 = __this->get__charsGenerator_6();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_m4C91D0E84532DF10C654917487D82CB0AB27693B_gshared_inline (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_current_3();
		return (RuntimeObject *)L_0;
	}
}
