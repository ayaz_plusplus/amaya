﻿namespace Assets.Scripts
{
    struct SceneConfig
    {
        public const string CHARS_RESOURCES_FOLDER = "Chars/";
        public const string CHARS_SCENE_TAG = "CharsScene";
        public const string LEVEL_MANAGEMENT_TAG = "LevelProvider";
        public const ushort LEVEL_LETTERS_OFFSET = 3;
        public const ushort START_LETTERS_COUNT = 3;
    }
}
