using UnityEngine;

public class GlobalFunctions
{
    /// <summary>
    /// ������� ��������� ������� �� ����
    /// </summary>
    /// <typeparam name="T">������������ ��� �� MonoBehavior</typeparam>
    /// <param name="tag">��� ��� ������ ������� �� �����</param>
    /// <returns></returns>
    public static T GetComponentObject<T>(string tag)
    {
        T component = default(T);
        var globalGameObject = GameObject.FindGameObjectWithTag(tag);

        if (globalGameObject == null)
            Debug.LogError($"�� ������� ����� ������ � ����� {tag}");

        if (!globalGameObject.TryGetComponent(out component))
            Debug.LogError($"�� ������� ����� ��������� {nameof(T)} � ������� {globalGameObject.name}");

        return component;
    }

}
