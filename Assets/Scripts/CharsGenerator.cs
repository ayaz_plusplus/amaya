using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CharsGenerator : MonoBehaviour
{

    [SerializeField]
    private GameObject _cellPrefab;

    private LevelProvider _levelProvider;
    private RectTransform _charsScene;
    private CharsContainer _charsContainer = new CharsContainer();
    private List<CharControl> charsOnScene = new List<CharControl>();

    private List<CharsData> charsPrepareOnScene = new List<CharsData>();

    public List<CharControl> CharsOnScene { get => charsOnScene; set => charsOnScene = value; }
    public List<CharsData> CharsPrepareOnScene { get => charsPrepareOnScene; set => charsPrepareOnScene = value; }

    private void Awake()
    {
        _charsContainer.LoadResources();
        _charsScene = GlobalFunctions.GetComponentObject<RectTransform>(SceneConfig.CHARS_SCENE_TAG);
        _levelProvider = GlobalFunctions.GetComponentObject<LevelProvider>(SceneConfig.LEVEL_MANAGEMENT_TAG);
    }

    // Start is called before the first frame update
    private void Start()
    {
        _levelProvider.NextLevel();
    }

    /// <summary>
    /// ������������� �������� ����� �������
    /// </summary>
    public void CharGenerate()
    {
        CharsOnScene.Clear();
        CharsPrepareOnScene.Clear();
        _levelProvider.Score = 0;
        foreach (Transform oldChar in _charsScene)
            Destroy(oldChar.gameObject);

        var currentCharBundle = _charsContainer.CharBundles[Random.Range(0, _charsContainer.CharBundles.Length)];

        // �������� ���������� �������� ������� 2��, �.� �� ����� ����� ��������� �� ���
        for (ushort index = 1; index <= _levelProvider.CurrentCharsCount / 2; index++)
        {
            var relevantBundle = currentCharBundle.CharsData
                .Where(data => charsPrepareOnScene.Where(charPrepare => charPrepare.Identifier == data.Identifier).Count() == 0).ToArray();

            if (relevantBundle.Length > 0)
            {
                var bundle = relevantBundle[Random.Range(0, relevantBundle.Length)];
                charsPrepareOnScene.Add(bundle);
                charsPrepareOnScene.Add(bundle);
            }
            else
            {
                // ���� ��� ��� ������������ ������� �� ����� �� ����� ���������
                var randomChar = currentCharBundle.CharsData[Random.Range(0, currentCharBundle.CharsData.Length)];

                charsPrepareOnScene.Add(randomChar);
                charsPrepareOnScene.Add(randomChar);

            }
        }
        // ���������� �������������� ����� �� �����
        SetCharsOnScene();

        StartCoroutine(InitializeEffect());
    }

    private void SetCharsOnScene()
    {
        var shuffleArray = ShuffleArray(charsPrepareOnScene.ToArray());

        foreach (var charData in shuffleArray)
            AddCharOnScene(charData);
    }

    private CharsData[] ShuffleArray(CharsData[] data)
    {
        System.Random rnd = new System.Random();
        for (int i = data.Length - 1; i >= 1; i--)
        {
            int j = rnd.Next(i+1);
            // �������� �������� data[j] � data[i]
            var temp = data[j];
            data[j] = data[i];
            data[i] = temp;
        }
        return data;
    }

    private void AddCharOnScene(CharsData randomCharData)
    {
        var newChar = Instantiate(_cellPrefab, _charsScene);
        var control = newChar.GetComponent<CharControl>();
        control.Chardata = randomCharData;
        CharsOnScene.Add(control);

    }

    /// <summary>
    /// ������ ������������� ��������� ����� ��������� �����, �.�. ����� � Layout ��� �������� 
    /// �������� � ��������� ����������������
    /// </summary>
    /// <returns></returns>
    IEnumerator InitializeEffect()
    {
        yield return new WaitForSeconds(0.5f);
        foreach (var control in CharsOnScene)
            control.TweenEffect.StrongBounceEffect();
    }
}
