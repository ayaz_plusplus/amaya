using DG.Tweening;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class LoadingScreen : MonoBehaviour
{
    private float _fadeInDuration = 4f;


    public void StartGame()
    {
        StartCoroutine(LoadScreen());
    }

    public void PanelActive(bool isActive)
    {
        var winPanelImage = GetComponent<Image>();

        if (isActive)
            winPanelImage.DOFade(1, _fadeInDuration);
        else
            winPanelImage.DOFade(0, _fadeInDuration);
    }

    IEnumerator LoadScreen()
    {
        PanelActive(false);
        yield return new WaitForSeconds(3f);
        gameObject.SetActive(false);
    }

}
