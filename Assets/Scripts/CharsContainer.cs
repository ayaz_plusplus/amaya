using Assets.Scripts;
using UnityEngine;

public class CharsContainer
{
    private CharsBundleData[] _charBundles;

    public CharsBundleData[] CharBundles { get => _charBundles; set => _charBundles = value; }

    /// <summary>
    /// ��������� ��� ������ ������ �� ����������� ����������
    /// </summary>
    public void LoadResources()
    {
        //��������� �������
        _charBundles = Resources.LoadAll<CharsBundleData>($"{SceneConfig.CHARS_RESOURCES_FOLDER}");     
    }
}
