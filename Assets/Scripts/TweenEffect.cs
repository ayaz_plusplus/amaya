using DG.Tweening;
using UnityEngine;

public class TweenEffect : MonoBehaviour
{
    private float _bounceDuration = 2f;

    private Vector3 _bounceStrenght = new Vector3(0, 3, 0);
    private Vector3 _bounceStrongStrenght = new Vector3(0, 5, 0);

    public void BounceEffect()
    {
        transform.DOShakePosition(_bounceDuration, strength: _bounceStrenght, vibrato: 5, randomness: 1, snapping: false, fadeOut: true);
    }

    public void StrongBounceEffect()
    {
        transform.DOShakePosition(_bounceDuration, strength: _bounceStrongStrenght, vibrato: 10, randomness: 1, snapping: false, fadeOut: true);
    }
}
