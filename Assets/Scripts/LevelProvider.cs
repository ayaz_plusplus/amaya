using Assets.Scripts;
using DG.Tweening;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class LevelProvider : MonoBehaviour
{

    private int score;

    [SerializeField]
    private Text _taskLabel;

    [SerializeField]
    private CharsGenerator _charsGenerator;

    [SerializeField]
    private GameObject _winPanel;

    private CharControl _currentChar;

    [SerializeField]
    private int _currentCharsCount;

    private float _fadeInDuration = 5f;


    public CharControl CurrentChar
    {
        get => _currentChar;
        set
        {          
            _taskLabel.DOFade(1, _fadeInDuration);
            _currentChar = value;
        }
    }

    public int CurrentCharsCount
    {
        get => _currentCharsCount;
        set
        {
            _currentCharsCount = value;
        }
    }

    public CharsGenerator CharsGenerator { get => _charsGenerator; set => _charsGenerator = value; }

    public int Score { 
        get => score;
        set
        {
            _taskLabel.text = $"Score: {value}";
            score = value;
        }
    }


    /// <summary>
    /// ���������� �������� �� ��������� �������
    /// </summary>
    public void NextLevel()
    {
        _taskLabel.DOFade(0, 0);
        _charsGenerator.CharGenerate();
    }

    /// <summary>
    /// ������ ���� ������
    /// </summary>
    public void RestartGame()
    {
        NextLevel();
        WinPanelActive(false);
    }

    /// <summary>
    /// ���������� ���� ������
    /// </summary>
    /// <param name="isActive"></param>
    public void WinPanelActive(bool isActive)
    {
        var winPanelImage = _winPanel.GetComponent<Image>();
        winPanelImage.DOFade(0, 0);
        _winPanel.SetActive(isActive);

        if (isActive)
            winPanelImage.DOFade(1, _fadeInDuration);
        else
            winPanelImage.DOFade(0, _fadeInDuration);
    }
}
