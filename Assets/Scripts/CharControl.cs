using Assets.Scripts;
using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class CharControl : MonoBehaviour
{
    private Button _sourceButton;
    private LevelProvider _levelProvider;
    private CharsData _chardata;
    private TweenEffect _tweenEffect;
    private Image _charImage;
    private Sprite _backgroundSprite;
    private bool isSelected;

    public CharsData Chardata
    {
        get => _chardata;
        set
        {
            this.name = value.Identifier;
            _chardata = value;
        }
    }

    public TweenEffect TweenEffect { get => _tweenEffect; set => _tweenEffect = value; }
    public Image CharImage { get => _charImage; set => _charImage = value; }
    public bool IsSelected { get => isSelected; set => isSelected = value; }

    private void OnEnable()
    {
        _sourceButton = this.GetComponent<Button>();
        //Register Button Events
        _sourceButton.onClick.AddListener(() => ButtonCallBack());
        _levelProvider = GlobalFunctions.GetComponentObject<LevelProvider>(SceneConfig.LEVEL_MANAGEMENT_TAG);
        _tweenEffect = GetComponent<TweenEffect>();
        _charImage = GetComponent<Image>();
        _backgroundSprite = _charImage.sprite;
    }

    private void ButtonCallBack()
    {

        TweenEffect.BounceEffect();
        if (!IsSelected)
            StartCoroutine(CheckRelevate());
    }

    IEnumerator CheckRelevate()
    {

        CharImage.sprite = _chardata.Sprite;
        IsSelected = true;
        yield return new WaitForSeconds(1f);

        if (_levelProvider.CurrentChar != null)
        {
            if (_levelProvider.CurrentChar.Chardata.Identifier != _chardata.Identifier)
            {
                _levelProvider.CurrentChar.CharImage.sprite = _backgroundSprite;
                CharImage.sprite = _backgroundSprite;
                _levelProvider.CurrentChar.IsSelected = false;
                IsSelected = false;
            }
            else
                _levelProvider.Score++;

            _levelProvider.CurrentChar = null;
         
            if (_levelProvider.CharsGenerator.CharsOnScene.Where(ch => ch.IsSelected == false).Count() == 0)
                _levelProvider.WinPanelActive(true);
        }
        else 
        {
            _levelProvider.CurrentChar = this;
        }

    }
    private void OnDisable()
    {
        _sourceButton.onClick.RemoveAllListeners();
    }
}
