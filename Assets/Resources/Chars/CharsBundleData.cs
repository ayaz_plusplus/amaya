using UnityEngine;

[CreateAssetMenu(fileName = "New CharsBundleData", menuName = "Chars Bundle Data", order = 10)]
public class CharsBundleData : ScriptableObject
{
    [SerializeField]
    private CharsData[] _charsData;

    public CharsData[] CharsData { get => _charsData; set => _charsData = value; }
}
