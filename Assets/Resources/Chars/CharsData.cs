using System;
using UnityEngine;

[Serializable]
public class CharsData 
{
    [SerializeField]
    private string _identifier;

    [SerializeField]
    private Sprite _sprite;

    public string Identifier { get => _identifier; set => _identifier = value; }
    public Sprite Sprite { get => _sprite; set => _sprite = value; }
}
